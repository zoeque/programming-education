﻿using AutoMapper;
using Demo.Model;
using Demo.Model.DTOs;

namespace Demo.Utils.DTOsMapper;

public class ClassStatusMapper : Profile
{
    public ClassStatusMapper()
    {
        base.CreateMap<ClassStatus, ClassStatusDTOs>().ForMember(u => u.ID, opt =>
        {
            opt.MapFrom(src=>src.ID); 
        }).ForMember(c=>c.StatusName,opt=>
        {
            opt.MapFrom(src=>src.StatusName);
        });
    }
}