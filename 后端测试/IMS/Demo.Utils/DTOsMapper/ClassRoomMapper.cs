﻿using AutoMapper;
using Demo.Model;
using Demo.Model.DTOs;

namespace Demo.Utils.DTOsMapper;

public class ClassRoomMapper : Profile
{
    public ClassRoomMapper()
    {
        base.CreateMap<ClassRoom, ClassRoomDTOs>().ForMember(u => u.ID, opt =>
        {
            opt.MapFrom(src=>src.ID); 
        }).ForMember(c=>c.ClassRoomNo, opt =>
        {
            opt.MapFrom(c=>c.ClassRoomNo);
        }).ForMember(c=>c.CmName, opt =>
        {
            opt.MapFrom(c=>c.D_Campus.CmName);
        }).ForMember(c=>c.ClassRoomSite, opt =>
        {
            opt.MapFrom(c=>c.ClassRoomSite);
        }).ForMember(c=>c.IsOccupy, opt =>
        {
            opt.MapFrom(c=>c.IsOccupy);
        });
    }
}