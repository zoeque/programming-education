﻿using AutoMapper;
using Demo.Model;
using Demo.Model.DTOs;

namespace Demo.Utils.DTOsMapper;

public class TeacherMapper : Profile    
{
    //在构造函数中配置
    public TeacherMapper() //此时的src相当于Teachaer表 
    {
        //base.CreateMap<>()  --> 创建一个映射
        //base.CreateMap<源头(谁要映射),映射给谁>()
        //ForMember() 用于为目标对象的特定属性设置自定义映射规则 或者说为类中哪个属性进行映射
        base.CreateMap<Teacher, TeacherDTOs>().ForMember(u => u.ID, opt =>
        {
            opt.MapFrom(src=>src.ID); 
        }).ForMember(u => u.UseID, opt =>
        {
            opt.MapFrom(src=>src.UseID); // 对老师的用户id映射
        }).ForMember(t => t.UserName, opt =>
        {
            opt.MapFrom(src=>src.TcD_user.Name);  // 通过导航属性对老师的名字进行映射
        }).ForMember(t => t.Sex, opt =>
        {
            opt.MapFrom(src=>src.TcD_user.Sex);  // 通过导航属性对老师的性别进行映射
        }).ForMember(t => t.Email, opt =>
        {
            opt.MapFrom(src=>src.TcD_user.Email);  // 通过导航属性对老师的邮箱进行映射
        }).ForMember(t => t.Icon, opt =>
        {
            opt.MapFrom(src=>src.TcD_user.Icon);  // 通过导航属性对老师的头像进行映射
        }).ForMember(t => t.Intro, opt =>
        {
            opt.MapFrom(src=>src.TcD_user.Intro);  // 通过导航属性对老师的简介进行映射
        }).ForMember(t => t.TcEducation, opt =>
        {
            opt.MapFrom(src=>src.TcEducation);  // 对老师的学历进行映射
        }).ForMember(t => t.TcSchool, opt =>
        {
            opt.MapFrom(src=>src.TcSchool);  // 对老师的毕业学校进行映射
        }).ForMember(t => t.Major, opt =>
        {
            opt.MapFrom(src=>src.Major);  // 对老师的所学专业进行映射
        }).ForMember(t => t.TcCard, opt =>
        {
            opt.MapFrom(src=>src.TcCard);  // 对老师的身份证进行映射
        }).ForMember(t => t.LevelName, opt =>
        {
            opt.MapFrom(src=>src.TcD_tclevel.LevelName);  // 通过导航属性对老师的教育等级进行映射
        }).ForMember(t => t.TcClassName, opt =>
        {
            opt.MapFrom(src=>src.TcD_class.Select(c=>c.CalssNo));  // 通过导航属性对老师的所上的班级进行映射
        }).ForMember(t => t.IsWorking, opt =>
        {
            opt.MapFrom(src=>src.IsWorking);  // 对老师是否在职进行映射
        }).ForMember(t => t.EntryTime, opt =>
        {
            opt.MapFrom(src=>src.EntryTime);  // 对老师的入职时间进行映射
        });
    }
}