﻿using AutoMapper;
using Demo.Model;
using Demo.Model.DTOs;

namespace Demo.Utils.DTOsMapper;
// LoginMapper这个类 就是用来实现把Model映射到ModelDTO
public class LoginMapper : Profile
{   //在构造函数中配置
    public LoginMapper()//此时的src相当于User表 
    {
        //base.CreateMap<>()  --> 创建一个映射
        //base.CreateMap<源头(谁要映射),映射给谁>()
        //ForMember() 用于为目标对象的特定属性设置自定义映射规则 或者说为类中哪个属性进行映射
        base.CreateMap<User, DTOLogins>().ForMember(l => l.Account, opt =>
        {
            opt.MapFrom(src=>src.Account); // 对用户的账号进行映射
        }).ForMember(l => l.Password, opt =>
        {
            opt.MapFrom(src=>src.Password);// 对用户的密码进行映射
        });
    }
}