﻿using AutoMapper;
using Demo.Model;
using Demo.Model.DTOs;

namespace Demo.Utils.DTOsMapper;

public class RoleMapper : Profile
{
    public RoleMapper()
    {
        base.CreateMap<Role, RoleDTOs>().ForMember(r=>r.RoleId, opt =>
        {
            opt.MapFrom(src=>src.Id);
        }).ForMember(r=>r.Name, opt =>
        {
            opt.MapFrom(src=>src.Name);
        });
    }
}