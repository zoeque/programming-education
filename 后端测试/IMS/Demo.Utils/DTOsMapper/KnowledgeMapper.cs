﻿using AutoMapper;
using Demo.Model;
using Demo.Model.DTOs;

namespace Demo.Utils.DTOsMapper;

public class KnowledgeMapper : Profile
{
    public KnowledgeMapper()
    {
        base.CreateMap<Knowledge, KnowledgeDTOs>().ForMember(u => u.ID, opt =>
        {
            opt.MapFrom(src=>src.ID); 
        }).ForMember(k=>k.KLName, opt =>
        {
            opt.MapFrom(k=>k.KLName);
        }).ForMember(k=>k.KLDescribe, opt =>
        {
            opt.MapFrom(k=>k.KLDescribe);
        }).ForMember(k=>k.CourseTypeName, opt =>
        {
            opt.MapFrom(k=>k.D_coursefile.CourseTypeName);
        });
    }
}