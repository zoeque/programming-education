﻿using AutoMapper;
using Demo.Model;
using Demo.Model.DTOs;

namespace Demo.Utils.DTOsMapper;

public class EndClassMapper : Profile
{
    public EndClassMapper()
    {
        base.CreateMap<EndClass, EndClassDTO>().ForMember(e=>e.Id, opt =>
        {
            opt.MapFrom(src=>src.ID);
        }).ForMember(e=>e.IsEndClass, opt =>
        {
            opt.MapFrom(src=>src.IsEndClass);
        }).ForMember(e=>e.ClassId, opt =>
        {
            opt.MapFrom(src=>src.ClassId);
        });
    }
}