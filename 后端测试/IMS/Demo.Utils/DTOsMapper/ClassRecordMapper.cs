using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Demo.Model;
using Demo.Model.DTOs;
namespace Demo.Utils.DTOsMapper
{
    public class ClassRecordMapper : Profile
    {
        public ClassRecordMapper()
        {
            base.CreateMap<ClassRecord,ClassRecordDTOs>().ForMember(c=>c.Id,opt=>{
                opt.MapFrom(src=>src.ID);
            }).ForMember(c=>c.InClassNo,opt=>{
                opt.MapFrom(src=>src.InClassNo); // 班级
            }).ForMember(c=>c.StopTime,opt=>{
                opt.MapFrom(src=>src.StopTime); // 下课时间
            }).ForMember(c=>c.CourseArrangId,opt=>{
                opt.MapFrom(src=>src.CourseArrangId); // 排课表id
            }).ForMember(c=>c.StatusId,opt=>{
                opt.MapFrom(src=>src.StatusId); // 状态
            });
        }
    }
}