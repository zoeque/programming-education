﻿namespace Demo.Utils.ApiReturn;

public record ApiReturns
{
    public int Code { get; set; }  // 状态码
    public string Msg { get; set; } // 消息
    public dynamic Data { get; set; } // 数据
}