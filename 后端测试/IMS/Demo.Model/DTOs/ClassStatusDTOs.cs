﻿namespace Demo.Model.DTOs;

public class ClassStatusDTOs
{
    // ID
    public Guid ID { get; set; }
    // 上课状态  待上课  正在上课 已上完
    public string? StatusName {get;set;}
}