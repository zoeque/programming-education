﻿namespace Demo.Model.DTOs;

public class LevelDTOs
{
    // ID
    public Guid ID { get; set; }
    // 等级名称
    public string  LevelName{get;set;} 
    
    
            //  仅供查询使用  添加数据时请勿写以下字段
            
    // 多个课程资料对应一个等级
    public List<string>? CourseTypeName { get; set; } = new List<string>();
}