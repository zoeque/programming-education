﻿namespace Demo.Model.DTOs;

public class ClassRecordDTOs
{
        // Id
        public Guid Id { get; set; }
        //上课的班级
        public Guid InClassNo{get;set;}
        //下课时间
        public DateTime? StopTime{get;set;}
        // 排课表id
        public Guid? CourseArrangId { get; set; }
        // 一个记录对应一个状态
        public Guid? StatusId { get; set; }
        
        
         
}