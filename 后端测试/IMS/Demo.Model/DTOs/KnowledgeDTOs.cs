﻿namespace Demo.Model.DTOs;

public class KnowledgeDTOs
{
    // ID
    public Guid ID { get; set; }
    public string kid { get; set; }
    // 知识点名称
    public string KLName {get;set;}
    // 知识点描述
    public string? KLDescribe { get; set; }
    // 课程类型Id  多个知识点对应一个课程
    public Guid? CourseFileId { get; set; }
    
    
            //  仅供查询使用  添加数据时请勿写以下字段
    // 属于哪个类型的课程
    public string? CourseTypeName { get; set; }
}