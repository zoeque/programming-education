﻿namespace Demo.Model.DTOs;

public class RoleDTOs
{
    // 角色id
    public Guid RoleId { get; set; }
    // 角色名字
    public string Name { get; set; }
}