﻿namespace Demo.Model.DTOs;

public class EndClassDTO
{
    // id
    public Guid Id { get; set; }
    public string IsEndClass { get; set; }
    // 班级外键
    public Guid? ClassId { get; set; }
}