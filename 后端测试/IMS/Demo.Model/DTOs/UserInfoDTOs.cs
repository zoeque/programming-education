﻿using Microsoft.AspNetCore.Http;
namespace Demo.Model.DTOs;

public class UserInfoDTOs
{
    // ID
    public Guid ID { get; set; }
    //账号
    public string? Account{get;set;}
    //密码
    public string? Password{get;set;} 
    // 用户名字
    public string? UserName { get; set; }
    // 中文名字
    public string? Name { get; set; }
    //性别
    public string?  Sex {get;set;}
    // 邮箱 
    public string? Email { get; set; }
        
    //头像
    public string? Icon{get;set;}
    //简介
    public string? Intro{get;set;}
    //用户角色
    public Guid RoleID{get;set;}
    // 软删除
    public Boolean IsDeleted { get; set; }

    
    
    
            /*//  仅供查询使用  添加数据时请勿写以下字段
    
    // 用户角色
    //一个用户对应一个角色
    public string? RoleName {get;set;}*/

}