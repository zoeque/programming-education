﻿namespace Demo.Model.DTOs;

public class CourseFileDTOs
{
    // ID
    public Guid ID { get; set; }
    //课程类型名称
    public string CourseTypeName {get;set;}
    //等级
    public Guid? LevelID {get;set;}
    //课时
    public string? CourseHour {get;set;}
    // 视频路径
    public string? VideoPath { get; set; }
    
    // 软删除
    public Boolean? IsDeleted {get; set; }
    
    
            //  仅供查询使用  添加数据时请勿写以下字段
    //等级
    public string? LevelName {get;set;}
    
    //一个课程对多个知识点  知识点名称
    public List<string>? KLName { get; set; } = new List<string>();
}