using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.Model
{
    //知识点表
    public class Knowledge :BaseID
    {
        public string? kid { get; set; }
        // 知识点名称
        public string KLName {get;set;}
        // 知识点描述
        public string? KLDescribe { get; set; }
        // 课程类型Id  多个知识点对应一个课程
        public Guid? CourseFileId { get; set; }
        //导航属性一个知识点对应一个课程
        public CourseFile? D_coursefile{get;set;}
    }
}