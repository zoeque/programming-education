using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace Demo.Model
{
    //用户信息表
    public class User : IdentityUser<Guid>
    {
        //账号
        public string Account{get;set;}
        //密码
        public string Password{get;set;} 
        // 用户英文名
        //public string UserName { get; set; }
        // 中文名字
        public string? Name { get; set; }
        //性别
        public string  Sex {get;set;}
        // 邮箱 
        public string Email { get; set; }
        
        //头像
        public string? Icon{get;set;}
        //简介
        public string? Intro{get;set;}
        // 软删除
        public Boolean IsDeleted { get; set; }
        //用户角色
        public Guid RoleID{get;set;}
        //一个用户对应一个角色
        public Role? D_role{get;set;}
        
        // 导航属性
        // public Student? UserStudent { get; set; } // 一个用户对应一个学生
        // public Teacher? UsertTeacher { get; set; } // 一个用户对应一个学生

    }
}