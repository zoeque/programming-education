using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Demo.Model
{
    //用户表
    public class Role : IdentityRole<Guid>
    {
        //一个角色对应多个用户
        public List<User>? RD_user{get;set;}
    }
}