using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.Model
{
    //排课信息表
    public class CourseArrang :BaseID
    {
        //课程类型
        public Guid CourseType {get;set;}
        //上课的班级
        public Guid InClassNo{get;set;}

        //上课老师
        public Guid? InTcnameID{get;set;}
        
        //上课时间
        public DateTime? InCourseTime{get;set;}
        
        //上课的教室
        public Guid InClassRoomNo{get;set;}
        
        // 软删除
        public Boolean IsDeleted { get; set; }
        
        
                    // 导航属性设置
        //一课时对应一个老师
        public Teacher? CaD_teacher {get;set;}
        //一课时对应一间教室 
        public ClassRoom? CaD_classroom{get;set;}
        
        // 一节课只有一个班级上
        public ClassA? ClassA { get; set; }
        // 一节课上一个类型的课
        public CourseFile? CourseFile { get; set; }

        // 一个排课信息对应一条上课记录
        public ClassRecord? classRecord { get; set; }
    }
}