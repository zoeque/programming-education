using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.Model
{
    //等级表
    public class Level : BaseID
    {
      // 等级名称
      public string  LevelName{get;set;} 
      //导航属性 一个等级对应一个老师 一个资料
      public List<Teacher>? D_teacher{get;set;}
      //一个等对应多个课程
      public List<CourseFile>? D_coursefile{get;set;}
    }
}