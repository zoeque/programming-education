﻿using Demo.Model;

namespace Demo.IRepository;

public interface IRoleRepository : IBaseRepository<Role>
{
    
}