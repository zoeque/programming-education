﻿using Demo.EFCoreEnv.DbContext;
using Demo.IRepository;
using Demo.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Repository
{
    public class CampusRepository:BaseRepository<Campus>, ICampusRepository
    {
        protected readonly IMSContext _IMSContext;
        public CampusRepository(IMSContext context)
        {
            base.IMSContext = context;
            _IMSContext = context;
        }
        public async override Task<Campus> FindIdAsync(Guid id)
        {
            return await _IMSContext.campuses.Include(c=>c.D_classroom).FirstOrDefaultAsync(c=>c.ID==id);
        }
        public async override Task<Campus> FindOneAsync(Expression<Func<Campus, bool>> exp)
        {
            return await _IMSContext.campuses.Include(c=>c.D_classroom).FirstOrDefaultAsync(exp);
        }
        public async override Task<List<Campus>> FindAllAsync()
        {
            return await _IMSContext.campuses.Include(c=>c.D_classroom).ToListAsync();
        }
        public async override Task<List<Campus>> FindManyAsync(Expression<Func<Campus, bool>> exp)
        {
            return await _IMSContext.campuses.Include(c=>c.D_classroom).Where(exp).ToListAsync();
        }
    }
}
