﻿using Demo.EFCoreEnv.DbContext;
using Demo.IRepository;
using Demo.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Repository
{
    public class ClassARepository:BaseRepository<ClassA>,IClassARepository
    {
        protected readonly IMSContext _IMSContext;
        public ClassARepository(IMSContext context)
        {
            base.IMSContext = context;
            _IMSContext = context;
        }
        public async override Task<ClassA> FindIdAsync(Guid id)
        {
            return await _IMSContext.classeas.OrderBy(c=>c.CalssNo).FirstOrDefaultAsync(c => c.ID == id);
        }
        public async override Task<ClassA> FindOneAsync(Expression<Func<ClassA, bool>> exp)
        {
            return await _IMSContext.classeas.OrderBy(c=>c.CalssNo).FirstOrDefaultAsync(exp);
        }
        public async override Task<List<ClassA>> FindAllAsync()
        {
            return await _IMSContext.classeas.OrderBy(c=>c.CalssNo).ToListAsync();
        }
        public async override Task<List<ClassA>> FindManyAsync(Expression<Func<ClassA, bool>> exp)
        {
            return await _IMSContext.classeas.OrderBy(c=>c.CalssNo).Where(exp).ToListAsync();
        }
        public async override  Task<List<ClassA>> PagingAsync(int page, int size)
        {
            return await IMSContext.classeas.OrderBy(c => c.CalssNo).ToListAsync();
        }
    }
}
