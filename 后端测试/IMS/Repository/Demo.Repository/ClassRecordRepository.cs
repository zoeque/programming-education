﻿using Demo.EFCoreEnv.DbContext;
using Demo.IRepository;
using Demo.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Repository
{
    public class ClassRecordRepository:BaseRepository<ClassRecord>,IClassRecordRepository
    {
        protected readonly IMSContext _IMSContext;
        public ClassRecordRepository(IMSContext context)
        {
           base.IMSContext = context;
            _IMSContext = context;
        }
        public async override Task<ClassRecord> FindIdAsync(Guid id)
        {
            return await _IMSContext.classrecords.Include(c => c.ClassA).Include(c=>c.classStatus).Include(c=>c.coursearrang).FirstOrDefaultAsync(c => c.ID == id);
        }
        public async override Task<ClassRecord> FindOneAsync(Expression<Func<ClassRecord, bool>> exp)
        {
            return await _IMSContext.classrecords.Include(c => c.ClassA).Include(c => c.classStatus).Include(c => c.coursearrang).FirstOrDefaultAsync(exp);
        }
        public async override Task<List<ClassRecord>> FindAllAsync()
        {
            return await _IMSContext.classrecords.Include(c => c.ClassA).Include(c => c.classStatus).Include(c => c.coursearrang).ToListAsync();
        }
        public async override Task<List<ClassRecord>> FindManyAsync(Expression<Func<ClassRecord, bool>> exp)
        {
            return await _IMSContext.classrecords.Include(c => c.ClassA).Include(c => c.classStatus).Include(c => c.coursearrang).Where(exp).ToListAsync();
        }
    }
}
