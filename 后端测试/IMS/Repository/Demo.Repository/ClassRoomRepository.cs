﻿using Demo.EFCoreEnv.DbContext;
using Demo.IRepository;
using Demo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Demo.Repository
{
   public class ClassRoomRepository:BaseRepository<ClassRoom>, IClassRoomRepository
    {
        protected readonly IMSContext _IMSContext;
        public ClassRoomRepository(IMSContext context)
        {
            base.IMSContext = context;
            _IMSContext = context;
        }

        public async override Task<ClassRoom> FindIdAsync(Guid id)
        {
            return await _IMSContext.classrooms.Include(c => c.D_Campus).OrderBy(c=>c.ClassRoomSite).FirstOrDefaultAsync(c => c.ID == id);
        }

        public async override Task<ClassRoom> FindOneAsync(Expression<Func<ClassRoom, bool>> exp)
        {
            return await _IMSContext.classrooms.Include(c => c.D_Campus).OrderBy(c=>c.ClassRoomSite).FirstOrDefaultAsync(exp);
        }

        public async override Task<List<ClassRoom>> FindAllAsync()
        {
            return await _IMSContext.classrooms.Include(c => c.D_Campus).OrderBy(c=>c.ClassRoomSite).ToListAsync();
        }

        public async override Task<List<ClassRoom>> FindManyAsync(Expression<Func<ClassRoom, bool>> exp)
        {
            return await _IMSContext.classrooms.Include(c => c.D_Campus).Where(exp).OrderBy(c=>c.ClassRoomSite).ToListAsync();
        }

        public async override Task<List<ClassRoom>> PagingAsync(int page, int size)
        {
            return await _IMSContext.classrooms.Include(c => c.D_Campus).OrderBy(c=>c.ClassRoomSite).Skip((page - 1) * size).Take(size)
                .ToListAsync();
        }
    }
}
