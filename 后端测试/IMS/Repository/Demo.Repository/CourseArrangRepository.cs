﻿using Demo.EFCoreEnv.DbContext;
using Demo.IRepository;
using Demo.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Repository
{
    public class CourseArrangRepository:BaseRepository<CourseArrang>,ICourseArrangRepository
    {
        private readonly IMSContext _IMSContext;
        public CourseArrangRepository(IMSContext context)
        {
            base.IMSContext = context;
            _IMSContext = context;
        }
        public async override Task<CourseArrang> FindIdAsync(Guid id)
        {
            return await _IMSContext.courseArrangs.Include(c => c.CourseFile).Include(c => c.ClassA).Include(c => c.CaD_classroom).Include(c => c.CaD_teacher).ThenInclude(t => t.TcD_user).FirstOrDefaultAsync(c => c.ID == id);
        }
        public async override Task<CourseArrang> FindOneAsync(Expression<Func<CourseArrang, bool>> exp)
        {
            return await _IMSContext.courseArrangs.Include(c => c.CourseFile).Include(c => c.ClassA).Include(c => c.CaD_classroom).Include(c => c.CaD_teacher).ThenInclude(t => t.TcD_user).FirstOrDefaultAsync(exp);
        }
        public async override Task<List<CourseArrang>> FindAllAsync()
        {
            return await _IMSContext.courseArrangs.Include(c => c.CourseFile).Include(c => c.ClassA).Include(c => c.CaD_classroom).Include(c => c.CaD_teacher).ThenInclude(t => t.TcD_user).ToListAsync();
        }
        public async override Task<List<CourseArrang>> FindManyAsync(Expression<Func<CourseArrang, bool>> exp)
        {
            return await _IMSContext.courseArrangs.Include(c => c.CourseFile).
                Include(c => c.ClassA).Include(c => c.CaD_classroom).
                Include(c => c.CaD_teacher).ThenInclude(t => t.TcD_user).Where(exp).ToListAsync();
        }

        public async override Task<List<CourseArrang>> PagingAsync(int page, int size)
        {
            return await _IMSContext.courseArrangs.Include(c => c.CourseFile).Include(c => c.ClassA)
                .Include(c => c.CaD_classroom).Include(c => c.CaD_teacher).ThenInclude(t => t.TcD_user)
                .Skip((page - 1) * size).Take(size).ToListAsync();
        }
    }
}
