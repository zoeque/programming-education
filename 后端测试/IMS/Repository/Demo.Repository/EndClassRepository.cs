﻿using Demo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Demo.IRepository;
using Demo.EFCoreEnv.DbContext;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Demo.Repository
{
    public class EndClassRepository : BaseRepository<EndClass> , IEndClassRepository
    {
        private readonly IMSContext _IMSContext;

        public EndClassRepository(IMSContext context) 
        {
            base.IMSContext = context;
            _IMSContext = context;
        }
        /*public async override Task<EndClass> FindIdAsync(Guid id)
        {
            return await _IMSContext.endClasses.Include(e => e.End_Class).FirstOrDefaultAsync(e => e.ID == id);
        }
        public async override Task<EndClass> FindOneAsync(Expression<Func<EndClass, bool>> exp)
        {
            return await _IMSContext.endClasses.Include(e => e.End_Class).FirstOrDefaultAsync(exp);
        }
        public async override Task<List<EndClass>> FindAllAsync()
        {
            return await _IMSContext.endClasses.Include(e => e.End_Class).ToListAsync();
        }
        public async override Task<List<EndClass>> FindManyAsync(Expression<Func<EndClass, bool>> exp)
        {
            return await _IMSContext.endClasses.Include(e => e.End_Class).Where(exp).ToListAsync();
        }*/
    }
}
