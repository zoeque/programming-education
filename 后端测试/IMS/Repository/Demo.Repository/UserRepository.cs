﻿using Demo.EFCoreEnv.DbContext;
using Demo.IRepository;
using Demo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Demo.Repository
{
    public class UserRepository:BaseRepository<User>,IUserRepository
    {
        private readonly IMSContext _IMSContext;
        public UserRepository(IMSContext context)
        {
            base.IMSContext = context;
            _IMSContext = context;
        }

        public async override Task<List<User>> FindAllAsync()
        {
            return await _IMSContext.users.Include(u => u.D_role).OrderBy(u=>u.RoleID).ToListAsync();
        }

        public async override Task<User> FindOneAsync(Expression<Func<User, bool>> exp)
        {
            return await _IMSContext.users.Include(u => u.D_role).FirstOrDefaultAsync(exp);
        }

        public async override Task<User> FindIdAsync(Guid id)
        {
            return await _IMSContext.users.Include(u => u.D_role).FirstOrDefaultAsync(u => u.Id==id);
        }

        public async override Task<List<User>> FindManyAsync(Expression<Func<User, bool>> exp)
        {
            return await _IMSContext.users.Include(u => u.D_role).Where(exp).ToListAsync();
        }
    }
}
