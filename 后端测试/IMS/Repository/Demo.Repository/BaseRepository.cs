﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Demo.IRepository;
using Demo.EFCoreEnv.DbContext;
using System.Reflection.Metadata;
using Microsoft.EntityFrameworkCore;

namespace Demo.Repository
{
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : class, new()
    {
        protected IMSContext IMSContext;
        public async Task<bool> CreateAsync(T entity)
        {
            await IMSContext.Set<T>().AddAsync(entity);
            return await IMSContext.SaveChangesAsync() > 0;
        }

        public async Task<bool> DeleteAsync(T entity)
        {
            return await UpdateAsync(entity);
        }

        public virtual async Task<List<T>> FindAllAsync()
        {
            return await IMSContext.Set<T>().ToListAsync();
        }

        public virtual async Task<List<T>> FindManyAsync(Expression<Func<T, bool>> exp)
        {
            return await IMSContext.Set<T>().Where(exp).ToListAsync();
        }

        public virtual async Task<T> FindOneAsync(Expression<Func<T, bool>> exp)
        {
            return await IMSContext.Set<T>().FirstOrDefaultAsync(exp);
        }
        public virtual async Task<T> FindIdAsync(Guid id)
        {
            return await IMSContext.Set<T>().FindAsync(id);
        }
    
        public virtual async Task<bool> UpdateAsync(T entity)
        {
            IMSContext.Set<T>().Update(entity);
            return await IMSContext.SaveChangesAsync() > 0;
        }

        public virtual async Task<List<T>> PagingAsync(int page, int size)
        {
           return await IMSContext.Set<T>().Skip((page-1)*size).Take(size).ToListAsync();
        }
    }
}