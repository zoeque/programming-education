﻿using Demo.EFCoreEnv.DbContext;
using Demo.IRepository;
using Demo.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Repository
{
    public class LevelRepository:BaseRepository<Level>,ILevelRepository
    {
        private readonly IMSContext _IMSContext;
        public LevelRepository(IMSContext context)
        {
            base.IMSContext = context;
            _IMSContext = context;
        }
        public async override Task<Level> FindIdAsync(Guid id)
        {
            return await _IMSContext.Levels.Include(l=>l.D_coursefile).FirstOrDefaultAsync(l=>l.ID==id);
        }
        public async override Task<Level> FindOneAsync(Expression<Func<Level, bool>> exp)
        {
            return await _IMSContext.Levels.Include(l=>l.D_coursefile).FirstOrDefaultAsync(exp);
        }
        public async override Task<List<Level>> FindAllAsync()
        {
            return await _IMSContext.Levels.Include(l=>l.D_coursefile).ToListAsync();
        }
        public async override Task<List<Level>> FindManyAsync(Expression<Func<Level, bool>> exp)
        {
            return await _IMSContext.Levels.Include(l=>l.D_coursefile).Where(exp).ToListAsync();
        }
    }
}
