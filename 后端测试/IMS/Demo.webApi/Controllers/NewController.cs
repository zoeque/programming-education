using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Demo.webApi.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class NewController : ControllerBase
    {
        [HttpGet]
        public string Get()
        {
            return "你好";
        }
    }
}