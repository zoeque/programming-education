using System;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Demo.Model;
using Demo.IService;
using Demo.Utils.ApiReturn;
using Demo.Model.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace Demo_webApi.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class UserController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IRoleService _roleService;
        private readonly IUserService _userService;
        private readonly IStudentService _studentService;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        public UserController(IUserService userService, IRoleService roleService, IMapper mapper,
            IStudentService studentService, UserManager<User> userManager,
            RoleManager<Role> roleManager)
        {
            _userService = userService;
            _roleService = roleService;
            _mapper = mapper;
            _studentService = studentService;
            _userManager = userManager;
            _roleManager = roleManager;
        }
    [HttpGet]
        // 所有用户的基础信息  只有管理员可以查看
        [Authorize(Roles = "manager",AuthenticationSchemes = "Bearer")]
        public async Task<ApiReturns> FindUserAll(int page,int size)
        {
            var total = await _userService.FindAllAsync();
            var res = await _userService.PagingAsync(page,size);
            if (res.Count > 0)
            {
                var dto = _mapper.Map<List<UserInfoDTOs>>(res);
                return ApiReturnHelper.Success(new{data=dto,total = total.Count});
            }
            else
            {
                return ApiReturnHelper.Fail("没有数据");
            }
        }
        
        [HttpGet]
        // 根据id 查看信息  普通用户，学生，教师，管理员都可以查看自己的基本信息
        public async Task<ApiReturns> FindUserId(Guid Id)
        {
            var res = await _userService.FindIdAsync(Id);
            if (res != null)
            {
                var dto = _mapper.Map<UserInfoDTOs>(res);
                return ApiReturnHelper.Success(dto);
            }
            else
            {
                return ApiReturnHelper.Fail("没有数据");
            }
        }
        [HttpGet]
        // 根据中英文名 账号 邮箱 查看信息  普通用户，学生，教师，管理员都可以查看自己的基本信息
        public async Task<ApiReturns> FindOne(string text)
        {
            var res = await _userService.FindOneAsync(u =>
                u.UserName == text || u.Account == text || u.Email == text || u.Name == text);
            if (res != null)
            {
                var dto = _mapper.Map<UserInfoDTOs>(res);
                return ApiReturnHelper.Success(dto);
            }
            else
            {
                return ApiReturnHelper.Fail("没有数据");
            }
        }
        [HttpGet]
        // 条件查询找到对应用户的基础信息  只有管理员可以查看
        [Authorize(Roles = "manager",AuthenticationSchemes = "Bearer")]
        public async Task<ApiReturns> FindUserMany(string text)
        { 
            // 模糊查询  管理员可以通过用户的账号，英文名字，中文名字，邮箱，简介，性别进行查询
            var res = await _userService.FindManyAsync
            (
                u => 
                    u.Account == text || u.UserName == text || u.Name == text ||
                     u.Email == text || u.Intro.Contains(text) || u.Sex == text
            );
            // 如果 res 的长度大于 0 代表找到了对应的数据
            if (res.Count > 0)
            {
                var dto = _mapper.Map<List<UserInfoDTOs>>(res);
                // 返回找到的数据
                return ApiReturnHelper.Success(dto);
            }
            else
            {
                return ApiReturnHelper.Fail("没有数据");
            }
        }
        
        [HttpPost]
        // 添加用户  用户注册
        public async Task<IActionResult> CreateUser(UserInfoDTOs user)
        {
            //判断是否有该账号
                var account = await _userService.FindOneAsync(u => u.Account == user.Account);
                if (account != null) return new OkObjectResult(ApiReturnHelper.Fail("该账号已经存在,不需要添加"));
                
                //判断是否已经有该邮箱
                var email = await _userService.FindOneAsync(u => u.Email == user.Email);
                if (email != null) return new OkObjectResult(ApiReturnHelper.Fail("该邮箱已经存在,请重新添加邮箱"));
                
                //查找当前用户是否已存在
                var username = await _userService.FindOneAsync(u => u.UserName == user.UserName);
                if (username != null) return new OkObjectResult(ApiReturnHelper.Fail("该用户名已经存在,请重新添加"));

                var addUser = new User()
                {
                    Id = new Guid(),
                    Account = user.Account,
                    UserName = user.UserName,
                    Name = user.Name,
                    Password = user.Password,
                    Sex = user.Sex,
                    Email = user.Email,
                    Icon = "user.jpg",
                    Intro = user.Intro,
                    RoleID = Guid.Parse("2944e863-5358-aeeb-e341-f8101291bb0d"),
                    IsDeleted = false,
                    AccessFailedCount = 5
                };
            
                var res = await _userManager.CreateAsync(addUser,user.Password);
                // 判断是否成功添加用户
                if (res.Succeeded)
                {
                    // 获取到 normal 角色
                    var role = await _roleManager.FindByIdAsync(user.RoleID.ToString());
                    if (role != null)
                    {
                        // 给用户添加角色  一开始都为 normal 
                        await _userManager.AddToRoleAsync(addUser, role.Name);
                        // 成功添加角色返回 bool 值
                        return new OkObjectResult(ApiReturnHelper.Success(res.Succeeded));
                    }
                }
                return new OkObjectResult(ApiReturnHelper.Fail("添加失败"));
        }
        // 更新用户信息
        [HttpPut]
        // 修改用户的基础信息  所有人都可以修改自己的基础信息 但是只有管理员可以修改用户的角色
        public async Task<IActionResult> Update([FromForm] UserInfoDTOs user)
        {
            //根据id获取要修改的数据
            var updateuser = await _userService.FindIdAsync(user.ID);
            if (updateuser == null) return new OkObjectResult(ApiReturnHelper.Fail("该id不存在"));
            if (user.Account.Length == 0) return new OkObjectResult(ApiReturnHelper.Fail("请输入用户名"));
            //判断是否已经有该邮箱
            var email = await _userService.FindOneAsync(u => u.Email == user.Email);
            if (email != null) return new OkObjectResult(ApiReturnHelper.Fail("该邮箱已经存在,请重新添加邮箱"));
            updateuser.Account = user.Account;
            updateuser.Password = user.Password;
            updateuser.Name = user.Name;
            updateuser.UserName = user.UserName;
            updateuser.Sex = user.Sex;
            updateuser.Email = user.Email;
            /*//获取所有图片路径
            string[] file = Directory.GetFiles(Path.Combine(Directory.GetCurrentDirectory(), "Image"),
                "*.*", SearchOption.AllDirectories).ToArray();
            var list = new List<string>();
            foreach (var i in file)
            {
                //获取所有图片名
                list.Add(Path.GetFileName(i));
            }*/
            // 获取到原用户的角色
            var role = await _roleManager.FindByIdAsync(updateuser.D_role.Id.ToString());
            updateuser.Intro = user.Intro;

            if (user.RoleID == null)// 如果为用户修改自己的个人信息 则用户不可以修改自己的角色 
            {
                var res =  await _userService.UpdateAsync(updateuser);
                if (res)
                {
                    return new OkObjectResult(ApiReturnHelper.Success(res));
                }
                else
                {
                    return new OkObjectResult(ApiReturnHelper.Fail("修改失败"));
                }
            }
            // 如果是管理员修改用户的角色  那么往下修改角色
            // 将原来的角色更改为要修改的角色
            updateuser.RoleID = user.RoleID;
            
            
            // 获取到修改后的角色
            var newrole = await _roleManager.FindByIdAsync(user.RoleID.ToString());
            
            if (await _roleManager.RoleExistsAsync(newrole.Name))
            {
                
                await _userManager.IsInRoleAsync(updateuser, newrole.Name!);
                await _userManager.SetUserNameAsync(updateuser, updateuser.UserName);
                // 给用户添加新的角色
                var addRoleResult   = await _userManager.AddToRoleAsync(updateuser, newrole.Name);
                // 修改新的角色用户删除之前的角色
                await _userManager.RemoveFromRoleAsync(updateuser, role.Name);
                await _userManager.UpdateAsync(updateuser);
                
                if (addRoleResult .Succeeded)
                {
                    return new OkObjectResult(ApiReturnHelper.Success(addRoleResult .Succeeded));
                }
            }
            return new OkObjectResult(ApiReturnHelper.Fail("修改失败"));
        }

        [HttpDelete]
        // 删除当前用户  只有管理员可以删除
        [Authorize(Roles = "manager",AuthenticationSchemes = "Bearer")]
        public async Task<ApiReturns> Delete(Guid id)
        {
            var deluser = await _userService.FindIdAsync(id);
            // 软删除
            deluser.IsDeleted = true;
            var res = await _userService.DeleteAsync(deluser);
            if (res)
            {
                return ApiReturnHelper.Success(res);
            }
            else
            {
                return ApiReturnHelper.Fail("该id不存在或已删除");
            }
        }
    }
}