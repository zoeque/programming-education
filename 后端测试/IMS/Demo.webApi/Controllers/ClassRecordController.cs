﻿using AutoMapper;
using Demo.IService;
using Demo.Model;
using Demo.Model.DTOs;
using Demo.Utils.ApiReturn;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Demo_webApi.Controllers;

[ApiController]
[Route("api/[controller]/[action]")]
public class ClassRecordController : Controller
{
   private readonly IClassRecordService _classRecordService;
    private readonly IMapper _mapper;

    public ClassRecordController(IClassRecordService classRecordService,IMapper mapper)
   {
      _classRecordService = classRecordService;
      _mapper = mapper;
   }

   [HttpGet]
   // 上课记录  除普通用户之外都可以
   [Authorize(Roles = "manager,teacher,student",AuthenticationSchemes = "Bearer")]
   public async Task<ApiReturns> FindAll()
   {
      var res = await _classRecordService.FindAllAsync();
      if (res.Count>0)
      {
         var dto = _mapper.Map<List<ClassRecordDTOs>>(res);
         return ApiReturnHelper.Success(dto);
      }
      else
      {
         return ApiReturnHelper.Fail("没有数据");
      }
   }
   [HttpGet]
   // 上课记录  除普通用户之外都可以
   [Authorize(Roles = "manager,teacher,student",AuthenticationSchemes = "Bearer")]
   public async Task<ApiReturns> FindOne(string text)
   {
      var res = await _classRecordService.FindManyAsync
      (
         c=>
            c.ID == Guid.Parse(text) ||
            c.InClassNo == Guid.Parse(text) ||
            c.StatusId == Guid.Parse(text) || 
            c.CourseArrangId == Guid.Parse(text)
      );
      if (res!=null)
      {
         var dto = _mapper.Map<List<ClassRecordDTOs>>(res);
         return ApiReturnHelper.Success(dto);
      }
      else
      {
         return ApiReturnHelper.Fail("没有数据");
      }
   }
   [HttpPost]
   // 上课记录  老师和管理员可以添加
   [Authorize(Roles = "manager,teacher",AuthenticationSchemes = "Bearer")]
   public async Task<ApiReturns> Create(ClassRecordDTOs classRecord)
   {
      var addclassrecord = new ClassRecord()
      {
         InClassNo = classRecord.InClassNo,
         StopTime = classRecord.StopTime,
         CourseArrangId = classRecord.CourseArrangId,
         StatusId = classRecord.StatusId
      };
      var res = await _classRecordService.CreateAsync(addclassrecord);
      if (res)
      {
         return ApiReturnHelper.Success(res);
      }
      else
      {
         return ApiReturnHelper.Fail("添加失败");
      }
   }

   [HttpPut]
   // 上课记录  老师和管理员可以修改
   [Authorize(Roles = "manager,teacher",AuthenticationSchemes = "Bearer")]
   public async Task<ApiReturns> Update(ClassRecordDTOs classRecord)
   {
      var updateclassrecord = await _classRecordService.FindIdAsync(classRecord.Id);
      updateclassrecord.InClassNo = classRecord.InClassNo;
      updateclassrecord.StopTime = classRecord.StopTime;
      updateclassrecord.CourseArrangId = classRecord.CourseArrangId;
      updateclassrecord.StatusId = classRecord.StatusId;
      var res = await _classRecordService.UpdateAsync(updateclassrecord);
      if (res)
      {
         return ApiReturnHelper.Success(res);
      }
      else
      {
         return ApiReturnHelper.Fail("修改失败");
      }
   }
}