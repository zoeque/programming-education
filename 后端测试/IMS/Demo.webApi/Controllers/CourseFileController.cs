using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Demo.IService;
using Demo.Model;
using Demo.Model.DTOs;
using Demo.Utils.ApiReturn;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace Demo_webApi.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class CourseFileController : ControllerBase
    {
        private readonly ICourseFileService _courseFileService;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IClassAService _classAService;
        private readonly IStudentService _studentService;
        private readonly ITeacherService _teacherService;
        private readonly ILevelService _levelService;

        public CourseFileController
            (
                ICourseFileService courseFileService,
                IMapper mapper,
                IUserService userService,
                IClassAService classAService,
                IStudentService studentService,
                ITeacherService teacherService,
                ILevelService levelService
            )
        {
            _courseFileService  = courseFileService;
            _mapper = mapper;
            _userService = userService;
            _classAService = classAService;
            _studentService = studentService;
            _teacherService = teacherService;
            _levelService = levelService;
        }
        [HttpGet]
        // 课程类型  管理员，教师，学生都可以查看
        [Authorize(Roles = "manager,teacher,student",AuthenticationSchemes = "Bearer")]
        public async Task<ApiReturns> FindAll(){
            var res = await _courseFileService.FindAllAsync();
            if(res.Count>0){
                var dto = _mapper.Map<List<CourseFileDTOs>>(res);
                return ApiReturnHelper.Success(dto);
            }else{
                return ApiReturnHelper.Fail("没有数据");
            }
        }

        [HttpGet]
        // 课程类型  管理员，教师，学生都可以查看
        [Authorize(Roles = "manager,teacher,student",AuthenticationSchemes = "Bearer")]
        public async Task<ApiReturns> FindOne(string text)
        {
            var res = await _courseFileService.FindOneAsync
            (
            c => 
                c.ID == Guid.Parse(text) ||
                c.CourseTypeName == text ||
                c.LevelID == Guid.Parse(text) ||
                c.CourseHour == text
            );
            if (res!=null)
            {
                var dto = _mapper.Map<CourseFileDTOs>(res);
                return ApiReturnHelper.Success(dto);
            }
            else
            {
                return ApiReturnHelper.Fail("没有数据");
            }
        }

        [HttpGet]
        // 课程类型  管理员，教师，学生都可以查看
        [Authorize(Roles = "manager,teacher,student",AuthenticationSchemes = "Bearer")]
        public async Task<ApiReturns> FindId(Guid id)
        {
            var res = await _courseFileService.FindIdAsync(id);
            if (res!=null)
            {
                var dto = _mapper.Map<CourseFileDTOs>(res);
                return ApiReturnHelper.Success(dto);
            }
            else
            {
                return ApiReturnHelper.Fail("该id不存在");
            }
        }
        [HttpPost]
        // 添加课程类型  只有管理员可以添加
        [Authorize(Roles = "manager",AuthenticationSchemes = "Bearer")]
        public async Task<ApiReturns> Create(CourseFileDTOs courseFile)
        {
            var CourseName = await _courseFileService.FindOneAsync(c => c.CourseTypeName == courseFile.CourseTypeName);
            if(CourseName!=null) return ApiReturnHelper.Fail("该课程已经存在");
            var addcoursefile = new CourseFile()
            {
                CourseTypeName = courseFile.CourseTypeName,
                LevelID = courseFile.LevelID,
                CourseHour = courseFile.CourseHour,
                IsDeleted = false
            };
            var res = await _courseFileService.CreateAsync(addcoursefile);
            if (res)
            {
                return ApiReturnHelper.Success(res);
            }
            else
            {
                return ApiReturnHelper.Fail("添加失败");
            }
        }

        [HttpPut]
        // 修改课程类型  只有管理员可以修改
        [Authorize(Roles = "manager",AuthenticationSchemes = "Bearer")]
        public async Task<ApiReturns> Update(CourseFileDTOs courseFile)
        {
            var updatecoursefile = await _courseFileService.FindIdAsync(courseFile.ID);
            if (updatecoursefile == null) return ApiReturnHelper.Fail("该id不存在");
            //查询该课程名是否存在
            var CourseName = await _courseFileService.FindOneAsync(c => c.CourseTypeName == courseFile.CourseTypeName);
            if(CourseName!=null) return ApiReturnHelper.Fail("该课程已经存在");
            updatecoursefile.CourseTypeName = courseFile.CourseTypeName;
            updatecoursefile.LevelID = courseFile.LevelID;
            updatecoursefile.CourseHour = courseFile.CourseHour;
            var res = await _courseFileService.UpdateAsync(updatecoursefile);
            if (res)
            {
                return ApiReturnHelper.Success(res);
            }
            else
            {
                return ApiReturnHelper.Fail("修改失败");
            }
        }

        [HttpDelete]
        public async Task<ApiReturns> Delete(Guid id)
        {
            var delcoursefile = await _courseFileService.FindIdAsync(id);
            delcoursefile.IsDeleted = true;
            var res = await _courseFileService.DeleteAsync(delcoursefile);
            if (res)
            {
                return ApiReturnHelper.Success(res);
            }
            else
            {
                return ApiReturnHelper.Fail("删除失败");
            }
        }
        
        /*
         
         新修改
         
         */
        /// <summary>
        /// 用于找到当前教师等级的课程类型  当小与教师等级的课程类型教师都可以看
        /// </summary>
        /// <param name="id">教师ID</param>
        /// <returns>API返回结果</returns>
        [HttpGet]
        public async Task<ApiReturns> TeacherCourseFile(Guid id)
        {
            // 找到当前教师在教师表的信息
            var teacherinfo = await _teacherService.FindIdAsync(id);
            //  找到教师的等级
            var tealevel = await _levelService.FindOneAsync(l => l.ID == teacherinfo.LevelID);
            
            // 三个等级的id
            var chuji = await _levelService.FindOneAsync(l=>l.LevelName=="初级");
            var zhoji = await _levelService.FindOneAsync(l=>l.LevelName=="中级");
            var gaoji = await _levelService.FindOneAsync(l=>l.LevelName=="高级");
            
            if (tealevel.LevelName==chuji.LevelName) // 如果你的教师等级是初级
            {
                // 通过当前的等级去匹配课程类型的等级
                var res = await _courseFileService.FindManyAsync(c => c.LevelID == chuji.ID);
                if (res!=null)
                {
                    var dto = _mapper.Map<List<CourseFileDTOs>>(res);
                    return ApiReturnHelper.Success(dto);
                }
                else
                {
                    return ApiReturnHelper.Fail("没有数据");
                }
            }
            else if (tealevel.LevelName==zhoji.LevelName)
            {
                // 通过当前教师的等级去匹配课程类型的等级
                var res = await _courseFileService.FindManyAsync(c => c.LevelID == zhoji.ID || c.LevelID==chuji.ID);
                if (res!=null)
                {
                    var dto = _mapper.Map<List<CourseFileDTOs>>(res);
                    return ApiReturnHelper.Success(dto);
                }
                else
                {
                    return ApiReturnHelper.Fail("没有数据");
                }
            }else if (tealevel.LevelName==gaoji.LevelName)
            {
                // 通过当前教师的等级去匹配课程类型的等级
                var res = await _courseFileService.FindAllAsync();
                if (res!=null)
                {
                    var dto = _mapper.Map<List<CourseFileDTOs>>(res);
                    return ApiReturnHelper.Success(dto);
                }
                else
                {
                    return ApiReturnHelper.Fail("没有数据");
                }
            }else
            {
                
            }
            return ApiReturnHelper.Fail("没有数据");
        }


    }
}