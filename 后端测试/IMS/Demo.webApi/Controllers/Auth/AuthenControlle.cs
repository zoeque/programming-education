﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Demo.IService;
using Demo.Model;
using Demo.Model.DTOs;
using Demo.Utils.ApiReturn;
using Demo.Utils.JWT;
using Google.Protobuf.WellKnownTypes;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Demo_webApi.Controllers.Auth;
[ApiController]
[Route("Api/Controller/[action]")]
public class AuthenControlle : ControllerBase
{
    private readonly RoleManager<Role> _roleManager;
    private readonly UserManager<User> _userManager;
    private readonly IUserService _userService;
    private readonly IOptions<JwtSettings> _jwt;

    public AuthenControlle(IOptions<JwtSettings> jwt,IUserService userService,UserManager<User> userManager,RoleManager<Role> roleManager)
    {
        _userService = userService;
        _userManager = userManager;
        _jwt = jwt;
        _roleManager = roleManager;
    }
    //授权,颁发令牌，根据用户的账号密码匹配，如果匹配成功则颁发令牌用于登陆，
        //否则不予登陆
        [HttpPost("auth")]
        public async Task<ActionResult<ApiReturns>> GetJwt(DTOLogins login)
        {
            // 通过账号匹配用户信息
            var user = await _userService.FindOneAsync(user => user.Account == login.Account);
            // 判断 user 是否为空
            if (user is null)
            {
                return ApiReturnHelper.Fail("没有该用户，授权失败,请检查用户信息");
            }
            //使用UserManager判断用户是否被冻结,返回的是一个boolean值，如果为true，
            //表明用户已超出登陆次数，需要等待解冻
            if (await _userManager.IsLockedOutAsync(user))
            {
                return ApiReturnHelper.Fail($"用户{user.UserName}登陆次数过多，{user.LockoutEnd}秒后解冻");
            }
            //如果用户名正确和未超出登陆次数，那么判断该用户密码是否正确
            var result = await _userManager.CheckPasswordAsync(user, login.Password);

            //密码正确，重置失败登陆次数并颁发令牌
            if (result)
            {
                //重置失败登陆次数
                await _userManager.ResetAccessFailedCountAsync(user);
                //颁发令牌，声明payload，Sign，
                //1.payload
                //1.1获取当前用户的身份，后面需要进行鉴权
                var userRoles = await _userManager.GetRolesAsync(user);
                var roleClaims = userRoles.Select(role => new Claim(ClaimTypes.Role, role)).ToArray();
                //1.2声明payload
                var claims = new[]{
                    new Claim(ClaimTypes.NameIdentifier,user.Id.ToString()),
                    new Claim(ClaimTypes.Name,user.UserName),
                    // new Claim(ClaimTypes.Role,roleClaims.)
                }.Concat(roleClaims).ToList();

                //2.使用加密算法生成秘钥
                var encoding = Encoding.UTF8.GetBytes(_jwt.Value.SecKey!);
                var cred = new SigningCredentials(new SymmetricSecurityKey(encoding),
                SecurityAlgorithms.HmacSha256);

                //3.颁发令牌
                var jwtSecurityToken = new JwtSecurityToken(
                    _jwt.Value.Issuer, //签发者
                    _jwt.Value.Audience, // 签收者
                    claims, //payload
                    expires: DateTime.Now.AddSeconds(_jwt.Value.ExpireSeconds), //过期时间
                    signingCredentials: cred);

                var token = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);

                //返回Token
                return ApiReturnHelper.Success(token);
            }

            return Ok();
        }


}