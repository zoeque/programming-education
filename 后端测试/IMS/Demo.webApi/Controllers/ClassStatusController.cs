﻿using AutoMapper;
using Demo.IService;
using Demo.Model;
using Demo.Model.DTOs;
using Demo.Utils.ApiReturn;
using Microsoft.AspNetCore.Mvc;

namespace Demo_webApi.Controllers;
[ApiController]
[Route("api/[controller]/[action]")]
public class ClassStatusController : Controller
{
    private readonly IClassStatusService _classStatusService;
    private readonly IMapper _mapper;

    public ClassStatusController(IMapper mapper, IClassStatusService classStatusService)
    {
        _classStatusService = classStatusService;
        _mapper = mapper;
    }
    [HttpGet]
    public async Task<ApiReturns> FindAll()
    {
        var res = await _classStatusService.FindAllAsync();
        if (res.Count > 0)
        {
            var dto = _mapper.Map<List<ClassStatusDTOs>>(res);
            return ApiReturnHelper.Success(dto);
        }
        else
        {
            return ApiReturnHelper.Fail("没有数据");
        }
    }

    [HttpGet]
    public async Task<ApiReturns> FindOne(string text)
    {
        var res = await _classStatusService.FindOneAsync(c=>c.StatusName == text);
        if (res!=null)
        {
            var dto = _mapper.Map<ClassStatusDTOs>(res);
            return ApiReturnHelper.Success(dto);
        }
        else
        {
            return ApiReturnHelper.Fail("没有数据");
        }
    }
}