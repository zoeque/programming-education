using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Demo.IService;
using Demo.Model;
using Demo.Model.DTOs;
using Demo.Utils.ApiReturn;
using Org.BouncyCastle.Asn1.X509;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace Demo_webApi.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class StudentController : ControllerBase
    {
        private readonly IStudentService _studentService;
        private readonly IUserService  _userService;
        private readonly IClassAService _classAService;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;

        public StudentController
        (
            IStudentService studentService,
            IUserService userService,
            IClassAService classAService,
            UserManager<User> userManager,
            RoleManager<Role> roleManager,
            IMapper mapper
        )
        {
            _studentService = studentService;
            _userService = userService;
            _classAService = classAService;
            _mapper = mapper;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        [HttpGet]
        // 查询所有的学生信息  只有管理员可以查看
        [Authorize(Roles = "manager",AuthenticationSchemes = "Bearer")]
        public async Task<ApiReturns> FindAll(int page,int size)
        {
            var total = await _studentService.FindAllAsync();
            var res = await _studentService.PagingAsync(page, size);
            if(res.Count>0)
            {
                var dto = _mapper.Map<List<StudentInfoDTOs>>(res);
                return ApiReturnHelper.Success(new{data=dto,total = total.Count});
            }else{
                return ApiReturnHelper.Fail("没有数据");
            }
        }

        [HttpGet]
        // id查询学生的基础信息  管理员可以查看 ， 学生也可以看自己的信息
        [Authorize(Roles = "manager,student",AuthenticationSchemes = "Bearer")]
        public async Task<ApiReturns> FindId(Guid id)
        {
            var res = await _studentService.FindIdAsync(id);
            if (res != null)
            {
                var dto = _mapper.Map<StudentInfoDTOs>(res);
                return ApiReturnHelper.Success(dto);
            }
            else
            {
                return ApiReturnHelper.Fail("该id不存在");
            }
        }

        [HttpPost]
        // 添加一个新的学生  只有管理员才可以添加学生
        [Authorize(Roles = "manager,normal",AuthenticationSchemes = "Bearer")]
        public async Task<ApiReturns> Create(StudentInfoDTOs studentInfo)
        {
            //查找该用户是否存在
            var student = await _userService.FindOneAsync(s => s.Id == studentInfo.UseId);
            if (student == null) return ApiReturnHelper.Fail("该用户不存在");

            // //判断该班级是否超过10个人
            // var classa = await _studentService.FindManyAsync(s => s.ClassNo == studentInfo.ClassNo);
            // if (classa.Count > 10) return ApiReturnHelper.Fail("该人数已经超过10位");
            
            
            var addstudent = new Student()
            {
                UseId = studentInfo.UseId,
                ClassNo = studentInfo.ClassNo,
                EnrollTime = DateTime.Now,
                IsDeleted = false
            };
            
            // 获取到原用户的角色
            var role = await _roleManager.FindByIdAsync(student.D_role.Id.ToString());
            // 将他的角色外键改为学生
            // 获取到角色表中学生角色的Id
            var sturole = await _roleManager.FindByNameAsync("student");
            
            if (await _roleManager.RoleExistsAsync(sturole.Name))
            {
                
                await _userManager.IsInRoleAsync(student, sturole.Name!);
                await _userManager.SetUserNameAsync(student, student.UserName);
                // 给用户添加新的角色
                var addRoleResult   = await _userManager.AddToRoleAsync(student, sturole.Name);
                // 修改新的角色用户删除之前的角色
                await _userManager.RemoveFromRoleAsync(student, role.Name);
                student.RoleID = sturole.Id;
                await _userService.UpdateAsync(student);
                
                if (addRoleResult .Succeeded)
                {
                    
                    var res = await _studentService.CreateAsync(addstudent);
                    
                    if (res)
                    {
                        return ApiReturnHelper.Success(res);
                    }

                }
            }
            return ApiReturnHelper.Fail("添加学生失败");
            
            
            
            
        }

        [HttpPut]
        // 修改学生信息 对于学生所属班级  只有管理员和老师可以修改
        [Authorize(Roles = "manager,teacher",AuthenticationSchemes = "Bearer")]
        public async Task<ApiReturns> StudentUpdate(StudentInfoDTOs studentInfo)
        {
            //根据id查找对应的数据
            var updatestudent =await _studentService.FindIdAsync(studentInfo.ID);
            
            // //查找该用户是否存在
            // var user = await _userService.FindIdAsync(studentInfo.UseId);
            // if (user == null) return ApiReturnHelper.Fail("该用户不存在");
            // if (user.D_role.Name != "student") return ApiReturnHelper.Fail("该用户的不是学生");
            //判断该班级是否超过10个人
            
            var classa = await _studentService.FindManyAsync(s => s.ClassNo == studentInfo.ClassNo);
            
            if (classa.Count > 10) return ApiReturnHelper.Fail("该人数已经超过10位");
            
            // 判断是否有这个班级
            var isclass = _classAService.FindOneAsync(c=>c.ID==studentInfo.ClassNo);
            if (isclass == null) return ApiReturnHelper.Fail("不存在该班级");
            
            // updatestudent.UseId = studentInfo.UseId;
            
            updatestudent.ClassNo = studentInfo.ClassNo;
            
            // 更新学生的信息
            var res = await _studentService.UpdateAsync(updatestudent);
            if (res)
            {
                return ApiReturnHelper.Success(res);
            }
            else
            {
                return ApiReturnHelper.Fail("修改失败");
            }
        }

        
        [HttpGet]
        // 删除学生之前把当前这个学生修先改为普通角色
        [Authorize(Roles = "manager",AuthenticationSchemes = "Bearer")]
        public async Task<ApiReturns> DeleteStuRole(Guid id) // 传入当前学生id
        {
            // 当前学生
            var delstudent = await _studentService.FindIdAsync(id);
            // 要删除学生先要把学生的角色从teacher改为normal

            // 1. 获取到学生角色
            var role = await _roleManager.FindByNameAsync("student");
            // 2. 获取到这名学生在用户表的信息

            var userstu = await _userManager.FindByIdAsync(delstudent.UseId.ToString());

            // 3. 获取到普通用户的角色
            var rolenormal = await _roleManager.FindByNameAsync("normal");
            // 如果 res 为 false 那么没删除成功 返回 id 不存在  删除成功则继续删除学生的角色
            
            // 修改用户表的外键为普通用户
            userstu.RoleID = rolenormal.Id;
            
            if (await _roleManager.RoleExistsAsync(rolenormal.Name!))
            {
                
                await _userManager.IsInRoleAsync(userstu, rolenormal.Name!);
                await _userManager.SetUserNameAsync(userstu, userstu.UserName);
                // 给用户添加新的角色
                var addRoleResult   = await _userManager.AddToRoleAsync(userstu, rolenormal.Name);
                // 修改新的角色用户删除之前的角色
                await _userManager.RemoveFromRoleAsync(userstu, role.Name);
                var a =  await _userManager.UpdateAsync(userstu);

                if (addRoleResult .Succeeded)
                {
                    return ApiReturnHelper.Success(addRoleResult .Succeeded);
                }
            }
            return ApiReturnHelper.Fail("修改学生的角色失败");

        }
        
        // 删除   
        [HttpDelete]
        // 删除学生  只有管理员可以删除学生
        [Authorize(Roles = "manager",AuthenticationSchemes = "Bearer")]
        public async Task<ApiReturns> Delete(Guid id)
        {
            var student = await _studentService.FindIdAsync(id);
            if (student == null) return ApiReturnHelper.Fail("该学生不存在");
            student.IsDeleted = true;
            // 软删除
            var res = await _studentService.UpdateAsync(student);
            if (res)
            {
                return ApiReturnHelper.Success(res);
            }
            else
            {
                return ApiReturnHelper.Fail("没有该学生或者该学生已毕业");
            }
        }
        
        
        /*
         * 
         * 新修改
         *
         * 
         */
        /// <summary>
        /// 找到没有班级的学生
        /// </summary>
        /// <returns>返回包含没有班级的学生信息的DTO对象</returns>
        [HttpGet]  //  没有班级的学生   
        public async Task<ApiReturns> NoClassStu()
        {
            var res = await _studentService.FindManyAsync
            (
                s=>
                    s.ClassNo==null
            );
            if(res.Count>0)
            {
                var dto = _mapper.Map<List<StudentInfoDTOs>>(res);
                return ApiReturnHelper.Success(dto);
            }else{
                return ApiReturnHelper.Fail("没有数据");
            }
        }
        
        /// <summary>
        /// 用于实现教师把未有班级的学生  添加到自己的班级上 
        /// </summary>
        /// <param name="id">学生ID</param>
        /// <param name="classid">班级ID</param>
        /// <returns>API返回结果</returns>
        [HttpGet]                                      // 学生id     添加到哪个班级的id
        public async Task<ApiReturns> UpdateStuAddClass(Guid id,Guid classid)
        {
            // 找到当前学生的数据
            var updatestudent = await _studentService.FindIdAsync(id);
            // 要添加的班级
            var classadd = await _classAService.FindIdAsync(classid);
            // 判断这个班级是否已经达到10个人
            var classa = await _studentService.FindManyAsync(s => s.ClassNo == classid);
            if (classa.Count > 10) return ApiReturnHelper.Fail("该人数已经超过10位");
            // 添加学生到班级
            updatestudent.ClassNo = classadd.ID;
            // 保存到数据库
            var res = await _studentService.UpdateAsync(updatestudent);
            if(res!=null)
            { ;
                return ApiReturnHelper.Success(res);
            }else{
                return ApiReturnHelper.Fail("没有数据");
            }
        }
    }
}