﻿using System.Reflection.Metadata;
using System.Text.RegularExpressions;
using AutoMapper;
using Azure.Core;
using Demo.IService;
using Demo.Model;
using Demo.Model.DTOs;
using Demo.Utils.ApiReturn;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Demo_webApi.Controllers;
[ApiController]
[Route("api/[controller]/[action]")]
public class CourseArrangController : Controller
{
    private readonly ICourseArrangService _courseArrangService;
    private readonly ICourseFileService _courseFileService;
    private readonly IMapper _mapper;
    private readonly IClassAService _classAService;

    public CourseArrangController(ICourseArrangService courseArrangService,ICourseFileService courseFileService,IClassAService classAService,IMapper mapper)
    {
        _courseArrangService = courseArrangService;
        _classAService = classAService;
        _courseFileService = courseFileService;
        _mapper = mapper;
    }

    [HttpGet]
    // 课程安排  除了普通用户所有人都可查看
    [Authorize(Roles = "manager,teacher,student",AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> FindAll(int page,int size)
    {
        var total = await _courseArrangService.FindAllAsync();
        var dtototal = _mapper.Map<List<CourseArrangDTOs>>(total);
        var res = await _courseArrangService.PagingAsync(page, size);
        if (res.Count > 0)
        {
            var dto = _mapper.Map<List<CourseArrangDTOs>>(res);
            return ApiReturnHelper.Success(new{data=dto,total = dtototal.Count});
        }
        else
        {
            return ApiReturnHelper.Fail("没有数据");
        }
    }

    [HttpGet]
    // 课程安排  除了普通用户所有人都可查看
    [Authorize(Roles = "manager,teacher,student",AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> FindOne(Guid id)
    {
        var res = await _courseArrangService.FindOneAsync
        (
            c=>
                c.ID == id ||
                c.CourseType == id ||
                c.InClassNo == id ||
                c.InTcnameID == id ||
                c.InClassRoomNo == id
        );
        if (res != null)
        {
            var dto = _mapper.Map<CourseArrangDTOs>(res);
            return ApiReturnHelper.Success(dto);
        }
        else
        {
            return ApiReturnHelper.Fail("该id不存在");
        }
    }
    [HttpGet]
    // 课程安排  除了普通用户所有人都可查看
    [Authorize(Roles = "manager,teacher,student",AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> FindMany(string text)
    {
        var res = await _courseArrangService.FindManyAsync
            (
                c=>
                    c.ID == Guid.Parse(text) ||
                    c.CourseType == Guid.Parse(text) ||
                    c.InClassNo == Guid.Parse(text) ||
                    c.InTcnameID == Guid.Parse(text) ||
                    c.InClassRoomNo == Guid.Parse(text)
            );
        if (res != null)
        {
            var dto = _mapper.Map<List<CourseArrangDTOs>>(res);
            return ApiReturnHelper.Success(dto);
        }
        else
        {
            return ApiReturnHelper.Fail("该id不存在");
        }
    }
    [HttpPost]
    // 添加课程安排  管理员和教师都可以
    [Authorize(Roles = "manager,teacher",AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> Create(CourseArrangDTOs courseArrang)
    {
        var addcoursearrang = new CourseArrang()
        {
            InCourseTime = courseArrang.InCourseTime, // 上课时间
            CourseType = courseArrang.CourseType, // 上课类型
            InTcnameID = courseArrang.InTcnameID, // 上课的老师
            InClassRoomNo = courseArrang.InClassRoomNo, // 上课的教室
            InClassNo = courseArrang.InClassNo // 上课的班级
        };
        var res = await _courseArrangService.CreateAsync(addcoursearrang);
        if (res)
        {
            return ApiReturnHelper.Success(res);
        }
        else
        {
            return ApiReturnHelper.Fail("添加失败");
        }
    }

    [HttpPut]
    // 修改课程安排  管理员和教师都可以修改
    [Authorize(Roles = "manager,teacher",AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> Update(CourseArrangDTOs courseArrang)
    {
        var updateArrang = await _courseArrangService.FindIdAsync(courseArrang.ID);
        if (updateArrang == null) return ApiReturnHelper.Fail("该id不存在");
        updateArrang.InCourseTime = courseArrang.InCourseTime;
        updateArrang.CourseType = courseArrang.CourseType;
        updateArrang.InTcnameID = courseArrang.InTcnameID;
        updateArrang.InClassRoomNo = courseArrang.InClassRoomNo;
        updateArrang.InClassNo = courseArrang.InClassNo;
        var res = await _courseArrangService.UpdateAsync(updateArrang);
        if (res)
        {
            return ApiReturnHelper.Success(res);
        }
        else
        {
            return ApiReturnHelper.Fail("修改失败");
        }
    }

    [HttpDelete]
    // 删除排课信息
    // sc课程安排  管理员和教师都可以删除
    [Authorize(Roles = "manager,teacher",AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> Update(Guid id)
    {
        var updateArrang = await _courseArrangService.FindIdAsync(id);
        if (updateArrang == null) return ApiReturnHelper.Fail("该排课信息不存在");
        updateArrang.IsDeleted = true;
        var res = await _courseArrangService.DeleteAsync(updateArrang);
        if (res)
        {
            return ApiReturnHelper.Success(res);
        }
        else
        {
            return ApiReturnHelper.Fail("修改失败");
        }
    }
    
}