﻿using System.Runtime.CompilerServices;
using AutoMapper;
using Azure.Core;
using Demo.IService;
using Demo.Model;
using Demo.Model.DTOs;
using Demo.Utils.ApiReturn;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Demo_webApi.Controllers;

[ApiController]
[Route("api/[controller]/[action]")]
public class KnowledgeController : Controller
{
    private readonly IKnowkedgeService _knowledgeService;
    private readonly IMapper _mapper;
    private readonly IStudentService _studentService;
    private readonly IClassAService _classAService;
    private readonly ICourseFileService _courseFileService;
    public KnowledgeController(IKnowkedgeService knowkedgeService, IMapper mapper,ICourseFileService courseFileService,
        IStudentService studentService,
        IClassAService classAService)
    {
        _studentService = studentService;
        _classAService = classAService;
        _knowledgeService = knowkedgeService;
        _mapper = mapper;
        _courseFileService = courseFileService;
    }
    
    /// <summary>
    /// 找到没有班级的学生
    /// </summary>
    /// <param name="id">UserId</param>
    /// <returns>返回包含没有班级的学生信息的DTO对象</returns>
    [HttpGet]
    public async Task<ApiReturns> StudentCourseFile(Guid id)
    {
        var students = await _studentService.FindOneAsync(s=>s.UseId==id);
        var classas = await _classAService.FindOneAsync(c=>c.ID==students.ClassNo);
        var coursefiles = await _courseFileService.FindOneAsync(c => c.CourseTypeName == classas.ClassType);
        var res = await _knowledgeService.FindManyAsync(s => s.CourseFileId == coursefiles.ID);
        if(res!=null){
            var dto = _mapper.Map<List<KnowledgeDTOs>>(res);
            return ApiReturnHelper.Success(dto);
        }else{
            return ApiReturnHelper.Fail("没有数据");
        }
    }
    
    [HttpGet]
    // 学习资料 除了普通用户不可以访问 其他都可以访问
    [Authorize(Roles = "manager,teacher,student", AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> FindAll(int page, int size)
    {
        var total = await _knowledgeService.FindAllAsync();
        var res = await _knowledgeService.PagingAsync(page, size);
        if (res.Count > 0)
        {
            var dto = _mapper.Map<List<KnowledgeDTOs>>(res);
            return ApiReturnHelper.Success(new{data=dto,total = total.Count});
        }
        else
        {
            return ApiReturnHelper.Fail("没有数据");
        }
    }

    [HttpGet]
    // 学习资料 除了普通用户不可以访问 其他都可以访问
    [Authorize(Roles = "manager,teacher,student", AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> FindId(Guid id)
    {
        var res = await _knowledgeService.FindIdAsync(id);
        if (res != null)
        {
            var dto = _mapper.Map<KnowledgeDTOs>(res);
            return ApiReturnHelper.Success(dto);
        }
        else
        {
            return ApiReturnHelper.Fail("该id不存在");
        }
    }

    [HttpGet]
    // 学习资料 除了普通用户不可以访问 其他都可以访问
    [Authorize(Roles = "manager,teacher,student", AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> FindOne(string text)
    {
        var res = await _knowledgeService.FindOneAsync
        (k =>
            k.kid == text ||
            k.KLName.Contains(text) ||
            k.KLDescribe.Contains(text)
        );

        if (res != null)
        {
            var dto = _mapper.Map<KnowledgeDTOs>(res);
            return ApiReturnHelper.Success(dto);
        }
        else
        {
            return ApiReturnHelper.Fail("没有数据");
        }
    }

    [HttpGet]
    // 学习资料 除了普通用户不可以访问 其他都可以访问
    [Authorize(Roles = "manager,teacher,student", AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> FindMany(string text)
    {
        var res = await _knowledgeService.FindManyAsync
        (k =>
            k.kid == text ||
            k.KLName.Contains(text) ||
            k.KLDescribe.Contains(text) ||
            k.CourseFileId.ToString() == text
        );
        if (res.Count > 0)
        {
            var dto = _mapper.Map<List<KnowledgeDTOs>>(res);
            return ApiReturnHelper.Success(dto);
        }
        else
        {
            return ApiReturnHelper.Fail("没有数据");
        }
    }

    [HttpPost]
    // 添加学习资料  只有管理员可以添加新的学习资料  必须先有知识点的类型才能有对应的知识点
    [Authorize(Roles = "manager", AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> Create(KnowledgeDTOs knowledge)
    {
        var addknowledge = new Knowledge()
        {
            kid = knowledge.kid,
            KLName = knowledge.KLName,
            KLDescribe = knowledge.KLDescribe,
            CourseFileId = knowledge.CourseFileId
        };
        var res = await _knowledgeService.CreateAsync(addknowledge);
        if (res)
        {
            return ApiReturnHelper.Success(res);
        }
        else
        {
            return ApiReturnHelper.Fail("添加失败");
        }
    }

    [HttpPut]
    // 修改学习资料  只有管理员可以修改学习资料  必须先有知识点的类型才能有对应的知识点
    [Authorize(Roles = "manager", AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> Update(KnowledgeDTOs knowledge)
    {
        var updateknowledge = await _knowledgeService.FindIdAsync(knowledge.ID);
        updateknowledge.kid = knowledge.kid;
        updateknowledge.KLDescribe = knowledge.KLDescribe;
        updateknowledge.KLName = knowledge.KLName;
        updateknowledge.CourseFileId = knowledge.CourseFileId;
        var res = await _knowledgeService.UpdateAsync(updateknowledge);
        if (res)
        {
            return ApiReturnHelper.Success(res);
        }
        else
        {
            return ApiReturnHelper.Fail("修改失败");
        }
    }
}