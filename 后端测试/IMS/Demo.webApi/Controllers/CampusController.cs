﻿using AutoMapper;
using Demo.IService;
using Demo.Model;
using Demo.Model.DTOs;
using Demo.Utils.ApiReturn;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Demo_webApi.Controllers;
[ApiController]
[Route("api/[controller]/[action]")]
public class CampusController : Controller
{
    private readonly ICampusService _campusService;
    private readonly IMapper _mapper;

    public CampusController(ICampusService campusService,IMapper mapper)
    {
        _campusService = campusService;
        _mapper = mapper;
    }

    [HttpGet]
    // 校区信息  所有人都可以查看
    public async Task<ApiReturns> FindAll()
    {
        var res = await _campusService.FindAllAsync();
        if (res.Count > 0)
        {
            var dto = _mapper.Map<List<CampusDTOs>>(res);
            return ApiReturnHelper.Success(dto);
        }
        else
        {
            return ApiReturnHelper.Fail("没有数据");
        }
    }

    [HttpGet]
    // 校区信息  所有人都可以查看
    public async Task<ApiReturns> FindMany(string text)
    {
        var res = await _campusService.FindManyAsync(
        c=>c.CmName.Contains(text) || c.CmText.Contains(text) || c.CmSite.Contains(text)
        );
        if (res.Count > 0)
        {
            var dto = _mapper.Map<List<CampusDTOs>>(res);
            return ApiReturnHelper.Success(dto);
        }
        else
        {
            return ApiReturnHelper.Fail("没有数据");
        }
    }
    [HttpGet]
    // 校区信息  所有人都可以查看
    public async Task<ApiReturns> FindIdAsync(Guid id)
    {
        var res = await _campusService.FindIdAsync(id);
        if (res!=null)
        {
            var dto = _mapper.Map<CampusDTOs>(res);
            return ApiReturnHelper.Success(dto);
        }
        else
        {
            return ApiReturnHelper.Fail("该id不存在");
        }
    }
    [HttpPost]
    // 添加校区 只有管理员可以
    [Authorize(Roles = "manager",AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> Create(CampusDTOs campus)
    {
        var addcampus = new Campus()
        {
            CmName = campus.CmName,
            CmSite = campus.CmSite,
            CmText = campus.CmText
        };
        var res = await _campusService.CreateAsync(addcampus);
        if (res)
        {
            return ApiReturnHelper.Success(res);
        }
        else
        {
            return ApiReturnHelper.Fail("添加失败");
        }
    }

    [HttpPut]
    // 修改校区信息 只有管理员可以
    [Authorize(Roles = "manager",AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> Update(CampusDTOs campus)
    {
        var updatecampus = await _campusService.FindIdAsync(campus.ID);
        updatecampus.CmName = campus.CmName;
        updatecampus.CmSite = campus.CmSite;
        updatecampus.CmText = campus.CmText;
        var res = await _campusService.UpdateAsync(updatecampus);
        if (res)
        {
            return ApiReturnHelper.Success(res);
        }
        else
        {
            return ApiReturnHelper.Fail("修改失败");
        }
    }
}