﻿using Demo.IRepository;
using Demo.IService;
using Demo.Model;

namespace Demo.Service;

public class RoleService: BaseService<Role>, IRoleService
{
    protected readonly IRoleRepository _roleRepository;

    public RoleService(IRoleRepository roleRepository)
    {
        base._base = roleRepository;
        _roleRepository = roleRepository;
    }

}