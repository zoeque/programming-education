﻿using Demo.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Demo.IRepository;
namespace Demo.Service
{
    public abstract class BaseService<T> : IBaseService<T> where T : class, new()
    {
        protected IBaseRepository<T> _base;
        public async Task<bool> CreateAsync(T entity)
        {
            return await _base.CreateAsync(entity);
        }

        public async Task<bool> DeleteAsync(T entity)
        {
            return await _base.DeleteAsync(entity);
        }

        public async Task<List<T>> FindAllAsync()
        {
            return await _base.FindAllAsync();
        }

        public async Task<List<T>> FindManyAsync(Expression<Func<T, bool>> exp)
        {
            return await _base.FindManyAsync(exp);
        }

        public async Task<T> FindOneAsync(Expression<Func<T, bool>> exp)
        {
            return await _base.FindOneAsync(exp);
        }
        public async Task<T> FindIdAsync(Guid id)
        {
            return await _base.FindIdAsync(id);
        }

        public async Task<bool> UpdateAsync(T entity)
        {
            return await _base.UpdateAsync(entity);
        }

        public async Task<List<T>> PagingAsync(int page, int size)
        {
            return await _base.PagingAsync(page,size);
        }
    }
}