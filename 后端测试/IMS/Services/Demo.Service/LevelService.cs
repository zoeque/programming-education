﻿using Demo.IRepository;
using Demo.IService;
using Demo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Service
{
    public class LevelService:BaseService<Level>,ILevelService
    {
        private readonly ILevelRepository _repository;
        public LevelService(ILevelRepository repository)
        {
            base._base=repository;
            _repository = repository;
        }
    }
}
