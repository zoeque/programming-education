﻿using Demo.IRepository;
using Demo.IService;
using Demo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Service
{
   public class CourseArrangService:BaseService<CourseArrang>,ICourseArrangService
    {
        private ICourseArrangRepository _repository;
        public CourseArrangService(ICourseArrangRepository repository)
        {
            base._base = repository;
            _repository = repository;
        }
    }
}
