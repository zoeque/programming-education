﻿using Demo.IRepository;
using Demo.IService;
using Demo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Service
{
    public class ClassRoomSerive:BaseService<ClassRoom>,IClassRoomService
    {
        private IClassRoomRepository _repository;
        public ClassRoomSerive(IClassRoomRepository repository)
        {
            base._base=repository;
            _repository = repository;
        }
    }
}
