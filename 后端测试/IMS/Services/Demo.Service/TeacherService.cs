﻿using Demo.IRepository;
using Demo.IService;
using Demo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Service
{
   public class TeacherService:BaseService<Teacher>,ITeacherService
    {
        private ITeacharRepository _repository;
        public TeacherService(ITeacharRepository repository)
        {
            base._base = repository;
            _repository = repository;
        }
    }
}
