﻿using Demo.IRepository;
using Demo.IService;
using Demo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Service
{
    public class ClassAService : BaseService<ClassA>, IClassAService
    {
        private IClassARepository _repository;
        public ClassAService(IClassARepository repository)
        {
            base._base=repository;
            _repository = repository;
        }
    }
}
