﻿using Demo.IRepository;
using Demo.IService;
using Demo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Service
{
    public class EndClassService : BaseService<EndClass>,IEndClassService
    {
        private readonly IEndClassRepository _repository;

        public EndClassService(IEndClassRepository repository) 
        {
            base._base = repository;
            _repository = repository;
        }
    }
}
