﻿using Demo.IRepository;
using Demo.IService;
using Demo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Service
{
   public class KnowledgeService:BaseService<Knowledge>,IKnowkedgeService
    {
        private IKnowledgeRepository _repository;
        public KnowledgeService(IKnowledgeRepository repository)
        {
            base._base = repository;
            _repository = repository;
        }
    }
}
