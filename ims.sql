/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306_1
 Source Server Type    : MySQL
 Source Server Version : 80033 (8.0.33)
 Source Host           : localhost:3306
 Source Schema         : ims

 Target Server Type    : MySQL
 Target Server Version : 80033 (8.0.33)
 File Encoding         : 65001

 Date: 03/08/2023 14:28:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for __efmigrationshistory
-- ----------------------------
DROP TABLE IF EXISTS `__efmigrationshistory`;
CREATE TABLE `__efmigrationshistory`  (
  `MigrationId` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ProductVersion` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`MigrationId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of __efmigrationshistory
-- ----------------------------
INSERT INTO `__efmigrationshistory` VALUES ('20230714030941_one', '7.0.8');

-- ----------------------------
-- Table structure for aspnetuserroles
-- ----------------------------
DROP TABLE IF EXISTS `aspnetuserroles`;
CREATE TABLE `aspnetuserroles`  (
  `UserId` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `RoleId` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`UserId`, `RoleId`) USING BTREE,
  INDEX `IX_aspnetuserroles_RoleId`(`RoleId` ASC) USING BTREE,
  CONSTRAINT `FK_aspnetuserroles_Role_RoleId` FOREIGN KEY (`RoleId`) REFERENCES `role` (`Id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `FK_aspnetuserroles_User_UserId` FOREIGN KEY (`UserId`) REFERENCES `user` (`Id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of aspnetuserroles
-- ----------------------------
INSERT INTO `aspnetuserroles` VALUES ('e5578a2f-8ae6-f9db-20bb-343084cd2aee', '2944e863-5358-aeeb-e341-f8101291bb0d');
INSERT INTO `aspnetuserroles` VALUES ('e62b22f8-16d0-59f2-428f-ff0db3f93e7a', '2944e863-5358-aeeb-e341-f8101291bb0d');
INSERT INTO `aspnetuserroles` VALUES ('e6ce59ec-0538-d120-2246-2a12381531a5', '2944e863-5358-aeeb-e341-f8101291bb0d');
INSERT INTO `aspnetuserroles` VALUES ('e6d5b5ba-9e98-7405-0ab1-f893fb515d2e', '2944e863-5358-aeeb-e341-f8101291bb0d');
INSERT INTO `aspnetuserroles` VALUES ('e6e7aa68-8883-ba7b-cb6c-0820b77fd85b', '2944e863-5358-aeeb-e341-f8101291bb0d');
INSERT INTO `aspnetuserroles` VALUES ('e762aee9-6635-8eed-5aa8-bbf3b1bea783', '2944e863-5358-aeeb-e341-f8101291bb0d');
INSERT INTO `aspnetuserroles` VALUES ('e8542066-8450-207c-f362-f21ea56d8004', '2944e863-5358-aeeb-e341-f8101291bb0d');
INSERT INTO `aspnetuserroles` VALUES ('e86b0af5-12b8-8b0e-9fc7-8049dbcb3d14', '2944e863-5358-aeeb-e341-f8101291bb0d');
INSERT INTO `aspnetuserroles` VALUES ('e8b068b1-aba6-0cf6-a668-a1fa07b7a85e', '2944e863-5358-aeeb-e341-f8101291bb0d');
INSERT INTO `aspnetuserroles` VALUES ('e90ab4ab-9cf3-ea49-461c-423062b950e7', '2944e863-5358-aeeb-e341-f8101291bb0d');
INSERT INTO `aspnetuserroles` VALUES ('ea5cb916-fd40-56f2-0fc1-2c2347368fe3', '2944e863-5358-aeeb-e341-f8101291bb0d');
INSERT INTO `aspnetuserroles` VALUES ('eb9f57ce-5053-5ed4-f570-a802d632996d', '2944e863-5358-aeeb-e341-f8101291bb0d');
INSERT INTO `aspnetuserroles` VALUES ('ec28cec4-96e6-7b28-8813-d7dad8a7aab4', '2944e863-5358-aeeb-e341-f8101291bb0d');
INSERT INTO `aspnetuserroles` VALUES ('ec2a1928-fa78-1937-53fa-0bd77e4b6400', '2944e863-5358-aeeb-e341-f8101291bb0d');
INSERT INTO `aspnetuserroles` VALUES ('ec656074-b8cb-b03e-d47b-5277c5978448', '2944e863-5358-aeeb-e341-f8101291bb0d');
INSERT INTO `aspnetuserroles` VALUES ('edb7b44c-cd60-258e-71b9-5347a757ce76', '2944e863-5358-aeeb-e341-f8101291bb0d');
INSERT INTO `aspnetuserroles` VALUES ('ee8cd765-0150-d576-c95c-bc054e0a1e13', '2944e863-5358-aeeb-e341-f8101291bb0d');
INSERT INTO `aspnetuserroles` VALUES ('eed7e69e-74c1-f3a8-1f88-ae509089b6b1', '2944e863-5358-aeeb-e341-f8101291bb0d');
INSERT INTO `aspnetuserroles` VALUES ('ef5945bd-f6cb-ed95-5d53-efaed920fc7f', '2944e863-5358-aeeb-e341-f8101291bb0d');
INSERT INTO `aspnetuserroles` VALUES ('f03842db-4762-96ca-d9f6-c2da9294bf7b', '2944e863-5358-aeeb-e341-f8101291bb0d');
INSERT INTO `aspnetuserroles` VALUES ('05b51f7e-6c69-bfd6-2f98-c36850b617ab', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('06adeba4-9ddf-e467-12fc-68642735cc9e', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('643318c9-31cb-0821-8fb0-ff9b1e8b140d', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('64498954-a8cf-bcf0-95db-a9386af8e5f6', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('6500523e-baa3-7522-d27c-19f5982d395f', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('654caf20-5b95-5dc0-2940-1d087740c0de', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('661c50d7-fae2-8c7c-c0d9-38acbc861ca7', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('68066327-fc81-67a7-e947-39b19d1ef474', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('691094d9-9889-6197-82c6-5fb120570f89', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('6a5ad723-7fcb-f09f-af4b-09a67c17c0e1', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('6a95de76-57c8-392c-6d14-016625560d2b', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('6b0c8af4-ae60-b591-bc59-5a2687231174', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('6c6bdda4-ed80-8b29-1b66-05c15a1e00dd', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('6c875f77-70be-37e8-a474-ff15b040086a', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('6dadc7ed-33f1-9173-4a84-d9716ad8ee4b', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('6e4e8838-2a8e-9c92-7554-72cec56e2f4b', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('6e926198-0907-8839-9fd1-95b64bd6fee2', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('6eaf67b7-6e71-1248-478b-d6328081dfc9', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('6f4ce45e-e04e-fc9b-6889-ed29625bb531', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('70bc9033-50b6-3d90-cdd0-337aee28084e', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('720de2b3-663c-8d05-dd7a-f16d26edfbae', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('780a7d23-ad29-6ae7-3952-d6e295ea89b5', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('782ebe6d-2fdf-e26d-ad65-d533b14a53fd', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('7870aa88-69e8-325d-d4e9-ba50369ac675', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('79167cf2-38c5-ef41-95b9-00bb4e4d9b42', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('79893f5c-1468-9ff2-e0b2-f98e691d6ad2', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('79ab3f16-3d66-1ab9-7059-627e242b59dd', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('79d81ab4-52a5-bb5c-9f8a-ac98ac4803e5', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('7a1687b7-d485-c99e-013f-835545c06227', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('7b0571a8-6271-a067-8ac0-d64743386096', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('7b3bb4f6-9246-5db1-7501-23c748548bf4', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('7b7bdcdb-8016-6524-232a-21c20248b789', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('7b7cff58-56b8-986e-0221-072daac7628a', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('7b882394-fa1e-2e81-3c2f-b1ee72fb2679', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('7be65f48-80ba-4e0d-abe5-41e2ced143c8', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('7d96f0cd-4d73-7bf2-baee-7f1483d6fb11', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('7e6cb620-c698-f85d-5152-8874b2672080', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('7ec47520-79cb-8b96-81e5-43ebc02a4aae', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('819a0f43-4936-7b84-35e8-37fa84a8f5b2', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('81b9e8b7-1db6-b216-d7e4-4f83eae386d7', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('81f41193-5411-1a9b-2163-d5d2b750f976', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('823e7e03-5d41-4307-c7ec-03f00195d435', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('8471cc46-388c-2f6e-afb2-b30eedc2b006', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('8475cc74-6013-b3cc-3029-c00203f1fe18', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('84cbbbde-e67f-1e6a-12ac-7a917337783e', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('86dd22cc-a931-5d7a-2563-d30c245e16ea', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('87da5976-25e4-13aa-8c97-58c5f8b20c65', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('883c3508-d483-2300-fd2f-a009b6347dbb', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('89672054-0010-8233-927e-32bb9c69e85f', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('8a1ba186-c68a-9b7e-73c4-bc7abe623f60', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('8ba46336-cf78-ed45-576e-87a2cd6d1e39', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('8c4184e0-56b4-3679-ee69-001531640aaf', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('8eb32118-2a5b-1964-6210-1c92b17694e0', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('8ebe595c-130c-51f2-db49-4fe2233bbdef', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('8f241ca4-07cc-588c-b9ed-d04dc191d7c2', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('9019c28a-ad1f-9907-acec-96b17d323faf', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('914bf30c-0524-e6e3-5530-560d7fbfe0a8', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('914e033a-9255-22a8-0d05-6aa0621eb92a', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('91f06e03-0565-c5b9-3331-2a4f19c195ca', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('92354232-ea1f-fe57-6f95-3d1bfd50d846', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('92c6f0be-ab80-78db-2926-608d44ecdb3d', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('92f72ee4-ed14-0ee1-2d06-690fd673afb1', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('93883b55-66a5-09a1-d131-e19f3e19949e', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('93ae29fa-4cfe-3cbb-36e2-5f70fc0c0865', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('95cfcadc-7e46-bc2c-e010-ced831c01e6e', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('960d11c4-f45a-3727-a335-9e37009757da', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('97a4ffca-eb5e-123f-96f1-7ba0812096dd', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('97b09f81-dffd-7ab1-96cd-76cfa15a0f40', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('97cf3e06-de97-a8c9-4b64-151bcca62356', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('9819e3b1-8d80-596e-1836-762a966e0c39', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('98a48c69-8e25-ff64-af18-a7e0fe01d432', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('998b4238-6fc4-d3a6-e1d5-a9b4737bde48', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('999925e1-db64-8f3b-554d-0fdfbff3e22d', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('9b040a4e-83d8-7f6a-cca5-2950743447f6', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('9ba3f4d6-d776-eed1-d162-41bb28e70735', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('9babd805-ff45-7e4c-bf78-b2fcd60cad5f', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('9be8c479-fb67-b5ef-ca24-cd6bc249fd99', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('9d5a8226-ffbd-f431-5679-e8dfc6c64147', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('9e24bf24-935f-d0f5-dfd1-59025de90fcb', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('9e602aa2-4211-492e-8af1-24ede1e98a0f', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('9f45c4e3-30f2-238b-94c1-d36851518270', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('9fbbeeb9-3b02-324d-9523-2996f96653da', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('a246d7dc-4486-3c49-ced2-fa45cc1e7d24', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('a2528783-9459-5b6e-fa29-874d88b3ec51', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('a32e10d7-1e7f-f056-8761-a03ed033e031', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('a49643ce-af0e-33bc-82c9-759c613405d1', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('a64e2e5d-bfca-9ae9-cdf9-444de12b0975', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('a68e832e-14e5-1723-a3e3-76f774cd68a3', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('a7037bd6-82a1-53ec-d2c9-88decec54565', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('a77a6c39-403b-b170-ec09-f163993754e5', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('a851792d-4a41-812c-1ec1-347a187f090e', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('a8de34aa-2370-fa43-8beb-e9039095758b', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('a919c5f2-49ed-f3e1-9860-e8a1995e3d37', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('a9f76284-4739-3fa7-bc9f-b3636e99aea1', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('aa276510-1816-8865-92df-88e819242025', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('abad81f0-b9c8-3115-87b9-6b1b56aa49df', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('ac244e2c-3444-51c8-ed0e-158043ed4dc6', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('ac3c6cd3-1d7c-1f67-692b-e85a789e5d37', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('ad1a1ead-cd62-38b2-75f2-df00d38ae566', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('ad24f7ea-d758-01c9-0305-6d1ed273a95c', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('ad6ca290-a828-0feb-e1b9-b7abba814010', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('ad9cd476-3dc0-f95b-292f-88a38764bb9f', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('adfaefbd-484f-f1e8-d02d-8757d1d84afb', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('b0202efe-4197-0660-93dd-0e06cc9944ae', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('b25108f8-c399-266e-e071-762d4c36ed2e', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('b25257ff-a225-a49d-5e9b-766396c80873', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('b3b47931-b135-e9bc-029d-4896b640a7fe', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('b51fa004-1aaa-c360-d768-ac8768442c0d', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('b6b96d1d-02f1-99d3-6c51-55d0139aa5eb', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('b6c088ea-5d36-6724-dd06-f331fe143c11', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('b7341b30-d793-96f0-8a82-664c78d92f2d', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('b7457a89-49e8-9913-25ea-bcae11ee8a47', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('bae40eca-2811-9352-f543-78b758f8866d', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('bb5be7bc-f71f-f587-3d04-062fabdddf6e', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('bbfbd3e8-ca9b-638f-7919-82c2b4e2c245', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('bd5234a2-e044-b7f3-7595-de0f0eaf0ac1', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('be58cb3f-77d1-c614-f9d5-2b5696acdf4f', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('c22fe868-5ef6-52e2-4a9a-1b28d407307c', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('c24233ef-3c78-6200-cd11-9dd5d9be4159', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('c29c1da0-ebff-3eef-28a0-bd9be5e3930f', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('c39d292e-b2ac-4175-ca43-5703372a06d2', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('c4256393-1a61-06fb-8846-662738220cb1', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('c4d8b5c4-a624-de9d-54db-16b5292d50fc', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('c6003085-2efb-b94c-a64d-2e44e27d5d6c', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('c8b9afc4-94e1-416d-37e2-60130783d949', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('c9bfbc24-a60e-4986-9394-0958d744441c', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('ca9d4435-8bf1-50ab-c67c-e267a5d963d6', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('cc1a5caa-f669-32f8-af4d-0ce5b3f10936', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('cc25af3d-2736-50c1-f047-8b05f25f75d0', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('cceac40c-fb4b-7280-18e5-25f56f49c253', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('cd0adb85-eee1-a70b-cbf9-d460fdef97b0', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('cd5f416f-3362-5137-0d1f-cbbf178ee54c', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('cdbfe4fe-6a6b-437e-c7fc-765b07191895', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('cdfa9f0b-c19b-9530-733e-a7693a5cf896', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('ce0bb6df-6ae7-38ff-f606-b11d3f755d26', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('cfd5df2a-cb2f-58de-16df-2f2348f51fd8', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('d0cdcbeb-3f99-3f4f-0819-30e40e068428', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('d158bba5-6bfe-b1ab-c1e2-e18ad1a4c5ca', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('d178dd7e-85fb-aab9-8091-ec8d2d86b016', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('d229a340-bf05-bd6c-748e-aaaa842de523', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('d250ff81-d6f0-6958-1c19-7d625c0a83cd', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('d2e2acb9-7325-8392-da07-fac8b7a2bfaa', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('d7ea80fb-910d-8c97-aa41-18fd5f085d89', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('d7eaaeef-ce09-e5c3-64f4-3e30fde047bd', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('d822584a-8544-63a4-1744-079cc68d9f7a', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('d87fbe27-00ff-3ae9-96d2-089d2ff73010', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('d8b1e9c4-09c3-f108-2de0-7664c407dce5', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('d8d4998f-7f6e-5164-3038-04c023ab35a1', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('da486596-6548-1458-26de-42340db3d974', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('dac5552f-1c10-f074-a9f8-d630069b3557', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('db3f6b68-45b7-4f5c-e5bb-2774a23145a3', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('dc144322-616a-ef7b-f471-c3c76b134256', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('dc31d036-bc5c-e085-50db-bebb459b3d61', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('dcd440a7-aef6-4609-a712-cef9ec65fa77', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('dd0b878b-7d1a-c635-1b9e-4b989b2cd08d', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('ddf22a29-316f-233f-bd1e-80229487773b', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('df347751-ee9f-ed4d-e66f-24856ff968ad', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('e0ac6c79-0bb4-ef04-d53f-5ca5c153748e', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('e2a93fbc-d95a-e2be-cfa2-26f546d4f88c', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('e358e498-8e7d-731a-6abf-7510b1a61d37', '344e3b7e-fbde-bb12-053b-6979595be1fc');
INSERT INTO `aspnetuserroles` VALUES ('037158ef-ac5f-4122-98b9-3d4382bc6697', '4f25ea99-7117-217e-10f4-92c75dd841d6');
INSERT INTO `aspnetuserroles` VALUES ('038d846e-fdfc-424d-badb-091eb44b8255', '4f25ea99-7117-217e-10f4-92c75dd841d6');
INSERT INTO `aspnetuserroles` VALUES ('053dbe49-7de5-40fb-923a-04ae4b300a64', '4f25ea99-7117-217e-10f4-92c75dd841d6');
INSERT INTO `aspnetuserroles` VALUES ('1bfce5a1-5943-42e5-87fe-236959a015bb', '4f25ea99-7117-217e-10f4-92c75dd841d6');
INSERT INTO `aspnetuserroles` VALUES ('2440ea84-93ae-44dd-a29e-b7f87864ccfc', '4f25ea99-7117-217e-10f4-92c75dd841d6');
INSERT INTO `aspnetuserroles` VALUES ('28951d84-d502-4de9-8674-99a84f4c8a40', '4f25ea99-7117-217e-10f4-92c75dd841d6');
INSERT INTO `aspnetuserroles` VALUES ('2dc831f6-82f0-43a2-b92a-bddd8c578732', '4f25ea99-7117-217e-10f4-92c75dd841d6');
INSERT INTO `aspnetuserroles` VALUES ('3de40c37-f6c7-4e47-a622-cc34d41d6c26', '4f25ea99-7117-217e-10f4-92c75dd841d6');
INSERT INTO `aspnetuserroles` VALUES ('3eafb3c9-aa04-4b60-b1ca-56c3389aab9e', '4f25ea99-7117-217e-10f4-92c75dd841d6');
INSERT INTO `aspnetuserroles` VALUES ('47c31f3b-6a9e-48e7-9efb-42dc3ceaa4c0', '4f25ea99-7117-217e-10f4-92c75dd841d6');
INSERT INTO `aspnetuserroles` VALUES ('489aba26-fb61-46f1-8f3e-3851f3a31f09', '4f25ea99-7117-217e-10f4-92c75dd841d6');
INSERT INTO `aspnetuserroles` VALUES ('57dfec8a-fef8-4a86-8779-f2d3d89a07f7', '4f25ea99-7117-217e-10f4-92c75dd841d6');
INSERT INTO `aspnetuserroles` VALUES ('7197727d-2cf9-495f-a380-15266965a29d', '4f25ea99-7117-217e-10f4-92c75dd841d6');
INSERT INTO `aspnetuserroles` VALUES ('80e94c3b-ca99-4126-9438-86d411e1c960', '4f25ea99-7117-217e-10f4-92c75dd841d6');
INSERT INTO `aspnetuserroles` VALUES ('82ed3da7-79db-416e-ac1d-ca048d6432e0', '4f25ea99-7117-217e-10f4-92c75dd841d6');
INSERT INTO `aspnetuserroles` VALUES ('8e73cced-d51f-407f-aaac-14cc412a4fe7', '4f25ea99-7117-217e-10f4-92c75dd841d6');
INSERT INTO `aspnetuserroles` VALUES ('b4ad8b4d-ad00-49ea-8916-02731636fe52', '4f25ea99-7117-217e-10f4-92c75dd841d6');
INSERT INTO `aspnetuserroles` VALUES ('bf0059e2-987b-4b40-bd42-1bf294ee0cf5', '4f25ea99-7117-217e-10f4-92c75dd841d6');
INSERT INTO `aspnetuserroles` VALUES ('027574a0-eaa6-2f7b-da1d-fb7d49a05ba9', '7256c38c-768d-63e4-9b8f-12cd54fd43d1');

-- ----------------------------
-- Table structure for campuses
-- ----------------------------
DROP TABLE IF EXISTS `campuses`;
CREATE TABLE `campuses`  (
  `ID` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `CmName` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `CmSite` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `CmText` varchar(1000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of campuses
-- ----------------------------
INSERT INTO `campuses` VALUES ('c4e713be-dc60-9fe3-3396-d28cb323da01', '致一教育龙岩校区', '福建省龙岩市新罗区', '地处龙岩市中心 交通便利，本校授课为计算机编程语言，授课内容与时俱进，现代社会计算机专业前景良好，无论您是想要工作还是考试都是不错的选择，欢迎报考我校!!!!!');
INSERT INTO `campuses` VALUES ('f01918ea-3d82-d00c-cd7f-6d7cb5724ef9', '致一教育福州校区', '福建省福州市仓山区', '地处福州市中心 交通便利，本校授课为计算机编程语言，授课内容与时俱进，现代社会计算机专业前景良好，无论您是想要工作还是考试都是不错的选择，欢迎报考我校!!!!!');
INSERT INTO `campuses` VALUES ('fe9f4c36-c330-7baa-3747-00a455f75431', '致一教育厦门校区', '福建省厦门市集美区', '厦门区思明区致一计算机技术有限公司，深耕福建软件培训10年，现拥有厦门集美校区、福州仓山校区、龙岩分校区。迄今为止，致一教育开发并完善了Java、WEB前端/Python、嵌入式AI、等极具竞争力的课程体系，覆盖当今热门的软件行业开发技术。');

-- ----------------------------
-- Table structure for classeas
-- ----------------------------
DROP TABLE IF EXISTS `classeas`;
CREATE TABLE `classeas`  (
  `ID` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `CalssNo` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `CampusNameID` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `TeacherId` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `ClassType` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `CourseOK` int NULL DEFAULT NULL,
  `IsGraduate` tinyint(1) NOT NULL DEFAULT 0,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `IX_classeas_CampusNameID`(`CampusNameID` ASC) USING BTREE,
  INDEX `IX_classeas_TeacherId`(`TeacherId` ASC) USING BTREE,
  CONSTRAINT `FK_classeas_campuses_CampusNameID` FOREIGN KEY (`CampusNameID`) REFERENCES `campuses` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_classeas_teacheres_TeacherId` FOREIGN KEY (`TeacherId`) REFERENCES `teacheres` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of classeas
-- ----------------------------
INSERT INTO `classeas` VALUES ('07fb639c-c66d-50de-71f1-8ba0a0ba8833', 'X.一班', 'fe9f4c36-c330-7baa-3747-00a455f75431', '36cf8496-e4d2-6657-5bd5-d8e3b496118d', 'HTMLCSS', 0, 0, 0);
INSERT INTO `classeas` VALUES ('2a32710e-a4fa-ea29-6263-815ab8bc3838', 'L.七班', 'c4e713be-dc60-9fe3-3396-d28cb323da01', '77f505cc-7d23-9853-ca56-85db7f280b62', 'HTMLCSS', 0, 0, 0);
INSERT INTO `classeas` VALUES ('360de5a6-1717-bdbe-6bad-5279e2af03af', 'F.三班', 'f01918ea-3d82-d00c-cd7f-6d7cb5724ef9', '85267914-34df-2c83-3ed0-8f2e75a45128', 'ASPNETCoreMVC', 0, 0, 0);
INSERT INTO `classeas` VALUES ('37aa9628-103a-67b2-5ccf-174e6fdb966b', 'X.四班', 'fe9f4c36-c330-7baa-3747-00a455f75431', '27375864-0298-39a1-f6eb-7375b0fe4a80', 'ASPNETCoreMVC', 0, 0, 0);
INSERT INTO `classeas` VALUES ('3c5ce332-9156-4abb-0cfc-d615c53e8da3', 'F.五班', 'f01918ea-3d82-d00c-cd7f-6d7cb5724ef9', '918197c9-5cc9-8699-486d-f5a3b838d998', 'HTMLCSS', 0, 0, 0);
INSERT INTO `classeas` VALUES ('49b61dcd-e9f7-56e6-5a57-d2a24d85e583', 'F.六班', 'f01918ea-3d82-d00c-cd7f-6d7cb5724ef9', 'c99e03f2-0941-81da-e224-e49c50ef5d69', 'VUE', 0, 0, 0);
INSERT INTO `classeas` VALUES ('57a30c9a-fd39-f761-2c27-1e65ba5a8312', 'X.五班', 'fe9f4c36-c330-7baa-3747-00a455f75431', '2d18c9b1-3192-3aa9-0a18-9becb94efa78', 'ASPNETCoreMVC', 0, 0, 0);
INSERT INTO `classeas` VALUES ('743d0853-4883-b225-78ae-ef1199aaeb58', 'L.三班', 'c4e713be-dc60-9fe3-3396-d28cb323da01', '77f505cc-7d23-9853-ca56-85db7f280b62', 'HTMLCSS', 0, 0, 0);
INSERT INTO `classeas` VALUES ('795614cd-573a-5e0b-67f0-a9f8548290e8', 'L.四班', 'c4e713be-dc60-9fe3-3396-d28cb323da01', '59eec08d-66ff-e1e8-1ab7-090c28a775c7', 'ASPNETCoreMVC', 0, 0, 0);
INSERT INTO `classeas` VALUES ('7b66fbda-40f9-8ad5-8a67-803c25300414', 'X.七班', 'fe9f4c36-c330-7baa-3747-00a455f75431', '36cf8496-e4d2-6657-5bd5-d8e3b496118d', 'HTMLCSS', 0, 0, 0);
INSERT INTO `classeas` VALUES ('865c36cd-8b08-a32c-ef9a-b83e086a246f', 'L.五班', 'c4e713be-dc60-9fe3-3396-d28cb323da01', '5d688b38-dd54-4bc7-59b3-5efa6abd2ddf', 'ASPNETCoreMVC', 0, 0, 0);
INSERT INTO `classeas` VALUES ('8c6b7ff4-60e8-37a6-53d9-16b09d0aed82', 'X.六班', 'fe9f4c36-c330-7baa-3747-00a455f75431', '2262b970-2d25-d25f-a5d6-7498a91b7d5f', 'VUE', 0, 0, 0);
INSERT INTO `classeas` VALUES ('a30bc4cc-0833-79aa-e072-a106edae1d62', 'F.七班', 'f01918ea-3d82-d00c-cd7f-6d7cb5724ef9', 'e50fbf73-23f6-8596-4886-69f0023862cb', 'VUE', 0, 0, 0);
INSERT INTO `classeas` VALUES ('ad952135-bceb-77a5-632a-b5bc27f59838', 'L.二班', 'c4e713be-dc60-9fe3-3396-d28cb323da01', '9e869700-f818-5f45-dcfa-64bb8ed73198', 'VUE', 20, 1, 0);
INSERT INTO `classeas` VALUES ('adbf3f28-1d91-2400-894e-9044eefecdc9', 'F.四班', 'f01918ea-3d82-d00c-cd7f-6d7cb5724ef9', 'c99e03f2-0941-81da-e224-e49c50ef5d69', 'VUE', 0, 0, 0);
INSERT INTO `classeas` VALUES ('b26068a0-4ace-5cde-a36c-795990f74006', 'L.六班', 'c4e713be-dc60-9fe3-3396-d28cb323da01', '9cf1ec8e-e25c-79ae-d3ff-15de01936750', 'VUE', 0, 0, 0);
INSERT INTO `classeas` VALUES ('bf73403d-7ebb-5a57-05f9-2ddb3063ef55', 'L.一班', 'c4e713be-dc60-9fe3-3396-d28cb323da01', '09016268-39de-9d10-fddf-c46023ebae78', 'ASPNETCoreWebAPI', 0, 0, 0);
INSERT INTO `classeas` VALUES ('e7badc0e-6468-90df-e8ee-c5343b4ad7af', 'F.二班', 'f01918ea-3d82-d00c-cd7f-6d7cb5724ef9', 'e50fbf73-23f6-8596-4886-69f0023862cb', 'VUE', 0, 0, 0);
INSERT INTO `classeas` VALUES ('e9b62d1b-095a-a94f-e6e1-cba6fab3bf29', 'F.一班', 'f01918ea-3d82-d00c-cd7f-6d7cb5724ef9', '13c3637f-722a-fb04-b8dd-9e6f6a3c76e6', 'ASPNETCoreWebAPI', 0, 0, 0);
INSERT INTO `classeas` VALUES ('f0d3138e-431b-fa73-1452-52efd912c7f7', 'X.二班', 'fe9f4c36-c330-7baa-3747-00a455f75431', '006b0ece-a585-6609-23f7-70f42454c060', 'ASPNETCoreWebAPI', 20, 1, 0);
INSERT INTO `classeas` VALUES ('f1086fe1-f176-e541-1ad6-658d0543486c', 'X.三班', 'fe9f4c36-c330-7baa-3747-00a455f75431', '006b0ece-a585-6609-23f7-70f42454c060', 'ASPNETCoreWebAPI', 0, 0, 0);

-- ----------------------------
-- Table structure for classrecords
-- ----------------------------
DROP TABLE IF EXISTS `classrecords`;
CREATE TABLE `classrecords`  (
  `ID` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `InClassNo` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `StopTime` datetime(6) NULL DEFAULT NULL,
  `CourseArrangId` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `StatusId` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `IX_classrecords_CourseArrangId`(`CourseArrangId` ASC) USING BTREE,
  INDEX `IX_classrecords_StatusId`(`StatusId` ASC) USING BTREE,
  INDEX `FK_classrecords_classeas_InClassNo`(`InClassNo` ASC) USING BTREE,
  CONSTRAINT `FK_classrecords_classeas_InClassNo` FOREIGN KEY (`InClassNo`) REFERENCES `classeas` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_classrecords_classstatuses_StatusId` FOREIGN KEY (`StatusId`) REFERENCES `classstatuses` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_classrecords_coursearrangs_CourseArrangId` FOREIGN KEY (`CourseArrangId`) REFERENCES `coursearrangs` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of classrecords
-- ----------------------------
INSERT INTO `classrecords` VALUES ('07b798a1-3b9c-1abc-a71c-9e604d601757', '2a32710e-a4fa-ea29-6263-815ab8bc3838', '2023-07-03 17:50:00.000000', '3dd5bf61-08b1-96e3-4b42-888cbcd1b3a3', 'e17fb75f-9320-47bf-523a-79f167d55d0a');
INSERT INTO `classrecords` VALUES ('094de40e-99e8-c230-3407-ec47108c034a', '2a32710e-a4fa-ea29-6263-815ab8bc3838', '2023-07-04 15:50:00.000000', '57822fab-bc4c-d68e-bf3c-340e64a62b33', 'e17fb75f-9320-47bf-523a-79f167d55d0a');
INSERT INTO `classrecords` VALUES ('0a3fc410-dea2-7cb8-ffbb-3184a08612e1', '2a32710e-a4fa-ea29-6263-815ab8bc3838', '2023-07-06 15:50:00.000000', '8cb4b933-08d5-85fc-3612-7b5280edb7a6', 'e17fb75f-9320-47bf-523a-79f167d55d0a');
INSERT INTO `classrecords` VALUES ('21c7495c-9d92-aab8-8ea8-d57f97c4ea25', '2a32710e-a4fa-ea29-6263-815ab8bc3838', '2023-07-07 15:50:00.000000', '8d5fedc3-0f44-ab79-b0ee-1ff01985f523', 'e17fb75f-9320-47bf-523a-79f167d55d0a');
INSERT INTO `classrecords` VALUES ('2b393a98-d1a7-1e32-61a6-281c17d0594a', '7b66fbda-40f9-8ad5-8a67-803c25300414', '2023-07-03 17:50:00.000000', '640146b4-d7ba-0187-11a1-68ef10ff413a', 'e17fb75f-9320-47bf-523a-79f167d55d0a');
INSERT INTO `classrecords` VALUES ('30b07509-f89b-1c6b-3be1-407c51132c1f', '7b66fbda-40f9-8ad5-8a67-803c25300414', '2023-07-04 15:50:00.000000', 'afbb01e6-f4da-8c31-1c79-3f0c98a7da9a', 'e17fb75f-9320-47bf-523a-79f167d55d0a');
INSERT INTO `classrecords` VALUES ('34a421b2-ca1d-7f3e-0dd5-5b34ffd5c9e4', '7b66fbda-40f9-8ad5-8a67-803c25300414', '2023-07-06 15:50:00.000000', 'd7f1ea52-c43c-43de-110b-b719db73114e', 'e17fb75f-9320-47bf-523a-79f167d55d0a');
INSERT INTO `classrecords` VALUES ('35b1eb4a-5a56-03d1-dce0-289db282d236', '7b66fbda-40f9-8ad5-8a67-803c25300414', '2023-07-07 15:50:00.000000', 'ddd9c4ff-36d7-38fe-03be-d0b111b0ebbb', 'e17fb75f-9320-47bf-523a-79f167d55d0a');

-- ----------------------------
-- Table structure for classrooms
-- ----------------------------
DROP TABLE IF EXISTS `classrooms`;
CREATE TABLE `classrooms`  (
  `ID` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ClassRoomNo` int NOT NULL,
  `CampusID` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `ClassRoomSite` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `IsOccupy` tinyint(1) NULL DEFAULT 0,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `IX_classrooms_CampusID`(`CampusID` ASC) USING BTREE,
  CONSTRAINT `FK_classrooms_campuses_CampusID` FOREIGN KEY (`CampusID`) REFERENCES `campuses` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of classrooms
-- ----------------------------
INSERT INTO `classrooms` VALUES ('0d3676bb-af03-299b-a3cc-433007326a96', 1, 'c4e713be-dc60-9fe3-3396-d28cb323da01', '新罗区曹溪街道18号1楼', 0, 0);
INSERT INTO `classrooms` VALUES ('10a36cff-3aee-343a-eb96-21a8abcf9b0c', 1, 'f01918ea-3d82-d00c-cd7f-6d7cb5724ef9', '福州市仓山区新民路33号4楼', 0, 0);
INSERT INTO `classrooms` VALUES ('124d5147-b267-909a-7779-22c68190f764', 2, 'f01918ea-3d82-d00c-cd7f-6d7cb5724ef9', '福州市仓山区新民路33号4楼', 0, 0);
INSERT INTO `classrooms` VALUES ('233060b0-a8de-5bb3-335e-e8223d83d854', 1, 'fe9f4c36-c330-7baa-3747-00a455f75431', '厦门市集美区嘉庚路五缘校区A栋3楼', 0, 0);
INSERT INTO `classrooms` VALUES ('291b03e7-0d63-009b-f6c4-def54196631d', 2, 'fe9f4c36-c330-7baa-3747-00a455f75431', '厦门市集美区嘉庚路五缘校区A栋3楼', 0, 0);
INSERT INTO `classrooms` VALUES ('32f0b371-f681-95fd-79aa-b43907d01106', 2, 'c4e713be-dc60-9fe3-3396-d28cb323da01', '新罗区曹溪街道18号1楼', 0, 0);
INSERT INTO `classrooms` VALUES ('3f33ba46-1fe6-fe16-4798-a71217212d7b', 3, 'c4e713be-dc60-9fe3-3396-d28cb323da01', '新罗区曹溪街道18号1楼', 0, 0);
INSERT INTO `classrooms` VALUES ('617b5233-83d1-57a0-29c6-81f37b7889c5', 4, 'c4e713be-dc60-9fe3-3396-d28cb323da01', '新罗区曹溪街道18号2楼', 0, 0);
INSERT INTO `classrooms` VALUES ('661b9dad-96f3-888c-fffb-d7cfaa280641', 3, 'fe9f4c36-c330-7baa-3747-00a455f75431', '厦门市集美区嘉庚路五缘校区A栋3楼', 0, 0);
INSERT INTO `classrooms` VALUES ('6834e3f4-2b05-6085-57bb-b8484c46ac78', 3, 'f01918ea-3d82-d00c-cd7f-6d7cb5724ef9', '福州市仓山区新民路33号4楼', 0, 0);
INSERT INTO `classrooms` VALUES ('715b34a7-696f-3dbe-7200-e8fc5d12dd76', 4, 'f01918ea-3d82-d00c-cd7f-6d7cb5724ef9', '福州市仓山区新民路35号2楼', 0, 0);
INSERT INTO `classrooms` VALUES ('781ad551-febf-7fb6-7092-ad9f3ff2c8cb', 4, 'fe9f4c36-c330-7baa-3747-00a455f75431', '厦门市集美区嘉庚路五缘校区A栋3楼', 0, 0);
INSERT INTO `classrooms` VALUES ('9930a88f-8c4f-2240-3362-36a91287b357', 5, 'c4e713be-dc60-9fe3-3396-d28cb323da01', '新罗区曹溪街道18号2楼', 0, 0);
INSERT INTO `classrooms` VALUES ('9b1660ea-8eec-ea26-a762-055f697d8b9f', 5, 'fe9f4c36-c330-7baa-3747-00a455f75431', '厦门市集美区嘉庚路五缘校区A栋3楼', 0, 0);
INSERT INTO `classrooms` VALUES ('ad8e7eb0-3696-9785-87aa-27421c2a00ed', 6, 'fe9f4c36-c330-7baa-3747-00a455f75431', '厦门市集美区嘉庚路五缘校区A栋3楼', 0, 0);
INSERT INTO `classrooms` VALUES ('aea41a1d-d2a4-32d4-603f-f5f0c1e0d3aa', 5, 'f01918ea-3d82-d00c-cd7f-6d7cb5724ef9', '福州市仓山区新民路35号2楼', 0, 0);
INSERT INTO `classrooms` VALUES ('b3d6adc8-5514-2399-7da5-86eff6eec7a3', 6, 'c4e713be-dc60-9fe3-3396-d28cb323da01', '新罗区曹溪街道18号2楼', 0, 0);
INSERT INTO `classrooms` VALUES ('ef75b2ef-1a45-1f90-0c33-e84a61c69e4b', 6, 'f01918ea-3d82-d00c-cd7f-6d7cb5724ef9', '福州市仓山区新民路35号2楼', 0, 0);

-- ----------------------------
-- Table structure for classstatuses
-- ----------------------------
DROP TABLE IF EXISTS `classstatuses`;
CREATE TABLE `classstatuses`  (
  `ID` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `StatusName` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of classstatuses
-- ----------------------------
INSERT INTO `classstatuses` VALUES ('109b2152-6da1-4e13-333b-23a9008fe4d0', '待上课');
INSERT INTO `classstatuses` VALUES ('5654243b-4439-edbc-55c7-e1146ec26de8', '正在上课');
INSERT INTO `classstatuses` VALUES ('e17fb75f-9320-47bf-523a-79f167d55d0a', '已上完');

-- ----------------------------
-- Table structure for coursearrangs
-- ----------------------------
DROP TABLE IF EXISTS `coursearrangs`;
CREATE TABLE `coursearrangs`  (
  `ID` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `CourseType` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `InClassNo` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `InTcnameID` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `InCourseTime` datetime(6) NULL DEFAULT NULL,
  `InClassRoomNo` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `IsDeleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `IX_courseArrangs_CourseType`(`CourseType` ASC) USING BTREE,
  INDEX `IX_courseArrangs_InClassNo`(`InClassNo` ASC) USING BTREE,
  INDEX `IX_courseArrangs_InClassRoomNo`(`InClassRoomNo` ASC) USING BTREE,
  INDEX `IX_courseArrangs_InTcnameID`(`InTcnameID` ASC) USING BTREE,
  CONSTRAINT `FK_courseArrangs_classeas_InClassNo` FOREIGN KEY (`InClassNo`) REFERENCES `classeas` (`ID`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `FK_courseArrangs_classrooms_InClassRoomNo` FOREIGN KEY (`InClassRoomNo`) REFERENCES `classrooms` (`ID`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `FK_courseArrangs_coursefiles_CourseType` FOREIGN KEY (`CourseType`) REFERENCES `coursefiles` (`ID`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `FK_courseArrangs_teacheres_InTcnameID` FOREIGN KEY (`InTcnameID`) REFERENCES `teacheres` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of coursearrangs
-- ----------------------------
INSERT INTO `coursearrangs` VALUES ('022d8b53-c4d6-4095-93eb-e514635f48c0', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', 'b26068a0-4ace-5cde-a36c-795990f74006', 'e50fbf73-23f6-8596-4886-69f0023862cb', '2023-07-31 08:00:00.000000', 'b3d6adc8-5514-2399-7da5-86eff6eec7a3', 0);
INSERT INTO `coursearrangs` VALUES ('0446b4a5-3ef7-4734-8ef7-ab30a65692ac', '30ccc950-8b50-7ada-7b08-2fda53f022fd', '795614cd-573a-5e0b-67f0-a9f8548290e8', '59eec08d-66ff-e1e8-1ab7-090c28a775c7', '2023-07-31 16:00:00.000000', '617b5233-83d1-57a0-29c6-81f37b7889c5', 0);
INSERT INTO `coursearrangs` VALUES ('05b7d68a-337b-432c-895f-65c1e8327f82', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', 'b26068a0-4ace-5cde-a36c-795990f74006', 'e50fbf73-23f6-8596-4886-69f0023862cb', '2023-08-02 10:00:00.000000', 'b3d6adc8-5514-2399-7da5-86eff6eec7a3', 0);
INSERT INTO `coursearrangs` VALUES ('07506e5d-483d-4736-9bc9-ca935be12bd1', 'ee9c9a4d-2c70-8025-646f-13ede5096372', '07fb639c-c66d-50de-71f1-8ba0a0ba8833', '36cf8496-e4d2-6657-5bd5-d8e3b496118d', '2023-07-31 08:00:00.000000', '233060b0-a8de-5bb3-335e-e8223d83d854', 0);
INSERT INTO `coursearrangs` VALUES ('08f9cf95-6d80-4f56-a41d-8b0617dee35f', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', 'e7badc0e-6468-90df-e8ee-c5343b4ad7af', 'e50fbf73-23f6-8596-4886-69f0023862cb', '2023-07-31 10:00:00.000000', '124d5147-b267-909a-7779-22c68190f764', 0);
INSERT INTO `coursearrangs` VALUES ('09bc8f88-056a-4308-ac70-f715aeced008', '30ccc950-8b50-7ada-7b08-2fda53f022fd', '360de5a6-1717-bdbe-6bad-5279e2af03af', '85267914-34df-2c83-3ed0-8f2e75a45128', '2023-07-31 14:00:00.000000', '6834e3f4-2b05-6085-57bb-b8484c46ac78', 0);
INSERT INTO `coursearrangs` VALUES ('0a594941-8634-44f9-ae33-e0190a22f6bf', 'ee9c9a4d-2c70-8025-646f-13ede5096372', '743d0853-4883-b225-78ae-ef1199aaeb58', '77f505cc-7d23-9853-ca56-85db7f280b62', '2023-07-31 14:00:00.000000', '3f33ba46-1fe6-fe16-4798-a71217212d7b', 0);
INSERT INTO `coursearrangs` VALUES ('0f0b6c36-9326-41fa-9376-693bc1f8a4be', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', '8c6b7ff4-60e8-37a6-53d9-16b09d0aed82', '2262b970-2d25-d25f-a5d6-7498a91b7d5f', '2023-07-31 08:00:00.000000', 'ad8e7eb0-3696-9785-87aa-27421c2a00ed', 0);
INSERT INTO `coursearrangs` VALUES ('0fb2c8bc-4e58-4ca7-8449-56539e070e14', '30ccc950-8b50-7ada-7b08-2fda53f022fd', '360de5a6-1717-bdbe-6bad-5279e2af03af', '85267914-34df-2c83-3ed0-8f2e75a45128', '2023-08-01 10:00:00.000000', '6834e3f4-2b05-6085-57bb-b8484c46ac78', 0);
INSERT INTO `coursearrangs` VALUES ('127d4478-d051-4bc6-b305-e5236323a3d2', '497ad04c-f956-d2d8-e23c-1e25706ba8ff', 'bf73403d-7ebb-5a57-05f9-2ddb3063ef55', '09016268-39de-9d10-fddf-c46023ebae78', '2023-07-31 08:00:00.000000', '0d3676bb-af03-299b-a3cc-433007326a96', 0);
INSERT INTO `coursearrangs` VALUES ('13fc710f-2112-473a-8f96-466099b04ffb', '497ad04c-f956-d2d8-e23c-1e25706ba8ff', 'f1086fe1-f176-e541-1ad6-658d0543486c', '006b0ece-a585-6609-23f7-70f42454c060', '2023-07-31 14:00:00.000000', '661b9dad-96f3-888c-fffb-d7cfaa280641', 0);
INSERT INTO `coursearrangs` VALUES ('1efbb8a5-5921-487d-af71-b4b71cdbded0', '30ccc950-8b50-7ada-7b08-2fda53f022fd', '865c36cd-8b08-a32c-ef9a-b83e086a246f', '9cf1ec8e-e25c-79ae-d3ff-15de01936750', '2023-07-31 10:00:00.000000', '9930a88f-8c4f-2240-3362-36a91287b357', 0);
INSERT INTO `coursearrangs` VALUES ('2231a908-18b8-42a0-bf78-b539cdf6f1d3', '497ad04c-f956-d2d8-e23c-1e25706ba8ff', 'f1086fe1-f176-e541-1ad6-658d0543486c', '006b0ece-a585-6609-23f7-70f42454c060', '2023-08-01 10:00:00.000000', '661b9dad-96f3-888c-fffb-d7cfaa280641', 0);
INSERT INTO `coursearrangs` VALUES ('241ad4d1-88db-421b-aa8a-e1f9f7c2a07b', '497ad04c-f956-d2d8-e23c-1e25706ba8ff', 'e9b62d1b-095a-a94f-e6e1-cba6fab3bf29', '13c3637f-722a-fb04-b8dd-9e6f6a3c76e6', '2023-07-31 08:00:00.000000', '10a36cff-3aee-343a-eb96-21a8abcf9b0c', 0);
INSERT INTO `coursearrangs` VALUES ('247d668c-4130-4e99-a6f0-1d4799b814ff', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', 'a30bc4cc-0833-79aa-e072-a106edae1d62', 'e50fbf73-23f6-8596-4886-69f0023862cb', '2023-07-31 16:00:00.000000', 'ef75b2ef-1a45-1f90-0c33-e84a61c69e4b', 0);
INSERT INTO `coursearrangs` VALUES ('25210c1f-bceb-4719-9ec3-8f954ade0028', 'ee9c9a4d-2c70-8025-646f-13ede5096372', '3c5ce332-9156-4abb-0cfc-d615c53e8da3', '918197c9-5cc9-8699-486d-f5a3b838d998', '2023-07-31 10:00:00.000000', 'aea41a1d-d2a4-32d4-603f-f5f0c1e0d3aa', 0);
INSERT INTO `coursearrangs` VALUES ('28378189-4441-42ed-b071-7594b11c03cb', '30ccc950-8b50-7ada-7b08-2fda53f022fd', '37aa9628-103a-67b2-5ccf-174e6fdb966b', '27375864-0298-39a1-f6eb-7375b0fe4a80', '2023-07-31 16:00:00.000000', '781ad551-febf-7fb6-7092-ad9f3ff2c8cb', 0);
INSERT INTO `coursearrangs` VALUES ('2c27c7d9-0dbc-421d-b22b-4dfcffbea5b9', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', '49b61dcd-e9f7-56e6-5a57-d2a24d85e583', 'c99e03f2-0941-81da-e224-e49c50ef5d69', '2023-07-31 08:00:00.000000', 'ef75b2ef-1a45-1f90-0c33-e84a61c69e4b', 0);
INSERT INTO `coursearrangs` VALUES ('2d6121df-2c47-4878-934c-b78918422d56', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', '8c6b7ff4-60e8-37a6-53d9-16b09d0aed82', '2262b970-2d25-d25f-a5d6-7498a91b7d5f', '2023-08-02 10:00:00.000000', 'ad8e7eb0-3696-9785-87aa-27421c2a00ed', 0);
INSERT INTO `coursearrangs` VALUES ('2f7550c0-2b75-41d2-b9eb-9ad902324c4d', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', 'b26068a0-4ace-5cde-a36c-795990f74006', 'e50fbf73-23f6-8596-4886-69f0023862cb', '2023-08-04 10:00:00.000000', 'b3d6adc8-5514-2399-7da5-86eff6eec7a3', 0);
INSERT INTO `coursearrangs` VALUES ('3536b868-3f91-4a52-8040-ec9a358733aa', '30ccc950-8b50-7ada-7b08-2fda53f022fd', '865c36cd-8b08-a32c-ef9a-b83e086a246f', '9cf1ec8e-e25c-79ae-d3ff-15de01936750', '2023-08-01 16:00:00.000000', '9930a88f-8c4f-2240-3362-36a91287b357', 0);
INSERT INTO `coursearrangs` VALUES ('397a86b9-c9bf-4b52-8674-ca4ad995fad2', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', 'b26068a0-4ace-5cde-a36c-795990f74006', 'e50fbf73-23f6-8596-4886-69f0023862cb', '2023-08-05 10:00:00.000000', 'b3d6adc8-5514-2399-7da5-86eff6eec7a3', 0);
INSERT INTO `coursearrangs` VALUES ('39adeb57-8887-4e28-952e-26c2a766cabc', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', 'ad952135-bceb-77a5-632a-b5bc27f59838', '9e869700-f818-5f45-dcfa-64bb8ed73198', '2023-07-31 10:00:00.000000', '32f0b371-f681-95fd-79aa-b43907d01106', 0);
INSERT INTO `coursearrangs` VALUES ('3dd5bf61-08b1-96e3-4b42-888cbcd1b3a3', 'ee9c9a4d-2c70-8025-646f-13ede5096372', '2a32710e-a4fa-ea29-6263-815ab8bc3838', '77f505cc-7d23-9853-ca56-85db7f280b62', '2023-07-03 16:00:00.000000', 'b3d6adc8-5514-2399-7da5-86eff6eec7a3', 0);
INSERT INTO `coursearrangs` VALUES ('3f72cd55-1112-4601-9104-04595e7008cf', '497ad04c-f956-d2d8-e23c-1e25706ba8ff', 'f0d3138e-431b-fa73-1452-52efd912c7f7', '006b0ece-a585-6609-23f7-70f42454c060', '2023-07-31 10:00:00.000000', '291b03e7-0d63-009b-f6c4-def54196631d', 0);
INSERT INTO `coursearrangs` VALUES ('40d266d5-5cda-49b3-a6be-ac2d90dca835', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', 'e7badc0e-6468-90df-e8ee-c5343b4ad7af', 'e50fbf73-23f6-8596-4886-69f0023862cb', '2023-08-01 14:00:00.000000', '124d5147-b267-909a-7779-22c68190f764', 0);
INSERT INTO `coursearrangs` VALUES ('445e12cb-5145-40d0-b890-1f3f267eb909', '497ad04c-f956-d2d8-e23c-1e25706ba8ff', 'f1086fe1-f176-e541-1ad6-658d0543486c', '006b0ece-a585-6609-23f7-70f42454c060', '2023-08-03 14:00:00.000000', '661b9dad-96f3-888c-fffb-d7cfaa280641', 0);
INSERT INTO `coursearrangs` VALUES ('44f5286a-d16d-44fb-8bea-307bf2454804', '30ccc950-8b50-7ada-7b08-2fda53f022fd', '795614cd-573a-5e0b-67f0-a9f8548290e8', '59eec08d-66ff-e1e8-1ab7-090c28a775c7', '2023-08-01 08:00:00.000000', '617b5233-83d1-57a0-29c6-81f37b7889c5', 0);
INSERT INTO `coursearrangs` VALUES ('4a761040-cc1f-4cd2-a51b-19e8cbd8845f', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', 'adbf3f28-1d91-2400-894e-9044eefecdc9', 'c99e03f2-0941-81da-e224-e49c50ef5d69', '2023-07-31 16:00:00.000000', '715b34a7-696f-3dbe-7200-e8fc5d12dd76', 0);
INSERT INTO `coursearrangs` VALUES ('4aec2a67-300e-4c55-82a4-ea63ab73c6a9', 'ee9c9a4d-2c70-8025-646f-13ede5096372', '07fb639c-c66d-50de-71f1-8ba0a0ba8833', '36cf8496-e4d2-6657-5bd5-d8e3b496118d', '2023-08-01 10:00:00.000000', '233060b0-a8de-5bb3-335e-e8223d83d854', 0);
INSERT INTO `coursearrangs` VALUES ('4dbf0de7-7e18-4237-bb95-35ea57fdd8af', '497ad04c-f956-d2d8-e23c-1e25706ba8ff', 'e9b62d1b-095a-a94f-e6e1-cba6fab3bf29', '13c3637f-722a-fb04-b8dd-9e6f6a3c76e6', '2023-08-01 10:00:00.000000', '10a36cff-3aee-343a-eb96-21a8abcf9b0c', 0);
INSERT INTO `coursearrangs` VALUES ('50794144-1fb2-4b4f-95c8-1ff12586524d', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', '8c6b7ff4-60e8-37a6-53d9-16b09d0aed82', '2262b970-2d25-d25f-a5d6-7498a91b7d5f', '2023-08-04 10:00:00.000000', 'ad8e7eb0-3696-9785-87aa-27421c2a00ed', 0);
INSERT INTO `coursearrangs` VALUES ('57488932-8a9f-4659-8659-159f757773d4', 'ee9c9a4d-2c70-8025-646f-13ede5096372', '07fb639c-c66d-50de-71f1-8ba0a0ba8833', '36cf8496-e4d2-6657-5bd5-d8e3b496118d', '2023-08-03 10:00:00.000000', '233060b0-a8de-5bb3-335e-e8223d83d854', 0);
INSERT INTO `coursearrangs` VALUES ('57822fab-bc4c-d68e-bf3c-340e64a62b33', 'ee9c9a4d-2c70-8025-646f-13ede5096372', '2a32710e-a4fa-ea29-6263-815ab8bc3838', '77f505cc-7d23-9853-ca56-85db7f280b62', '2023-07-04 14:00:00.000000', 'b3d6adc8-5514-2399-7da5-86eff6eec7a3', 0);
INSERT INTO `coursearrangs` VALUES ('580d5902-7592-40e6-b647-9536101b6584', '30ccc950-8b50-7ada-7b08-2fda53f022fd', '37aa9628-103a-67b2-5ccf-174e6fdb966b', '27375864-0298-39a1-f6eb-7375b0fe4a80', '2023-08-01 08:00:00.000000', '781ad551-febf-7fb6-7092-ad9f3ff2c8cb', 0);
INSERT INTO `coursearrangs` VALUES ('584efb0e-d3e3-4de3-82ab-a900e520ce4a', '30ccc950-8b50-7ada-7b08-2fda53f022fd', '37aa9628-103a-67b2-5ccf-174e6fdb966b', '27375864-0298-39a1-f6eb-7375b0fe4a80', '2023-08-03 16:00:00.000000', '781ad551-febf-7fb6-7092-ad9f3ff2c8cb', 0);
INSERT INTO `coursearrangs` VALUES ('5b20f758-da70-4e6a-924c-258d40c53d4a', 'ee9c9a4d-2c70-8025-646f-13ede5096372', '3c5ce332-9156-4abb-0cfc-d615c53e8da3', '918197c9-5cc9-8699-486d-f5a3b838d998', '2023-08-04 08:00:00.000000', 'aea41a1d-d2a4-32d4-603f-f5f0c1e0d3aa', 0);
INSERT INTO `coursearrangs` VALUES ('5f35c9f6-3001-41d4-a352-c30988fddb87', '497ad04c-f956-d2d8-e23c-1e25706ba8ff', 'f0d3138e-431b-fa73-1452-52efd912c7f7', '006b0ece-a585-6609-23f7-70f42454c060', '2023-08-01 14:00:00.000000', '291b03e7-0d63-009b-f6c4-def54196631d', 0);
INSERT INTO `coursearrangs` VALUES ('640146b4-d7ba-0187-11a1-68ef10ff413a', 'ee9c9a4d-2c70-8025-646f-13ede5096372', '7b66fbda-40f9-8ad5-8a67-803c25300414', '36cf8496-e4d2-6657-5bd5-d8e3b496118d', '2023-07-31 16:00:00.000000', 'ad8e7eb0-3696-9785-87aa-27421c2a00ed', 0);
INSERT INTO `coursearrangs` VALUES ('64c6a685-8c17-4ef4-b58b-a05ba419188b', 'ee9c9a4d-2c70-8025-646f-13ede5096372', '743d0853-4883-b225-78ae-ef1199aaeb58', '77f505cc-7d23-9853-ca56-85db7f280b62', '2023-08-01 10:00:00.000000', '3f33ba46-1fe6-fe16-4798-a71217212d7b', 0);
INSERT INTO `coursearrangs` VALUES ('64fc7bf1-585b-4040-9f96-10df9e2c180d', '30ccc950-8b50-7ada-7b08-2fda53f022fd', '360de5a6-1717-bdbe-6bad-5279e2af03af', '85267914-34df-2c83-3ed0-8f2e75a45128', '2023-08-03 14:00:00.000000', '6834e3f4-2b05-6085-57bb-b8484c46ac78', 0);
INSERT INTO `coursearrangs` VALUES ('692b2342-cf27-49e1-9055-263fd6f5ec50', '30ccc950-8b50-7ada-7b08-2fda53f022fd', '57a30c9a-fd39-f761-2c27-1e65ba5a8312', '2d18c9b1-3192-3aa9-0a18-9becb94efa78', '2023-07-31 10:00:00.000000', '9b1660ea-8eec-ea26-a762-055f697d8b9f', 0);
INSERT INTO `coursearrangs` VALUES ('721e7b73-8975-4292-9d81-1269f79fe1ff', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', 'a30bc4cc-0833-79aa-e072-a106edae1d62', 'e50fbf73-23f6-8596-4886-69f0023862cb', '2023-08-01 14:00:00.000000', 'ef75b2ef-1a45-1f90-0c33-e84a61c69e4b', 0);
INSERT INTO `coursearrangs` VALUES ('725775e9-8f76-43b3-bf5b-7a935b9b17b0', '30ccc950-8b50-7ada-7b08-2fda53f022fd', '795614cd-573a-5e0b-67f0-a9f8548290e8', '59eec08d-66ff-e1e8-1ab7-090c28a775c7', '2023-08-03 16:00:00.000000', '617b5233-83d1-57a0-29c6-81f37b7889c5', 0);
INSERT INTO `coursearrangs` VALUES ('73cc12d8-d202-486f-8d59-724ba63a5ab1', '30ccc950-8b50-7ada-7b08-2fda53f022fd', '865c36cd-8b08-a32c-ef9a-b83e086a246f', '9cf1ec8e-e25c-79ae-d3ff-15de01936750', '2023-08-04 08:00:00.000000', '9930a88f-8c4f-2240-3362-36a91287b357', 0);
INSERT INTO `coursearrangs` VALUES ('7839818c-eae2-463f-9f45-84e7a75a732d', '497ad04c-f956-d2d8-e23c-1e25706ba8ff', 'f0d3138e-431b-fa73-1452-52efd912c7f7', '006b0ece-a585-6609-23f7-70f42454c060', '2023-08-03 08:00:00.000000', '291b03e7-0d63-009b-f6c4-def54196631d', 0);
INSERT INTO `coursearrangs` VALUES ('7a7a9dfd-0ae7-446e-b94d-ae6cf36b87b3', 'ee9c9a4d-2c70-8025-646f-13ede5096372', '3c5ce332-9156-4abb-0cfc-d615c53e8da3', '918197c9-5cc9-8699-486d-f5a3b838d998', '2023-08-01 16:00:00.000000', 'aea41a1d-d2a4-32d4-603f-f5f0c1e0d3aa', 0);
INSERT INTO `coursearrangs` VALUES ('7e343ce9-9773-42cd-9dda-881e1e08b8cc', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', 'adbf3f28-1d91-2400-894e-9044eefecdc9', 'c99e03f2-0941-81da-e224-e49c50ef5d69', '2023-08-01 08:00:00.000000', '715b34a7-696f-3dbe-7200-e8fc5d12dd76', 0);
INSERT INTO `coursearrangs` VALUES ('8272c9a3-1743-47b0-8596-e188000650e3', '497ad04c-f956-d2d8-e23c-1e25706ba8ff', 'bf73403d-7ebb-5a57-05f9-2ddb3063ef55', '09016268-39de-9d10-fddf-c46023ebae78', '2023-08-01 10:00:00.000000', '0d3676bb-af03-299b-a3cc-433007326a96', 0);
INSERT INTO `coursearrangs` VALUES ('833bbe3e-bec5-4260-ab9f-a84f262ae394', '30ccc950-8b50-7ada-7b08-2fda53f022fd', '57a30c9a-fd39-f761-2c27-1e65ba5a8312', '2d18c9b1-3192-3aa9-0a18-9becb94efa78', '2023-08-01 16:00:00.000000', '9b1660ea-8eec-ea26-a762-055f697d8b9f', 0);
INSERT INTO `coursearrangs` VALUES ('8970acef-db5c-453a-9f9f-6656c3299e54', '497ad04c-f956-d2d8-e23c-1e25706ba8ff', 'bf73403d-7ebb-5a57-05f9-2ddb3063ef55', '09016268-39de-9d10-fddf-c46023ebae78', '2023-08-03 10:00:00.000000', '0d3676bb-af03-299b-a3cc-433007326a96', 0);
INSERT INTO `coursearrangs` VALUES ('8ae69ff0-d39c-4882-9675-8dc98ed7f424', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', 'ad952135-bceb-77a5-632a-b5bc27f59838', '9e869700-f818-5f45-dcfa-64bb8ed73198', '2023-08-01 14:00:00.000000', '32f0b371-f681-95fd-79aa-b43907d01106', 0);
INSERT INTO `coursearrangs` VALUES ('8ca07b7b-c3b7-44f3-901e-ff6016567590', '497ad04c-f956-d2d8-e23c-1e25706ba8ff', 'bf73403d-7ebb-5a57-05f9-2ddb3063ef55', '09016268-39de-9d10-fddf-c46023ebae78', '2023-08-04 14:00:00.000000', '0d3676bb-af03-299b-a3cc-433007326a96', 0);
INSERT INTO `coursearrangs` VALUES ('8cb4b933-08d5-85fc-3612-7b5280edb7a6', 'ee9c9a4d-2c70-8025-646f-13ede5096372', '2a32710e-a4fa-ea29-6263-815ab8bc3838', '77f505cc-7d23-9853-ca56-85db7f280b62', '2023-07-06 14:00:00.000000', 'b3d6adc8-5514-2399-7da5-86eff6eec7a3', 0);
INSERT INTO `coursearrangs` VALUES ('8d5fedc3-0f44-ab79-b0ee-1ff01985f523', 'ee9c9a4d-2c70-8025-646f-13ede5096372', '2a32710e-a4fa-ea29-6263-815ab8bc3838', '77f505cc-7d23-9853-ca56-85db7f280b62', '2023-07-07 14:00:00.000000', 'b3d6adc8-5514-2399-7da5-86eff6eec7a3', 0);
INSERT INTO `coursearrangs` VALUES ('92e1b5a2-8777-440a-8e8a-21edf9d99e62', '30ccc950-8b50-7ada-7b08-2fda53f022fd', '360de5a6-1717-bdbe-6bad-5279e2af03af', '85267914-34df-2c83-3ed0-8f2e75a45128', '2023-08-04 10:00:00.000000', '6834e3f4-2b05-6085-57bb-b8484c46ac78', 0);
INSERT INTO `coursearrangs` VALUES ('9e131de5-ecd7-4664-960c-e66749d97f0e', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', '49b61dcd-e9f7-56e6-5a57-d2a24d85e583', 'c99e03f2-0941-81da-e224-e49c50ef5d69', '2023-08-02 10:00:00.000000', 'ef75b2ef-1a45-1f90-0c33-e84a61c69e4b', 0);
INSERT INTO `coursearrangs` VALUES ('9f8e2f57-9073-4259-97ad-802192e99d34', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', 'ad952135-bceb-77a5-632a-b5bc27f59838', '9e869700-f818-5f45-dcfa-64bb8ed73198', '2023-08-03 08:00:00.000000', '32f0b371-f681-95fd-79aa-b43907d01106', 0);
INSERT INTO `coursearrangs` VALUES ('a5e3463b-b6a2-4b99-8ff6-444777d28ed2', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', '8c6b7ff4-60e8-37a6-53d9-16b09d0aed82', '2262b970-2d25-d25f-a5d6-7498a91b7d5f', '2023-08-05 10:00:00.000000', 'ad8e7eb0-3696-9785-87aa-27421c2a00ed', 0);
INSERT INTO `coursearrangs` VALUES ('a92a54d9-8e61-4eff-ab72-b6dce4cdcb0f', 'ee9c9a4d-2c70-8025-646f-13ede5096372', '07fb639c-c66d-50de-71f1-8ba0a0ba8833', '36cf8496-e4d2-6657-5bd5-d8e3b496118d', '2023-08-04 14:00:00.000000', '233060b0-a8de-5bb3-335e-e8223d83d854', 0);
INSERT INTO `coursearrangs` VALUES ('a95ba0a7-efd7-4349-bca5-72c3da7de2e8', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', 'a30bc4cc-0833-79aa-e072-a106edae1d62', 'e50fbf73-23f6-8596-4886-69f0023862cb', '2023-08-03 14:00:00.000000', 'ef75b2ef-1a45-1f90-0c33-e84a61c69e4b', 0);
INSERT INTO `coursearrangs` VALUES ('ae3d2f5d-d8e6-4bd2-974d-264aab02f9f8', '497ad04c-f956-d2d8-e23c-1e25706ba8ff', 'e9b62d1b-095a-a94f-e6e1-cba6fab3bf29', '13c3637f-722a-fb04-b8dd-9e6f6a3c76e6', '2023-08-03 10:00:00.000000', '10a36cff-3aee-343a-eb96-21a8abcf9b0c', 0);
INSERT INTO `coursearrangs` VALUES ('afbb01e6-f4da-8c31-1c79-3f0c98a7da9a', 'ee9c9a4d-2c70-8025-646f-13ede5096372', '7b66fbda-40f9-8ad5-8a67-803c25300414', '36cf8496-e4d2-6657-5bd5-d8e3b496118d', '2023-08-01 14:00:00.000000', 'ad8e7eb0-3696-9785-87aa-27421c2a00ed', 0);
INSERT INTO `coursearrangs` VALUES ('b0837750-61ae-4750-9854-8fa3f94c11ab', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', '49b61dcd-e9f7-56e6-5a57-d2a24d85e583', 'c99e03f2-0941-81da-e224-e49c50ef5d69', '2023-08-04 10:00:00.000000', 'ef75b2ef-1a45-1f90-0c33-e84a61c69e4b', 0);
INSERT INTO `coursearrangs` VALUES ('b7497432-4921-4b09-9ab9-f9337ff7bd27', 'ee9c9a4d-2c70-8025-646f-13ede5096372', '743d0853-4883-b225-78ae-ef1199aaeb58', '77f505cc-7d23-9853-ca56-85db7f280b62', '2023-08-03 14:00:00.000000', '3f33ba46-1fe6-fe16-4798-a71217212d7b', 0);
INSERT INTO `coursearrangs` VALUES ('b9f2ff67-ac96-4049-9613-2dd054df3379', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', 'adbf3f28-1d91-2400-894e-9044eefecdc9', 'c99e03f2-0941-81da-e224-e49c50ef5d69', '2023-08-03 16:00:00.000000', '715b34a7-696f-3dbe-7200-e8fc5d12dd76', 0);
INSERT INTO `coursearrangs` VALUES ('bb8038de-331a-4e73-b9d0-ac090a762b1f', '497ad04c-f956-d2d8-e23c-1e25706ba8ff', 'e9b62d1b-095a-a94f-e6e1-cba6fab3bf29', '13c3637f-722a-fb04-b8dd-9e6f6a3c76e6', '2023-08-04 14:00:00.000000', '10a36cff-3aee-343a-eb96-21a8abcf9b0c', 0);
INSERT INTO `coursearrangs` VALUES ('bc22b2f6-f546-46aa-ba8e-16f8a3bd1437', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', 'e7badc0e-6468-90df-e8ee-c5343b4ad7af', 'e50fbf73-23f6-8596-4886-69f0023862cb', '2023-08-03 08:00:00.000000', '124d5147-b267-909a-7779-22c68190f764', 0);
INSERT INTO `coursearrangs` VALUES ('c0ece297-f873-4029-ab06-032117ccf403', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', '49b61dcd-e9f7-56e6-5a57-d2a24d85e583', 'c99e03f2-0941-81da-e224-e49c50ef5d69', '2023-08-05 10:00:00.000000', 'ef75b2ef-1a45-1f90-0c33-e84a61c69e4b', 0);
INSERT INTO `coursearrangs` VALUES ('c360b6ef-157e-4899-aa9d-84be4d7f6cae', '497ad04c-f956-d2d8-e23c-1e25706ba8ff', 'f1086fe1-f176-e541-1ad6-658d0543486c', '006b0ece-a585-6609-23f7-70f42454c060', '2023-08-04 10:00:00.000000', '661b9dad-96f3-888c-fffb-d7cfaa280641', 0);
INSERT INTO `coursearrangs` VALUES ('c4a921d7-00c9-4cd5-96e0-79e573f2622e', '30ccc950-8b50-7ada-7b08-2fda53f022fd', '57a30c9a-fd39-f761-2c27-1e65ba5a8312', '2d18c9b1-3192-3aa9-0a18-9becb94efa78', '2023-08-04 08:00:00.000000', '9b1660ea-8eec-ea26-a762-055f697d8b9f', 0);
INSERT INTO `coursearrangs` VALUES ('cc593df1-faba-47c8-984b-5f41589dc829', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', 'ad952135-bceb-77a5-632a-b5bc27f59838', '9e869700-f818-5f45-dcfa-64bb8ed73198', '2023-08-04 16:00:00.000000', '32f0b371-f681-95fd-79aa-b43907d01106', 0);
INSERT INTO `coursearrangs` VALUES ('ccd7099c-e1e7-4e15-8808-73e08facb580', 'ee9c9a4d-2c70-8025-646f-13ede5096372', '3c5ce332-9156-4abb-0cfc-d615c53e8da3', '918197c9-5cc9-8699-486d-f5a3b838d998', '2023-08-05 16:00:00.000000', 'aea41a1d-d2a4-32d4-603f-f5f0c1e0d3aa', 0);
INSERT INTO `coursearrangs` VALUES ('d695e385-f208-4750-b44b-e300864c532a', '30ccc950-8b50-7ada-7b08-2fda53f022fd', '865c36cd-8b08-a32c-ef9a-b83e086a246f', '9cf1ec8e-e25c-79ae-d3ff-15de01936750', '2023-08-05 16:00:00.000000', '9930a88f-8c4f-2240-3362-36a91287b357', 0);
INSERT INTO `coursearrangs` VALUES ('d7d7f592-1d9e-42e7-87b6-7368c0764c09', '497ad04c-f956-d2d8-e23c-1e25706ba8ff', 'f0d3138e-431b-fa73-1452-52efd912c7f7', '006b0ece-a585-6609-23f7-70f42454c060', '2023-08-04 16:00:00.000000', '291b03e7-0d63-009b-f6c4-def54196631d', 0);
INSERT INTO `coursearrangs` VALUES ('d7f1ea52-c43c-43de-110b-b719db73114e', 'ee9c9a4d-2c70-8025-646f-13ede5096372', '7b66fbda-40f9-8ad5-8a67-803c25300414', '36cf8496-e4d2-6657-5bd5-d8e3b496118d', '2023-08-03 14:00:00.000000', 'ad8e7eb0-3696-9785-87aa-27421c2a00ed', 0);
INSERT INTO `coursearrangs` VALUES ('ddd9c4ff-36d7-38fe-03be-d0b111b0ebbb', 'ee9c9a4d-2c70-8025-646f-13ede5096372', '7b66fbda-40f9-8ad5-8a67-803c25300414', '36cf8496-e4d2-6657-5bd5-d8e3b496118d', '2023-08-04 14:00:00.000000', 'ad8e7eb0-3696-9785-87aa-27421c2a00ed', 0);
INSERT INTO `coursearrangs` VALUES ('de2790cd-107f-4f23-a801-912697db2e47', '30ccc950-8b50-7ada-7b08-2fda53f022fd', '795614cd-573a-5e0b-67f0-a9f8548290e8', '59eec08d-66ff-e1e8-1ab7-090c28a775c7', '2023-08-04 08:00:00.000000', '617b5233-83d1-57a0-29c6-81f37b7889c5', 0);
INSERT INTO `coursearrangs` VALUES ('e700570d-b035-45cc-b330-f17ef46fcd87', 'ee9c9a4d-2c70-8025-646f-13ede5096372', '743d0853-4883-b225-78ae-ef1199aaeb58', '77f505cc-7d23-9853-ca56-85db7f280b62', '2023-08-04 10:00:00.000000', '3f33ba46-1fe6-fe16-4798-a71217212d7b', 0);
INSERT INTO `coursearrangs` VALUES ('ea284298-669c-47a8-894f-185f95fe5067', '30ccc950-8b50-7ada-7b08-2fda53f022fd', '37aa9628-103a-67b2-5ccf-174e6fdb966b', '27375864-0298-39a1-f6eb-7375b0fe4a80', '2023-08-04 08:00:00.000000', '781ad551-febf-7fb6-7092-ad9f3ff2c8cb', 0);
INSERT INTO `coursearrangs` VALUES ('ed4d3778-4516-4505-81ff-49cd08b9c055', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', 'e7badc0e-6468-90df-e8ee-c5343b4ad7af', 'e50fbf73-23f6-8596-4886-69f0023862cb', '2023-08-04 16:00:00.000000', '124d5147-b267-909a-7779-22c68190f764', 0);
INSERT INTO `coursearrangs` VALUES ('ee9b0291-bc1f-43a8-8d8a-7228a82d4448', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', 'adbf3f28-1d91-2400-894e-9044eefecdc9', 'c99e03f2-0941-81da-e224-e49c50ef5d69', '2023-08-04 08:00:00.000000', '715b34a7-696f-3dbe-7200-e8fc5d12dd76', 0);
INSERT INTO `coursearrangs` VALUES ('f33d9a6d-4a18-4d44-aa7d-cab523684ba7', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', 'a30bc4cc-0833-79aa-e072-a106edae1d62', 'e50fbf73-23f6-8596-4886-69f0023862cb', '2023-08-04 14:00:00.000000', 'ef75b2ef-1a45-1f90-0c33-e84a61c69e4b', 0);
INSERT INTO `coursearrangs` VALUES ('fa92abe0-351a-4e3a-a69e-bd0529ef4780', '30ccc950-8b50-7ada-7b08-2fda53f022fd', '57a30c9a-fd39-f761-2c27-1e65ba5a8312', '2d18c9b1-3192-3aa9-0a18-9becb94efa78', '2023-08-05 16:00:00.000000', '9b1660ea-8eec-ea26-a762-055f697d8b9f', 0);

-- ----------------------------
-- Table structure for coursefiles
-- ----------------------------
DROP TABLE IF EXISTS `coursefiles`;
CREATE TABLE `coursefiles`  (
  `ID` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `CourseTypeName` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `LevelID` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CourseHour` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `VideoPath` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `IsDeleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `IX_coursefiles_LevelID`(`LevelID` ASC) USING BTREE,
  CONSTRAINT `FK_coursefiles_Levels_LevelID` FOREIGN KEY (`LevelID`) REFERENCES `levels` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of coursefiles
-- ----------------------------
INSERT INTO `coursefiles` VALUES ('30ccc950-8b50-7ada-7b08-2fda53f022fd', 'ASPNETCoreMVC', '1628af2d-1bc3-5de4-3bfa-74a0bc98888b', '20', NULL, 0);
INSERT INTO `coursefiles` VALUES ('497ad04c-f956-d2d8-e23c-1e25706ba8ff', 'ASPNETCoreWebAPI', 'e5bbcd83-0cac-86c2-ffdc-0439fbe58f38', '20', NULL, 0);
INSERT INTO `coursefiles` VALUES ('e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6', 'Vue', '846244b0-d24c-7684-9ad2-24d2d1175933', '20', NULL, 0);
INSERT INTO `coursefiles` VALUES ('ee9c9a4d-2c70-8025-646f-13ede5096372', 'HTMLCSS', '1628af2d-1bc3-5de4-3bfa-74a0bc98888b', '20', NULL, 0);

-- ----------------------------
-- Table structure for endclasses
-- ----------------------------
DROP TABLE IF EXISTS `endclasses`;
CREATE TABLE `endclasses`  (
  `ID` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `IsEndClass` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ClassId` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  UNIQUE INDEX `IX_endClasses_ClassId`(`ClassId` ASC) USING BTREE,
  CONSTRAINT `FK_endClasses_classeas_ClassId` FOREIGN KEY (`ClassId`) REFERENCES `classeas` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of endclasses
-- ----------------------------
INSERT INTO `endclasses` VALUES ('0ad74b18-42ed-45cb-b99b-0114d61c0f20', 'L.七班', '2a32710e-a4fa-ea29-6263-815ab8bc3838');
INSERT INTO `endclasses` VALUES ('f4c0b1e8-e66a-4618-93f9-f4cfbb06cdb6', 'X.七班', '7b66fbda-40f9-8ad5-8a67-803c25300414');

-- ----------------------------
-- Table structure for knowledges
-- ----------------------------
DROP TABLE IF EXISTS `knowledges`;
CREATE TABLE `knowledges`  (
  `ID` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `kid` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `KLName` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `KLDescribe` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `CourseFileId` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `IX_knowledges_CourseFileId`(`CourseFileId` ASC) USING BTREE,
  CONSTRAINT `FK_knowledges_coursefiles_CourseFileId` FOREIGN KEY (`CourseFileId`) REFERENCES `coursefiles` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of knowledges
-- ----------------------------
INSERT INTO `knowledges` VALUES ('002bef03-e11f-1dc6-3e57-27f4f6e7fc10', 'H1', '课程介绍', 'HTML（超文本标记语言）是一种用于创建网页结构和内容的标记语言。通过使用HTML，您可以定义网页中的标题、段落、图像、链接等元素，并指定它们的布局和样式。', 'ee9c9a4d-2c70-8025-646f-13ede5096372');
INSERT INTO `knowledges` VALUES ('00458ce1-d5e9-21c1-5634-6e4c40948eb9', 'MVC4', '路由和 URL', '理解路由的概念和原理，配置路由规则，以及通过生成和解析 URL 来实现页面之间的跳转和导航。', '30ccc950-8b50-7ada-7b08-2fda53f022fd');
INSERT INTO `knowledges` VALUES ('082eaf06-ccfd-ab5e-fb6a-8f3a7c0796ec', 'Api2', '创建和配置 ASP.NET Core WebApi 项目', '使用 Visual Studio 或 .NET CLI 创建 ASP.NET Core WebApi 项目，学习如何配置项目的基本设置和依赖项', '497ad04c-f956-d2d8-e23c-1e25706ba8ff');
INSERT INTO `knowledges` VALUES ('09c847f7-5925-b0cb-6308-3ec2df8a6ec9', 'Api3', '控制器和路由', '创建 API 控制器并定义路由规则，实现 API 操作方法，处理请求和响应', '497ad04c-f956-d2d8-e23c-1e25706ba8ff');
INSERT INTO `knowledges` VALUES ('0af04885-3c41-405b-f505-3e846f1de802', 'Api4', '数据传输对象(DTO)和模型验证', '学习如何使用数据传输对象进行数据传输，实施模型验证来验证客户端请求的数据', '497ad04c-f956-d2d8-e23c-1e25706ba8ff');
INSERT INTO `knowledges` VALUES ('0be12f24-5453-fb29-92b6-b208c690857a', 'Api5', '数据库连接和 Entity Framework Core', '连接到数据库并使用 Entity Framework Core 进行数据操作，学习常用的查询和更新操作', '497ad04c-f956-d2d8-e23c-1e25706ba8ff');
INSERT INTO `knowledges` VALUES ('0e9839b4-9fa4-ed3e-7e2f-7758568cbac0', 'H7', '颜色、背景、文本样式的设置', '颜色、背景、文本样式的设置', 'ee9c9a4d-2c70-8025-646f-13ede5096372');
INSERT INTO `knowledges` VALUES ('12cfd61e-8643-c731-9e63-c8a68326a62c', 'H8', '盒模型及边距、填充的使用，浮动和定位的概念和应用', '盒模型及边距、填充的使用，浮动和定位的概念和应用', 'ee9c9a4d-2c70-8025-646f-13ede5096372');
INSERT INTO `knowledges` VALUES ('168a52d9-4e22-4610-29a4-c66b75730dc6', 'MVC5', '视图布局和部分视图', '创建共享视图布局以确保应用程序的一致性，创建和使用部分视图来实现视图的重用', '30ccc950-8b50-7ada-7b08-2fda53f022fd');
INSERT INTO `knowledges` VALUES ('19937e28-3247-7dfa-e445-cd7173602003', 'Api7', '身份验证和授权', '实施基于令牌和角色的身份验证\r\n学习如何授权用户对 API 的访问权限', '497ad04c-f956-d2d8-e23c-1e25706ba8ff');
INSERT INTO `knowledges` VALUES ('20071c8b-0532-7e4e-16ee-4fe9fde2fbef', 'Api15', '性能监控和日志记录', '监控 API 的性能指标和调用日志，学习如何使用性能监控工具和日志记录器', '497ad04c-f956-d2d8-e23c-1e25706ba8ff');
INSERT INTO `knowledges` VALUES ('2c53cb25-c664-1fe0-35ba-bc31012eac23', 'MVC7', '数据访问和 Entity Framework Core', '使用 Entity Framework Core 进行数据访问，创建数据库上下文和实体模型，实现数据的增删改查操作', '30ccc950-8b50-7ada-7b08-2fda53f022fd');
INSERT INTO `knowledges` VALUES ('2cdc1ee1-ed1b-fe2a-0ece-8c06a02d2dea', 'H13', '响应式设计', '移动设备的适应性和响应式设计原理，使用媒体查询创建响应式布局，图片和媒体资源的自适应处理', 'ee9c9a4d-2c70-8025-646f-13ede5096372');
INSERT INTO `knowledges` VALUES ('31c5522f-4979-cd5b-490b-d7682940e1e6', 'Api9', '异常处理和错误处理', '处理异常并返回统一的错误响应，学习如何记录和处理错误日志', '497ad04c-f956-d2d8-e23c-1e25706ba8ff');
INSERT INTO `knowledges` VALUES ('342fd0f6-bf68-ead3-e1fb-45d1d6ebf2d5', 'V11', 'Vue 生态系统和常用插件', '了解 Vue.js 的生态系统，掌握常用的 Vue 插件和工具，学习如何在项目中使用第三方插件和库', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6');
INSERT INTO `knowledges` VALUES ('3c40a307-6ae6-4851-adcb-2f7abc5c18fa', 'V12', 'Vue 组件库和样式框架', '探索常用的 Vue 组件库和样式框架，学习如何使用这些组件库和样式框架来提升开发效率', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6');
INSERT INTO `knowledges` VALUES ('3d34a5a1-417f-40f6-15bb-d3b427effa32', 'V13', 'Vue 单元测试和组件测试', '编写和运行 Vue 单元测试和组件测试，使用测试工具和框架进行自动化测试', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6');
INSERT INTO `knowledges` VALUES ('3f60e8af-7ef9-aef9-af40-b9ac4fa44e9b', 'MVC8', '认证和授权', '实现用户认证和授权，使用 ASP.NET Core Identity 管理用户和角色，应用基于角色的授权策略', '30ccc950-8b50-7ada-7b08-2fda53f022fd');
INSERT INTO `knowledges` VALUES ('40aa0481-0d3d-4db4-7fcb-db7ec86125db', 'Api6', 'RESTful API 设计和版本控制', '学习如何设计符合 RESTful 风格的 API，探索 API 版本控制的不同策略和实现方式', '497ad04c-f956-d2d8-e23c-1e25706ba8ff');
INSERT INTO `knowledges` VALUES ('4389a30e-b083-ed19-e9e2-5e489d0f0692', 'V16', 'Vue 移动应用开发和适配', '探索 Vue 移动应用开发的特点和工具，学习如何进行移动应用的适配和优化', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6');
INSERT INTO `knowledges` VALUES ('43963935-9773-3c7d-be2e-8f1a33e1a00f', 'MVC14', 'API 开发和 RESTful 架构', '学习如何使用 ASP.NET Core 构建 Web API，了解 RESTful 架构的概念和设计原则，并实践创建和测试 API。', '30ccc950-8b50-7ada-7b08-2fda53f022fd');
INSERT INTO `knowledges` VALUES ('44c24c69-3867-4efc-f949-6bb98d3b0556', 'H3', '学习 HTML 文档结构和语法', '常见的HTML文件结构以及基础语法', 'ee9c9a4d-2c70-8025-646f-13ede5096372');
INSERT INTO `knowledges` VALUES ('466846a8-fab9-e170-4054-9f86b7f62d79', 'MVC13', '跨平台开发和移动端支持', '学习如何进行跨平台开发，包括使用 Xamarin 开发移动应用程序，了解移动应用程序开发的基本原理', '30ccc950-8b50-7ada-7b08-2fda53f022fd');
INSERT INTO `knowledges` VALUES ('491b962f-c4f3-9fe5-3f42-91f7ababc053', 'H5', '超链接和图片的添加，表格的创建和样式设置', '超链接和图片的添加，表格的创建和样式设置', 'ee9c9a4d-2c70-8025-646f-13ede5096372');
INSERT INTO `knowledges` VALUES ('54e14bc4-602a-be82-7afb-6694628dcb5b', 'Api16', '安全性和防护攻击', '实施安全性措施以防止常见攻击，如 XSS 和 CSRF，学习如何保护 API 的敏感数据', '497ad04c-f956-d2d8-e23c-1e25706ba8ff');
INSERT INTO `knowledges` VALUES ('54e91bce-0e94-4e0f-0e7d-57990b9730eb', 'H9', 'HTML5 新特性和语义化', 'HTML5 标签的使用，表单增强功能，如输入验证和本地存储，媒体元素（音频、视频）的添加和控制', 'ee9c9a4d-2c70-8025-646f-13ede5096372');
INSERT INTO `knowledges` VALUES ('54ecddaf-5c6f-3cab-98c2-08c05581b3ff', 'MVC6', '模型绑定和操作结果', '理解模型绑定的原理和用法，学习不同类型的操作结果，如重定向、返回视图等，并处理这些操作结果。', '30ccc950-8b50-7ada-7b08-2fda53f022fd');
INSERT INTO `knowledges` VALUES ('5652b2e6-7943-cedb-920c-054b8aba8a34', 'Api10', '文件上传和下载', '实现文件上传和下载功能，学习如何处理大文件和断点续传', '497ad04c-f956-d2d8-e23c-1e25706ba8ff');
INSERT INTO `knowledges` VALUES ('57eb5b80-0c71-1b6b-f8f7-42ee625e745b', 'MVC3', '模型和数据验证', '学习创建模型类来定义应用程序中的数据结构，以及使用数据注解对模型进行数据验证和校验', '30ccc950-8b50-7ada-7b08-2fda53f022fd');
INSERT INTO `knowledges` VALUES ('5b03d53b-a4de-01b4-5fa8-ff3946241d4d', 'V17', 'Vue 3.0 新特性和迁移', '了解 Vue 3.0 的新特性和改进，学习如何迁移到 Vue 3.0，并使用新的语法和功能', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6');
INSERT INTO `knowledges` VALUES ('5bb68a79-940a-4cc2-6608-1695fffa63b8', 'Api20', '最佳实践和项目总结', '掌握 ASP.NET Core WebApi 的最佳实践和开发技巧，对整个项目进行总结和回顾', '497ad04c-f956-d2d8-e23c-1e25706ba8ff');
INSERT INTO `knowledges` VALUES ('5bfd7475-7ec3-c819-61e7-59cda0da6a2b', 'MVC2', '创建控制器、动作方法和视图', '学习创建控制器类和在其中编写动作方法来处理用户请求，以及创建视图并与控制器关联来展示数据', '30ccc950-8b50-7ada-7b08-2fda53f022fd');
INSERT INTO `knowledges` VALUES ('5d32a7b3-63c2-f086-6515-0f02ee30c47c', 'MVC16', '单元测试和集成测试', '学习如何编写和运行单元测试和集成测试，以确保应用程序的质量和稳定性。', '30ccc950-8b50-7ada-7b08-2fda53f022fd');
INSERT INTO `knowledges` VALUES ('67e5eece-10d6-2478-dfc0-ec0da8c23f28', 'V18', 'Vue 性能优化和调试技巧', '实施 Vue 应用程序的性能优化，掌握常用的调试技巧和工具', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6');
INSERT INTO `knowledges` VALUES ('6b46b5ce-e5e5-d3d9-cb1a-7337e485b62b', 'MVC11', '使用 ASP.NET Core Identity 管理用户和角色', '学习如何使用 ASP.NET Core Identity 进行用户身份验证和角色管理，包括用户注册、登录、注销以及角色授权等功能。', '30ccc950-8b50-7ada-7b08-2fda53f022fd');
INSERT INTO `knowledges` VALUES ('6c2a3db9-977f-d73a-aed0-a1802b22137c', 'V19', 'Vue 项目部署和发布', '学习如何将 Vue 项目部署到生产环境，配置服务器和配置发布流程', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6');
INSERT INTO `knowledges` VALUES ('6d908443-69ee-33fd-1668-28ed90070863', 'Api12', '数据过滤和排序', '实现数据过滤和排序功能，掌握常用的过滤器和排序器', '497ad04c-f956-d2d8-e23c-1e25706ba8ff');
INSERT INTO `knowledges` VALUES ('6dc2f4a4-d81a-b790-d1aa-a85d7ece291a', 'H18', '调试和性能优化(二)', '网站性能分析与优化工具的使用', 'ee9c9a4d-2c70-8025-646f-13ede5096372');
INSERT INTO `knowledges` VALUES ('72426175-36e9-044c-6115-a7d7904e90e6', 'V20', 'Vue 最佳实践和项目总结', '掌握 Vue 最佳实践和开发技巧，对整个 Vue 项目进行总结和回顾', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6');
INSERT INTO `knowledges` VALUES ('76bfbb4d-e76a-0d0a-4dd1-7632cce7fd54', 'Api11', '响应格式和内容协商', '学习如何根据客户端的需求返回不同格式的响应，探索内容协商和内容协商器的使用', '497ad04c-f956-d2d8-e23c-1e25706ba8ff');
INSERT INTO `knowledges` VALUES ('778e9f6f-e8ae-322b-b93b-8970ae3e20a8', 'H15', 'CSS 预处理器(一)', 'Sass 或 Less 的基本语法和使用方法  ', 'ee9c9a4d-2c70-8025-646f-13ede5096372');
INSERT INTO `knowledges` VALUES ('78e7b869-8531-d9ad-ba79-cc0582656c17', 'Api1', 'ASP.NET Core WebApi 简介和基本概念', '介绍 ASP.NET Core WebApi 的概念和特点，学习 ASP.NET Core WebApi 的基本架构和工作原理', '497ad04c-f956-d2d8-e23c-1e25706ba8ff');
INSERT INTO `knowledges` VALUES ('7d7474d4-116d-5829-148c-9996f7702009', 'H20', '复习和项目实践(二)', '针对之前所学知识进行总结以及实践使用', 'ee9c9a4d-2c70-8025-646f-13ede5096372');
INSERT INTO `knowledges` VALUES ('811b7952-2135-ab30-4b8a-323c72e5e2d7', 'Api8', '数据缓存和性能优化', '使用缓存机制提高 API 的性能，学习如何有效地利用内存和分布式缓存', '497ad04c-f956-d2d8-e23c-1e25706ba8ff');
INSERT INTO `knowledges` VALUES ('84462a66-9fd9-c1c4-efee-f5bc5824b0d0', 'MVC10', '日志和错误处理', '配置和使用日志记录，处理异常和错误页面，以及监控和调试应用程序。', '30ccc950-8b50-7ada-7b08-2fda53f022fd');
INSERT INTO `knowledges` VALUES ('85ba1c03-5cb7-279c-ea5e-1ef83b7db77a', 'MVC15', '前端开发和 JavaScript 交互', '掌握在 ASP.NET Core MVC 中进行前端开发的基础知识，学习在前端与后端之间进行数据交互和通信的方法。', '30ccc950-8b50-7ada-7b08-2fda53f022fd');
INSERT INTO `knowledges` VALUES ('899184ba-939e-a41f-09b1-b482b2d239dc', 'V2', 'Vue 实例和数据绑定', '创建 Vue 实例并进行数据绑定，学习如何绑定数据到 HTML 元素和 Vue 实例', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6');
INSERT INTO `knowledges` VALUES ('9323d3ce-0273-1494-e69c-30556d6f643c', 'V3', '条件和循环渲染', '使用条件指令实现根据条件显示或隐藏元素，使用循环指令渲染列表数据', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6');
INSERT INTO `knowledges` VALUES ('93a92c98-a0d8-7f4f-31f1-d8c41d626654', 'H14', 'Web 字体和图标', '自定义字体的引入和使用，第三方字体库的应用，矢量图标库的使用', 'ee9c9a4d-2c70-8025-646f-13ede5096372');
INSERT INTO `knowledges` VALUES ('965e945f-5f9f-5013-08ba-f6d2f6adb3d9', 'H19', '复习和项目实践(一)', '针对之前所学知识进行总结以及实践使用', 'ee9c9a4d-2c70-8025-646f-13ede5096372');
INSERT INTO `knowledges` VALUES ('97c3c550-f6b5-42e6-507b-73e52f44fceb', 'V8', 'HTTP 请求和数据交互', '使用 Vue Resource 或 Axios 发起 HTTP 请求，学习如何处理异步请求和更新数据', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6');
INSERT INTO `knowledges` VALUES ('9afe8a9c-d068-5ffd-0561-fb8d3e88d8b5', 'V4', '计算属性和监听器', '使用计算属性进行复杂的数据计算，学习如何监听数据的变化并触发相应的操作', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6');
INSERT INTO `knowledges` VALUES ('9b5565e1-b40f-9ceb-b7bc-a34890a25d6f', 'MVC9', '中间件和管道', '理解 ASP.NET Core 请求处理管道的工作方式，学习使用中间件处理请求和响应，并自定义和配置中间件。', '30ccc950-8b50-7ada-7b08-2fda53f022fd');
INSERT INTO `knowledges` VALUES ('9ed05e9b-2a49-4d92-3976-1f8de9d7cb55', 'V5', '表单输入绑定和表单验证', '绑定表单输入元素到 Vue 实例的数据，学习如何进行表单验证和处理用户输入', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6');
INSERT INTO `knowledges` VALUES ('a04e27b8-9a43-30e2-9d42-76deafab884e', 'MVC12', '性能优化和缓存', '探索如何优化 ASP.NET Core 应用程序的性能，包括数据缓存、输出缓存、页面优化等技术手段', '30ccc950-8b50-7ada-7b08-2fda53f022fd');
INSERT INTO `knowledges` VALUES ('a2e749e2-7bea-19c3-cf4b-4cc9fa68add9', 'V6', '组件化开发和组件通信', '学习如何创建和使用 Vue 组件，实现组件之间的通信和共享数据', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6');
INSERT INTO `knowledges` VALUES ('a4d47318-7c2d-fb1c-5060-72318fee32ec', 'H17', '调试和性能优化(一)', '常见的调试工具和技巧，代码优化和性能提升的策略', 'ee9c9a4d-2c70-8025-646f-13ede5096372');
INSERT INTO `knowledges` VALUES ('a7fb78e8-014f-e4b1-c06b-054f501b19a8', 'MVC17', '部署和发布应用程序', '了解如何将 ASP.NET Core 应用程序部署到生产环境中，包括配置服务器、发布应用程序和管理应用程序的生命周期', '30ccc950-8b50-7ada-7b08-2fda53f022fd');
INSERT INTO `knowledges` VALUES ('ab1e7fb7-7a70-f5e3-2b7f-5d72776c6ad1', 'MVC18', '微服务架构和容器化部署', '介绍微服务架构的概念和原理，并学习如何使用容器技术（如 Docker）将应用程序进行容器化部署', '30ccc950-8b50-7ada-7b08-2fda53f022fd');
INSERT INTO `knowledges` VALUES ('b0d321a6-18c9-4c8b-e7dd-f2b5b69dcdcc', 'MVC20', '项目实战或复习总结', '进行一个实际的 ASP.NET Core MVC 项目实践，或者对之前所学内容进行复习总结，强化已掌握的知识', '30ccc950-8b50-7ada-7b08-2fda53f022fd');
INSERT INTO `knowledges` VALUES ('b1b1900d-bfb3-c64f-dbfb-4e0cb55c6d3b', 'MVC1', '介绍 ASP.NET Core MVC 框架、安装和配置开发环境', '介绍 ASP.NET Core MVC 框架的基本概念和原理，包括 MVC 架构模式，以及如何安装和配置开发环境。', '30ccc950-8b50-7ada-7b08-2fda53f022fd');
INSERT INTO `knowledges` VALUES ('b2bc634e-fcb1-9d58-1ca7-625c0027d1fb', 'H16', 'CSS 预处理器(二)', '变量、混合、嵌套的应用,编译预处理器代码为 CSS  ', 'ee9c9a4d-2c70-8025-646f-13ede5096372');
INSERT INTO `knowledges` VALUES ('b72c5f89-67a3-dd8b-0655-a9d241f92275', 'H12', 'CSS3 动画和过渡', '使用 CSS3 实现过渡效果，利用关键帧实现动画效果，变形和过渡属性的运用', 'ee9c9a4d-2c70-8025-646f-13ede5096372');
INSERT INTO `knowledges` VALUES ('b77c083f-e4f4-ace2-6b08-89ee7bce535e', 'H11', '响应式布局和媒体查询', '响应式布局和媒体查询', 'ee9c9a4d-2c70-8025-646f-13ede5096372');
INSERT INTO `knowledges` VALUES ('ba1781b7-d235-01ca-bc9f-5d7cdb1edc40', 'V9', 'Vuex 状态管理', '学习如何使用 Vuex 进行状态管理，实现全局共享的状态和数据管理', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6');
INSERT INTO `knowledges` VALUES ('bc4bc7a8-ce0c-3d5e-4946-e729bc948acd', 'H10', '模型布局：流动布局和定位布局的区别和应用', '模型布局：流动布局和定位布局的区别和应用', 'ee9c9a4d-2c70-8025-646f-13ede5096372');
INSERT INTO `knowledges` VALUES ('befb7738-b783-1d07-ddfb-ee881349b63c', 'V10', 'Vue CLI 和项目构建', '使用 Vue CLI 初始化和构建项目，学习如何管理项目的依赖和打包发布', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6');
INSERT INTO `knowledges` VALUES ('c97b3467-4415-d9a6-e281-ab216abe1bd4', 'Api13', '分页和数据压缩', '学习如何进行分页查询和显示，实施数据压缩以提高网络传输效率', '497ad04c-f956-d2d8-e23c-1e25706ba8ff');
INSERT INTO `knowledges` VALUES ('ce3fe339-6969-3362-452e-4d06c1d0b36b', 'Api14', 'API 文档和测试', '自动生成 API 文档并提供在线测试工具，学习如何使用 Swagger 和 Postman 进行 API 文档和测试', '497ad04c-f956-d2d8-e23c-1e25706ba8ff');
INSERT INTO `knowledges` VALUES ('ceb8aaa5-8789-ffe9-4ab3-f633e1eb9c63', 'V7', 'Vue 路由和导航', '使用 Vue Router 实现前端路由和导航，学习如何配置路由和实现页面跳转', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6');
INSERT INTO `knowledges` VALUES ('d30df903-9a00-d6eb-5860-81465d3626b6', 'V14', 'Vue SSR 和服务端渲染', '了解 Vue SSR 的概念和原理，学习如何使用 Vue.js 进行服务端渲染', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6');
INSERT INTO `knowledges` VALUES ('d71bd89f-8c43-2a33-192c-fb0e780ffe02', 'H6', 'CSS 的基本语法和选择器', '讲解CSS的基本语法组成：选择器和声明块。以及简单的使用选择器', 'ee9c9a4d-2c70-8025-646f-13ede5096372');
INSERT INTO `knowledges` VALUES ('de5f1076-5fd9-c8f9-4d9d-405e2466fdab', 'H4', '标题、段落、列表等常用标签的使用', '简单的使用标题、段落、列表等常用标签', 'ee9c9a4d-2c70-8025-646f-13ede5096372');
INSERT INTO `knowledges` VALUES ('e0740b7e-01fe-70b4-ec13-44f6d0bc5193', 'H2', '基础知识', 'HTML的基础知识包括以下内容：HTML文档结构，HTML标签，元素和内容，标签属性，常用HTML标签，嵌套和层次结构，注释', 'ee9c9a4d-2c70-8025-646f-13ede5096372');
INSERT INTO `knowledges` VALUES ('e737136a-d99d-76bb-4de7-f56c032fc811', 'Api17', 'API 测试和集成测试', '编写单元测试和集成测试来验证 API 的功能和性能，使用测试工具和框架进行自动化测试', '497ad04c-f956-d2d8-e23c-1e25706ba8ff');
INSERT INTO `knowledges` VALUES ('e7cfd022-d41b-d47c-5eb1-a21358d1abb2', 'Api18', '缩放和负载均衡', '学习如何通过水平缩放和负载均衡来提高 API 的性能和可用性，探索不同的负载均衡策略和配置', '497ad04c-f956-d2d8-e23c-1e25706ba8ff');
INSERT INTO `knowledges` VALUES ('ea91333c-25fd-af2b-8d21-bc3918b7d840', 'Api19', '部署和发布', '将 ASP.NET Core WebApi 项目部署到服务器或云端，学习如何管理应用程序的配置和环境变量', '497ad04c-f956-d2d8-e23c-1e25706ba8ff');
INSERT INTO `knowledges` VALUES ('eb711ac4-31e9-ff50-ba04-9cce85726f26', 'V15', 'Vue 动画和过渡效果', '学习如何使用 Vue.js 实现动画效果和过渡效果，掌握常用的动画 API 和过渡指令', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6');
INSERT INTO `knowledges` VALUES ('f02fe858-17d1-fa2b-4df7-119da0a85750', 'MVC19', '安全性和漏洞防范', '探索网站和应用程序的安全性问题，并学习如何预防和处理常见的安全漏洞和攻击。', '30ccc950-8b50-7ada-7b08-2fda53f022fd');
INSERT INTO `knowledges` VALUES ('f94d1c86-66d3-ad06-77b3-bf081c8571a3', 'V1', 'Vue.js 简介和基本概念', '介绍 Vue.js 的概念和特点，学习 Vue.js 的基本语法和模板语法', 'e126e9f5-53a3-1ce1-7db2-2faf97e8d7e6');

-- ----------------------------
-- Table structure for levels
-- ----------------------------
DROP TABLE IF EXISTS `levels`;
CREATE TABLE `levels`  (
  `ID` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `LevelName` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of levels
-- ----------------------------
INSERT INTO `levels` VALUES ('1628af2d-1bc3-5de4-3bfa-74a0bc98888b', '初级');
INSERT INTO `levels` VALUES ('846244b0-d24c-7684-9ad2-24d2d1175933', '中级');
INSERT INTO `levels` VALUES ('e5bbcd83-0cac-86c2-ffdc-0439fbe58f38', '高级');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `Id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Name` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `NormalizedName` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `ConcurrencyStamp` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('2944e863-5358-aeeb-e341-f8101291bb0d', 'normal', 'NORMAL', '');
INSERT INTO `role` VALUES ('344e3b7e-fbde-bb12-053b-6979595be1fc', 'student', 'STUDENT', '');
INSERT INTO `role` VALUES ('4f25ea99-7117-217e-10f4-92c75dd841d6', 'teacher', 'TEACHER', '');
INSERT INTO `role` VALUES ('7256c38c-768d-63e4-9b8f-12cd54fd43d1', 'manager', 'MANAGER', '');

-- ----------------------------
-- Table structure for students
-- ----------------------------
DROP TABLE IF EXISTS `students`;
CREATE TABLE `students`  (
  `ID` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `UseId` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ClassNo` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `EnrollTime` datetime(6) NULL DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `IX_students_ClassNo`(`ClassNo` ASC) USING BTREE,
  INDEX `IX_students_UseId`(`UseId` ASC) USING BTREE,
  CONSTRAINT `FK_students_classeas_ClassNo` FOREIGN KEY (`ClassNo`) REFERENCES `classeas` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_students_User_UseId` FOREIGN KEY (`UseId`) REFERENCES `user` (`Id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of students
-- ----------------------------
INSERT INTO `students` VALUES ('04456979-be97-f5f8-b303-e913bcf3eb6c', '643318c9-31cb-0821-8fb0-ff9b1e8b140d', '2a32710e-a4fa-ea29-6263-815ab8bc3838', '2023-01-16 03:26:54.000000', 0);
INSERT INTO `students` VALUES ('081bede7-b996-9f85-7b83-c7e94273d3c5', '64498954-a8cf-bcf0-95db-a9386af8e5f6', 'f0d3138e-431b-fa73-1452-52efd912c7f7', '2023-01-05 17:47:52.000000', 0);
INSERT INTO `students` VALUES ('14441cb4-d452-f285-f414-ea2689fd9e05', '6500523e-baa3-7522-d27c-19f5982d395f', '07fb639c-c66d-50de-71f1-8ba0a0ba8833', '2023-01-02 06:43:17.000000', 0);
INSERT INTO `students` VALUES ('1d64f937-735f-d521-3ac3-a04b5b4cdeef', '654caf20-5b95-5dc0-2940-1d087740c0de', 'e9b62d1b-095a-a94f-e6e1-cba6fab3bf29', '2023-01-09 04:26:39.000000', 0);
INSERT INTO `students` VALUES ('201b822d-2ba8-f627-1e2c-5e7080ea2eea', '661c50d7-fae2-8c7c-c0d9-38acbc861ca7', 'bf73403d-7ebb-5a57-05f9-2ddb3063ef55', '2023-01-27 08:11:45.000000', 0);
INSERT INTO `students` VALUES ('2338fff5-dc62-3b8c-3eac-6d49f0f21ac4', '68066327-fc81-67a7-e947-39b19d1ef474', 'f0d3138e-431b-fa73-1452-52efd912c7f7', '2023-01-02 16:57:21.000000', 0);
INSERT INTO `students` VALUES ('2eb323dc-97c1-d75f-90cc-d18a9b0c5efa', '691094d9-9889-6197-82c6-5fb120570f89', 'e9b62d1b-095a-a94f-e6e1-cba6fab3bf29', '2023-01-08 04:37:23.000000', 0);
INSERT INTO `students` VALUES ('31111f5e-20b9-5887-46ff-b166bceab80e', '6a5ad723-7fcb-f09f-af4b-09a67c17c0e1', 'bf73403d-7ebb-5a57-05f9-2ddb3063ef55', '2023-01-14 03:11:43.000000', 0);
INSERT INTO `students` VALUES ('33838068-392a-d4d3-c431-88cf4ae59e3c', '6a95de76-57c8-392c-6d14-016625560d2b', '2a32710e-a4fa-ea29-6263-815ab8bc3838', '2023-01-18 08:35:32.000000', 0);
INSERT INTO `students` VALUES ('33cb064b-7f6a-3ee6-81c8-9fd1bd93374d', '6b0c8af4-ae60-b591-bc59-5a2687231174', 'f0d3138e-431b-fa73-1452-52efd912c7f7', '2023-01-14 20:52:50.000000', 0);
INSERT INTO `students` VALUES ('349d40b9-5d6a-94e3-62b3-f8eaad0def70', '6c6bdda4-ed80-8b29-1b66-05c15a1e00dd', '2a32710e-a4fa-ea29-6263-815ab8bc3838', '2023-01-19 02:22:06.000000', 0);
INSERT INTO `students` VALUES ('349f061e-11dd-bbff-0e5a-1f8289355205', '6c875f77-70be-37e8-a474-ff15b040086a', 'f0d3138e-431b-fa73-1452-52efd912c7f7', '2023-01-29 22:57:35.000000', 0);
INSERT INTO `students` VALUES ('34c61858-c21d-d63a-e9c9-829a1e5e7e38', '6dadc7ed-33f1-9173-4a84-d9716ad8ee4b', '743d0853-4883-b225-78ae-ef1199aaeb58', '2023-01-20 12:44:17.000000', 0);
INSERT INTO `students` VALUES ('35f98775-c3f4-df5d-28f4-6f93df2400e5', '6e4e8838-2a8e-9c92-7554-72cec56e2f4b', '37aa9628-103a-67b2-5ccf-174e6fdb966b', '2023-01-02 13:50:12.000000', 0);
INSERT INTO `students` VALUES ('370327be-038a-3f20-7a42-86f15f8e45af', '6e926198-0907-8839-9fd1-95b64bd6fee2', '37aa9628-103a-67b2-5ccf-174e6fdb966b', '2023-01-14 06:56:30.000000', 0);
INSERT INTO `students` VALUES ('38b5ab30-b2ed-0320-f9ad-f0be56bbf0ea', '6eaf67b7-6e71-1248-478b-d6328081dfc9', 'a30bc4cc-0833-79aa-e072-a106edae1d62', '2023-01-14 10:17:53.000000', 0);
INSERT INTO `students` VALUES ('396eb971-3f64-431d-7d4d-8074b2a3463b', '6f4ce45e-e04e-fc9b-6889-ed29625bb531', '49b61dcd-e9f7-56e6-5a57-d2a24d85e583', '2023-01-11 17:41:37.000000', 0);
INSERT INTO `students` VALUES ('3983c048-b388-87b0-43dc-b81d97588a57', '70bc9033-50b6-3d90-cdd0-337aee28084e', '07fb639c-c66d-50de-71f1-8ba0a0ba8833', '2023-01-05 11:43:12.000000', 0);
INSERT INTO `students` VALUES ('3a62df66-4ebf-a3f8-d93b-6e628034556a', '720de2b3-663c-8d05-dd7a-f16d26edfbae', 'bf73403d-7ebb-5a57-05f9-2ddb3063ef55', '2023-01-09 14:04:49.000000', 0);
INSERT INTO `students` VALUES ('41df7619-bd36-5840-0745-1bc80088627b', '780a7d23-ad29-6ae7-3952-d6e295ea89b5', 'ad952135-bceb-77a5-632a-b5bc27f59838', '2023-01-05 16:34:03.000000', 0);
INSERT INTO `students` VALUES ('41f93419-f874-6e5c-781d-485ea592136d', '782ebe6d-2fdf-e26d-ad65-d533b14a53fd', '49b61dcd-e9f7-56e6-5a57-d2a24d85e583', '2023-01-20 00:39:26.000000', 0);
INSERT INTO `students` VALUES ('425cb856-82cd-2377-489f-c2e508e01275', '7870aa88-69e8-325d-d4e9-ba50369ac675', '7b66fbda-40f9-8ad5-8a67-803c25300414', '2023-01-05 01:26:54.000000', 0);
INSERT INTO `students` VALUES ('42997bdb-89e1-f8df-229c-fac434869ce1', '79167cf2-38c5-ef41-95b9-00bb4e4d9b42', 'bf73403d-7ebb-5a57-05f9-2ddb3063ef55', '2023-01-04 00:16:55.000000', 0);
INSERT INTO `students` VALUES ('4473fe48-429d-9c8b-34f9-9dede6586616', '79893f5c-1468-9ff2-e0b2-f98e691d6ad2', 'f1086fe1-f176-e541-1ad6-658d0543486c', '2023-01-02 06:14:56.000000', 0);
INSERT INTO `students` VALUES ('4493468a-7210-2d20-ad1f-94cc0f7c2e1e', '79ab3f16-3d66-1ab9-7059-627e242b59dd', '865c36cd-8b08-a32c-ef9a-b83e086a246f', '2023-01-24 23:33:49.000000', 0);
INSERT INTO `students` VALUES ('44ebf4fa-8a4b-307f-0dda-29b34f6dd49d', '79d81ab4-52a5-bb5c-9f8a-ac98ac4803e5', 'bf73403d-7ebb-5a57-05f9-2ddb3063ef55', '2023-01-16 01:17:59.000000', 0);
INSERT INTO `students` VALUES ('459c243d-ca4f-e881-64fe-061a92081ffa', '7a1687b7-d485-c99e-013f-835545c06227', 'bf73403d-7ebb-5a57-05f9-2ddb3063ef55', '2023-01-14 05:49:01.000000', 0);
INSERT INTO `students` VALUES ('45ba4806-dcbb-7fbc-0a69-a2883d316701', '7b0571a8-6271-a067-8ac0-d64743386096', '360de5a6-1717-bdbe-6bad-5279e2af03af', '2023-01-05 21:12:51.000000', 0);
INSERT INTO `students` VALUES ('4a9b875c-65ec-7766-b4ab-98ccc9c3b95e', '7b3bb4f6-9246-5db1-7501-23c748548bf4', 'ad952135-bceb-77a5-632a-b5bc27f59838', '2023-02-01 13:58:21.000000', 0);
INSERT INTO `students` VALUES ('4b197129-3434-c544-86d9-cb060748a04d', '7b7bdcdb-8016-6524-232a-21c20248b789', 'f0d3138e-431b-fa73-1452-52efd912c7f7', '2023-01-07 11:19:21.000000', 0);
INSERT INTO `students` VALUES ('4c22c452-8b7c-9af1-8ebb-1132333055ca', '7b7cff58-56b8-986e-0221-072daac7628a', 'a30bc4cc-0833-79aa-e072-a106edae1d62', '2023-01-16 06:06:28.000000', 0);
INSERT INTO `students` VALUES ('4c503a6a-45cd-0eef-4912-8e3a746dc821', '7b882394-fa1e-2e81-3c2f-b1ee72fb2679', 'adbf3f28-1d91-2400-894e-9044eefecdc9', '2023-01-15 03:51:51.000000', 0);
INSERT INTO `students` VALUES ('4fb75627-1d91-a072-5794-fe627f124fd7', '7be65f48-80ba-4e0d-abe5-41e2ced143c8', '743d0853-4883-b225-78ae-ef1199aaeb58', '2023-01-21 14:38:38.000000', 0);
INSERT INTO `students` VALUES ('51097e8f-e35e-cba3-27fc-6686c6e7730f', '7d96f0cd-4d73-7bf2-baee-7f1483d6fb11', 'bf73403d-7ebb-5a57-05f9-2ddb3063ef55', '2023-01-15 13:56:29.000000', 0);
INSERT INTO `students` VALUES ('53f20665-4cd3-5f3c-685f-7451111adeb3', '7e6cb620-c698-f85d-5152-8874b2672080', '795614cd-573a-5e0b-67f0-a9f8548290e8', '2023-01-28 17:20:58.000000', 0);
INSERT INTO `students` VALUES ('543f8254-2650-21da-89a9-4f89e71c9dc4', '7ec47520-79cb-8b96-81e5-43ebc02a4aae', '3c5ce332-9156-4abb-0cfc-d615c53e8da3', '2023-01-17 08:25:00.000000', 0);
INSERT INTO `students` VALUES ('54a56310-c018-5de3-edc3-fa1860c92c0f', '819a0f43-4936-7b84-35e8-37fa84a8f5b2', '795614cd-573a-5e0b-67f0-a9f8548290e8', '2023-01-12 07:10:59.000000', 0);
INSERT INTO `students` VALUES ('54b70c27-a731-cac8-713e-cdf4d3527432', '81b9e8b7-1db6-b216-d7e4-4f83eae386d7', '865c36cd-8b08-a32c-ef9a-b83e086a246f', '2023-01-06 03:45:00.000000', 0);
INSERT INTO `students` VALUES ('5756f99d-9fce-4b56-50eb-fd181104f509', '81f41193-5411-1a9b-2163-d5d2b750f976', '8c6b7ff4-60e8-37a6-53d9-16b09d0aed82', '2023-01-02 12:12:23.000000', 0);
INSERT INTO `students` VALUES ('57b15cca-f820-7ed5-1d04-efc961d764f1', '823e7e03-5d41-4307-c7ec-03f00195d435', 'ad952135-bceb-77a5-632a-b5bc27f59838', '2023-01-19 09:12:01.000000', 0);
INSERT INTO `students` VALUES ('590c810d-a10f-97b0-95b1-a49bf966dce8', '8471cc46-388c-2f6e-afb2-b30eedc2b006', '37aa9628-103a-67b2-5ccf-174e6fdb966b', '2023-01-18 03:55:33.000000', 0);
INSERT INTO `students` VALUES ('593257a0-7819-7142-cf0b-723df204e141', '8475cc74-6013-b3cc-3029-c00203f1fe18', '3c5ce332-9156-4abb-0cfc-d615c53e8da3', '2023-01-05 15:52:21.000000', 0);
INSERT INTO `students` VALUES ('5b9f3640-8e90-c9c7-00ff-31743828a7bc', '84cbbbde-e67f-1e6a-12ac-7a917337783e', '865c36cd-8b08-a32c-ef9a-b83e086a246f', '2023-01-03 12:12:10.000000', 0);
INSERT INTO `students` VALUES ('5c84829b-db89-ed56-f5dc-2ad06e0d7ede', '86dd22cc-a931-5d7a-2563-d30c245e16ea', '795614cd-573a-5e0b-67f0-a9f8548290e8', '2023-01-02 11:32:30.000000', 0);
INSERT INTO `students` VALUES ('5d82409d-0b1c-04eb-3de1-04e2f973de4d', '87da5976-25e4-13aa-8c97-58c5f8b20c65', 'b26068a0-4ace-5cde-a36c-795990f74006', '2023-01-28 03:26:35.000000', 0);
INSERT INTO `students` VALUES ('5d838ac9-75d5-7894-ab14-33e8c609ab77', '883c3508-d483-2300-fd2f-a009b6347dbb', 'f1086fe1-f176-e541-1ad6-658d0543486c', '2023-01-26 11:39:04.000000', 0);
INSERT INTO `students` VALUES ('5d91f24c-7cbb-53a6-5da5-2080ada1bea8', '89672054-0010-8233-927e-32bb9c69e85f', '49b61dcd-e9f7-56e6-5a57-d2a24d85e583', '2023-01-08 12:22:25.000000', 0);
INSERT INTO `students` VALUES ('5d964528-b778-d501-890f-4b731e547679', '8a1ba186-c68a-9b7e-73c4-bc7abe623f60', 'e9b62d1b-095a-a94f-e6e1-cba6fab3bf29', '2023-01-12 17:16:20.000000', 0);
INSERT INTO `students` VALUES ('5e1c13cd-79ac-7127-55e1-ccd92a6b6bd8', '8ba46336-cf78-ed45-576e-87a2cd6d1e39', '49b61dcd-e9f7-56e6-5a57-d2a24d85e583', '2023-01-16 09:11:23.000000', 0);
INSERT INTO `students` VALUES ('60a194a8-c3e0-0c66-5eac-5fdd740f0eb1', '8c4184e0-56b4-3679-ee69-001531640aaf', '360de5a6-1717-bdbe-6bad-5279e2af03af', '2023-01-11 23:28:26.000000', 0);
INSERT INTO `students` VALUES ('62194670-0cdc-0459-7476-606f845d93a5', '8eb32118-2a5b-1964-6210-1c92b17694e0', '795614cd-573a-5e0b-67f0-a9f8548290e8', '2023-01-10 00:54:35.000000', 0);
INSERT INTO `students` VALUES ('62316016-e555-86b4-7527-7d0ad3ee3d12', '8ebe595c-130c-51f2-db49-4fe2233bbdef', '865c36cd-8b08-a32c-ef9a-b83e086a246f', '2023-01-09 04:30:34.000000', 0);
INSERT INTO `students` VALUES ('624e4ba6-8d6e-6901-7f51-3b6c0c8d6a32', '8f241ca4-07cc-588c-b9ed-d04dc191d7c2', '49b61dcd-e9f7-56e6-5a57-d2a24d85e583', '2023-01-22 20:06:22.000000', 0);
INSERT INTO `students` VALUES ('635f56d4-4474-48bc-51c6-85ee3e68deb2', '9019c28a-ad1f-9907-acec-96b17d323faf', '57a30c9a-fd39-f761-2c27-1e65ba5a8312', '2023-01-07 00:11:47.000000', 0);
INSERT INTO `students` VALUES ('63e357ab-b8cd-7bc6-84ab-cda026c7fd67', '914bf30c-0524-e6e3-5530-560d7fbfe0a8', 'bf73403d-7ebb-5a57-05f9-2ddb3063ef55', '2023-01-03 05:31:15.000000', 0);
INSERT INTO `students` VALUES ('66353313-90e3-99c7-5534-6a843a1c8451', '914e033a-9255-22a8-0d05-6aa0621eb92a', 'f1086fe1-f176-e541-1ad6-658d0543486c', '2023-01-17 02:22:58.000000', 0);
INSERT INTO `students` VALUES ('66da4e33-c3bb-03d1-8ceb-a55844cb01db', '91f06e03-0565-c5b9-3331-2a4f19c195ca', 'ad952135-bceb-77a5-632a-b5bc27f59838', '2023-01-28 02:29:44.000000', 0);
INSERT INTO `students` VALUES ('67fc1836-b06d-93f4-e471-1b66d469f8c6', '92354232-ea1f-fe57-6f95-3d1bfd50d846', '57a30c9a-fd39-f761-2c27-1e65ba5a8312', '2023-01-18 16:55:39.000000', 0);
INSERT INTO `students` VALUES ('68d74549-5825-054f-05b6-921050c18a93', '92c6f0be-ab80-78db-2926-608d44ecdb3d', 'f1086fe1-f176-e541-1ad6-658d0543486c', '2023-01-08 23:52:56.000000', 0);
INSERT INTO `students` VALUES ('692cd110-fd16-7f28-e71b-2ceb2c2dcefe', '92f72ee4-ed14-0ee1-2d06-690fd673afb1', '360de5a6-1717-bdbe-6bad-5279e2af03af', '2023-01-03 05:55:50.000000', 0);
INSERT INTO `students` VALUES ('6c7bda9b-1f71-748b-6b93-7d010c07b851', '93883b55-66a5-09a1-d131-e19f3e19949e', '07fb639c-c66d-50de-71f1-8ba0a0ba8833', '2023-01-28 12:54:07.000000', 0);
INSERT INTO `students` VALUES ('6d17c540-e1b8-6697-d718-8a0aaf6d25fc', '93ae29fa-4cfe-3cbb-36e2-5f70fc0c0865', '2a32710e-a4fa-ea29-6263-815ab8bc3838', '2023-01-11 15:15:03.000000', 0);
INSERT INTO `students` VALUES ('6d68e17b-1c88-9210-5fd2-08e4a0b0c774', '95cfcadc-7e46-bc2c-e010-ced831c01e6e', '360de5a6-1717-bdbe-6bad-5279e2af03af', '2023-01-10 22:37:25.000000', 0);
INSERT INTO `students` VALUES ('6efa6bcd-b603-8651-f4fb-b7fdcbfe9d23', '960d11c4-f45a-3727-a335-9e37009757da', '57a30c9a-fd39-f761-2c27-1e65ba5a8312', '2023-01-20 09:15:23.000000', 0);
INSERT INTO `students` VALUES ('70b9bd4f-6a51-794e-0612-86763c72f1e3', '97a4ffca-eb5e-123f-96f1-7ba0812096dd', '3c5ce332-9156-4abb-0cfc-d615c53e8da3', '2023-01-11 21:13:07.000000', 0);
INSERT INTO `students` VALUES ('711995e8-5aac-9ea3-3d3a-3fc6a8b004ce', '97b09f81-dffd-7ab1-96cd-76cfa15a0f40', '360de5a6-1717-bdbe-6bad-5279e2af03af', '2023-01-05 18:32:21.000000', 0);
INSERT INTO `students` VALUES ('7213b76f-26be-1c05-775b-a122ca1e58de', '97cf3e06-de97-a8c9-4b64-151bcca62356', '795614cd-573a-5e0b-67f0-a9f8548290e8', '2023-01-15 21:13:18.000000', 0);
INSERT INTO `students` VALUES ('74b98283-a700-bf02-0c39-91740c0ec58c', '9819e3b1-8d80-596e-1836-762a966e0c39', 'adbf3f28-1d91-2400-894e-9044eefecdc9', '2023-01-06 18:20:06.000000', 0);
INSERT INTO `students` VALUES ('79277545-00bd-9c60-b4c8-e3dc93ad4b60', '98a48c69-8e25-ff64-af18-a7e0fe01d432', 'a30bc4cc-0833-79aa-e072-a106edae1d62', '2023-01-24 11:55:07.000000', 0);
INSERT INTO `students` VALUES ('79cc90f5-cd7a-1f84-270e-6ef4827835fc', '998b4238-6fc4-d3a6-e1d5-a9b4737bde48', '7b66fbda-40f9-8ad5-8a67-803c25300414', '2023-01-04 19:45:26.000000', 0);
INSERT INTO `students` VALUES ('7a1b397e-48cf-fefd-6850-f37e22e325ca', '999925e1-db64-8f3b-554d-0fdfbff3e22d', '3c5ce332-9156-4abb-0cfc-d615c53e8da3', '2023-01-19 06:35:04.000000', 0);
INSERT INTO `students` VALUES ('7bb63361-60d9-2565-baa3-ae84967dc9ac', '9b040a4e-83d8-7f6a-cca5-2950743447f6', 'e9b62d1b-095a-a94f-e6e1-cba6fab3bf29', '2023-01-22 12:05:41.000000', 0);
INSERT INTO `students` VALUES ('7c3ecc8b-0c70-9ab4-7147-44f324802e21', '9ba3f4d6-d776-eed1-d162-41bb28e70735', 'a30bc4cc-0833-79aa-e072-a106edae1d62', '2023-01-01 07:30:50.000000', 0);
INSERT INTO `students` VALUES ('7d7dfd1e-756d-e785-37ed-c5934edf95ec', '9babd805-ff45-7e4c-bf78-b2fcd60cad5f', 'b26068a0-4ace-5cde-a36c-795990f74006', '2023-01-08 21:20:47.000000', 0);
INSERT INTO `students` VALUES ('8078cf86-956a-e745-bd47-50be62bcbbcc', '9be8c479-fb67-b5ef-ca24-cd6bc249fd99', 'e9b62d1b-095a-a94f-e6e1-cba6fab3bf29', '2023-01-02 20:12:50.000000', 0);
INSERT INTO `students` VALUES ('80e23883-1082-9ead-786f-620333667af7', '9d5a8226-ffbd-f431-5679-e8dfc6c64147', '57a30c9a-fd39-f761-2c27-1e65ba5a8312', '2023-01-14 16:17:53.000000', 0);
INSERT INTO `students` VALUES ('81cddd27-163a-8b48-fd31-915d3d8be90b', '9e24bf24-935f-d0f5-dfd1-59025de90fcb', '49b61dcd-e9f7-56e6-5a57-d2a24d85e583', '2023-01-13 05:17:16.000000', 0);
INSERT INTO `students` VALUES ('83ad26c2-bd1f-0213-a78d-e58aa41f294f', '9e602aa2-4211-492e-8af1-24ede1e98a0f', '7b66fbda-40f9-8ad5-8a67-803c25300414', '2023-01-01 11:12:39.000000', 0);
INSERT INTO `students` VALUES ('843f58c5-3dc2-15c6-9803-b74e212c3932', '9f45c4e3-30f2-238b-94c1-d36851518270', 'f1086fe1-f176-e541-1ad6-658d0543486c', '2023-01-08 16:17:44.000000', 0);
INSERT INTO `students` VALUES ('84ccfddf-f6d5-a783-ea9e-09b3202e8dd9', '9fbbeeb9-3b02-324d-9523-2996f96653da', 'adbf3f28-1d91-2400-894e-9044eefecdc9', '2023-01-14 19:49:47.000000', 0);
INSERT INTO `students` VALUES ('85c13be8-45c5-f4d7-fb83-dbe5058d382b', 'a246d7dc-4486-3c49-ced2-fa45cc1e7d24', '3c5ce332-9156-4abb-0cfc-d615c53e8da3', '2023-01-07 22:25:34.000000', 0);
INSERT INTO `students` VALUES ('87153d64-8a95-6027-2162-1a7bfc2fb95f', 'a2528783-9459-5b6e-fa29-874d88b3ec51', '37aa9628-103a-67b2-5ccf-174e6fdb966b', '2023-01-06 05:06:48.000000', 0);
INSERT INTO `students` VALUES ('8765725a-a7b2-0a9a-a7dd-9fbcc2435a4e', 'a32e10d7-1e7f-f056-8761-a03ed033e031', 'f0d3138e-431b-fa73-1452-52efd912c7f7', '2023-01-01 16:02:14.000000', 0);
INSERT INTO `students` VALUES ('88c13107-86fd-0deb-9bda-0dd9a75518b9', 'a49643ce-af0e-33bc-82c9-759c613405d1', 'b26068a0-4ace-5cde-a36c-795990f74006', '2023-01-04 05:51:22.000000', 0);
INSERT INTO `students` VALUES ('892fd022-34c5-ab0d-514a-c4321c5a5698', 'a64e2e5d-bfca-9ae9-cdf9-444de12b0975', '07fb639c-c66d-50de-71f1-8ba0a0ba8833', '2023-01-08 18:08:16.000000', 0);
INSERT INTO `students` VALUES ('8ab49f87-cf1d-905f-c6be-a0a9b9953b7c', 'a68e832e-14e5-1723-a3e3-76f774cd68a3', 'f1086fe1-f176-e541-1ad6-658d0543486c', '2023-01-19 18:32:15.000000', 0);
INSERT INTO `students` VALUES ('8c31745d-c32f-084b-fc67-cd1358b78e12', 'a7037bd6-82a1-53ec-d2c9-88decec54565', 'f1086fe1-f176-e541-1ad6-658d0543486c', '2023-01-10 19:54:06.000000', 0);
INSERT INTO `students` VALUES ('8d460d5c-0690-4d35-b831-74ed8aeb26e0', 'a77a6c39-403b-b170-ec09-f163993754e5', '743d0853-4883-b225-78ae-ef1199aaeb58', '2023-01-14 02:31:44.000000', 0);
INSERT INTO `students` VALUES ('91d1784f-11b9-5798-32a3-56f5b60d4660', 'a851792d-4a41-812c-1ec1-347a187f090e', '49b61dcd-e9f7-56e6-5a57-d2a24d85e583', '2023-01-02 12:50:03.000000', 0);
INSERT INTO `students` VALUES ('955d253f-57e5-e4ff-195d-0e1419c2822b', 'a8de34aa-2370-fa43-8beb-e9039095758b', '07fb639c-c66d-50de-71f1-8ba0a0ba8833', '2023-01-14 07:24:19.000000', 0);
INSERT INTO `students` VALUES ('957df0a1-c99d-9e8f-a589-6325e6a2ac2a', 'a919c5f2-49ed-f3e1-9860-e8a1995e3d37', 'adbf3f28-1d91-2400-894e-9044eefecdc9', '2023-01-15 07:36:20.000000', 0);
INSERT INTO `students` VALUES ('967af8ba-717a-997c-25e8-d7ec64baba2d', 'a9f76284-4739-3fa7-bc9f-b3636e99aea1', 'b26068a0-4ace-5cde-a36c-795990f74006', '2023-01-18 19:52:07.000000', 0);
INSERT INTO `students` VALUES ('986e5703-40b9-2d50-7822-0828491cdf95', 'aa276510-1816-8865-92df-88e819242025', 'ad952135-bceb-77a5-632a-b5bc27f59838', '2023-01-14 19:08:54.000000', 0);
INSERT INTO `students` VALUES ('98b7afde-d6b4-6d9f-8221-21238cbfff51', 'abad81f0-b9c8-3115-87b9-6b1b56aa49df', 'ad952135-bceb-77a5-632a-b5bc27f59838', '2023-01-24 02:01:09.000000', 0);
INSERT INTO `students` VALUES ('9c8e420c-c207-2818-d48b-fc0a03fc3617', 'ac244e2c-3444-51c8-ed0e-158043ed4dc6', '8c6b7ff4-60e8-37a6-53d9-16b09d0aed82', '2023-01-01 08:52:57.000000', 0);
INSERT INTO `students` VALUES ('9cfe69d8-3c1a-2b6f-f84f-1371c884bc02', 'ac3c6cd3-1d7c-1f67-692b-e85a789e5d37', '743d0853-4883-b225-78ae-ef1199aaeb58', '2023-01-27 10:21:04.000000', 0);
INSERT INTO `students` VALUES ('9e0b3680-d907-986c-ab27-9f180f3aa2df', 'ad1a1ead-cd62-38b2-75f2-df00d38ae566', '8c6b7ff4-60e8-37a6-53d9-16b09d0aed82', '2023-01-06 15:22:10.000000', 0);
INSERT INTO `students` VALUES ('9eba7bfa-b0ab-ae2d-4ef6-dcb65fe7b706', 'ad24f7ea-d758-01c9-0305-6d1ed273a95c', '37aa9628-103a-67b2-5ccf-174e6fdb966b', '2023-01-11 12:30:45.000000', 0);
INSERT INTO `students` VALUES ('a06afa95-b14f-6ae1-96e3-b988ce33fb4c', 'ad6ca290-a828-0feb-e1b9-b7abba814010', 'adbf3f28-1d91-2400-894e-9044eefecdc9', '2023-01-28 01:50:58.000000', 0);
INSERT INTO `students` VALUES ('a0c0b203-b06a-3f9c-bba3-71104a336f8e', 'ad9cd476-3dc0-f95b-292f-88a38764bb9f', 'b26068a0-4ace-5cde-a36c-795990f74006', '2023-01-17 06:59:16.000000', 0);
INSERT INTO `students` VALUES ('a271bd06-b8aa-bc2b-a829-8e2171f28163', 'adfaefbd-484f-f1e8-d02d-8757d1d84afb', '2a32710e-a4fa-ea29-6263-815ab8bc3838', '2023-01-16 10:51:46.000000', 0);
INSERT INTO `students` VALUES ('a28eab91-bd4e-9c71-c448-431b2a883dd6', 'b0202efe-4197-0660-93dd-0e06cc9944ae', 'f0d3138e-431b-fa73-1452-52efd912c7f7', '2023-01-20 02:13:22.000000', 0);
INSERT INTO `students` VALUES ('a50615b3-91a7-18a0-1298-233f7e4b2e02', 'b25108f8-c399-266e-e071-762d4c36ed2e', '57a30c9a-fd39-f761-2c27-1e65ba5a8312', '2023-01-08 14:30:40.000000', 0);
INSERT INTO `students` VALUES ('a532a754-0ba3-686c-ce5a-1cdd335d6cd1', 'b25257ff-a225-a49d-5e9b-766396c80873', '2a32710e-a4fa-ea29-6263-815ab8bc3838', '2023-01-02 11:17:10.000000', 0);
INSERT INTO `students` VALUES ('a53d14a0-ff85-eadb-a58f-90a60a9e6869', 'b3b47931-b135-e9bc-029d-4896b640a7fe', 'bf73403d-7ebb-5a57-05f9-2ddb3063ef55', '2023-01-16 21:33:53.000000', 0);
INSERT INTO `students` VALUES ('a7c60324-bfa1-43e9-60ee-fd71da880a30', '05b51f7e-6c69-bfd6-2f98-c36850b617ab', '7b66fbda-40f9-8ad5-8a67-803c25300414', '2023-01-10 08:56:26.000000', 0);
INSERT INTO `students` VALUES ('a8134806-7ffc-ba5f-fa0d-0524628cdfd0', 'b51fa004-1aaa-c360-d768-ac8768442c0d', 'e7badc0e-6468-90df-e8ee-c5343b4ad7af', '2023-01-31 02:53:55.000000', 0);
INSERT INTO `students` VALUES ('a833aa30-18d4-ba73-56d2-e7eddc19ea1a', 'b6b96d1d-02f1-99d3-6c51-55d0139aa5eb', 'b26068a0-4ace-5cde-a36c-795990f74006', '2023-01-28 00:41:01.000000', 0);
INSERT INTO `students` VALUES ('a8603bd2-04aa-177e-0483-ae911cd52326', 'b6c088ea-5d36-6724-dd06-f331fe143c11', '8c6b7ff4-60e8-37a6-53d9-16b09d0aed82', '2023-01-19 05:39:16.000000', 0);
INSERT INTO `students` VALUES ('a8788c78-9f24-f448-81d4-f809bbb0bda6', 'b7341b30-d793-96f0-8a82-664c78d92f2d', 'b26068a0-4ace-5cde-a36c-795990f74006', '2023-01-09 18:00:28.000000', 0);
INSERT INTO `students` VALUES ('a9480c06-1c90-5ab7-bf40-84b256039d09', 'b7457a89-49e8-9913-25ea-bcae11ee8a47', '8c6b7ff4-60e8-37a6-53d9-16b09d0aed82', '2023-01-02 08:07:54.000000', 0);
INSERT INTO `students` VALUES ('a9715c69-60b0-cecc-fd1e-ef628b5fe0c4', 'bae40eca-2811-9352-f543-78b758f8866d', '7b66fbda-40f9-8ad5-8a67-803c25300414', '2023-01-15 07:55:43.000000', 0);
INSERT INTO `students` VALUES ('abb76d40-3101-b24a-ff08-752738fad5a6', 'bb5be7bc-f71f-f587-3d04-062fabdddf6e', 'e9b62d1b-095a-a94f-e6e1-cba6fab3bf29', '2023-01-15 15:32:25.000000', 0);
INSERT INTO `students` VALUES ('abd5d728-96b4-2cc9-ef2e-6d42c646eab2', 'bbfbd3e8-ca9b-638f-7919-82c2b4e2c245', 'adbf3f28-1d91-2400-894e-9044eefecdc9', '2023-01-09 04:21:28.000000', 0);
INSERT INTO `students` VALUES ('ac9f0208-139f-9104-845b-d0e7d01faebd', 'bd5234a2-e044-b7f3-7595-de0f0eaf0ac1', 'adbf3f28-1d91-2400-894e-9044eefecdc9', '2023-01-06 08:11:15.000000', 0);
INSERT INTO `students` VALUES ('aefa6653-baea-e10e-e9e2-7dbc45f7057d', 'be58cb3f-77d1-c614-f9d5-2b5696acdf4f', 'e7badc0e-6468-90df-e8ee-c5343b4ad7af', '2023-01-17 07:57:01.000000', 0);
INSERT INTO `students` VALUES ('af5dc4d6-da40-aeec-fbe2-550f11cd8b46', '06adeba4-9ddf-e467-12fc-68642735cc9e', '57a30c9a-fd39-f761-2c27-1e65ba5a8312', '2023-01-01 01:58:41.000000', 0);
INSERT INTO `students` VALUES ('b15f2b53-9fc9-d69c-d251-205520cc5cb2', 'c22fe868-5ef6-52e2-4a9a-1b28d407307c', 'a30bc4cc-0833-79aa-e072-a106edae1d62', '2023-01-20 17:33:28.000000', 0);
INSERT INTO `students` VALUES ('b2089a02-bd14-ba68-2ad2-31a217bde4da', 'c24233ef-3c78-6200-cd11-9dd5d9be4159', 'e9b62d1b-095a-a94f-e6e1-cba6fab3bf29', '2023-01-12 23:39:27.000000', 0);
INSERT INTO `students` VALUES ('b45ec779-c046-b698-572d-3c821f022f3f', 'c29c1da0-ebff-3eef-28a0-bd9be5e3930f', 'e7badc0e-6468-90df-e8ee-c5343b4ad7af', '2023-01-05 03:22:17.000000', 0);
INSERT INTO `students` VALUES ('b65c3d86-0ce7-bab9-365a-5b3589bcf949', 'c39d292e-b2ac-4175-ca43-5703372a06d2', 'ad952135-bceb-77a5-632a-b5bc27f59838', '2023-01-21 21:58:47.000000', 0);
INSERT INTO `students` VALUES ('b68b719b-0ffc-cd94-bf7f-acde4421edc0', 'c4256393-1a61-06fb-8846-662738220cb1', '8c6b7ff4-60e8-37a6-53d9-16b09d0aed82', '2023-01-09 05:33:07.000000', 0);
INSERT INTO `students` VALUES ('bace73e1-b2eb-656e-061f-88986320ec77', 'c4d8b5c4-a624-de9d-54db-16b5292d50fc', '07fb639c-c66d-50de-71f1-8ba0a0ba8833', '2023-01-02 11:55:58.000000', 0);
INSERT INTO `students` VALUES ('bbe4435b-74dd-1bef-f6ef-24be6a138384', 'c6003085-2efb-b94c-a64d-2e44e27d5d6c', 'a30bc4cc-0833-79aa-e072-a106edae1d62', '2023-01-04 22:24:48.000000', 0);
INSERT INTO `students` VALUES ('bd161af7-6390-2176-6427-c7c8602321d7', 'c8b9afc4-94e1-416d-37e2-60130783d949', '865c36cd-8b08-a32c-ef9a-b83e086a246f', '2023-01-07 06:51:40.000000', 0);
INSERT INTO `students` VALUES ('c3f09384-5e7c-59d7-eef4-5ca47fb8ba48', 'c9bfbc24-a60e-4986-9394-0958d744441c', '865c36cd-8b08-a32c-ef9a-b83e086a246f', '2023-01-18 23:15:18.000000', 0);
INSERT INTO `students` VALUES ('c406607a-aa76-2598-b577-655c345a3210', 'ca9d4435-8bf1-50ab-c67c-e267a5d963d6', 'e7badc0e-6468-90df-e8ee-c5343b4ad7af', '2023-01-05 09:08:26.000000', 0);
INSERT INTO `students` VALUES ('c43950b6-e451-6705-9b77-5ba3a8e35727', 'cc1a5caa-f669-32f8-af4d-0ce5b3f10936', '2a32710e-a4fa-ea29-6263-815ab8bc3838', '2023-01-21 20:57:45.000000', 0);
INSERT INTO `students` VALUES ('c4bedc6a-0755-ded4-b444-d862e3ff357f', 'cc25af3d-2736-50c1-f047-8b05f25f75d0', '8c6b7ff4-60e8-37a6-53d9-16b09d0aed82', '2023-01-28 21:50:31.000000', 0);
INSERT INTO `students` VALUES ('c53f98ee-8b7f-1132-8478-26c9209d113c', 'cceac40c-fb4b-7280-18e5-25f56f49c253', '49b61dcd-e9f7-56e6-5a57-d2a24d85e583', '2023-01-15 00:13:54.000000', 0);
INSERT INTO `students` VALUES ('c55bad22-d6ca-99c3-2ac3-a5162f478ab8', 'cd0adb85-eee1-a70b-cbf9-d460fdef97b0', 'adbf3f28-1d91-2400-894e-9044eefecdc9', '2023-01-13 02:45:55.000000', 0);
INSERT INTO `students` VALUES ('c613fb0c-7de5-f334-3753-e58ab1257592', 'cd5f416f-3362-5137-0d1f-cbbf178ee54c', '865c36cd-8b08-a32c-ef9a-b83e086a246f', '2023-01-15 19:50:16.000000', 0);
INSERT INTO `students` VALUES ('c6778bc5-ea11-145d-acfd-90064ccb9bf1', 'cdbfe4fe-6a6b-437e-c7fc-765b07191895', '360de5a6-1717-bdbe-6bad-5279e2af03af', '2023-01-10 22:57:47.000000', 0);
INSERT INTO `students` VALUES ('c7a7585b-d6a8-a63b-1904-e87eeb9f3cf9', 'cdfa9f0b-c19b-9530-733e-a7693a5cf896', 'f0d3138e-431b-fa73-1452-52efd912c7f7', '2023-01-21 05:04:25.000000', 0);
INSERT INTO `students` VALUES ('c94e33d6-6974-a3be-2f5a-08432dbe0500', 'ce0bb6df-6ae7-38ff-f606-b11d3f755d26', '3c5ce332-9156-4abb-0cfc-d615c53e8da3', '2023-01-03 09:59:12.000000', 0);
INSERT INTO `students` VALUES ('ca0f1909-4256-b5e0-a358-057cfce5632e', 'cfd5df2a-cb2f-58de-16df-2f2348f51fd8', 'e9b62d1b-095a-a94f-e6e1-cba6fab3bf29', '2023-01-20 13:15:31.000000', 0);
INSERT INTO `students` VALUES ('cae7e866-1ad3-7d78-e85a-e90bdb06a01d', 'd0cdcbeb-3f99-3f4f-0819-30e40e068428', '743d0853-4883-b225-78ae-ef1199aaeb58', '2023-01-01 18:49:28.000000', 0);
INSERT INTO `students` VALUES ('cc0d5568-f8c6-cb26-65d9-48737e17b734', 'd158bba5-6bfe-b1ab-c1e2-e18ad1a4c5ca', '360de5a6-1717-bdbe-6bad-5279e2af03af', '2023-01-02 22:48:01.000000', 0);
INSERT INTO `students` VALUES ('cc3350b5-a23b-2d13-3467-11b4f59c9529', 'd178dd7e-85fb-aab9-8091-ec8d2d86b016', '743d0853-4883-b225-78ae-ef1199aaeb58', '2023-01-20 23:30:50.000000', 0);
INSERT INTO `students` VALUES ('cc677d0b-7097-7cc7-666a-3d04269eed15', 'd229a340-bf05-bd6c-748e-aaaa842de523', '37aa9628-103a-67b2-5ccf-174e6fdb966b', '2023-01-18 04:41:37.000000', 0);
INSERT INTO `students` VALUES ('cf7302ab-8c18-b399-ea97-87b60d37035f', 'd250ff81-d6f0-6958-1c19-7d625c0a83cd', 'bf73403d-7ebb-5a57-05f9-2ddb3063ef55', '2023-01-03 03:39:31.000000', 0);
INSERT INTO `students` VALUES ('d50df699-1360-8008-7d81-4b9f88bc9027', 'd2e2acb9-7325-8392-da07-fac8b7a2bfaa', '795614cd-573a-5e0b-67f0-a9f8548290e8', '2023-01-05 17:05:29.000000', 0);
INSERT INTO `students` VALUES ('d7c9f393-c9a6-e628-1bd4-3f2b7c7cbf5e', 'd7ea80fb-910d-8c97-aa41-18fd5f085d89', 'adbf3f28-1d91-2400-894e-9044eefecdc9', '2023-01-13 13:33:17.000000', 0);
INSERT INTO `students` VALUES ('de4156cd-cef3-b3cf-82bd-efaa4df05ebb', 'd7eaaeef-ce09-e5c3-64f4-3e30fde047bd', '795614cd-573a-5e0b-67f0-a9f8548290e8', '2023-01-19 05:10:52.000000', 0);
INSERT INTO `students` VALUES ('e0bed427-ba38-a464-4006-6bcebecd32df', 'd822584a-8544-63a4-1744-079cc68d9f7a', 'a30bc4cc-0833-79aa-e072-a106edae1d62', '2023-01-22 18:58:01.000000', 0);
INSERT INTO `students` VALUES ('e33fc5a0-3599-cd16-2b4a-91c5666cc75a', 'd87fbe27-00ff-3ae9-96d2-089d2ff73010', 'e9b62d1b-095a-a94f-e6e1-cba6fab3bf29', '2023-02-01 05:53:12.000000', 0);
INSERT INTO `students` VALUES ('e35fa4ef-865f-858d-9b8d-5dc38bf84409', 'd8b1e9c4-09c3-f108-2de0-7664c407dce5', 'e7badc0e-6468-90df-e8ee-c5343b4ad7af', '2023-01-13 03:36:23.000000', 0);
INSERT INTO `students` VALUES ('e411d56c-cd60-4cda-3430-d5be9edcfa86', 'd8d4998f-7f6e-5164-3038-04c023ab35a1', 'f1086fe1-f176-e541-1ad6-658d0543486c', '2023-01-06 08:51:53.000000', 0);
INSERT INTO `students` VALUES ('e59ee221-f052-9dad-46dd-fc4d464f6f5d', 'da486596-6548-1458-26de-42340db3d974', 'e9b62d1b-095a-a94f-e6e1-cba6fab3bf29', '2023-01-25 18:51:21.000000', 0);
INSERT INTO `students` VALUES ('e656aafd-7562-23e8-3f6a-acdf2043adf7', 'dac5552f-1c10-f074-a9f8-d630069b3557', 'e7badc0e-6468-90df-e8ee-c5343b4ad7af', '2023-01-13 21:16:05.000000', 0);
INSERT INTO `students` VALUES ('e882557c-6a25-3567-b8a5-dbc5925ff19a', 'db3f6b68-45b7-4f5c-e5bb-2774a23145a3', '743d0853-4883-b225-78ae-ef1199aaeb58', '2023-01-09 19:44:30.000000', 0);
INSERT INTO `students` VALUES ('ecdeb296-929f-e222-b0c4-9da4525fdee8', 'dc144322-616a-ef7b-f471-c3c76b134256', '07fb639c-c66d-50de-71f1-8ba0a0ba8833', '2023-01-13 13:52:37.000000', 0);
INSERT INTO `students` VALUES ('ed5dc4a3-9d00-b2bc-b5e8-b0cb9f626b49', 'dc31d036-bc5c-e085-50db-bebb459b3d61', 'f1086fe1-f176-e541-1ad6-658d0543486c', '2023-01-09 04:58:43.000000', 0);
INSERT INTO `students` VALUES ('f0c1ec5a-1ea9-b5d5-8ca4-2d102d971ea1', 'dcd440a7-aef6-4609-a712-cef9ec65fa77', '7b66fbda-40f9-8ad5-8a67-803c25300414', '2023-02-01 01:39:17.000000', 0);
INSERT INTO `students` VALUES ('f5f42415-f4ad-b5ab-3790-27beb10cd167', 'dd0b878b-7d1a-c635-1b9e-4b989b2cd08d', '7b66fbda-40f9-8ad5-8a67-803c25300414', '2023-01-02 19:52:15.000000', 0);
INSERT INTO `students` VALUES ('f8247173-a7bd-12d7-1e0c-c6c97b781ac2', 'ddf22a29-316f-233f-bd1e-80229487773b', 'f0d3138e-431b-fa73-1452-52efd912c7f7', '2023-01-06 01:38:16.000000', 0);
INSERT INTO `students` VALUES ('fae7f543-6bb7-4f0d-eb60-bdaddac35368', 'df347751-ee9f-ed4d-e66f-24856ff968ad', '3c5ce332-9156-4abb-0cfc-d615c53e8da3', '2023-01-05 09:43:54.000000', 0);
INSERT INTO `students` VALUES ('fbc43348-b930-beea-6314-4a51f51ebff4', 'e0ac6c79-0bb4-ef04-d53f-5ca5c153748e', '37aa9628-103a-67b2-5ccf-174e6fdb966b', '2023-01-03 22:40:46.000000', 0);
INSERT INTO `students` VALUES ('ff15c78c-0ba2-50ff-da9d-c65f14deab58', 'e2a93fbc-d95a-e2be-cfa2-26f546d4f88c', 'e7badc0e-6468-90df-e8ee-c5343b4ad7af', '2023-01-15 08:20:32.000000', 0);
INSERT INTO `students` VALUES ('ff41fe1a-7c2f-d9f8-bdd2-006002006d59', 'e358e498-8e7d-731a-6abf-7510b1a61d37', '57a30c9a-fd39-f761-2c27-1e65ba5a8312', '2023-01-15 11:02:19.000000', 0);

-- ----------------------------
-- Table structure for teacheres
-- ----------------------------
DROP TABLE IF EXISTS `teacheres`;
CREATE TABLE `teacheres`  (
  `ID` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `UseID` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `TcEducation` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `TcSchool` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Major` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `TcCard` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `LevelID` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `IsWorking` tinyint(1) NULL DEFAULT 0,
  `EntryTime` datetime(6) NULL DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID`) USING BTREE,
  UNIQUE INDEX `IX_teacheres_TcCard`(`TcCard` ASC) USING BTREE,
  INDEX `IX_teacheres_LevelID`(`LevelID` ASC) USING BTREE,
  INDEX `IX_teacheres_UseID`(`UseID` ASC) USING BTREE,
  CONSTRAINT `FK_teacheres_Levels_LevelID` FOREIGN KEY (`LevelID`) REFERENCES `levels` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_teacheres_User_UseID` FOREIGN KEY (`UseID`) REFERENCES `user` (`Id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of teacheres
-- ----------------------------
INSERT INTO `teacheres` VALUES ('006b0ece-a585-6609-23f7-70f42454c060', '1bfce5a1-5943-42e5-87fe-236959a015bb', '硕士', '华南理工大学', '软件工程', '842391209827392128', 'e5bbcd83-0cac-86c2-ffdc-0439fbe58f38', 1, '2018-11-15 01:54:39.000000', 0);
INSERT INTO `teacheres` VALUES ('09016268-39de-9d10-fddf-c46023ebae78', '47c31f3b-6a9e-48e7-9efb-42dc3ceaa4c0', '本科', '西安交通大学', '软件工程', '655481019842995840', 'e5bbcd83-0cac-86c2-ffdc-0439fbe58f38', 1, '2019-11-15 03:08:26.000000', 0);
INSERT INTO `teacheres` VALUES ('13c3637f-722a-fb04-b8dd-9e6f6a3c76e6', '037158ef-ac5f-4122-98b9-3d4382bc6697', '硕士', '南京航空航天大学', '软件工程', '826726965060246784', 'e5bbcd83-0cac-86c2-ffdc-0439fbe58f38', 1, '2019-07-07 07:03:08.000000', 0);
INSERT INTO `teacheres` VALUES ('2262b970-2d25-d25f-a5d6-7498a91b7d5f', '038d846e-fdfc-424d-badb-091eb44b8255', '本科', 'XX大学', '软件工程', '109043590285021264', '846244b0-d24c-7684-9ad2-24d2d1175933', 1, '2020-07-27 22:23:27.000000', 0);
INSERT INTO `teacheres` VALUES ('27375864-0298-39a1-f6eb-7375b0fe4a80', '7197727d-2cf9-495f-a380-15266965a29d', '本科', 'EE大学', '计算机科学与技术（211）', '282105611840646368', '1628af2d-1bc3-5de4-3bfa-74a0bc98888b', 1, '2020-06-16 00:00:00.000000', 0);
INSERT INTO `teacheres` VALUES ('2d18c9b1-3192-3aa9-0a18-9becb94efa78', '053dbe49-7de5-40fb-923a-04ae4b300a64', '本科', 'FF学院', '软件测试', '604058710070039680', '1628af2d-1bc3-5de4-3bfa-74a0bc98888b', 1, '2019-09-11 04:54:44.000000', 0);
INSERT INTO `teacheres` VALUES ('312f7606-0915-307f-a87a-0703347c2fc5', '489aba26-fb61-46f1-8f3e-3851f3a31f09', '本科', 'YY大学', '计算机科学与技术', '765361356206509824', '846244b0-d24c-7684-9ad2-24d2d1175933', 1, '2021-02-10 04:24:01.000000', 0);
INSERT INTO `teacheres` VALUES ('36cf8496-e4d2-6657-5bd5-d8e3b496118d', '57dfec8a-fef8-4a86-8779-f2d3d89a07f7', '本科', 'II大学', '计算机信息管理', '174708113466393280', '1628af2d-1bc3-5de4-3bfa-74a0bc98888b', 1, '2019-10-22 07:17:44.000000', 0);
INSERT INTO `teacheres` VALUES ('59eec08d-66ff-e1e8-1ab7-090c28a775c7', '2440ea84-93ae-44dd-a29e-b7f87864ccfc', '本科', 'JJ大学', '软件工程', '869815937477245952', '1628af2d-1bc3-5de4-3bfa-74a0bc98888b', 1, '2019-09-10 20:28:02.000000', 0);
INSERT INTO `teacheres` VALUES ('5d688b38-dd54-4bc7-59b3-5efa6abd2ddf', '2dc831f6-82f0-43a2-b92a-bddd8c578732', '本科', 'KK大学', '计算机应用技术', '224853221525044928', '1628af2d-1bc3-5de4-3bfa-74a0bc98888b', 1, '2021-08-06 12:03:08.000000', 0);
INSERT INTO `teacheres` VALUES ('77f505cc-7d23-9853-ca56-85db7f280b62', '3de40c37-f6c7-4e47-a622-cc34d41d6c26', '本科', 'MM大学', '软件技术', '488201790808449664', '1628af2d-1bc3-5de4-3bfa-74a0bc98888b', 1, '2021-07-15 23:44:20.000000', 0);
INSERT INTO `teacheres` VALUES ('85267914-34df-2c83-3ed0-8f2e75a45128', '3eafb3c9-aa04-4b60-b1ca-56c3389aab9e', '本科', 'NN学院', '计算机信息管理与服务', '366196568191313408', '1628af2d-1bc3-5de4-3bfa-74a0bc98888b', 1, '2020-07-12 07:09:46.000000', 0);
INSERT INTO `teacheres` VALUES ('918197c9-5cc9-8699-486d-f5a3b838d998', '28951d84-d502-4de9-8674-99a84f4c8a40', '本科', 'OO大学', '软件测试与质量控制', '942632633378835968', '1628af2d-1bc3-5de4-3bfa-74a0bc98888b', 1, '2019-09-22 20:21:22.000000', 0);
INSERT INTO `teacheres` VALUES ('9cf1ec8e-e25c-79ae-d3ff-15de01936750', '80e94c3b-ca99-4126-9438-86d411e1c960', '本科', 'AA学院', '软件开发', '213860467124944640', '846244b0-d24c-7684-9ad2-24d2d1175933', 1, '2020-12-08 20:54:01.000000', 0);
INSERT INTO `teacheres` VALUES ('9e869700-f818-5f45-dcfa-64bb8ed73198', '82ed3da7-79db-416e-ac1d-ca048d6432e0', '本科', 'BB大学', '计算机应用', '703836864974690432', '846244b0-d24c-7684-9ad2-24d2d1175933', 1, '2021-12-16 08:57:55.000000', 0);
INSERT INTO `teacheres` VALUES ('c99e03f2-0941-81da-e224-e49c50ef5d69', '8e73cced-d51f-407f-aaac-14cc412a4fe7', '本科', 'CC大学', '信息技术', '505491626153111808', '846244b0-d24c-7684-9ad2-24d2d1175933', 1, '2020-04-25 07:36:26.000000', 0);
INSERT INTO `teacheres` VALUES ('e50fbf73-23f6-8596-4886-69f0023862cb', 'b4ad8b4d-ad00-49ea-8916-02731636fe52', '本科', 'DD大学', '软件工程', '974014181578292096', '846244b0-d24c-7684-9ad2-24d2d1175933', 1, '2019-05-28 11:39:16.000000', 0);
INSERT INTO `teacheres` VALUES ('e674b8fd-b38d-f0c4-f47b-3b47198bae36', 'bf0059e2-987b-4b40-bd42-1bf294ee0cf5', '本科', 'PP大学', '计算机应用与维护', '578296064575060224', '1628af2d-1bc3-5de4-3bfa-74a0bc98888b', 1, '2020-11-02 04:43:35.000000', 0);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `Id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Account` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Password` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `UserName` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Name` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `Sex` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Icon` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `Intro` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT 0,
  `RoleID` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `NormalizedUserName` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `NormalizedEmail` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `EmailConfirmed` tinyint(1) NOT NULL DEFAULT 0,
  `PasswordHash` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `SecurityStamp` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `ConcurrencyStamp` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `PhoneNumber` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `PhoneNumberConfirmed` tinyint(1) NOT NULL DEFAULT 0,
  `TwoFactorEnabled` tinyint(1) NOT NULL DEFAULT 0,
  `LockoutEnd` datetime(6) NULL DEFAULT NULL,
  `LockoutEnabled` tinyint(1) NOT NULL DEFAULT 0,
  `AccessFailedCount` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`) USING BTREE,
  UNIQUE INDEX `IX_User_Email`(`Email` ASC) USING BTREE,
  UNIQUE INDEX `IX_User_PhoneNumber`(`PhoneNumber` ASC) USING BTREE,
  INDEX `IX_User_RoleID`(`RoleID` ASC) USING BTREE,
  CONSTRAINT `FK_User_Role_RoleID` FOREIGN KEY (`RoleID`) REFERENCES `role` (`Id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('027574a0-eaa6-2f7b-da1d-fb7d49a05ba9', 'admin', '123456', 'Cheryl', 'admin', '男', 'ruihuan@yahoo.com', 'https://tse3-mm.cn.bing.net/th/id/OIP-C.q0uY4jyb-dsKVGksY4LEjAHaHa?pid=ImgDet&rs=1', '勇敢无畏的战士', 0, '7256c38c-768d-63e4-9b8f-12cd54fd43d1', 'CHERYL', NULL, 0, 'AQAAAAIAAYagAAAAEI2KWbCVFo4Bw14qExl5KCWojgxsBApfqhocn31W+RUH4hsczoa/ZOiRyfZzUiCVcg==', '91c09b4a-1e10-438d-a2d0-6e5274c27ab5', '4fa201e8-5d1f-4a9c-87af-a1aa21359777', NULL, 0, 0, NULL, 0, 0);
INSERT INTO `user` VALUES ('037158ef-ac5f-4122-98b9-3d4382bc6697', '9364165', '574600', 'Raymond', '何詩涵', '女', 'hesh@icloud.com', 'https://inews.gtimg.com/newsapp_bt/0/14260868339/1000', '创造力激发者', 0, '4f25ea99-7117-217e-10f4-92c75dd841d6', NULL, NULL, 0, 'AQAAAAIAAYagAAAAENGVEr4LstpwZVLQOUVkikpimUT2SEKlxgwzrP9KEMUgTAs7siZ7rgjxSWIFyjxBKA==', '00712dc7-32e3-04c7-c5ca-21953cba1e56', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('038d846e-fdfc-424d-badb-091eb44b8255', '4981769', '729342', 'Raymond', '陆致远', '男', 'zhiyuanl@icloud.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.863707d9067f12fbbb388be241118810?rik=qUX8qK9IiNDeYA&riu=http%3a%2f%2fwx3.sinaimg.cn%2flarge%2f006fbYi5gy1fm038wx447j30pr0rqta5.jpg&ehk=d6Uh5IEJ0W0RXgOyZaiy1oJ%2b0jZeVcafG6OSjZFbRGQ%3d&risl=&pid=ImgRaw&r=0', '心灵探险家', 0, '4f25ea99-7117-217e-10f4-92c75dd841d6', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEBAL2CDQFanWrN1RW0Kn5dq4oWOlGXmKUCpco0ngZICBxPztiae/bDZqj/07UZSC4Q==', '0133c59d-bc46-e5db-76cd-dd4573b1422a', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('053dbe49-7de5-40fb-923a-04ae4b300a64', '4782546', '405297', 'Emma', '龙杰宏', '男', 'ljieh1989@gmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.022138c27995e39290e0a12bbe165c63?rik=g1brk1IeD96KHw&riu=http%3a%2f%2fwx3.sinaimg.cn%2flarge%2f006fbYi5gy1fm038w0cbej30qo0qo76m.jpg&ehk=R2Jf2lI5LI3zZtc9FiTqGqd8RqPbOy0M%2fdNjU962x4w%3d&risl=&pid=ImgRaw&r=0', '热爱自然的旅行者', 0, '4f25ea99-7117-217e-10f4-92c75dd841d6', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEK98yzaMZp/H8dl6g0vdSl9Ne361WRDwG1UUpNxBRbBF+GG1GifCo2Lltq/InPs9dg==', '03054055-1f69-32f5-067c-44e35fe74f1c', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('05b51f7e-6c69-bfd6-2f98-c36850b617ab', '1501620', '307786', 'Frank', '韦子异', '男', 'zwei@icloud.com', 'https://tupian.qqw21.com/article/UploadPic/2019-9/20199111851788683.jpg', '乐观积极的梦想家', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEAOQCa7PhEuZs2AN+87WmNNobqTMzVRtKFMsAa17FS85Xwanci9fXIO/yeUNcuRktw==', '0366c918-cd76-ad51-1cac-1f2c56edef77', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('06adeba4-9ddf-e467-12fc-68642735cc9e', '8740845', '666033', 'Lillian', '高秀英', '女', 'gax@mail.com', 'https://www.qqkw.com/d/file/p/2018/06-07/0efbfd8ba7186911fc6fcef1ec54b318.jpg', '技术狂热追逐者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEN61X8hjSg5CsYylwG0FI/NpcAWNyJxh9hj7Ho97AEl6YIld/Nf2UBfZTv04/aqHiw==', '03bd2a5e-9cbe-baf5-b2bf-2afbf90aa0d4', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('1bfce5a1-5943-42e5-87fe-236959a015bb', '4552200', '837408', 'Ruby', '刘云熙', '男', 'liuyunxi@outlook.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.158e9d0074d651aa5beed5f0be279a08?rik=upslQ6DOYpguWw&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2017112908%2fqf3auqyeyv0.jpg&ehk=UqAJVxEwnUxyE7d6F2ugSZ4I7V6MlfUhhW%2bKX8Ktcq4%3d&risl=&pid=ImgRaw&r=0', '冒险寻求者', 0, '4f25ea99-7117-217e-10f4-92c75dd841d6', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEPfnDUlCf/VN4g3HwAr3jQIdz47R2HAMdAs5kYieTsY3XqMokYrgt+EOUPyf9x3qIw==', '0469a1ae-2f7c-5b55-835e-de2d3ee70aeb', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('2440ea84-93ae-44dd-a29e-b7f87864ccfc', '9594973', '930020', 'Craig', '阎璐', '女', 'yalu930@icloud.com', 'https://www.qqkw.com/d/file/p/2018/06-07/f54e106c3eaa256005228b93f2bf77cd.jpg', '形象设计专家', 0, '4f25ea99-7117-217e-10f4-92c75dd841d6', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEB3yDV1ir05EnaYHhMfhEwW5DS3ZAS9wPggILSFPzT77+P8LDo0SA7g8G37rA5hvsQ==', '09d418f3-bae2-d6f9-446e-21f7a52aee46', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('28951d84-d502-4de9-8674-99a84f4c8a40', '4830854', '307207', 'Herbert', '邱致远', '男', 'zhiyuan42@hotmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.866f4a834004344d050bd7ab8c9fcbc3?rik=DhrKHeXa9uusWw&riu=http%3a%2f%2faaaju.oicp.net%3a93%2fpicture%2fbiaoqing%2fpic%2fbb%2f47%2f419369.jpeg&ehk=RozfTnNGddl9e%2fIoL%2fZgwf7YaYBvx76qDoVmiDoxCfU%3d&risl=&pid=ImgRaw&r=0', '自律奋进者', 0, '4f25ea99-7117-217e-10f4-92c75dd841d6', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEKLrlF7rvDIfWrqQv7KWCsPDDrJC6YwSdXuQQkndh0Y1iOd29eHvRnf6BfJ9YiW00Q==', '0b4cabb8-7552-2407-ff5c-0f4f3c0101ce', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('2dc831f6-82f0-43a2-b92a-bddd8c578732', '2246569', '536409', 'Travis', '苏子异', '男', 'su9@mail.com', 'https://tupian.qqw21.com/article/UploadPic/2020-7/20207268511957950.jpg', '细致入微的观察者', 0, '4f25ea99-7117-217e-10f4-92c75dd841d6', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEPjwoswoow1B7yiTUQMEW3kmwjjAP8N0SE7/swG90v3kRYCnF/CXPDf9jCEURge9+Q==', '0c3bdd4b-623d-20f5-34f8-1822b0bfbeac', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('3de40c37-f6c7-4e47-a622-cc34d41d6c26', '4540602', '492606', 'Bonnie', '罗子韬', '男', 'zluo605@hotmail.com', 'https://pic2.zhimg.com/v2-192469cb6f528128f6d11fa9c71c7af7_r.jpg?source=1940ef5c', '灵感源泉之一', 0, '4f25ea99-7117-217e-10f4-92c75dd841d6', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEIp5FatTzkGMDP5/h0DqbKGrjwaG7WohZ8sYveUuBJWfn84PgGi1xdRyNrM5ybAtuw==', '0f0c6b86-9a6f-5827-4f86-c66c14b95712', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('3eafb3c9-aa04-4b60-b1ca-56c3389aab9e', '5657629', '106208', 'Marilyn', '萧震南', '男', 'zhx@hotmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.a2665980de097f5905ac80b5dbd9c972?rik=g%2fwSgDARVJ4SkQ&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2018031808%2fqt0dwngvbc0.jpg&ehk=VPcZAn8m2rgBq8X13XwdxHI4%2b7USXVPsRKYYAMPXwag%3d&risl=&pid=ImgRaw&r=0', '精确计算先驱', 0, '4f25ea99-7117-217e-10f4-92c75dd841d6', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEPfl6IaNrww0pM/EW3i4MMW0+bWov7z0niO/BfF/yRDfBbmWRUFoQj7TUIbQQmfrGA==', '102e06f2-644f-e58b-22a5-a56e71f39401', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('47c31f3b-6a9e-48e7-9efb-42dc3ceaa4c0', '4914362', '947687', 'Hazel', '罗秀英', '女', 'xiuyinglu@outlook.com', 'https://tupian.qqw21.com/article/UploadPic/2020-11/2020112022511465651.jpg', '社交媒体达人', 0, '4f25ea99-7117-217e-10f4-92c75dd841d6', NULL, NULL, 0, 'AQAAAAIAAYagAAAAENecVCtnLaiRK4GRpYnsFChDlYijTToKobJSwmURiTWE1Cc8mEtB4TIScE5OECDHhA==', '10a52900-6176-9799-9919-52a2c48e0ff1', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('489aba26-fb61-46f1-8f3e-3851f3a31f09', '7774125', '961899', 'Kim', '程震南', '男', 'chengzhe@hotmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.6245891f31fe806b7e3c1b332320a6d0?rik=kldg2tMOLKmlDw&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2018032308%2fvpsoxzcmlls.jpg&ehk=YQX3ygMEQHA77BeRiXjITWW8UBFSjzkqAdaaSyHMODM%3d&risl=&pid=ImgRaw&r=0', '高效问题解决者', 0, '4f25ea99-7117-217e-10f4-92c75dd841d6', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEA9vklPWqlpk6xEGQS/23R2N60Zh0EwgGtv32R/w5zwn7k/+czH3PtvP8Q5fY+hTnQ==', '11fd1cfc-5eee-26b1-3a4c-608824afbd7f', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('57dfec8a-fef8-4a86-8779-f2d3d89a07f7', '7554112', '232355', 'Diane', '邓宇宁', '男', 'dengyuning@gmail.com', 'https://tupian.qqw21.com/article/UploadPic/2020-1/202011121502998047.jpg', '着装风格引领者', 0, '4f25ea99-7117-217e-10f4-92c75dd841d6', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEApmdjmLTksmCl+fZcTqSFH0FhwjeSxxt2XCVR640Vq1XullLPWc4B/DKHwii5n6kQ==', '1490e6dd-b2f7-ddc6-a8c5-3751bd648387', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('643318c9-31cb-0821-8fb0-ff9b1e8b140d', '3816562', '465659', 'Dorothy', '陶宇宁', '男', 'ytao@gmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.bf45e561765d5d0cf1bb2dc82fb97f1c?rik=905cqJLx4SQbFA&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2018032308%2f4g5ylwjwpx2.jpg&ehk=MBRd72zVcwi6iLlZDAFMkx44SatN1RvTUuCyNJQMPxo%3d&risl=&pid=ImgRaw&r=0', '多才多艺的表演者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEGJPkB0dDvGKjr0p5DBLH9t0OIqbGZZbcwGEq7mee2VagfY/Yud3Eb2behZK2nF+Qg==', '152b1317-9f73-0945-0333-00ab3641d477', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('64498954-a8cf-bcf0-95db-a9386af8e5f6', '5763481', '301153', 'William', '沈岚', '女', 'sl211@icloud.com', 'https://tupian.qqw21.com/article/UploadPic/2019-9/20199522143722295.jpg', '快乐音乐创作者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEML3h8dYR61am/aLSEdSfIi94dEuQX+sWFMAjKPvRTAn6rOCv4LBMTGRmawDK8mcJQ==', '164b4a5a-7060-93e7-6dc7-caefa59f31c7', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('6500523e-baa3-7522-d27c-19f5982d395f', '2061026', '400616', 'Marilyn', '贾秀英', '女', 'jx49@icloud.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.ccd517529f704deeef333830b95ee456?rik=hOdkahR4oQCh4g&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2017080704%2frbspaog2k0d.jpg&ehk=SpZY%2f%2bDTHgbnz8%2fgu3haUWkgZVDtR0onF6e9a4xGfdc%3d&risl=&pid=ImgRaw&r=0', '美食品尝家', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEAIYRyuUyaMqGsdCqoq/e27055yZR3b/XlaQV6Fyr0s2dHN5gg17yqS66JM1YBag4Q==', '17650640-1b12-51bb-cf46-065cdf93f2aa', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('654caf20-5b95-5dc0-2940-1d087740c0de', '7325574', '967109', 'Glenn', '韦睿', '男', 'wei3@gmail.com', 'https://tupian.qqw21.com/article/UploadPic/2019-9/201992021305876662.jpg', '视觉艺术大师', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEIagKK9UtQrijGsKep/Xr5RhpzalsXeb6K2mdejKJIiG9j8jB9hwwG4z5xuOt+5XAQ==', '191d94cf-3ce9-5e56-fcfb-ff0cc4dd2352', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('661c50d7-fae2-8c7c-c0d9-38acbc861ca7', '3515344', '457478', 'Edna', '雷秀英', '女', 'xiuyilei@yahoo.com', 'https://tupian.qqw21.com/article/UploadPic/2020-1/20201821442778818.jpg', '社区倡导者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAENaYcvGvNYXHqlbjOvLe7hyjCe9bEMv/aNJ/PuffSq0whs71GhlKEYqEJekV1u+mwQ==', '1b9b61f6-ccbb-6aa1-364d-3dc6bb931780', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('68066327-fc81-67a7-e947-39b19d1ef474', '9070554', '795100', 'Connie', '田嘉伦', '男', 'tianjialun@mail.com', 'https://www.qqkw.com/d/file/p/2018/05-17/8a4976826a1b8264ea0f21db219e939b.jpg', '信息安全守护者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEMc2SsK0Yh5rwoLw4Lf/MRX8aNFdtkOm3WXb1L/F4pmd18aGwKdkV/Rw5WB/iIgi9Q==', '1bd0a9de-8f95-fe1b-feee-a9d0e2abb7db', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('691094d9-9889-6197-82c6-5fb120570f89', '4631496', '547632', 'Ernest', '蒋致远', '男', 'zhiyuan5@icloud.com', 'https://tupian.qqw21.com/article/UploadPic/2019-9/20199522222812931.jpg', '手工艺制作者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEPRPSO7jFttX/DglT2AzwjSYgllrYsHMLzKFKuZj6o72x2+FVicCPV7VU/8hzSjXnQ==', '1c83546c-b930-c5ac-d36c-374a91ba1cff', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('6a5ad723-7fcb-f09f-af4b-09a67c17c0e1', '6917282', '458926', 'Edward', '龚致远', '男', 'gonz2@yahoo.com', 'https://gss0.baidu.com/-vo3dSag_xI4khGko9WTAnF6hhy/zhidao/pic/item/bf096b63f6246b60f99bc242e5f81a4c510fa26b.jpg', '创业企业家', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEG7aFlx96WXgmY9CQxgdhPzEodXsXn28HztqCvOagxF9YGxFi2EqkpgTDshffs1VWQ==', '1e73fc5f-cc8e-38fe-24ea-3457e3a09d74', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('6a95de76-57c8-392c-6d14-016625560d2b', '5641874', '259274', 'Frederick', '熊震南', '男', 'zhennanxion@gmail.com', 'https://tupian.qqw21.com/article/UploadPic/2021-6/202161317381475609.jpeg', '热衷公益事业的志愿者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAECq5Zjk9QkyAsoxRv5fbjj7qNq0U80klAKrv416qN0+DR7Z9j95NIdwUOvaUe2XUow==', '1e8ebd16-76e3-421f-25e2-d2a9ae648e7d', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('6b0c8af4-ae60-b591-bc59-5a2687231174', '7411685', '336437', 'Ruth', '龚睿', '男', 'gonr9@icloud.com', 'https://b-ssl.duitang.com/uploads/item/201608/16/20160816101341_wQBGd.thumb.700_0.jpeg', '表达思想的文字使者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEHTvzMEiXnk04xk/B/77mXkQ52SINjw5N0ARbgoV97e8+gmyDy4TwSyd+fvJLxB/6g==', '1efde838-73dc-f6e9-dd35-5be9cbf8afdd', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('6c6bdda4-ed80-8b29-1b66-05c15a1e00dd', '2170800', '592637', 'Manuel', '谢秀英', '女', 'xixie@hotmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.ffde104688f789ba52a07dbbeb1fb6f9?rik=zASEIrNV2pu%2fTw&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2018032907%2fc45ydvqcf51.jpg&ehk=l6NlbV2FEw%2bujBqxyOtLrFRVmUgA0qVK03CPgxzF0z8%3d&risl=&pid=ImgRaw&r=0', '文化遗产保护者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEKWrb7Yp48gXb40HUhYdQQe9Dfs11VwKt7fXbImLZfyFXtZuozntt+DtX+fG3qw6+Q==', '21436dc5-6cdb-96f7-cd0e-9709bf6e45f4', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('6c875f77-70be-37e8-a474-ff15b040086a', '4032200', '263940', 'Beverly', '江睿', '男', 'ruijiang@outlook.com', 'https://pic2.zhimg.com/v2-a93505ec029715d002f7032c61155480_r.jpg?source=1940ef5c', '运动健身狂热者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEGaZatf0MdYU0WaMqGutyHB2IuFFqMjH3ek602qJ5ZxRSNbhbVL7k2CMYMXSNy54FA==', '239f2232-2109-b3c9-a1f8-d3007c3b8353', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('6dadc7ed-33f1-9173-4a84-d9716ad8ee4b', '3747661', '510859', 'Wanda', '范安琪', '女', 'anqi511@icloud.com', 'https://c-ssl.duitang.com/uploads/blog/202109/15/20210915000643_df168.jpeg', '刺激探险家', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEAzhDFVeG/yJRKVYVV+IupUKPki0ZyQGC7IVkAleo6K9c7R6R9oTC/OoN06F6XAtzg==', '23b9c542-1520-0f5b-fa0c-c0689e76a549', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('6e4e8838-2a8e-9c92-7554-72cec56e2f4b', '5229298', '675691', 'Matthew', '范安琪', '女', 'anfan@gmail.com', 'https://tupian.qqw21.com/article/UploadPic/2020-6/202061122443150056.jpg', '美丽旅程记录者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEEHH8sjN0aj1n6vqqxJF7RdZT157wHd3IiirW8BZ6bYbS56WTrYpUCdRBsgN3uiuxw==', '26bc155a-23a0-2ff8-687c-4f1aaf3b8e82', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('6e926198-0907-8839-9fd1-95b64bd6fee2', '1694998', '581890', 'Antonio', '侯子韬', '男', 'zitaohou@mail.com', 'https://tupian.qqw21.com/article/UploadPic/2021-4/202142421291793853.jpg', '教育改革推动者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEEzm3r5pGUZDOUOVd2ahARUlWnU1Eu0soQZh0fB3ymYUw4EQxYgk349pLFwtWHy+Gw==', '29097e28-e433-828a-2e45-b431c7586aa5', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('6eaf67b7-6e71-1248-478b-d6328081dfc9', '3335432', '131260', 'Joel', '宋詩涵', '女', 'shisong901@outlook.com', 'https://tupian.qqw21.com/article/UploadPic/2020-1/20201422323878862.jpg', '高效时间管理专家', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEFaT9nnIsPnjBMeZ/+Zfx7sCacUCdwVzaY3yLAy95namFwkIEGd//Y6qdne6HPo61w==', '2e675848-0492-e354-03a5-59160cc02b57', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('6f4ce45e-e04e-fc9b-6889-ed29625bb531', '9663334', '574831', 'Dennis', '徐子韬', '男', 'zitaox@hotmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.1ecc4b41c38cbaf2470a91433ad20420?rik=ajojSPu7HH9nAw&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2016102114%2fcjwjo0dsado.png&ehk=KYhZ7PQFXZe5JIpiViq5gu6sRzGV5l1lglfAqLvnKrg%3d&risl=&pid=ImgRaw&r=0&sres=1&sresct=1', '对话引导者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAELV7+pIGRr4lHk/MbfQq4yd+bupIAxcFHqIzHsilo3PfVcewETE8dM1jr/hm83f4ig==', '2f54ac12-74e7-9a98-daf4-2aab7b1339d4', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('70bc9033-50b6-3d90-cdd0-337aee28084e', '6382452', '700547', 'Theresa', '谢子韬', '男', 'xiez818@outlook.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.1defad7e92d2dc37da6268fbc326378f?rik=Xy17hbz4s7sqIw&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2017080704%2fltc33k0xfug.jpg&ehk=lGWAKQdB0ojV36wDOiXB1uEaDY3BfSU%2bbGBFehjj9Es%3d&risl=&pid=ImgRaw&r=0', '健康生活倡导者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEJo2QKdwoLo9tDtqXz6b4Jl1Erxl2LZz4IBUGvapsBmIMMk3CnP7klQw8lFDYbTryA==', '337dde44-d726-84be-571c-d1e619277326', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('7197727d-2cf9-495f-a380-15266965a29d', '1614596', '518514', 'Nicole', '杨子异', '男', 'ziyi3@icloud.com', 'https://tupian.qqw21.com/article/UploadPic/2020-4/202041521195799272.jpg', '动物保护倡导者', 0, '4f25ea99-7117-217e-10f4-92c75dd841d6', NULL, NULL, 0, 'AQAAAAIAAYagAAAAECTUBdY/XtNljojHWqw3PpXWdQwZy6gvscafd2fjc8gG8SZhV3VGbj9aBA3GGSJDoA==', '34e5aa99-b6ed-0c70-2c0a-ef67ef20b15b', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('720de2b3-663c-8d05-dd7a-f16d26edfbae', '2745406', '303071', 'Robin', '林致远', '男', 'lin7@yahoo.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.c58c8aa202aa1c54cfccbf4308495ad3?rik=ua%2fkmbLUNV47ZA&riu=http%3a%2f%2fimg.mp.sohu.com%2fupload%2f20170619%2fe1d65856a6f94518a7fba553766015ad_th.png&ehk=yzSLxo2rbsbQhle1YJiedhWIDki1gzMkrlgvvGPi4Ls%3d&risl=&pid=ImgRaw&r=0', '科技创新领导者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEOwA7fVcm7xCI23nQfhUJ30Cq38U6Q0utD/KMjPiVtzyZzVQqbJR+aSTcgKFKCPwZQ==', '35d7f436-56f0-46a6-d7cf-e9b2a008c40d', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('780a7d23-ad29-6ae7-3952-d6e295ea89b5', '2920327', '490482', 'Florence', '孟安琪', '女', 'mengan@yahoo.com', 'https://tupian.qqw21.com/article/UploadPic/2020-3/20203913252377592.jpg', '经济洞察力的追求者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEOI4GHM/zHrYzGPXnmOjK16MxqNOqi/iXctvLucswznDXGTKj2cqJ4p+S2in8k394Q==', '35d86fe3-bd1d-b097-ebea-a9ef3e816da3', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('782ebe6d-2fdf-e26d-ad65-d533b14a53fd', '4401108', '558487', 'Louise', '田詩涵', '女', 'ts521@gmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.1e16342a0226d6908fddbe0f03f63e31?rik=roBni43KvEDIYQ&riu=http%3a%2f%2fguangyuanol.cn%2fuploads%2fallimg%2f190615%2f103S45225-14.jpg&ehk=e%2bNrwWccU1rde9hpdAOhwily3uc0vAVBlfP9%2bAAj%2frI%3d&risl=&pid=ImgRaw&r=0', '欢乐儿童陪伴者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEBRiv9VA90fbPNbSpjWqOMrE9TP8WWlPLUqvpXWKTKXaxk3adiKHD/RKAwK7ijgm5g==', '3607f183-5d63-305e-11be-baaf81ac429f', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('7870aa88-69e8-325d-d4e9-ba50369ac675', '8808368', '236874', 'Wendy', '傅嘉伦', '男', 'fuji7@hotmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.20b58c45dfdae95438c863edb7ce5e5e?rik=PUFavxFWDq5EoA&riu=http%3a%2f%2fm.keaiming.com%2fuploads%2fallimg%2f2020031311%2fjb30kpzbgwx.jpeg&ehk=Lb%2br%2bxczOIQPTXjP6R9Y2ZMjyjhn1FtVudY6bJlHQi0%3d&risl=&pid=ImgRaw&r=0', '人际关系建设者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEBC6zqSHoj/tjLuxppwRH57y40cogCWMiGc7DiXbPzMY+LixUpOZpq7whxvwT/ZGiw==', '3641d612-1cd6-9dd7-a99b-ab29466ef1e8', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('79167cf2-38c5-ef41-95b9-00bb4e4d9b42', '3193807', '169400', 'Frederick', '孟杰宏', '男', 'mengjiehong@icloud.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.093d00fcf138ddf391fbfa5a04d8e34b?rik=sC8jKkdU51Ppiw&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2016060813%2faxnyljdo5ci.jpg&ehk=wwRJJoH7aznBcamnJM%2fo%2bLKeWoiNYeEeE2sKvaE8Dqs%3d&risl=&pid=ImgRaw&r=0', '大自然的观赏者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEAfI4BQKKa2vSNMkQsC+Syk5jf0K66F+IkvX8KomjBtAejvwZ7D1ejXyeYhqk6261w==', '371b13c4-8e92-f0d9-dfdb-f5b6ce555736', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('79893f5c-1468-9ff2-e0b2-f98e691d6ad2', '2458576', '277134', 'Steven', '曹子韬', '男', 'zitao10@mail.com', 'https://www.keaitupian.cn/cjpic/frombd/1/253/3302325559/2682555120.jpg', '心理健康宣传者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEHcdnkTcg4jExivH5KgT0wDRT27QVqKE0aVfEHyT9xZkHJ7YdNkrdcnGJbDQxcvvjA==', '38b5a50d-79ca-58fa-caa6-d835220b1b0e', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('79ab3f16-3d66-1ab9-7059-627e242b59dd', '1872698', '925086', 'Elaine', '袁秀英', '女', 'yuanxiu1956@icloud.com', 'https://i.hexuexiao.cn/up/10/d8/8d/03e18955d7d9b160a082faa57b8dd810.jpg.300.jpg', '社交舞蹈爱好者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEPAsol81I2eZu6fuF2idZMklg1e27jyeKBhyWkDjoWtNUrmwsMNyLsHw/VjTqQPIjQ==', '39bf3b24-bdac-cb87-e6f9-c8d3a8c9b393', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('79d81ab4-52a5-bb5c-9f8a-ac98ac4803e5', '7204879', '817135', 'Eva', '程秀英', '女', 'xiuyc530@gmail.com', 'https://tupian.qqw21.com/article/UploadPic/2019-7/20197271732132099.jpeg', '创造美好生活的灵感源泉', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEC3BUA1n2aJGVAKVHKIKMnjYcoykmDGeMdRftomgTd0coHjhQI9JVoTGxXfmqKQ3WA==', '3ac852c8-b23f-c4b6-d900-4d19cd3d3fb9', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('7a1687b7-d485-c99e-013f-835545c06227', '3136904', '210728', 'Thomas', '严子异', '男', 'ziyiy@mail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.19e3a39608ba842fade7e9b217282629?rik=gpnfIqrtHyAQjg&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2016060813%2f4ykumyzgqdp.jpg&ehk=i%2foSVznO%2bFq6l7BJXsi%2fNYM76853QCe6fp3GVPSIKfs%3d&risl=&pid=ImgRaw&r=0', '文化交流使者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEDm+g29j0m6FKrFGtRb4jX330U061bcWezOnld9CIFcmdWuRskXRnSu4VjC81dCFPg==', '3cad8978-0ea2-414d-abe4-ba93e0e52116', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('7b0571a8-6271-a067-8ac0-d64743386096', '6188052', '866102', 'Barry', '余杰宏', '男', 'yujieh3@gmail.com', 'https://tse2-mm.cn.bing.net/th/id/OIP-C.j0e3_ArBlmq2JYF4yeNFbwHaHa?pid=ImgDet&rs=1', '音频内容制作人', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEPYDOXwzCZLkvaRJLBI9MLxLr2pz19HRXHe+NzOSU8b8cyNV8QdRpNze2mia5g9cbg==', '3d2aed1d-70c7-0d71-a3c1-6ea06710c2e4', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('7b3bb4f6-9246-5db1-7501-23c748548bf4', '9333393', '454791', 'Charlotte', '严杰宏', '男', 'jiehong1225@outlook.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.de5609898e81608d2f13cc05678456d1?rik=J0lMLkv62QQvtg&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2016060813%2ffqbd5h3grwl.jpg&ehk=HtbtiZuYEJrPTYi9ZcaSnR9oOTrAiDhwPcE7JlJpQ4I%3d&risl=&pid=ImgRaw&r=0', '互联网安全专家', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEKGZDbGLLYCjwh32vHM7SiDgyNDhKI+bsQHoKMlnbwgrqTA/sSV1fMn6v2IkUYRYnQ==', '3f6bf717-84ef-ca5b-8bba-fc5271e7eae8', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('7b7bdcdb-8016-6524-232a-21c20248b789', '3392754', '928452', 'Evelyn', '田詩涵', '女', 'tis3@outlook.com', 'https://tupian.qqw21.com/article/UploadPic/2019-7/201972717315710577.jpeg', '手工制作艺术品设计师', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEBhYzG7AaybwmnGs3/MOgtIqgv2+ydqqt+wUlSUQ04vuJznV619uzxsTpw273XiYwQ==', '3fb4dba8-a89d-ddd2-ed97-dd3df5b91a39', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('7b7cff58-56b8-986e-0221-072daac7628a', '5552678', '567716', 'Jimmy', '魏晓明', '男', 'xiw@outlook.com', 'https://www.keaitupian.cn/cjpic/frombd/2/253/2609850384/1791214564.jpg', '创造有影响力的内容制作者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEJw+Ovk84Ma7KxfyrTGyo5a7M+/2q3PxnSh/CAsZnsCG4v/vB+3rsJeJgo92sLqcsg==', '3fb874b8-20b6-4393-f839-2ec34f7736bc', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('7b882394-fa1e-2e81-3c2f-b1ee72fb2679', '6748872', '283488', 'Kathy', '梁璐', '女', 'lu5@mail.com', 'https://www.keaitupian.cn/cjpic/frombd/1/253/1296397004/126809707.jpg', '水下世界探险家', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAELS8+UdnuaEgi4OBRtVsZ3f+aWH8uIiuE1U/lPtkP7EtcwG0XFrewmSXGXCnD10Mcw==', '40152ae2-3960-47a2-35cb-6c4d2c52e814', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('7be65f48-80ba-4e0d-abe5-41e2ced143c8', '4831720', '859873', 'Clifford', '冯宇宁', '男', 'yuninf@outlook.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.4d464c3425ef3897610a6035baa357ac?rik=BRd9pEslBx871g&riu=http%3a%2f%2fimg.biaoqingla.com%2f01%2f081812440517102.gif&ehk=u9GNpvDrGCbUcdiZlxNczUvNKevdcNsO57bGyA4qyTo%3d&risl=&pid=ImgRaw&r=0', '社会公正倡导者 ', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEMzjLn0sLhDBb3KPtqxkttQDr0f+N5LVqVTkotbNCXtPATkRVaZS4baI79Y4eAROCA==', '41b70097-c1dd-fec1-9082-6f49769c03cd', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('7d96f0cd-4d73-7bf2-baee-7f1483d6fb11', '7403803', '624806', 'Amy', '黎宇宁', '男', 'yuli@mail.com', 'https://pic4.zhimg.com/v2-c52cf1a76f72e475eeac77ea7227cf67_l.jpg?source=172ae18b', '健康饮食倡导者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAELgs2l5GOw+p7yvFEIWzv2qLbqxb6Vi5iqWIwxzHzKsslyKqEeGPvjeLJ8c6iKe6aQ==', '41d9083b-a2c0-df1b-6f97-c37ae4b2bce7', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('7e6cb620-c698-f85d-5152-8874b2672080', '6801152', '140959', 'John', '顾致远', '男', 'zhiyuangu8@outlook.com', 'https://pic1.zhimg.com/v2-8afc6987373c1b0fb5960101c34cc5d3_r.jpg?source=1940ef5c', '热爱编码，善于解决难题的年轻程序员。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEM7+wcyXWZAH9Y/2Gd+/0SBtnqctt0u840iGvcInoC4fUu0BSOkOg27OF4VjezwSmQ==', '42d97ec0-9569-8e08-bb1f-493508acb3cb', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('7ec47520-79cb-8b96-81e5-43ebc02a4aae', '2035702', '325280', 'Juan', '李秀英', '女', 'lix829@gmail.com', 'https://pic3.zhimg.com/v2-40b422221d65c72d5a2ac80b7f9907be_r.jpg?source=1940ef5c', '对计算机世界充满好奇的初学者。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEPITx0KE4UGUJN1+ar/WrXSAtY10kfynRyrQcLjonUhg8wZNBac8fgjTQ35q7YKlTg==', '42e3371e-d7e8-0bd7-ae5f-d283722f415b', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('80e94c3b-ca99-4126-9438-86d411e1c960', '4334193', '448465', 'Stanley', '贾致远', '男', 'zhiyuan729@mail.com', 'https://pic1.zhimg.com/v2-05bcca632cd165dc842a6ab26a24c569_r.jpg?source=1940ef5c', '奋发学习，梦想成为顶尖的代码工匠。', 0, '4f25ea99-7117-217e-10f4-92c75dd841d6', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEOC4K1MmxieB6lVDR0E4F8wOfczCg2gBS9p6lbsyW2MJxQry+3YgbkhmEUgtDoWsxw==', '43509e31-a1d6-acdc-d4b6-4d7e403f5eec', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('819a0f43-4936-7b84-35e8-37fa84a8f5b2', '3109357', '292401', 'Clifford', '金云熙', '男', 'yunxi2016@icloud.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.d4dc82d27a55e21c6f7f881847f40f86?rik=d1p%2fOVnpSY3n6A&riu=http%3a%2f%2fimg.wxcha.com%2ffile%2f201910%2f19%2fa633ec2db0.jpg&ehk=GPXr9QyUpXdeK7tPxYGGn696QNRlpdUdbUb9asLGw5A%3d&risl=&pid=ImgRaw&r=0', '对算法和数据结构着迷的编程学徒。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEHIOE9Hxg4+CQJb/OTVURX0deJNge6epfb2jKTylhEYbN6pThzx5zBiV1Qg3XZXSLA==', '43b2bc57-a400-2f1e-f7a0-420b94338023', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('81b9e8b7-1db6-b216-d7e4-4f83eae386d7', '4840318', '802644', 'Sara', '侯璐', '女', 'holu@hotmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.674b43603a8109c9883a6267d7f4da0e?rik=TEu0qfXNMQ2y7A&riu=http%3a%2f%2fwww.bluefriday.cn%2fupload%2fheader%2fR674b43603a8109c9883a6267d7f4da0e.jpg&ehk=h00ikQUyasyA3dOmr22q5Uy8e%2bWfdHlkdFf%2fnPj%2bI%2bY%3d&risl=&pid=ImgRaw&r=0', '追求创新，致力于编写高质量代码的工程师。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEJMdpPAo4v5PJNVLTpqqKbz+keP623I1TgrMv6fgNAOvZtnd2niKq+Bsg7S8d+IJEg==', '447edef0-b255-e1f5-c1dd-354cd20a33fd', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('81f41193-5411-1a9b-2163-d5d2b750f976', '8168353', '726429', 'Billy', '常璐', '女', 'chang3@yahoo.com', 'https://bpic.588ku.com/Templet_origin_pic/05/34/66/70fa542234646ae7a2cac20bfea68e35.jpg', '渴望掌握各种程语言的技术探索者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAECFPmKIvQPFS/UmmRT345gcnM4KLm96h5ihWC2w5fqgNyxTkyQP9NZ+9gTD7Y/3nyg==', '4632f9fc-ca02-8703-29c2-315082b52aed', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('823e7e03-5d41-4307-c7ec-03f00195d435', '7675768', '349655', 'Jeffrey', '莫子韬', '男', 'mzit@mail.com', 'https://pic2.zhimg.com/50/v2-ad8abafd42d7bc8164474d0e0e75cb23_720w.jpg?source=1940ef5c', '热衷于编程竞赛和挑战的年轻开发者。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEAHYBBxdU27PuzreiQ842tKJ5ZyV1e8szdeMmeubs96fsiMcyYHJUWh92vPdgIsKMA==', '46b1fc9f-bb04-4eba-739f-2426753bcd83', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('82ed3da7-79db-416e-ac1d-ca048d6432e0', '8016441', '130974', 'Shirley', '郭杰宏', '男', 'jiehonggu@gmail.com', 'https://i01piccdn.sogoucdn.com/4cb880d62f9b852b', '努力学习，将编程应用实际问题的学生。', 0, '4f25ea99-7117-217e-10f4-92c75dd841d6', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEKpad5wn0Dtg8Ckv9slfNdNOacXMtUGiBAn5lEr0wndG3WNbcjezwUB8llNQLJAkqg==', '46cf425c-52a0-9e50-70db-d5ced0f8cf10', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('8471cc46-388c-2f6e-afb2-b30eedc2b006', '9991431', '107278', 'Sean', '宋震南', '男', 'szh@gmail.com', 'https://pica.zhimg.com/50/v2-aab9e5e48f806b2a42f66744b94fe97a_720w.jpg?source=1940ef5c', '拥抱开源，乐于分享技术的社区贡献者。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEBjvs3r98c+Z/ts1VBhfMjDa7lF6bc5e/YA2GoN+rvuarF/3UNpet6YW46d3aRvAEg==', '48f415ce-8b6d-fe7b-0ee9-708025d875c6', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('8475cc74-6013-b3cc-3029-c00203f1fe18', '4216726', '464171', 'Doris', '王宇宁', '男', 'wang128@icloud.com', 'https://tupian.qqw21.com/article/UploadPic/2020-5/20205718202349665.jpg', '具备前端与后端开发技能全栈工程师。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEGYkuSgrRVQ3hZBxgKenFLvNAJkdPYaiykhTbuUYDD6ivc7AaOpNnWaXKdMx40e7ug==', '4a70c5cd-6b00-9568-f591-fd8e24774796', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('84cbbbde-e67f-1e6a-12ac-7a917337783e', '6237070', '962053', 'Christopher', '蔡睿', '男', 'cairui@yahoo.com', 'https://tupian.qqw21.com/article/UploadPic/2020-3/202032619535849558.jpg', '善于团队合作，追求卓越的编程爱好者。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEASYxmpnzPDSjAtDsR/BbMpHNkJEs5vUJqp4LDpvAAwzKvfWLvJJLINIeueNKLpSmQ==', '4b76d2bb-2c01-7959-b285-4e000cd4e3fc', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('86dd22cc-a931-5d7a-2563-d30c245e16ea', '5274492', '765099', 'Louise', '蔡睿', '男', 'rucai2007@icloud.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.c3fa8af7d6bc222bf43f58c25a763e50?rik=t99X%2bqPUOnOEEQ&riu=http%3a%2f%2fq.qlogo.cn%2fheadimg_dl%3fdst_uin%3d149748%26spec%3d640%26img_type%3djpg&ehk=vwjkeWTugAuG6AaSfJ8qMLFFsgC5YxeIn7kB%2bPRtl0A%3d&risl=&pid=ImgRaw&r=0', '持续学习，追求个人成长的编程新手。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEJBpETzxpaeDmm/9vAQSQ4j7KGz0eiSNkxvOW5V0ixBD8t1CWWnFJqzbH6GwA8ZnEA==', '4f50f2ef-f0a2-504b-1427-b3a1e966e689', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('87da5976-25e4-13aa-8c97-58c5f8b20c65', '9006912', '777371', 'Marcus', '廖詩涵', '女', 'shihanlia@outlook.com', 'https://tupian.qqw21.com/article/UploadPic/2020-2/20202721484730319.jpg', '熟悉多种框架，擅长开发创新应用程序的工程师。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEI7vJwkBaZijMqfPNPFb2MPrY/h33Irti0Jevw1YlMAMk9vGj16s1bQMQcFRhxqT1A==', '4f921b1a-8cac-4fd3-b299-c7d49673787b', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('883c3508-d483-2300-fd2f-a009b6347dbb', '9239270', '314528', 'Keith', '薛致远', '男', 'zhiyuanxue201@hotmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.158e9d0074d651aa5beed5f0be279a08?rik=upslQ6DOYpguWw&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2017112908%2fqf3auqyeyv0.jpg&ehk=UqAJVxEwnUxyE7d6F2ugSZ4I7V6MlfUhhW%2bKX8Ktcq4%3d&risl=&pid=ImgRaw&r=0', '对人工智能和机器学习满热情的编程学子。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEICDZ6RAJj01UNWIwyTYshmw1SMA1Nf7Nw8TFFGU+RcOkYJMJcU5s8khaTFi8FitEw==', '5043d444-d09a-3a57-4331-c4345e0461fc', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('89672054-0010-8233-927e-32bb9c69e85f', '4317639', '583016', 'Roy', '郝杰宏', '男', 'haojie2010@outlook.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.158e9d0074d651aa5beed5f0be279a08?rik=upslQ6DOYpguWw&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2017112908%2fqf3auqyeyv0.jpg&ehk=UqAJVxEwnUxyE7d6F2ugSZ4I7V6MlfUhhW%2bKX8Ktcq4%3d&risl=&pid=ImgRaw&r=0', '追求代码优雅和辑严谨的程序设计。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAECA7RrraGkIEDJrdubkr1TFmOVypRdncBLShMP6SGxsVOAEPNnl4uqLNDxj18trp7A==', '513471a1-5db1-2107-e900-2c5da9aeaf2b', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('8a1ba186-c68a-9b7e-73c4-bc7abe623f60', '3143283', '527920', 'Kathleen', '孟子韬', '男', 'zitmeng@gmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.73da01d59591ffc003ce364cb2e61ad2?rik=dNzEj41H2j%2fc9g&riu=http%3a%2f%2fpic.616pic.com%2fys_bnew_img%2f00%2f02%2f69%2fPagY3lDHZh.jpg&ehk=qx984xDQnOaeGGd4fGIoPwfsk6mt4Rcz9M9%2bDn1NSag%3d&risl=&pid=ImgRaw&r=0', '喜欢挑战复杂问题和算法的编程好者。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEDKzRenXU9j+9wp9kz10Uc50+gnodH32FhhaKW1TFpp7JZRKMNhZSOt3HLLDJ2WKQw==', '51696ba1-54a7-a260-2b63-0ad79e0bfba5', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('8ba46336-cf78-ed45-576e-87a2cd6d1e39', '1399038', '938328', 'Emma', '林安琪', '女', 'alin@mail.com', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fsafe-img.xhscdn.com%2Fbw1%2F13015461-78a9-48cd-be43-6ec4594116cb%3FimageView2%2F2%2Fw%2F1080%2Fformat%2Fjpg&refer=http%3A%2F%2Fsafe-img.xhscdn.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1691677229&t=16d23cf5bf5388459f9b1443d1b3b5cc', '研究新技术，不断追求技术进步的开发者。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAED7mPBc9cHbAkeiFNVl1a/ZManioHbfsWW34TG8Hr1BTS9Fbom5VqGPtA/NfWyQqNA==', '53dc929d-54c5-2d48-cb9e-3853391ddf57', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('8c4184e0-56b4-3679-ee69-001531640aaf', '9994270', '555043', 'Louise', '戴嘉伦', '男', 'daijialun5@gmail.com', 'https://www.qqkw.com/d/file/p/2018/06-07/f54e106c3eaa256005228b93f2bf77cd.jpg', '精通前沿技术，投身于科技创新的工程师。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAECYG1TGex/PF5fXkI6Lg4TlQOX2hTRSjkMwVdib9xhzIRvYwmntUJEQ9Hg9fM+FDPA==', '54b8bf10-17a8-4ad2-e4b8-5973d03168f5', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('8e73cced-d51f-407f-aaac-14cc412a4fe7', '1225687', '824732', 'Jimmy', '卢詩涵', '女', 'shihanlu@icloud.com', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fsafe-img.xhscdn.com%2Fbw1%2F1ed56eab-f225-4c69-9a2b-2a92050caa77%3FimageView2%2F2%2Fw%2F1080%2Fformat%2Fjpg&refer=http%3A%2F%2Fsafe-img.xhscdn.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1691677229&t=816ecdf2c0093171fcc60decbbf1046a', '崇尚代码工艺，用优雅的代码诠释自己的艺术。', 0, '4f25ea99-7117-217e-10f4-92c75dd841d6', NULL, NULL, 0, 'AQAAAAIAAYagAAAAENve/gPbkGzWBOOOFmytQzierLIgmWd1wwDSym619WlQV/NpYAWz+loDAEea0vIBWA==', '55f63efa-b970-4d3c-875d-9648dff3c68b', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('8eb32118-2a5b-1964-6210-1c92b17694e0', '6572359', '413856', 'Heather', '刘晓明', '男', 'xiaoming2@gmail.com', 'https://www.qqkw.com/d/file/p/2018/06-07/f54e106c3eaa256005228b93f2bf77cd.jpg', '创造力无限，充满创业激情的编程创客。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEMSVJgzIfnpU2k8WL+wPF2hQJ8tV7+VjjAHGHMhWW+5uCvKxt/7lS34zw7Q3JQ/l4A==', '57c23f65-b9d6-6f6a-ea6e-9c8fa97fe909', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('8ebe595c-130c-51f2-db49-4fe2233bbdef', '1823107', '174206', 'Anna', '武震南', '男', 'wuz@gmail.com', 'https://www.qqkw.com/d/file/p/2018/06-07/f54e106c3eaa256005228b93f2bf77cd.jpg', '学无止境，一直在寻找新的编程挑战的学习者。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAED99Syltwpr4E4GT28mRxF6rnI2hPViamPUai3pEj+OKLNnONX7UbdpL+Slk9hU0Eg==', '5805bb5e-599b-d5d1-538f-cbfe2125f82a', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('8f241ca4-07cc-588c-b9ed-d04dc191d7c2', '8662808', '939811', 'Larry', '何璐', '女', 'lu1@outlook.com', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fsafe-img.xhscdn.com%2Fbw1%2F650645fe-f05e-4b2c-8f14-46a39d979c9e%3FimageView2%2F2%2Fw%2F1080%2Fformat%2Fjpg&refer=http%3A%2F%2Fsafe-img.xhscdn.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1691677229&t=50f31b0c6a4d2b038d6fed4de3280b16', '对安全和信息保护浓厚兴趣的黑客文化爱好者。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEFuhnJfRpfKthHA1WfGK5abUz0yzhfyhPLF3GYHrmn/Ea1UsGzJ5WdyhcEm9H5Tq6Q==', '5a27e110-ae26-0fe6-5137-4325f089e69a', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('9019c28a-ad1f-9907-acec-96b17d323faf', '2758887', '265095', 'Randy', '金安琪', '女', 'anqijin5@mail.com', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fsafe-img.xhscdn.com%2Fbw1%2F002f4ff5-2e4a-4ab1-866e-dc2b8da02e37%3FimageView2%2F2%2Fw%2F1080%2Fformat%2Fjpg&refer=http%3A%2F%2Fsafe-img.xhscdn.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1691677229&t=83137990281d15abcc6214a1f21b167c', '喜欢解决实际问题并开发实用工具的编程爱好者。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEC36J75GNRXlLTzFMdpSUo2DzyxzSJbWc1zpNZ0lNNKarZNjJkRWnyjhaZVXkYZIhA==', '5a6846e3-4e92-decf-5f50-ed7cbb387508', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('914bf30c-0524-e6e3-5530-560d7fbfe0a8', '2500345', '637018', 'Carolyn', '贾晓明', '男', 'xiaoming4@gmail.com', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fblog%2F202112%2F01%2F20211201222626_a6b6e.thumb.1000_0.jpeg&refer=http%3A%2F%2Fc-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1691677229&t=5403dae4f2cc374ae16ec59477b3735e', '热衷于移动应用开发，追求用户体验的移动开发者。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEGMdrRvrhN1rNqIWV4yzVOxKWQ62S76HHA4d2SAS6BPvA1WVwt7rXJEd7hI0c9UH6w==', '5aaf98bc-0a5e-3842-9fb8-073da93f60b5', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('914e033a-9255-22a8-0d05-6aa0621eb92a', '4774011', '884426', 'Randall', '彭安琪', '女', 'anpeng@hotmail.com', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fsafe-img.xhscdn.com%2Fbw1%2Ffeb45799-d6fe-4253-964c-b22cc119f56d%3FimageView2%2F2%2Fw%2F1080%2Fformat%2Fjpg&refer=http%3A%2F%2Fsafe-img.xhscdn.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1691677229&t=00d0619c78182f2d0f88057da6fa97d4', '使用代码塑造未来，追求技术影响力的工程师。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEE9xqHMrufBQ0O31F0GyXJUghSshBYbCT4ZidsJZngGb6/kdTjIrS0Y5oiDvjLdDpg==', '5af90d29-5f3a-e93d-8b7d-e694aeac6143', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('91f06e03-0565-c5b9-3331-2a4f19c195ca', '1695696', '927715', 'Ricky', '姚晓明', '男', 'yaxiaoming@icloud.com', 'https://pic4.zhimg.com/v2-e0a37d9359f11922993a591a0a485177_b.jpg', '善于思考，喜欢挑战自己思极限的编程学。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEEwfAR3w4DBBimdmJrQP6ZlbSTzv3+uh67yQ4LcXnAnd5JqcKjAsHBvX0cqqX8Tukw==', '5b9f1691-062d-4cb2-3865-b8506eff83f8', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('92354232-ea1f-fe57-6f95-3d1bfd50d846', '9022853', '624400', 'Mario', '梁秀英', '女', 'liaxiuying@gmail.com', 'https://pic2.zhimg.com/v2-830bdefd10540c83de80ca56a8acab4e_r.jpg', '探索未领域，运用编程技术创造新的可能性的创新者。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEEUoBOgNYxb+j84FJ0NOMWnRahiXj8JdR833Z2CJofsOG9RnedLVgrOmvRUR43KKzQ==', '5c7702dc-e9cc-d8e5-51da-205839aa9140', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('92c6f0be-ab80-78db-2926-608d44ecdb3d', '1551635', '415507', 'Frank', '陆秀英', '女', 'xiuying10@outlook.com', 'https://pic1.zhimg.com/50/v2-6896016940fba4fe111b00972849924c_720w.jpg?source=1940ef5c', '大数据和数据分析有浓兴趣的数据科学爱好者。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEC1u4fnM7/q47W25LG7981IZ5GhKwlgrEKliZgVxWPYQgPCuhV+rWFlCkniV+a+uiQ==', '5cefe80f-cecd-8067-0cdd-6d1d9cdcfdd0', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('92f72ee4-ed14-0ee1-2d06-690fd673afb1', '7293292', '684268', 'Albert', '郭嘉伦', '男', 'jialguo@gmail.com', 'https://pic4.zhimg.com/v2-db9a0643bf960bcdf8b2f2bfa5216335_r.jpg?source=1940ef5c', '着迷于物联网技术和智能设备开发的编程工程师。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEE2seoqD+bhNRyHMYBi/Nnc8uYzpkNE/YlO7H3Wjd3atjx+aIT7pUnAa9dzR6PUDpg==', '5ebc906b-cbb2-e41b-6213-234dab4507c1', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('93883b55-66a5-09a1-d131-e19f3e19949e', '5352416', '986937', 'Rachel', '朱震南', '男', 'zhu10@gmail.com', 'https://www.beihaiting.com/uploads/allimg/141106/10723-141106221233O7.jpg', '强调代码可读性和可维护性的职业程序员。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAELlfxMEo/Q+PtrvILcet9T9/P0rql0OOKPHfp305xQDmeWNwXSLAVxDEJTdHZsz2hA==', '6196a09e-7554-3824-85cd-c6e627641010', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('93ae29fa-4cfe-3cbb-36e2-5f70fc0c0865', '2603591', '387029', 'Stephanie', '苏宇宁', '男', 'ysu10@gmail.com', 'https://c-ssl.duitang.com/uploads/item/202005/15/20200515183914_hvjou.jpg', '热爱开发游戏和娱乐应用的游戏开发者。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEF7Ip0R2R7GkaXVM/lMxS/9jd2I1JVfKYtnRMpYa7RFKk/phFjMLxx22BpJI9ROTlA==', '65d7210d-76d0-497e-9e74-ac584f28141a', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('95cfcadc-7e46-bc2c-e010-ced831c01e6e', '9230045', '534572', 'Juanita', '朱秀英', '女', 'zxiuy1218@gmail.com', 'https://tupian.qqw21.com/article/UploadPic/2019-10/2019102919524965050.jpg', '学以致用，将编程应于跨学科领域学术研究者。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEP3zBbOqFg+9nu4uzEOQ/uqYYzbpLfRAuWZgMUU61undTDXI2afackIlG2exYh8i3Q==', '6604f422-2b0f-3c09-6437-b986075ee2e0', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('960d11c4-f45a-3727-a335-9e37009757da', '1379359', '463995', 'Ricky', '汪子韬', '男', 'wz1979@outlook.com', 'https://www.beihaiting.com/uploads/allimg/150619/10723-150619103102E7.jpg', '在虚拟现实和增强现实领域探索新的虚拟现实开发者。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAELfcA1R9jBOkj3iOraDdGJ7XqNuD+5STweSARy3V8kiKWmLYjfrJ0/bwjMyknreMQw==', '66e62b27-3217-9ea0-dda9-a18ceaa05b4f', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('97a4ffca-eb5e-123f-96f1-7ba0812096dd', '3482713', '355331', 'Scott', '宋睿', '男', 'ruison2@gmail.com', 'https://tupian.qqw21.com/article/UploadPic/2020-1/20201721432581308.jpg', '热衷于自动化和机器人技术的机器人工程师。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEBXwGby6eWfSembb+AvOcpITjUVFJz3ZFbfbmaReqcX8HIpUlZKVcSo75HdCJfh3IA==', '674bfc1a-341e-3a66-6658-f1348e20ae0c', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('97b09f81-dffd-7ab1-96cd-76cfa15a0f40', '5057214', '528919', 'Janice', '彭子异', '男', 'pengziyi4@icloud.com', 'https://www.beihaiting.com/uploads/allimg/151205/10723-151205135334325.jpg', '着眼未来，充满创意的编程科技创业者。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEFKXFgnNjCa/cVkJxMAplRkAFzOTMFeIroX0rf+4LmCnV3Y61hPaQCMepResc7XyFA==', '67ebe7e4-9102-ae6e-fd43-218bfc8912cd', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('97cf3e06-de97-a8c9-4b64-151bcca62356', '6979396', '436192', 'Bernard', '龚安琪', '女', 'gona93@gmail.com', 'https://tupian.qqw21.com/article/UploadPic/2019-10/2019102919524817922.jpg', '爱折腾各种开源工具和技术的技术极客。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAENaSkUW3mt2JigBbKSz/KLoFLiVwDRiPRTQsHlDzSzpbQUMrmnUpPbKqyIp8DDCXDA==', '69e92a4c-cff8-13c7-c85f-835216e87b7a', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('9819e3b1-8d80-596e-1836-762a966e0c39', '4190818', '473931', 'Kevin', '任致远', '男', 'rzhi@mail.com', 'https://www.beihaiting.com/uploads/allimg/150612/10723-150612114Q5102.jpg', '倾心于网站和应用界面设计的前端开发者。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEAJKswuks6lLlI2+EYQKpUiyUp3IuCMvU08XWnhYtssjSA0ZLEuWKfED00H3dkZRBw==', '6b07693a-9c63-c3a4-d5d9-e845b2aae63d', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('98a48c69-8e25-ff64-af18-a7e0fe01d432', '7146278', '346055', 'Barry', '潘安琪', '女', 'paanqi505@icloud.com', 'https://tupian.qqw21.com/article/UploadPic/2020-1/20201721432758197.jpg', '远离代码洪流，独立思考的代码哲学家。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEJTZ7UzErxWttDKKOg56uQb1WUArI2GXgan+jk2Kb6Y8yH7hghJxYRjxae7LshwHug==', '6b7a3762-5e78-c3e9-e6ae-39973c7be76f', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('998b4238-6fc4-d3a6-e1d5-a9b4737bde48', '6488853', '472256', 'Billy', '彭晓明', '男', 'xiaomingpeng85@icloud.com', 'https://pica.zhimg.com/v2-43e9d8d7180679add663e097fcd39887_r.jpg?source=1940ef5c', '对人机交互和用户界面设计有浓厚兴趣的开发者。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEObwDZ/YfyXeeqLr9/ioeVDobrKtrCy43sOokinymKRI5Jj7qIPMP8nKfVKQq03CPQ==', '715ecaf6-9d7f-7f56-98a4-2fe65d5f1f3a', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('999925e1-db64-8f3b-554d-0fdfbff3e22d', '5944525', '943628', 'Martin', '蔡安琪', '女', 'aca45@outlook.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.eb220d66e62c51ec414220deb3be9b07?rik=IfYCRVrPQztF9Q&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2018010109%2feswfv3tx0fg.jpg&ehk=UHmQm3t5Z3BV9EYEu90cQaqY9b7tumvmD29uUPS9ZYI%3d&risl=&pid=ImgRaw&r=0', '着迷于3D建模和视觉效果的计算图形学爱好者。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEBHJ5FDkYiN/nlndJa/s+7t1dSS/QHIM0SUJLEowrlR6e/R91Ikmt/o+iYRkvDS2+A==', '724b523d-f6d6-510a-03db-4f15a4ad645b', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('9b040a4e-83d8-7f6a-cca5-2950743447f6', '8710352', '884335', 'Kevin', '邓岚', '女', 'landen1@hotmail.com', 'https://p.qqan.com/up/2021-4/16185426982905271.jpg', '追求代码效率和优化的高性能计算工程师。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEFYbBiuJVj0bUnmU4VxLwdYw2E+S2uMYlyFV9gm4Ybp3KdPw7eSBfxH2FhrPcDHzhg==', '72aa9750-553c-33e8-6742-bac8c1991012', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('9ba3f4d6-d776-eed1-d162-41bb28e70735', '8837937', '249774', 'Chad', '叶云熙', '男', 'yyunxi53@icloud.com', 'https://tupian.qqw21.com/article/UploadPic/2019-10/2019102919524817922.jpg', '对云计算和分布式系统研究的云架构师。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEGWo03NypfSmgbsUqOU89wBGJ9MgBIYtqmAZikdWTM/DzkNLNErYnWk6fQCuR3Z6aQ==', '73fbc9ff-2ad6-d1b4-c91d-6ba2ff11c6e2', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('9babd805-ff45-7e4c-bf78-b2fcd60cad5f', '6510429', '653828', 'Hazel', '贺震南', '男', 'hezhennan924@gmail.com', 'https://pic2.zhimg.com/v2-bec3ffde79ed5b82c10d4b2b5e91f01c_r.jpg?source=1940ef5c', '善于沟通和协作的全栈开发团队的核心成员。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEIGIA8LMmObiXcMQB+rQWG4QtZWDM9Depyp1pl8giBvORCyrFGdT9n/V7YS8/2ZB1A==', '74bf9fde-06c4-25b0-b1af-f98b102afc8e', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('9be8c479-fb67-b5ef-ca24-cd6bc249fd99', '4553394', '601597', 'Aaron', '邵岚', '女', 'lanshao@yahoo.com', 'https://tupian.qqw21.com/article/UploadPic/2020-1/202011617124735252.jpg', '前端艺术家，用创造性的代码令用户震撼。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEGVwNbtePVjOaxCucagbsxh1iCTSm9YcFq42zr+DG7RA7E7xoRV9Jo250EDoSrHnVg==', '7602359b-523c-d0fb-5c64-67513e00a04b', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('9d5a8226-ffbd-f431-5679-e8dfc6c64147', '1585935', '723482', 'Albert', '马詩涵', '女', 'mshih@mail.com', 'https://pic.qqtn.com/up/2018-1/2018011618124145743.jpg', '热衷于人机交互设计和用户体验优化的UX工程师', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAELmD5VnG26K1Xhlm0/BMv5WICuf6FEnMGTh5qf0pxD+1Ijn4kzLnRMfgOQ60Tdh2yQ==', '770fb319-aea1-2ac7-ff66-7e1712245985', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('9e24bf24-935f-d0f5-dfd1-59025de90fcb', '6382668', '106424', 'Esther', '彭子异', '男', 'pengz88@yahoo.com', 'https://tupian.qqw21.com/article/UploadPic/2020-2/20202112213816362.jpg', '对自动驾驶和智能交通技术有浓兴趣的工程师。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEHmVnbVgwMHKAykvCJEIQIUN65zHAVvgYKWA7cYVFoKIqd6Ib8qr+2tZt/ZovMUhLQ==', '772e1d2b-a368-23c0-5b0b-10e0ff39a9b6', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('9e602aa2-4211-492e-8af1-24ede1e98a0f', '6657441', '668057', 'Christopher', '钱嘉伦', '男', 'qianjialun8@gmail.com', 'https://tupian.qqw21.com/article/UploadPic/2019-10/201910722214820160.jpg', '在区块链和加密货币领域追求技术突破的编程开发者。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEM4XaUFnabxOrmExC7lIpY/M6t6kr4F8NJNhBX0pVJSa6Gbs8VUCbCz0ArmHsp4YxA==', '7d25c337-a860-29d9-f7f7-d53f44fe4d40', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('9f45c4e3-30f2-238b-94c1-d36851518270', '7357894', '757249', 'Michael', '张宇宁', '男', 'zhangy522@hotmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.2315027f971c11e9a04b5c391f057036?rik=llCv8XhSIzZIzQ&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2016070115%2fu4y4paafyz1.jpg&ehk=AzN6Qq%2fQRoTcyNHWaaLuuj8siraIMcGpIkRxn66AEbU%3d&risl=&pid=ImgRaw&r=0', '热爱自由软件运动和开源文化的软件自由战士。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAENa9huoAA7+27DzR6svu1hSJHR4RRumAXBRFtkdmoAU/YuhZDPPJ3U0p5eDxOk6EHA==', '7da5f26a-3d2a-0f3c-0c5b-5a30de9aa00e', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('9fbbeeb9-3b02-324d-9523-2996f96653da', '6522527', '718798', 'Terry', '陆璐', '女', 'lul@outlook.com', 'https://tupian.qqw21.com/article/UploadPic/2019-10/2019102919524924322.jpg', '喜欢挑战复杂数据结构和法问题的算法工师。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAECwMT9Hv+wEXlhUKPScIcmZZdwQpx42XZOm9UqowvWStIYEy6hQOfQxZH+UjeXxZcA==', '7dac6a31-d6d1-f09f-4e50-1c3130e02a14', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('a246d7dc-4486-3c49-ced2-fa45cc1e7d24', '5558449', '681343', 'Ronald', '薛子韬', '男', 'zixu1963@outlook.com', 'https://tupian.qqw21.com/article/UploadPic/2020-1/20201721432695461.jpg', '在人工智能和机器习领域追求创新的AI研究者', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEPSYAHRK2l5YfIg2GM8CYh4/E5R7LgCzhtOGXrJLM8pcxRGegzVmENBEir4LVFltjA==', '80ddc51c-2ed9-8cf6-abcb-30e0a009d8ce', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('a2528783-9459-5b6e-fa29-874d88b3ec51', '3914957', '895384', 'Marie', '邱致远', '男', 'zqi@outlook.com', 'https://tupian.qqw21.com/article/UploadPic/2020-1/20201721432530304.jpg', '对网络安全和防护技术痴迷的网络安全工程师。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEKBlfkQWeuDdWBAu6qWEEJ+2fl2XsBA9RrG45ka5b043EzeK8rw9bpUDT7BMXZ0N2A==', '81e35e59-03fd-5c3e-b1a4-526f07250a03', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('a32e10d7-1e7f-f056-8761-a03ed033e031', '5471994', '643541', 'Maria', '陶睿', '男', 'tarui@gmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.940db70a4a836135fe4867cf499d4efa?rik=Ax488RIBCmxuOg&riu=http%3a%2f%2ftouxiangkong.com%2fuploads%2fallimg%2f202032418%2f2020%2f3%2fQrAbIf.jpg&ehk=ponfXiX7d7rfCwLdUOtGahL8%2b2S478x0W5LUqkBKOYg%3d&risl=&pid=ImgRaw&r=0', '热衷于人工智能和自然语言处理的AI工程师。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEDili3BP2biagN2cK1HXpsqZK/djllSY2EpRIezi4fgAVskrHwXOF4a/RTkfSDCSRQ==', '8259dd63-b391-e82b-f0af-138d0555fd56', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('a49643ce-af0e-33bc-82c9-759c613405d1', '1706679', '985949', 'Eugene', '萧震南', '男', 'zhennan926@outlook.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.27707caced506de799c8f689084b29df?rik=S5MwwIJVjUl4dA&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2018032308%2fbrwq10gx2vd.jpg&ehk=i1hvUKX4xk0GW3UlvK5w8gTuOxJqtmybrM4SHJNH8Go%3d&risl=&pid=ImgRaw&r=0', '擅长开发电子游戏和虚拟现实应用的游戏开发者。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEIENb9T84Kbr8Fod78oc8GobeUoSVKwJ9Ggw/6ueqv/KrF+Rwp1GPWjVrtKPqZGK9w==', '83c5a1e9-9263-279c-0d3c-8cfd92d1b1be', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('a64e2e5d-bfca-9ae9-cdf9-444de12b0975', '4063190', '841003', 'Connie', '崔宇宁', '男', 'cuiyuning1@icloud.com', 'https://tupian.qqw21.com/article/UploadPic/2020-3/20203911195367655.jpeg', '追求高效编程和代码优化的性能调优专家。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEIWgx3etQZj6h2ppTKbYQRb1i0pDt/KtESRtv70nSXEKH/wRq2c1F8n98zy4/SBZQg==', '8436073c-d51d-1445-b1d1-d4c835de8bda', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('a68e832e-14e5-1723-a3e3-76f774cd68a3', '1676036', '682737', 'Leroy', '沈震南', '男', 'zshen@hotmail.com', 'https://www.beihaiting.com/uploads/allimg/160409/10723-160409133132320.jpg', '研究量子计算和量子算法的量子计算科学家。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEMr/+iRwsRr+o7afApBzRZ3Cq11Li0DMhanhDh0GAKFWOr77oJ3T/uqx5PrBjs9Kbw==', '844b1b08-df8f-83df-4509-c293f1ae0867', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('a7037bd6-82a1-53ec-d2c9-88decec54565', '9402909', '750673', 'Marilyn', '李震南', '男', 'liz@icloud.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.3a01787f7c47c17e20de93b847c67654?rik=qGsOi81L%2b3MTdA&riu=http%3a%2f%2fwww.520touxiang.com%2fuploads%2fallimg%2f2018032308%2fdn5wumumr3v.jpg&ehk=IqrHVKybpsttdYfItGMWVfLRgnD4sfsJ7T%2buNl79lx0%3d&risl=&pid=ImgRaw&r=0', '喜欢编写脚本和自动化任务的脚本大师。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAECrXsAqmHqWDtzUQAO5ioIFhKuWBjLl3CCaW7qQcwK+IrjdOP1pLYqS9nMOhzIutUg==', '84aed8d9-a7e7-9ae7-ab7f-7fa5e1de5616', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('a77a6c39-403b-b170-ec09-f163993754e5', '6062468', '965841', 'Brenda', '范詩涵', '女', 'fans@yahoo.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.54b283b00d66512c1bbd1f12fea85af2?rik=e7Z1tRoVsUwgGw&riu=http%3a%2f%2fwww.520touxiang.com%2fuploads%2fallimg%2f2018032308%2fx2ljlir1wui.jpg&ehk=dBzdEnXCLhvRvjI6%2bsmyAHrzkHNY0aaLHP9AyUnEysE%3d&risl=&pid=ImgRaw&r=0', '对大数据分析和数据挖掘充满好奇的数据科学家。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAELtcU4KXgYqaC11n8nN4JS8cQEBCTDTDUV0PAxIjsrZaC8RE/UIqmgSQIAq89Ml5fg==', '84c480fd-5e0e-d9f4-7f50-cd1a860095f2', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('a851792d-4a41-812c-1ec1-347a187f090e', '2597438', '876490', 'Russell', '廖宇宁', '男', 'liao67@gmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.2992e2900898b1bed8a6fbf969c373ef?rik=cgmy0eI%2fHNroeg&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2018010109%2fe0lyfb3byfq.jpg&ehk=DfSLx4da0KTRGq6WxOn3MZiSgmcw8T%2fDQTc%2bgR9jY1E%3d&risl=&pid=ImgRaw&r=0', '研究人机交互和智能用户界面的交互设计师。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEPDHaomcMCU70CBN8KK6yKPgMGe9WdFcdoET4d1y//ovTWqBINtL/Cdtg4W8DQQECA==', '850b8785-2be5-ce05-aaa1-807501555f3c', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('a8de34aa-2370-fa43-8beb-e9039095758b', '2115178', '475538', 'Fred', '沈云熙', '男', 'yunxis53@mail.com', 'https://tupian.qqw21.com/article/UploadPic/2021-2/202122222114766146.jpg', '通过编程实现创造性艺术项目的创意编程艺术家。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEHpRR1ZNTxUbp1hIfyvAucb/2Gl9BQ6Tj9NTmsJ3imixy/YuHEdFNgw2+pDqMIX+xA==', '85dd7d37-86fb-820c-4935-232bd0c3861f', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('a919c5f2-49ed-f3e1-9860-e8a1995e3d37', '7170107', '366180', 'Vincent', '任詩涵', '女', 'renshihan@gmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.148438c8c18b6943f108594a8745b162?rik=Mgfv05eft87e0g&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2018010109%2fk0najrpajuf.jpg&ehk=PcLKzRFQKZAntNdIZ56YA83O4pHid%2fV%2f8g%2b8JtaSxtE%3d&risl=&pid=ImgRaw&r=0', '在物联网和传感器技术领域探索创新的物联网工程师。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEBecX/AnUV2B+hQ22aI8kfSELMtzoJusfP8wTaezHR6mOWKLUGcf4/bBVpeepbsC+A==', '85e3967f-076c-2ae2-7a5c-af9261366623', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('a9f76284-4739-3fa7-bc9f-b3636e99aea1', '5438867', '773380', 'Edwin', '胡睿', '男', 'hrui@outlook.com', 'https://tupian.qqw21.com/article/UploadPic/2020-3/202031721292719090.jpg', '着迷于算法竞赛和编程挑战的编程竞技选手。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEBzeDGfQ4vAsieHGU/kTIkZPcVs1ezDt3eKIqaUb2AP4NsY9Et9y3wy0Geiz3hcqQQ==', '8905a580-5a36-8762-7475-0bd6acf22fc7', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('aa276510-1816-8865-92df-88e819242025', '5613184', '475251', 'Kelly', '孟致远', '男', 'zhiyuanme@yahoo.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.e3d4f2f7db491a73bcf96cce27265ac6?rik=RVNunG%2fNW4wo5g&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2018010109%2fqo2tx0juth2.jpg&ehk=y4o2Sj4B7doquMW6%2bO05pnonyLg4XVfTUdqP%2bQFQmfE%3d&risl=&pid=ImgRaw&r=0', '专注于平台开发和系统架构设计的系统工程师。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEHsKGUflisviiRKQYczNOxZysDxg4Ud1SBq7Kon7uhKg/EQe4halQLIAmIQ8EV7+Gg==', '8a768be3-ade4-a70f-33b0-a204a76466cc', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('abad81f0-b9c8-3115-87b9-6b1b56aa49df', '1359914', '580454', 'Tracy', '萧子韬', '男', 'xzitao@yahoo.com', 'https://tupian.qqw21.com/article/UploadPic/2019-4/201941518273925455.jpeg', '恒心学习，追求全面技术掌握的多面手程序员。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEHqMCw0h1WdJ6XR4En9nnlzwW+sNS6jX4gyks0rS1j+Irl58O+/DVpwDWijLN78Lmg==', '8af55847-d770-1d51-f862-576979613718', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('ac244e2c-3444-51c8-ed0e-158043ed4dc6', '8827811', '838489', 'Carmen', '戴岚', '女', 'dai8@gmail.com', 'https://tupian.qqw21.com/article/UploadPic/2021-7/2021782331652627.jpg', '熟悉金融科技和区块链技术的金融科技开发者。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEOKsDLSH9RT2cO3zqbhW37aoxPxASTyZAk6IEqSlFCmPTPRx5EruKZPjZ5cUqbuFOA==', '8b215fd4-32ee-240f-da5b-2490d468d297', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('ac3c6cd3-1d7c-1f67-692b-e85a789e5d37', '3754050', '339391', 'Henry', '雷杰宏', '男', 'jiehong2@icloud.com', 'https://tupian.qqw21.com/article/UploadPic/2020-3/20203911194366692.jpg', '对数据可视化和图形表达充满激情的数据可视化设计师。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEMnkW3ejM4KCgJtXoHQ+bh0Xf4kRxtEDQ1GezpfEv84ehed30bnpbnku0OPv61Cunw==', '8b43b21f-aee1-89dd-7448-1f73167020ba', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('ad1a1ead-cd62-38b2-75f2-df00d38ae566', '7537331', '552306', 'Connie', '郭云熙', '男', 'guoy@icloud.com', 'https://www.beihaiting.com/uploads/allimg/160101/10723-16010113192cK.jpg', '将编程应用于生命科学研究的计算生物学家。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEBNrS5v5jf3Q9+AqpoIZaOi5YE2gHAQXEKrOKwGruNLdZ5bwpZxt8ciGfPy0xt+xBg==', '8b6beb16-4c5b-3201-1e12-9db67b823fa6', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('ad24f7ea-d758-01c9-0305-6d1ed273a95c', '8955897', '709180', 'Anthony', '邓震南', '男', 'zhedeng9@icloud.com', 'https://tupian.qqw21.com/article/UploadPic/2020-1/20201302274288640.jpg', '善于解决复杂问题和系统优化的系统架构师。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAENr8ZUscA4qvI9Y5PPzhzoVz7vehcW4gT6cVCg4TnXjugo5uvoEHItSXjwOqJaYb0A==', '8e8853e7-7698-5ef6-94a2-1159ee53d424', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('ad6ca290-a828-0feb-e1b9-b7abba814010', '5806846', '586241', 'Valerie', '徐睿', '男', 'rui3@gmail.com', 'https://tupian.qqw21.com/article/UploadPic/2015-9/2015929233888273.jpg', '在人工智能和机器学习领域追求商业应用的工程师。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAENP2b7j1AvRJu3kNBFu38eQceCTnk6mZ0g+ufbOk2obOmRMlkjmzlqidcz8qCCAxbg==', '92ffb49c-b0f7-f84b-4e61-c6c0e952fa42', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('ad9cd476-3dc0-f95b-292f-88a38764bb9f', '4776295', '255564', 'Alice', '莫子异', '男', 'ziym@icloud.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.1db3139c8c13edd869d370f39b11aabe?rik=uR8aGmuVf56rLA&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2018010109%2fmjfzstvcoow.jpg&ehk=5QDjrTxjIpOTNjWU7aUMNScEvPqZXphOasdkWGEgnYI%3d&risl=&pid=ImgRaw&r=0', '迷恋黑客文化和网络安全技术的黑客艺术家。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEJ8yw5lyNTKs9qU+0DF4b8ZaSgYByUqNfsOtrkbd8tJIce9OmgcePDVAVKgdgiqLLA==', '9750ceb9-1a6f-330b-6e05-cdece9b4baee', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('adfaefbd-484f-f1e8-d02d-8757d1d84afb', '4241945', '313971', 'Barbara', '黎岚', '女', 'lan1129@icloud.com', 'https://tupian.qqw21.com/article/UploadPic/2019-6/201962123281214329.jpg', '对人机交互和用户体验设计精益求精的UX工程师。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEEpjZ7WZVQldxO8YporzTJSyioCi5HkyorLHtMOPaY9sRfFhZIHX+zHL413NZj2txA==', '97753236-fdf8-4452-89f5-45bfb43d69f9', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('b0202efe-4197-0660-93dd-0e06cc9944ae', '3101658', '557742', 'Annie', '崔子韬', '男', 'zicui65@outlook.com', 'https://tse3-mm.cn.bing.net/th/id/OIP-C.D9xb5iUyW-kUb9E2yvSX6AHaHa?pid=ImgDet&rs=1', '对自然语言处理和机器翻译有极大兴趣的自然语言处理研究员。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEGfbNDr2gCl20/ovrFD+afoNinganMAO+G6li87QSS7JeWmzsLlDKJAnTBo0kG/Q2Q==', '9872269c-85f2-572e-9ccf-b64fd4cd1279', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('b25108f8-c399-266e-e071-762d4c36ed2e', '9786214', '198021', 'Russell', '邓杰宏', '男', 'jiden@outlook.com', 'https://www.beihaiting.com/uploads/allimg/160101/10723-1601011319411b.jpg', '追求算法创新和数据处理能力的编程科学家。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEAChExhrsJLgLMC/ihzPdYlqCGwFO6CMARCIBE9v6l2vk3kbR1RQEW6+0niqGrp1ig==', '99a20e2e-4b5a-2936-98d4-46d8d35cfada', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('b25257ff-a225-a49d-5e9b-766396c80873', '3635833', '836769', 'Rebecca', '彭嘉伦', '男', 'pengjialun3@gmail.com', 'https://tupian.qqw21.com/article/UploadPic/2018-1/20181211863574730.jpg', '精通前端开发和用户界面设计的前端工程师。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEJ9d9IrA0T8+akm3URPfhXLNozAqUvMNqZfPOKVm9fDI2pBhxT/AYx8reXh33LYLdQ==', '9a1b0252-b5ab-38d0-5471-f55f3185f8e1', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('b3b47931-b135-e9bc-029d-4896b640a7fe', '3563676', '793969', 'Wanda', '朱杰宏', '男', 'zhujiehong@yahoo.com', 'https://tupian.qqw21.com/article/UploadPic/2018-1/20181211863552296.jpg', '擅长软件架构设计和技术团队管理的技术架构师。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEGVxRe29KMt+Y7tWelfwM7EzEZnvB3VDcUQExTg0RxkJBNxgGfKmYuZIhkstdpbwuA==', '9adee94d-05d9-b1ee-6aa4-881811a14e61', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('b4ad8b4d-ad00-49ea-8916-02731636fe52', '9351067', '459357', 'Ruby', '曹安琪', '女', 'anqic611@gmail.com', 'https://tupian.qqw21.com/article/UploadPic/2019-4/201941518274092080.jpeg', '对计算机图形学和渲染技术有浓厚兴趣的图形学家。', 0, '4f25ea99-7117-217e-10f4-92c75dd841d6', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEAffvjZWG7O9sgllr+JnTbpveVRLLetg4AhMvqX9c854OfJG0M5uCyuQvvdsBVPRdA==', '9eb4c1d7-f5ef-97db-1add-c5038eed044d', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('b51fa004-1aaa-c360-d768-ac8768442c0d', '3827608', '971099', 'Victor', '赵子韬', '男', 'zhaozi@gmail.com', 'https://www.beihaiting.com/uploads/allimg/160101/10723-160101133PV39.jpg', '热衷于学习编程和算法的计算机科学学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEG9oHuyvJRV3pb2cLIRrUZBwXAXBlTS7f74KTbpA2qs8cHMZf+I2dMCr7BrHtyqmGA==', 'a017326e-6aa4-6276-ec15-b97c8fdf69cb', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('b6b96d1d-02f1-99d3-6c51-55d0139aa5eb', '6666151', '133828', 'Cynthia', '雷安琪', '女', 'leia419@icloud.com', 'https://tupian.qqw21.com/article/UploadPic/2018-7/201872714141186450.jpg', '对人智能和机器学习充满好奇的人工智能学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEHaNOUAS0sMhR9hFKQJKnlD3RUStUqW6OgAovDwxj61UqA/wjafHm3P4MzH433cRmA==', 'a041bc75-de2c-d4c1-6c92-843b23d92d41', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('b6c088ea-5d36-6724-dd06-f331fe143c11', '5439160', '994998', 'Rose', '谢岚', '女', 'lxi4@hotmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.34792d32028064b1630a84016621275a?rik=t9BVWRtKKP92gw&riu=http%3a%2f%2fwww.520touxiang.com%2fuploads%2fallimg%2f2018032308%2fqm4njwyvnyl.jpg&ehk=pTteNfn4BK3hOYZAXL4OpUq1MI%2fa7sWgQ%2bF5BMFuOR8%3d&risl=&pid=ImgRaw&r=0', '学习开发网页和应用程序的前端开发学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEFnZpI2EbtcJltF0Oll9dxOCR+LT0DSih/QPCBVA3mD7kl3BAlYpdrk/++xOSpXaJQ==', 'a0a0ead2-74f5-f052-68b7-5dc8df6ab9e1', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('b7341b30-d793-96f0-8a82-664c78d92f2d', '9571048', '219840', 'Wright', '方震南', '男', 'zhennfang205@gmail.com', 'https://p.qqan.com/up/2021-3/16146513808882246.jpg', '追求高效代码和化技巧的编程生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEA+UuuLeVut3oEtBX3jeYjOP4nEwnSYrEmMkyBr1wmoxuLqYSLbK4gjfNsSIeXoJLA==', 'a267da06-679e-f271-1e40-0eee737d7bea', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('b7457a89-49e8-9913-25ea-bcae11ee8a47', '8757889', '299532', 'Gray', '冯詩涵', '女', 'fengshihan8@hotmail.com', 'https://tupian.qqw21.com/article/UploadPic/2019-5/20195322103870602.jpg', '研究数据分析和数据可视化的数据科学学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEHexjLzHNLH8c+4Zmt8F/HBygveuM9fTopeYmem+6UybmvOGY6zlRyt6A27SLoe7Fg==', 'a3e49e1c-898e-cadb-c273-170f285a1192', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('bae40eca-2811-9352-f543-78b758f8866d', '6643409', '796894', 'Perez', '段詩涵', '女', 'shihaduan@hotmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.5a210bb560e50324f3f8162e2410e792?rik=T4FPUz2R%2bGHHmw&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2017092011%2f3ynxtaxmj0m.jpg&ehk=gqJaUaKARqc9Z0iSDl8XV0qgWzDjNixCS2E3u4mfJN0%3d&risl=&pid=ImgRaw&r=0', '对软件开发系统设计有兴趣的软件工程学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEAwCJbuhIrfrEkbqq90Ie1WYYoZZq0XrYFBBMSsMeqcaZcTnExRuxNQwP48hiED61Q==', 'a5a1d62c-80d7-e3e9-49c5-416f1a438b23', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('bb5be7bc-f71f-f587-3d04-062fabdddf6e', '2765707', '386625', 'Owens', '毛宇宁', '男', 'yuma715@mail.com', 'https://p.qqan.com/up/2020-6/2020060308522835058.jpg', '学习物联网和传感器术的物联网专学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEJE5iW0Nnf3k97mn2V6Ne3/VNFV7cavMXE5anoYDtI77Y2Jqa4C1eShE8kRSevb/xw==', 'a604ba2c-f9d6-3ff6-95c2-1e5ea3894ecc', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('bbfbd3e8-ca9b-638f-7919-82c2b4e2c245', '3294585', '734884', 'Hill', '马秀英', '女', 'maxiuy9@hotmail.com', 'https://tupian.qqw21.com/article/UploadPic/2012-9/201293837253466.jpg', '着迷于游戏设计和开发的游戏开发学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAED2Wwec+J+tohmuvMU3v6Tb4AaXR9+Fo86fGr57VEBU9jeyYqyMFYWTzYuQU8vzfCw==', 'a67b7d29-77d0-5fc1-a6bd-e86891798a5a', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('bd5234a2-e044-b7f3-7595-de0f0eaf0ac1', '6806160', '305102', 'Hernandez', '高睿', '男', 'grui@outlook.com', 'https://pic1.zhimg.com/50/v2-39a3b491084836fdd127afa6fcb24211_hd.jpg?source=1940ef5c', '网络安全和系统防护有浓厚兴趣的网络安全学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEP8+5rZszUTP25Nqu2jK+10vmpgHuDHjIMYqu9mQEne2Y5hDC7axYfLv+fhv2wv6Eg==', 'aaff2816-da86-1014-11b7-159cb9a3f6a9', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('be58cb3f-77d1-c614-f9d5-2b5696acdf4f', '4862417', '439621', 'Harrison', '杨岚', '女', 'yla@hotmail.com', 'https://pic.qqtn.com/up/2018-4/15234396009288466.jpg', '学习数据结构和算法的计算机科学学。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEFoYYX6QaqIwK7bIS7Em1nxdrAVzDJvRwE70Y59QuWHihjThDZKCeGRs725EY6B1WA==', 'ab576763-54a1-e018-83f4-ae469745aca1', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('bf0059e2-987b-4b40-bd42-1bf294ee0cf5', '1091238', '302323', 'Vargas', '毛嘉伦', '男', 'jiamao@mail.com', 'https://tupian.qqw21.com/article/UploadPic/2015-10/2015101316245480707.jpg', '研究虚现实和增强现实技术的虚拟现实学生。', 0, '4f25ea99-7117-217e-10f4-92c75dd841d6', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEFYPRaIxj+TcHZ8614uf7Afb16JEvGYCpkjuqooEs+1E07Ciel7UgPttQ3FGb8V/hg==', 'ac058b7f-bfea-bf8e-c727-34855176bc5b', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('c22fe868-5ef6-52e2-4a9a-1b28d407307c', '2188974', '681255', 'Bennett', '唐杰宏', '男', 'jiehong93@gmail.com', 'https://tupian.qqw21.com/article/UploadPic/2020-3/20203911195660015.jpg', '对编写脚本和自动化任务感兴趣的脚本编程学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEH0muHxoiX2nbClF1ES1ErgJQnrPyQSq0Ax11TCQL3xZ4DFCQnwxUqli9s1TSJHQWQ==', 'adfae673-3047-609a-2058-1e4b1118ca27', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('c24233ef-3c78-6200-cd11-9dd5d9be4159', '2071238', '729208', 'Marshall', '沈致远', '男', 'shen17@icloud.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.f1bfc02a67bd5b1a2b0a9c34650081cc?rik=QKpoGcQy0NeXGg&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2017092011%2ffdgjwgdsj3k.jpg&ehk=gzU4CMTbXiQFhHe6ne8yXsyaOoHlmdthhqztiZn8ReQ%3d&risl=&pid=ImgRaw&r=0', '学习移动应用开发和用户界面设计的移动开发学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEPosXXRAleuO+9bXpmbhGHbXr3rMbHZyIrIxGWmI1Xet7+CAsOpB4Y2+R4AmOlMPmA==', 'ae4aa44a-217f-2c55-2d23-689e30359773', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('c29c1da0-ebff-3eef-28a0-bd9be5e3930f', '5277883', '352110', 'Moreno', '顾安琪', '女', 'anqig5@yahoo.com', 'https://tupian.qqw21.com/article/UploadPic/2020-3/20203152242456714.jpg', '热衷于人机交互和用户体验的用户体验设计学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEBtMK9OGV9qJMk71uzkTa88MccelGpK3QIhFs2RzmcX20VPPcbg9pbv02ukL5zJQdw==', 'b1419508-b199-b688-a93a-c3ff303e8b75', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('c39d292e-b2ac-4175-ca43-5703372a06d2', '6898707', '560447', 'Gonzalez', '胡睿', '男', 'hu413@gmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.83d6248fce9b41b08ec7af9b9709d912?rik=duB3xZl2F4YcDQ&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2017092012%2fchk32ra3cvj.jpg&ehk=fmDeTPrOAEWKisOECTJjw5zfxQUu9mmOMyb2em2ZwZo%3d&risl=&pid=ImgRaw&r=0', '学习数据管理和数据库开发的学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEDwJzp1MlOhNYy+7U0vKd8vTbA+mJ4QWvPwn6y+k/a3lt0l8kU8HvMEvBLhhtdl7eg==', 'b385106f-edd2-a935-70eb-3025e88496f7', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('c4256393-1a61-06fb-8846-662738220cb1', '5236400', '314925', 'Hawkins', '戴宇宁', '男', 'dai73@outlook.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.de49bd6f17ff9f8b42053a8e03e8234c?rik=kUHSaD1b01BNeQ&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2017110610%2fb1m0bva2ptb.jpg&ehk=mBPdo98sYmpuuISBmOk5cFeyfd38zSgHl8T0MrEeVCg%3d&risl=&pid=ImgRaw&r=0', '对机器学习和数据科学应用有浓厚兴趣的机器学习学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEOQQlj3TJ3Mf8u9mE6w2oju+YHM4+JRW7I7sNhgKbj3vlLnOC3KgSDbJVcbh2O+kLw==', 'b46da102-6a8b-c4e6-9803-e77f93f9a2bb', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('c4d8b5c4-a624-de9d-54db-16b5292d50fc', '7702616', '129021', 'Meyer', '谢晓明', '男', 'xiaomingxie@gmail.com', 'https://tse1-mm.cn.bing.net/th/id/OIP-C.CRBpDM6qxpEX9gqdyTgFaAAAAA?pid=ImgDet&rs=1', '研究人工智能和语言处理技术的自然语言处理学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEBOL0kDpPGKmfBvk00CPm/ttBsbeba6Dx1H4Lz7Qmu2Rf4Aub0KfbIQH6jmaD7h3JA==', 'b68bf720-4a39-a44e-7ab5-fa206d037618', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('c6003085-2efb-b94c-a64d-2e44e27d5d6c', '3822010', '876974', 'Joanne', '沈杰宏', '男', 'shenj@gmail.com', 'https://www.beihaiting.com/uploads/allimg/150610/10723-15061009592Ya.jpg', '学习软件测试和质量保证的软件测试学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEMOW8RI8CU3Bo6KKfyJ6OncyizZ3PzWOj8tP54h3aZPg1VM+VeZS855LbaNj78DSmw==', 'b74507a0-7d8a-75f0-d2e5-0d32d0a8873d', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('c8b9afc4-94e1-416d-37e2-60130783d949', '1526281', '219495', 'Alvarez', '周震南', '男', 'zhennan3@yahoo.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.2787fd84e7d3ef9baecbeecc00dd3243?rik=yjiTYYld9lm3nw&riu=http%3a%2f%2fpic.qqtn.com%2fup%2f2017-12%2f2017120911184235460.jpg&ehk=0QltbtT7PfwSqZGs%2fLzz9n8WBIjZbwC2z%2fzdHeGSUFY%3d&risl=&pid=ImgRaw&r=0', '对网络编程和网络安全感兴趣的网络工程学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAELD1Y4bgsSsWJK5jAs1yIMAL2Qht7jQ2i7YU1cOHAiXh1RQijFQpafWS6swAjwcM7w==', 'b8b82b1e-6d43-cae7-408b-b03cd31befab', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('c9bfbc24-a60e-4986-9394-0958d744441c', '7225807', '412690', 'Stewart', '贾岚', '女', 'lajia@icloud.com', 'https://tupian.qqw21.com/article/UploadPic/2019-4/201941518274065242.jpeg', '热衷于算法竞赛和编程挑战的编程竞技学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAECa16wcnOLXXGh32hlXaCcNlk+wZH+1pptxI1I8+3zImL5RVCvEeONWf3+6ZncyH1Q==', 'b91d1fbe-25ce-b40b-4bde-85476ad3b5e4', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('ca9d4435-8bf1-50ab-c67c-e267a5d963d6', '1731525', '523664', 'Owens', '马嘉伦', '男', 'jiama1030@outlook.com', 'https://pic4.zhimg.com/v2-70e686bea7cd1ef9a050cd10b5a61810_r.jpg?source=1940ef5c', '学习大数据分和数据挖掘的大数据学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEJuUwRVdUHS+s+mrf5GCRYR+FTE5tUMGkJSqIks8uq+mvsuvGL9mEFRuNzdLxUNrPg==', 'bea87d63-af90-b35f-a45c-d5969faf8753', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('cc1a5caa-f669-32f8-af4d-0ce5b3f10936', '7965312', '431691', 'Wright', '薛宇宁', '男', 'yxu@gmail.com', 'https://tupian.qqw21.com/article/UploadPic/2020-11/202011202324099312.jpg', '对电子游戏设计和开发痴迷的游戏设计学。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEOOBr1KCnEBdPlLg6rqTje12uHlTwyXZ5PBqrM+YGkUv728Vo+Iwcqh0bbXWDiQhSQ==', 'c20efea3-23d5-d8cd-e870-8dc09f0c6e10', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('cc25af3d-2736-50c1-f047-8b05f25f75d0', '8557493', '486459', 'Bryant', '叶嘉伦', '男', 'jialun97@gmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.4e76fa842a535cf82accb786230e4546?rik=i1R7mGihbAp5Ng&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f171123%2f3-1G123203S6-50.jpg&ehk=eJDcWzxvU4qnj3O8JDR2OKWj8ikMaR5W1Eas1iX9u5o%3d&risl=&pid=ImgRaw&r=0', '研究分布式系统和云计算的云计算学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAENl79GMtFGpPfjpUlml9MYUDcvfu2g2y47q2ezjftR4o0EHw5J85xUcX1y4Av93HOA==', 'c27b9b88-b996-a0b1-66a5-70d0f85c199f', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('cceac40c-fb4b-7280-18e5-25f56f49c253', '4168845', '900501', 'Reed', '严晓明', '男', 'yan1102@gmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.9797197122580f4c96f69d1a3a04db8a?rik=Y3c%2fSSehZwp12g&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2018032907%2f3v451d04vrh.jpg&ehk=J4VnJPK3b2L1lMWoOx9Xgzfu31xg3uhn5%2bunI7sNuwE%3d&risl=&pid=ImgRaw&r=0', '学习软件工程和团队协作的软件工程学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEAcJ3qI6KPq3Z5L9PqKaYPsUeIqRbhSWLasDHIe9ggM2Qzx6qbq0mH9TMsz38tH9QA==', 'c29d2942-e70a-53b1-2555-28732e92c78c', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('cd0adb85-eee1-a70b-cbf9-d460fdef97b0', '8224039', '889147', 'Guzman', '邵璐', '女', 'lush@icloud.com', 'https://tupian.qqw21.com/article/UploadPic/2021-6/20216320112779517.jpg', '热衷于人工智能和图像识别的计算机视觉学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAECqk+ii0AFenFCZRLTyCmsKdNHljlmf1Rrtk5ottE3pa4hJcfu9erdwL/2MKi+ffLQ==', 'c3b020da-697d-22cc-3c07-3460f3881382', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('cd5f416f-3362-5137-0d1f-cbbf178ee54c', '8558351', '106547', 'Woods', '吕云熙', '男', 'ylu@outlook.com', 'https://tupian.qqw21.com/article/UploadPic/2020-1/20201302274371137.jpg', '对物联和嵌入式系统感兴趣的物联网学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEOkbWXzuWNMNgBsuVrKmSYuUCKPVUjKD5Hj8QzuIugy/izLwQ9U7BW22YGgZSI3uNA==', 'c5d72f14-7571-1e44-d6de-cf90478d44e6', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('cdbfe4fe-6a6b-437e-c7fc-765b07191895', '3525859', '594019', 'Robinson', '彭安琪', '女', 'penganqi@hotmail.com', 'https://pic1.zhimg.com/v2-46aa6f3f6dfbf4e905d81575e4da66b9_r.jpg?source=1940ef5c', '学习金融科技和区块链技术的金融科技学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEIGaYpGAHWmXCF1VOlsRMOOEEoPyVvXmf/qdLyoofJI2JifPMyiBjIMsLZaY4cA7qA==', 'c837bef8-bfa2-bcac-986c-7fcb7c2ed7f5', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('cdfa9f0b-c19b-9530-733e-a7693a5cf896', '6212313', '821535', 'Webb', '黄致远', '男', 'zhiyuan73@icloud.com', 'https://p.qqan.com/up/2020-10/16025569008379072.jpg', '研究人机交互和图形处理图形学学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEPWYTG6tGBbDilEcsI0YiFD3wDzwu9XUt1hpbfMlFTSklC0Kyl9rgO0WzM10XNHRyQ==', 'c8e3eb8c-1473-bb35-0c75-267bfc2bbc35', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('ce0bb6df-6ae7-38ff-f606-b11d3f755d26', '2839487', '110865', 'Watson', '龚震南', '男', 'gongz@icloud.com', 'https://tupian.qqw21.com/article/UploadPic/2020-6/2020692142738691.jpg', '对人工智能和机器人技术有浓厚兴趣的机器人学生', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAECROCaZ1kXOyPbOGZSMThQh4ro8Vt6AAJTN7dPFy/weDtkxhcERTVANb19uEdisD/w==', 'c963423a-2b02-09bd-92f4-853a6aea56eb', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('cfd5df2a-cb2f-58de-16df-2f2348f51fd8', '7874146', '537377', 'Simmons', '朱云熙', '男', 'zhyunxi@icloud.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.36d34f4d5dd175fa9dcd5c7e2948b12e?rik=oAlQEqcz8ZIPrw&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2018011510%2fmkupeuezjig.jpg&ehk=Mryamk%2fm9ruG4wI3gfAYMUG%2b7SM%2fISRW7Y4UDjojvN8%3d&risl=&pid=ImgRaw&r=0', '学习自然语言处理和文本分析的自然语言学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEHwzGQSKcVcoieC8G0I1tjOalnah8AEM5PAwt16uDgYWNv/Y7Gvut6jJCVt/SUf0Rg==', 'c99ebe84-39b1-5609-56bc-54b031a11bd2', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('d0cdcbeb-3f99-3f4f-0819-30e40e068428', '3313766', '123591', 'Porter', '程宇宁', '男', 'yuningcheng@yahoo.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.e92ba9d7f399d8dc850f911608d9c8ab?rik=8xVsk4EMzNonCg&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f20151012%2f06845c13d82f6e82e60b9d7f40d1a60f.jpg&ehk=bibMFTnkb53HuaA1ye1WXSBH%2f5XXzgMce4g4G18Eits%3d&risl=&pid=ImgRaw&r=0', '热衷于语音识别和音频处理的语音处理学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEN2kFoiWdahSGKS2jp3S4/fCf+ReWL5RmCYiagNbRtKhQiVlpZPRacLV68Zml183rA==', 'c9e9dfb1-f81a-f504-fcda-1acccf292f6e', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('d158bba5-6bfe-b1ab-c1e2-e18ad1a4c5ca', '6469364', '409757', 'Adams', '郝秀英', '女', 'hao6@gmail.com', 'https://www.beihaiting.com/uploads/allimg/170312/10723-1F3121KTW22.jpg', '对网络安全和渗透测试有浓厚兴趣的网络安全学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEKgz1eBs3Zd8d4ptCftSNNz6xFJBiwwUyeil+wxnfwxvAXccixmLiMw40p1oejyXtA==', 'cc002ab9-12ae-3084-3915-96c25882adc0', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('d178dd7e-85fb-aab9-8091-ec8d2d86b016', '1127404', '321879', 'Ross', '胡安琪', '女', 'huan@icloud.com', 'https://tupian.qqw21.com/article/UploadPic/2020-3/202033122184611395.jpg', '学习算法设计和解决问题的算法学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEEefAWND86nL+WQhP05QeuGwtXUyd3BK99QsKZRPBDEXDRORBmsBcZ4vPe46145B2Q==', 'cf2dbec5-023a-b743-7f0a-99150856dd97', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('d229a340-bf05-bd6c-748e-aaaa842de523', '6750769', '511335', 'Morgan', '程致远', '男', 'cheng719@gmail.com', 'https://tupian.qqw21.com/article/UploadPic/2016-7/20167231737610439.jpg', '研究软件架构和系统优化的软件架构学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEJxJ9eobLUmo3BWe+y69jp6QEIKDqYlQH3nOsal2d2v30xCFiRNQJTU4AydXUsbS2A==', 'd30f2ec7-7bcb-1a21-65c0-137d47d7ef44', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('d250ff81-d6f0-6958-1c19-7d625c0a83cd', '2114223', '870593', 'Cole', '唐晓明', '男', 'xiaoming1022@gmail.com', 'https://n.sinaimg.cn/sinacn20117/2/w1600h1602/20190325/9a21-hutwezf2949135.jpg', '对信息安全和数据保护感兴趣的信息安全学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEDTWtnD1+Tz+6wuHoGarL9/Ons4uFkvIEhAvW7N9dae+8eyX6ZC1X1llxzSImzQV+g==', 'd55867da-f57e-b2e6-450c-d456bb343823', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('d2e2acb9-7325-8392-da07-fac8b7a2bfaa', '9503307', '531724', 'Deborah', '秦杰宏', '男', 'qinjiehong2@gmail.com', 'https://pic4.zhimg.com/v2-372345d45a2b30b127791429069c4fdf_r.jpg?source=1940ef5c', '学习人工智能和机器学习算法的AI学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAECj375spGG87yjAUJGQD/DWfT/hbapcVXoZMF05VVLIMrkRUzNKVlqIDRQ0NyotUZg==', 'd5ccce6b-6a45-7de8-eca8-eab3cf1b85a7', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('d7ea80fb-910d-8c97-aa41-18fd5f085d89', '7235756', '838316', 'Carmen', '刘宇宁', '男', 'liu6@hotmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.9d2c6091f105c18d361b41636a736f26?rik=1y4QjXVjvNVRcw&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2017110610%2frvtmhdqy0mu.jpg&ehk=ZQbsPlDpuTzB%2bUu1VcmrbnexMHkSFx55IfAMk%2bwQ0To%3d&risl=&pid=ImgRaw&r=0', '热衷于前端开发用户体验设计的前端开发生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAELWT4zS52B5y56GsL6fZUOIRewBWwtwM2YlEnOZ2k/y0BhW36r0iXPgLKlQ3ahexJg==', 'd616e049-123b-6f53-c1d0-0e64f93aff0f', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('d7eaaeef-ce09-e5c3-64f4-3e30fde047bd', '8570131', '873567', 'Monica', '刘晓明', '男', 'xliu60@hotmail.com', 'https://pic4.zhimg.com/v2-874b7cb7ded56394b549d34a68c76a00_r.jpg', '对全栈开发和多领域能有浓厚兴趣全栈开发学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEK4n5roI1iT2G0CDqDzRiG6myulXCObVm9LtcL0M7c9LmkLqo6+lJ9soQHk3I9yrHA==', 'd62cc7fe-cbe8-1b98-c8a2-cd0720c52235', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('d822584a-8544-63a4-1744-079cc68d9f7a', '6376572', '763734', 'Lori', '于宇宁', '男', 'yuyuni@hotmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.7db130fb7a9811d288965280aaaa6159?rik=G8u0EX1xPUg2yA&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f2018011510%2fufwvc5kucte.jpg&ehk=CYhW6yYnC12DtFxlWvQFWoMEnfB8PLrpkNpWo4%2bnA%2bo%3d&risl=&pid=ImgRaw&r=0', '学习计算机图形和可视化技术的计算机图形学学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEEqtiv5mySNJJ6AESDqdh2V50FYFr45JhErY6ivsxXZyZF3W5PFCW1Y/ArUSdwMWmA==', 'd6d12411-c2a6-b5a3-be7a-65b7a6b68f12', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('d87fbe27-00ff-3ae9-96d2-089d2ff73010', '7025804', '805540', 'Todd', '姚岚', '女', 'yaola@gmail.com', 'https://p.qqan.com/up/2018-11/2018112208174393596.jpg', '追求创造性编码和艺术表达的编程艺术学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEB2GMp0Ps58+OrcZs9ZKFSxKaaK+pStkeh3eu2tzsLXBIspkILvJGxFabYZlPbMnPA==', 'd892868d-27b0-ad27-59da-b8407dbb163c', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('d8b1e9c4-09c3-f108-2de0-7664c407dce5', '3916191', '481258', 'Jeffery', '韩震南', '男', 'zhennanhan@hotmail.com', 'https://pic2.zhimg.com/v2-51dda87f37dec11cdb3ae02908901753_r.jpg?source=1940ef5c', '研究机器学习和深度学习模型的机器学习学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEPEm59p4IVVKcmD3n/bRkHUcuus4A3ImAAly48zW8muCF2wAenlHj8EX8gYVaYKhfA==', 'da50a944-b81e-7596-10bb-af6801bbe633', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('d8d4998f-7f6e-5164-3038-04c023ab35a1', '9264500', '265305', 'Randy', '郝岚', '女', 'lanhao@gmail.com', 'https://pic3.zhimg.com/v2-c9473ffbdfd1d0b47f4ce1a9e54379d2_r.jpg?source=1940ef5c', '对数据科学和大规模数据处理有浓厚趣的数据学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEDSizsHpewPkk5CdCMkx9ZEA5Z/mo7u9ZGC1JOl11sfuNRNoE3rcrqmUg4yzTUfqoA==', 'db6e2752-2ec0-3c09-2739-4ac7384690a5', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('da486596-6548-1458-26de-42340db3d974', '7325413', '511637', 'April', '许云熙', '男', 'xuyunxi716@gmail.com', 'https://pic.qqtn.com/up/2020-4/2020042008331551819.jpg', '学习自然语言处理和语义分析的自然语言学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEDSpVP8pM/MkuJF/z4STxIQPr0HKpPRX7X6GDcHI9sQgRF8Jz1hDYvIrc91b+dgR6g==', 'dc2aa1a4-385c-189d-3da6-001e6520b4f7', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('dac5552f-1c10-f074-a9f8-d630069b3557', '5838507', '855090', 'Jimmy', '莫宇宁', '男', 'moyunin@yahoo.com', 'https://tupian.qqw21.com/article/UploadPic/2021-8/202181122364885315.jpg', '热衷于并发编程和分布式处理的并发编程学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEAHR3wSnddTLiaGEj0YMHv+Zop/tj4nNJbELzaTDZY0X8xQJ5f23kBIO3g0tdRuW/Q==', 'dc622786-8ef3-7c99-d994-6dc59675d222', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('db3f6b68-45b7-4f5c-e5bb-2774a23145a3', '9638772', '893765', 'Lois', '郝云熙', '男', 'hao75@mail.com', 'https://tupian.qqw21.com/article/UploadPic/2020-5/20205422363418829.jpg', '对网络协议和通信技术有浓厚兴趣的通信学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEHyAoOAajB4PVtc1tiw4udjTH6QseKVlMLuZo/sAscTnoARZIhYXTdUtL+ZeC9+e6w==', 'df30e63b-919a-8939-601e-538f7884f082', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('dc144322-616a-ef7b-f471-c3c76b134256', '5055948', '228895', 'Rita', '王嘉伦', '男', 'wjialun1@mail.com', 'https://pic4.zhimg.com/v2-38b1e187af53733d6a481a06a2837ce1_r.jpg?source=1940ef5c', '学习软件开发和系统管理的系统管理学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAECROJrxKr3IN0BIypvZf9uJawpWqdwH+izBdmuxJ58Rps9fWqwQU/tgecjpT0ItMvA==', 'df860e84-297a-11d5-c8fc-107400bbbd3e', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('dc31d036-bc5c-e085-50db-bebb459b3d61', '3616475', '697635', 'Mary', '罗子韬', '男', 'zluo@gmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.394a8e345b2cf249c9445d444b77268c?rik=5UGCzJbzjXEadQ&riu=http%3a%2f%2fimg.touxiangwu.com%2f2020%2f3%2fVbAzEj.jpeg&ehk=H39OLi3PDo%2bo2%2ftqWVSkhzs4bFx285d8onXAQ3wngqo%3d&risl=&pid=ImgRaw&r=0', '研究大数据分析和预测的大数据学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEHj1p6w6ZUSGlRjtxc7LEtOSITaW85HC4g98BgYPeBiUTqDP7iiaSBiREfYRWHfSEw==', 'e04fe56f-522c-9ae4-02d6-bf89ff4bf0cb', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('dcd440a7-aef6-4609-a712-cef9ec65fa77', '3044241', '607002', 'Maria', '李宇宁', '男', 'liyu@outlook.com', 'https://tupian.qqw21.com/article/UploadPic/2020-4/202042219285173033.jpg', '对质量和软件工程实践有浓厚兴趣的软件工程学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEH3ZeCIyQB/Z+1JZqfg8Kp3GIB4U+GEKbzh+I0jwxz+ZFiEXfh8BOpxm0ei2wyf6Tg==', 'e2dc76f3-9816-d0b6-bf11-80cc731130a7', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('dd0b878b-7d1a-c635-1b9e-4b989b2cd08d', '9665593', '804958', 'Gregory', '于詩涵', '女', 'yushihan@gmail.com', 'https://pic1.zhimg.com/v2-347f93e02a7e47cb4e508ac6fc74370e_r.jpg?source=1940ef5c', '学习网络安全和渗透测试的渗透测试学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEBC2UDn6rUEMtqL8eIy6k6bRpfmNsxuxhNrQbiYTUkIM/Kw+KKrPFdSgt/Kp5EWbIQ==', 'e38b7438-6349-cb30-35a8-5967bee2f047', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('ddf22a29-316f-233f-bd1e-80229487773b', '5373347', '804497', 'Scott', '熊云熙', '男', 'xiongyun427@mail.com', 'https://pic2.zhimg.com/v2-89b452e67e123304d7c1feb9626bd765_r.jpg', '对人工智能和自动化系统研究充满激情的机器学习学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEKMxB0vOmw6vhaB9cR5huXim4R1NYCWk+aeN+UnCQp7ZUn0rgo6g4+oO+uUb244fag==', 'e4320867-b386-b404-f2dc-97ad9b728e9a', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('df347751-ee9f-ed4d-e66f-24856ff968ad', '5628649', '893014', 'Carlos', '潘杰宏', '男', 'pan04@gmail.com', 'https://tse2-mm.cn.bing.net/th/id/OIP-C.gPdQGLzgJlmFlZBgO5dh9wAAAA?pid=ImgDet&rs=1', '学习网页设计和用户体验优化的前端开发学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEBTeVr4RVr75Ymqgc+xL1dGMZ68rNFSCFvrbO8bwrbnsxGmBjJL63TJ7NlCYULB+Eg==', 'e45f79c2-0da3-0e43-4f2e-246ef8b7afe5', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('e0ac6c79-0bb4-ef04-d53f-5ca5c153748e', '2225943', '151020', 'Grace', '石岚', '女', 'shi60@yahoo.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.404fad380b4370d194b64e7af90e4102?rik=mjNezvp3Z0r06A&riu=http%3a%2f%2fwww.gx8899.com%2fuploads%2fallimg%2f180410%2f3-1P4101G029.jpg&ehk=m3MZb%2fWFdsv9nyefqp5bw%2f60f9Z5UCIJlnDEV1EZcTE%3d&risl=&pid=ImgRaw&r=0', '研究大数据分析和数据挖掘技术的数据科学学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEJvrYQF/D3Xe0thDmasHYdggvnU1Mv4rTO2qQXpaqgX+9fzPMXLu67CDZStLxfj8Yw==', 'e4973947-f3ef-c3b4-0d6d-1caef9fe23a7', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('e2a93fbc-d95a-e2be-cfa2-26f546d4f88c', '6574297', '469009', 'Francisco', '林安琪', '女', 'linanq@mail.com', 'https://c-ssl.duitang.com/uploads/item/202005/13/20200513084828_iymih.jpg', '追求高质量软件和系统优化的软件工程学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEJ/34SvPiJOxIXEPeU+YTeTE3Y6PpSZc4niRAftqWV5vncLvOjAHsHIjpmzobA1vcg==', 'e6190a70-478b-ba81-bfc9-452c92d059fd', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('e358e498-8e7d-731a-6abf-7510b1a61d37', '9799065', '442839', 'Randall', '龙震南', '男', 'zhlong@icloud.com', 'https://img.zcool.cn/community/0167a75c0a2c23a801209252d0a271.jpg@1280w_1l_2o_100sh.jpg', '对网络安全系统防护有浓厚兴趣的网络安全学生。', 0, '344e3b7e-fbde-bb12-053b-6979595be1fc', NULL, NULL, 0, 'AQAAAAIAAYagAAAAECgKhhJU1YgNtx3CpQpH9LmjC0Cd2nyYsu6j3j1J4RAyIex6B21nvhzl7vYwEkU+Mg==', 'e7dde16b-a0a4-42cd-ab3a-ff354b955991', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('e5578a2f-8ae6-f9db-20bb-343084cd2aee', '9261773', '224217', 'Ricky', '韩晓明', '男', 'xiaominghan@mail.com', 'https://pic2.zhimg.com/v2-2b8040d89f549aee538c0f50da896a54_r.jpg?source=1940ef5c', '学习移动应用开发和用户界面设计的移动开发学生。', 0, '2944e863-5358-aeeb-e341-f8101291bb0d', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEACHcu3kHP0e+5ZrDtRVUKCH/iYcRHDCPx0R3zo5a13cyqoFUdEDlzUPIj+IphQetA==', 'e8ab1da9-0060-7213-257b-3e80596df72a', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('e62b22f8-16d0-59f2-428f-ff0db3f93e7a', '1155430', '127519', 'Marie', '余詩涵', '女', 'shihanyu@mail.com', 'https://pic2.zhimg.com/v2-4b1fd11946865bae58cfa5bbf427c23f_r.jpg?source=1940ef5c', '热衷于虚拟现实和增强现实技术的虚拟现实学生。', 0, '2944e863-5358-aeeb-e341-f8101291bb0d', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEIR5B+Kp0xVSYStnNF7wUxglwZJXMkvyAwewmfo7How+CZf0qrTHLCEbpcnR3wtslw==', 'eb60ef85-113a-3a53-9f16-0750bb9c1545', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('e6ce59ec-0538-d120-2246-2a12381531a5', '5775545', '158191', 'Don', '严子韬', '男', 'zitao1222@gmail.com', 'https://pic3.zhimg.com/v2-6e02307139f69e0e7c5bc3fdab603af2_r.jpg?source=1940ef5c', '对编写脚本和自动化任务感兴趣的脚本编程学生。', 0, '2944e863-5358-aeeb-e341-f8101291bb0d', NULL, NULL, 0, 'AQAAAAIAAYagAAAAECv+vqRi7cY3pVaBbaGpUEGMJUdhDl22TfJRF/Mruo646BMDiPjxvA+a3VLC4XklHQ==', 'ec40d703-2e06-91b7-0c89-90ba74676695', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('e6d5b5ba-9e98-7405-0ab1-f893fb515d2e', '2881165', '857274', 'Victor', '王岚', '女', 'walan@yahoo.com', 'https://pic3.zhimg.com/v2-b20813ec0f8e935787ad798bff827982_r.jpg?source=1940ef5c', '学习数据库管理和数据分析的数据库学生。', 0, '2944e863-5358-aeeb-e341-f8101291bb0d', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEI8SgG8YM78+lvtaMeuM9P9KYqiBhtmNdj71DXPMsBB8ON5dWynJ2HiIerJMDNoX2g==', 'edf6364e-c3cf-67de-2184-c7f2c8df255c', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('e6e7aa68-8883-ba7b-cb6c-0820b77fd85b', '7006863', '347045', 'Ann', '贾子异', '男', 'ziyij2@gmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.a8e978db3d5dd5b5b46854ad25f4bedc?rik=G7HMW4B74F%2bzGw&riu=http%3a%2f%2fimage.yjcf360.com%2fu%2fcms%2fwww%2f202008%2f26114217tn9z.png&ehk=XgWFdJAlm4ICgj4vi%2bKIxpLsZdzlVbtDLbjh%2fWNpLjM%3d&risl=&pid=ImgRaw&r=0', '研究人工智能和然语言处理的自然语言学生。', 0, '2944e863-5358-aeeb-e341-f8101291bb0d', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEK8SceG6Twk/KnuyU+ctIjMgwtGTd2ahIhff+yh1seTk1r2s3KtH8gst+7n/VbSWEw==', 'f13ea038-4e1e-96b3-3067-bcf3ac898bf3', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('e762aee9-6635-8eed-5aa8-bbf3b1bea783', '7258125', '505923', 'Lillian', '唐璐', '女', 'talu@gmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.a8e978db3d5dd5b5b46854ad25f4bedc?rik=G7HMW4B74F%2bzGw&riu=http%3a%2f%2fimage.yjcf360.com%2fu%2fcms%2fwww%2f202008%2f26114217tn9z.png&ehk=XgWFdJAlm4ICgj4vi%2bKIxpLsZdzlVbtDLbjh%2fWNpLjM%3d&risl=&pid=ImgRaw&r=0', '学习软件测试和质量控制的软件测试学生。', 0, '2944e863-5358-aeeb-e341-f8101291bb0d', NULL, NULL, 0, 'AQAAAAIAAYagAAAAENj222Vj308QVdFruaWPUJP4NnFxWXLNwwcKH0wAJMu6JDd+rmy+2F6813GlHGYzfQ==', 'f1d9d36b-02ae-c599-1018-b3be3a9dc134', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('e8542066-8450-207c-f362-f21ea56d8004', '9086396', '174052', 'Don', '高詩涵', '女', 'shihangao@hotmail.com', 'https://pic4.zhimg.com/v2-c8ef068a749584593d90d2a6696f460f_r.jpg?source=1940ef5c', '对网络编程和网络安全感兴趣的网络工程学生。', 0, '2944e863-5358-aeeb-e341-f8101291bb0d', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEM5AHIKCCUHUVzCvpp2XzVVb4F4iiO/JpeHdQNGN03PY4ydb1dQe0yleJqq7Jt2osg==', 'f1e8d839-32d2-98ca-019d-b1f7031b06fc', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('e86b0af5-12b8-8b0e-9fc7-8049dbcb3d14', '6858541', '392784', 'Bryan', '熊云熙', '男', 'yunxi41@yahoo.com', 'https://pic2.zhimg.com/v2-8b79c4324ecef7e20d74af1d3b4c151f_r.jpg?source=1940ef5c', '热衷于算法设计和解决复杂问题的算法学生。', 0, '2944e863-5358-aeeb-e341-f8101291bb0d', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEFd8OHo120EV27lVf4whwqvkmEIaXewihtfkh6iWcc+cyDGRiwqi5SBPqypCj3eUgA==', 'f2a1fb5e-5ed4-e2a4-39d3-36116560b576', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('e8b068b1-aba6-0cf6-a668-a1fa07b7a85e', '2139083', '337372', 'Joan', '林璐', '女', 'lulin@icloud.com', 'https://pic3.zhimg.com/v2-de0df3e1f7ab4a8f860a43eb0ff28d2a_r.jpg?source=1940ef5c', '学习大数据分析和数据可视化的大数据学生。', 0, '2944e863-5358-aeeb-e341-f8101291bb0d', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEOPPabFc0yqSoCEjsIt0kF368PqdWyIj3dKhRVfcOtTXoRwzKs46sh8/SdX7XO90Iw==', 'f390b4c6-2f56-5c45-88ef-65d26c3674c6', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('e90ab4ab-9cf3-ea49-461c-423062b950e7', '1830740', '729108', 'Carmenw', '邱云熙', '男', 'yunxi40@hotmail.com', 'https://pic2.zhimg.com/v2-9170964184631f6eccb5dc7003e5cbbd_r.jpg?source=1940ef5c', '对视频游戏设计和开发充满热情的游戏设计学生。', 0, '2944e863-5358-aeeb-e341-f8101291bb0d', NULL, NULL, 0, 'AQAAAAIAAYagAAAAENvpgdQe7AG+IiIrrOU4jQ+MTLhBeSCmwURGlRbp6Sw3N7YGMAt9998ZNKEE9UYf+A==', 'f5dbe25e-7d71-f7e3-b2b0-4b9a8e80cd70', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('ea5cb916-fd40-56f2-0fc1-2c2347368fe3', '9163253', '423613', 'Rebecca', '韩晓明', '男', 'xiaoming622@gmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.491644887528381158f52296af369bfc?rik=hZoMfOCjZqvugA&riu=http%3a%2f%2finews.gtimg.com%2fnewsapp_match%2f0%2f4386249545%2f0&ehk=OXEYgHynwl%2f8fFSndcSH8xDN0ySU%2b0IN29yExPktry0%3d&risl=&pid=ImgRaw&r=0', '研究分布式计算和云计算的云计算学生。', 0, '2944e863-5358-aeeb-e341-f8101291bb0d', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEDP+c81c3+RdA/05wwiCGH09uFxygmo5llv/GYxoM1XMHfZyate+nHP+YjNjlOnIzw==', 'f651ff8e-f1fc-df67-96f4-4d3e0a8db114', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('eb9f57ce-5053-5ed4-f570-a802d632996d', '5333179', '157865', 'Clara', '雷晓明', '男', 'xialei8@mail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.02b07e9873a3e2add1fd0a729a07aa21?rik=QE630%2fg3b98LpA&riu=http%3a%2f%2fstar.yule.com.cn%2fuploadfile%2f2018%2f1109%2f20181109051045830.jpg&ehk=%2bpqoGJZQpWRmKr8oDAcxM3k67BP%2b0seYQBvhIWacNms%3d&risl=&pid=ImgRaw&r=0', '学习软件开发和团队协作的软件工程学生。', 0, '2944e863-5358-aeeb-e341-f8101291bb0d', NULL, NULL, 0, 'AQAAAAIAAYagAAAAECb7igQj8izUas2F+O3VKMf9DWcpu/gxPowUEefRvfafzXC9a8coQtUU9LTiAYylBw==', 'f89f5db2-cfe5-6246-ed52-cfb3f798a09d', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('ec28cec4-96e6-7b28-8813-d7dad8a7aab4', '5022943', '269507', 'Nathan', '郑秀英', '女', 'xiuying9@gmail.com', 'https://pic2.zhimg.com/50/v2-222b3f64389bde575a8af38425c4fca7_hd.jpg?source=1940ef5c', '热衷于人工智能和计算机视觉的计算机视觉学生。', 0, '2944e863-5358-aeeb-e341-f8101291bb0d', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEAiFohRfSQnhbfo67XFwMGdSpXYysT63RltSuA9M4rbLqlNI848iGaKmQnJkASOO6A==', 'f97ac328-7f98-ea03-d10b-5bd24921d0e5', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('ec2a1928-fa78-1937-53fa-0bd77e4b6400', '8509200', '308794', 'Norma', '唐子异', '男', 'tang8@outlook.com', 'https://www.easthulk.com/d/file/p/2022/10-08/96b5a116e7ec66bd63862325a650d60a.jpg', '对物联网和嵌入式系统有浓厚兴趣的物联网学生。', 0, '2944e863-5358-aeeb-e341-f8101291bb0d', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEGpyjuFp5i15oTrdhPC1nxIm/vZxhFwkLtInaKdepJ+spx4xed+vEIBrmCNOJNlBVw==', 'fa058b06-4c04-07ba-8d78-9ea9df23f7ce', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('ec656074-b8cb-b03e-d47b-5277c5978448', '3803639', '602686', 'Adam', '毛致远', '男', 'maozhiyuan61@yahoo.com', 'https://pic1.zhimg.com/50/v2-723159568ee81ca692118fc74ff4f75a_720w.jpg?source=1940ef5c', '学习金融科技和区块链技术的金融科技学生。', 0, '2944e863-5358-aeeb-e341-f8101291bb0d', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEIyWb6KFfJKThjokDJVUqBC4cz+2bzmFRmHBcbnV9oSyG8CY6mxFU+EeSOmnZ79yGQ==', 'fb372476-df5c-643a-eaaf-ec0f5d2dda2e', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('edb7b44c-cd60-258e-71b9-5347a757ce76', '4332686', '350471', 'Brandon', '许子异', '男', 'xuziy2018@outlook.com', 'https://pic1.zhimg.com/50/v2-e61958a229ee8ab72bb81b0f0c824d92_hd.jpg?source=1940ef5c', '研究人机交互和图形处理的图形学学生。', 0, '2944e863-5358-aeeb-e341-f8101291bb0d', NULL, NULL, 0, 'AQAAAAIAAYagAAAAECQk1GaTWBq3Y3j19x8r43Mv/RlX2AJC0vnvjnqZbE+L1hbEEsIr+Bes1HbXwcAulQ==', 'fbfac134-83bc-910c-e6ac-bb9a1b6712c8', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('ee8cd765-0150-d576-c95c-bc054e0a1e13', '9961874', '113072', 'Teresa', '黎致远', '男', 'li214@outlook.com', 'https://pic2.zhimg.com/v2-f24cf7eb84a5a0d6e82d951e29638a51_r.jpg', '对人工智能和机器人技术感兴趣的机器人学生。', 0, '2944e863-5358-aeeb-e341-f8101291bb0d', NULL, NULL, 0, 'AQAAAAIAAYagAAAAECJmPl7GdOjXE/vtfuDh75lQymrrwzcM6uNvc1tK+4iIXINMZHsUQJKXYUTQ/UJrZw==', 'fc0c3a91-1272-ebd3-4dde-d53f8d595409', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('eed7e69e-74c1-f3a8-1f88-ae509089b6b1', '3069927', '177579', 'Jacqueline', '严詩涵', '女', 'shihan314@icloud.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.d0cdb390350600169835c8343480b0af?rik=gLkyDf9xwbXdQg&riu=http%3a%2f%2finews.gtimg.com%2fnewsapp_match%2f0%2f15103659087%2f0&ehk=zK0MTj34tChhjHcpXaZER22pNZjchBBOnrkMTllNa0w%3d&risl=&pid=ImgRaw&r=0', '学习自然语言处理和文本分析的自然语言生。', 0, '2944e863-5358-aeeb-e341-f8101291bb0d', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEMe5geOYSO+rLDnU4+KypNs/FLmkDzoiGh6wumBLpDji6BRYKnzGA+ShY9x4fxJ6Mg==', 'fc7c241e-7523-50a8-b31e-72a44f4b4a34', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('ef5945bd-f6cb-ed95-5d53-efaed920fc7f', '5194755', '923800', 'Betty', '汤秀英', '女', 'tanxiuyi1@gmail.com', 'https://ts1.cn.mm.bing.net/th/id/R-C.e701be5804570862077e8e10b27d51d5?rik=FqILmSGfSG5R5Q&riu=http%3a%2f%2finews.gtimg.com%2fnewsapp_match%2f0%2f15103659094%2f0&ehk=e5mtcBgeE%2fXLRmHpp5lj%2bYoeQvTfU8%2bVusN1PkxxYTM%3d&risl=&pid=ImgRaw&r=0', '热衷于语音识别和音频处理语音处理学生。', 0, '2944e863-5358-aeeb-e341-f8101291bb0d', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEMmWGTl8mcnhaKos1P7k4KtNcYUlPS9bC7047NGlFVrmYkmT8EDvHGrXKZEl4oV2XA==', 'fd0728c3-8e48-3054-0c91-a6abc4ef998f', NULL, NULL, 0, 0, NULL, 0, 5);
INSERT INTO `user` VALUES ('f03842db-4762-96ca-d9f6-c2da9294bf7b', '8070200', '229591', 'Eddie', '王云熙', '男', 'wangyunxi@gmail.com', 'https://shuji.tmwcn.com/upload/avatar/20220809/a_1660060460.jpg', '对网络安全和渗透测试有浓厚兴趣的网络安全学生。', 0, '2944e863-5358-aeeb-e341-f8101291bb0d', NULL, NULL, 0, 'AQAAAAIAAYagAAAAEABGYZqYqAOjZzrq9FWxyQoCDeKKH1AuIw/bsj1wv5FKEpun7LM/CkvTMB8W4RhXng==', 'ff2ecc9d-2440-8855-1c31-d8b517910ad3', NULL, NULL, 0, 0, NULL, 0, 5);

SET FOREIGN_KEY_CHECKS = 1;
