﻿using Demo.IRepository;
using Demo.IService;
using Demo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Service
{
    public class CourseFileService:BaseService<CourseFile>,ICourseFileService
    {
        private ICourseFileRepository _repository;
        public CourseFileService(ICourseFileRepository repository)
        {
            base._base = repository;
            _repository = repository;
        }
    }
}
