﻿using Demo.IRepository;
using Demo.IService;
using Demo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Service
{
    public class CampusService:BaseService<Campus>,ICampusService
    {
       private ICampusRepository _repository;
       public CampusService(ICampusRepository repository)
        {
            base._base = repository;
            _repository = repository;
        }
    }
}
