﻿using Demo.IRepository;
using Demo.IService;
using Demo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Service
{
    public class ClassRecordService:BaseService<ClassRecord>,IClassRecordService
    {
        private IClassRecordRepository _repository;
        public ClassRecordService(IClassRecordRepository repository)
        {
            base._base=repository;
            _repository = repository;
        }
    }
}
