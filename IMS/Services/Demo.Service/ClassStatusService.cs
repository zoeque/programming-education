﻿using Demo.IRepository;
using Demo.IService;
using Demo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Service
{
    public class ClassStatusService:BaseService<ClassStatus>,IClassStatusService
    {
        private IClassStatusRepository _repository;
        public ClassStatusService(IClassStatusRepository repository)
        {
            base._base =  repository;
            _repository = repository;
        }
    }
}
