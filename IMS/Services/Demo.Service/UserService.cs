﻿using Demo.IRepository;
using Demo.IService;
using Demo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Service
{
public class UserService:BaseService<User>,IUserService
{
    private IUserRepository _repository;
    public UserService(IUserRepository repository)
    {
        base._base= repository;
        _repository = repository;
    }
}
}
