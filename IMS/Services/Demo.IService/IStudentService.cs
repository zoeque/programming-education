﻿using Demo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.IService
{
    public interface IStudentService:IBaseService<Student>
    {
    }
}
