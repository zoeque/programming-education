﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Demo.IService
{
    public interface IBaseService<T> where T : class,new()
    {
        public Task<bool> CreateAsync(T entity);
        public Task<bool> UpdateAsync(T entity);
        public Task<bool> DeleteAsync(T entity);
        public Task<T> FindOneAsync(Expression<Func<T, bool>> exp); 
        public Task<T> FindIdAsync(Guid id);
        public Task<List<T>> FindManyAsync(Expression<Func<T, bool>> exp);
        public Task<List<T>> FindAllAsync();
        public Task<List<T>> PagingAsync(int page,int size);
    }
}
