﻿using Demo.Model;

namespace Demo.IService;

public interface IRoleService : IBaseService<Role>
{
    
}