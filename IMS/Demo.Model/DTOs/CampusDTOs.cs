﻿namespace Demo.Model.DTOs;

public class CampusDTOs
{
    // ID
    public Guid ID { get; set; }
    //校区名字
    public string CmName{get;set;}
    //校区地址
    public string? CmSite{get;set;}
    //校区简介
    public string? CmText{get;set;}

    
            //  仅供查询使用  添加数据时请勿写以下字段
    
    // 教室
    public List<string> Classroom { get; set; } = new List<string>();
}