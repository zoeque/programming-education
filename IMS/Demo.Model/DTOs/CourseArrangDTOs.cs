﻿namespace Demo.Model.DTOs;

public class CourseArrangDTOs
{
    // ID
    public Guid ID { get; set; }
    //上课时间
    public string? InCourseTime{get;set;}
    //课程类型
    public Guid CourseType {get;set;}
    //上课老师
    public Guid? InTcnameID{get;set;}
    //上课的教室
    public Guid InClassRoomNo{get;set;}
    //上课的班级
    public Guid InClassNo{get;set;}
    // 软删除
    public Boolean IsDeleted { get; set; }
    
    
            //  仅供查询使用  添加数据时请勿写以下字段

    //课程类型
    public string? CourseTypeName {get;set;}
    //上课老师
    public string? TeacherName{get;set;}
    //上课的教室
    public int? ClassRoomNo{get;set;}

}