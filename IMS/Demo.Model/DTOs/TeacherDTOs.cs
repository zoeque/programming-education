﻿namespace Demo.Model.DTOs;

public class TeacherDTOs
{
    // ID
    public Guid ID { get; set; }
    
    //ID所有用户外键
    public Guid UseID {get;set;}  

    //学历
    public string TcEducation{get;set;}
      
    //毕业学校
    public string TcSchool{get;set;}

    // 大学所学专业
    public string? Major {get;set;}

    //身份证
    public string TcCard{get;set;}

    //教师等级
    public Guid? LevelID{get;set;}

    //是否在职
    public Boolean? IsWorking{get;set;}
    // 入职时间
    public DateTime? EntryTime {get;set;}

    // 软删除
    public Boolean? IsDeleted {get;set;}
    
    
    
    
    
    
            //  仅供查询使用  添加数据时请勿写以下字段
    
    // 老师名字
    public string? UserName { get; set; }
    //性别
    public string?  Sex {get;set;}
    // 邮箱 
    public string? Email { get; set; }
    //头像
    public string? Icon{get;set;}
    //简介
    public string? Intro{get;set;}

    //教师等级
    public string? LevelName{get;set;}

    //所教班级
    public List<string>? TcClassName { get; set; } = new List<string>();
}