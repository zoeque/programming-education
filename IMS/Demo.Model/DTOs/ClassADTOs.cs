﻿namespace Demo.Model.DTOs;

public class ClassADTOs
{
    //id
    public Guid ID { get; set; }
    //班级编号
    public string CalssNo{get;set;}
    //归属校区
    public Guid? CampusNameID{get;set;}
    //班级id
    public Guid? TeacherId { get; set; }
    //班级类型
    public string? ClassType{get;set;}
    //完成课时
    public int CourseOK{get;set;}
    //是否毕业
    public Boolean? IsGraduate{get;set;} 
    
}