﻿namespace Demo.Model.DTOs;

public class StudentInfoDTOs
{
    // ID
    public Guid ID { get; set; }
    
    //学生在用户表的id
    public Guid UseId{get;set;}
    
    //所属班级
    public Guid? ClassNo{get;set;}
    
    // 入学时间
    public DateTime? EnrollTime {get;set;}
    
    // 软删除
    public Boolean IsDeleted { get; set; }
    
    
    
            //  仅供查询使用  添加数据时请勿写以下字段
    
    // 用户名字
    public string? StuName { get; set; }
    //所属班级
    public string? ClassName{get;set;}
    //性别
    public string?  Sex {get;set;}
    // 邮箱 
    public string? Email { get; set; }
    //头像
    public string? Icon{get;set;}
    //简介
    public string? Intro{get;set;}
    
}