﻿namespace Demo.Model.DTOs;

public class ClassRoomDTOs
{
    // ID
    public Guid ID { get; set; }
    //教室编号
    public int ClassRoomNo{get;set;}
    //归属校区
    public Guid? CampusID{get;set;}
    //教室地址
    public string? ClassRoomSite{get;set;}
    //是否占用
    public Boolean? IsOccupy{get;set;}
    // 软删除
    public Boolean IsDeleted { get; set; }
    
    
            //  仅供查询使用  添加数据时请勿写以下字段
    //归属校区
    public string? CmName{get;set;}
}