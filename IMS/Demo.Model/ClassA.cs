using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.Model
{
    //班级信息表
    public class ClassA : BaseID
    {
      //班级编号
      public string CalssNo{get;set;}
      //归属校区
      public Guid? CampusNameID{get;set;}
      // 任课老师
      public Guid? TeacherId { get; set; }
      //班级类型
      public string? ClassType{get;set;}
      //完成课时
      public int? CourseOK{get;set;}
      //是否毕业
      public Boolean IsGraduate{get;set;} 
      
        
      
                //  导航属性
      // 一个班级对多个学生
      public List<Student>? D_student{get;set;} 
      // 一个校区有多个班级
      public Campus? campus { get; set; }
      // 一个班级只能被一个老师教
      public Teacher? teacher { get; set; }
      // 一个班级只有一个结课记录
      public EndClass? endclass { get; set; }
      // 一个班级有多个课程信息
      public List<CourseArrang>?  courseArrangs{get;set;}
      // 课程记录
      public List<ClassRecord>? ClassRecord { get; set; }
    }
}