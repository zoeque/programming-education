﻿namespace Demo.Model;
// 结课表
public class EndClass : BaseID
{
    
    public string IsEndClass { get; set; }
    // 班级外键
    public Guid? ClassId { get; set; }
    // 导航属性
    public ClassA? End_Class { get; set; }
}