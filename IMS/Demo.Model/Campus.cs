using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Demo.Model
{
    //校区
    public class Campus : BaseID
    {
        //校区名字
        public string CmName{get;set;}
        //校区地址
        public string? CmSite{get;set;}
        //校区简介
        public string? CmText{get;set;}
        //导航属性 一个校区对应多个教室
        public List<ClassRoom>? D_classroom{get;set;}
    }
}