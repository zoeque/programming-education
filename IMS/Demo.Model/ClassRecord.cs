using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.Model
{
    //上课记录表
    public class ClassRecord : BaseID
    {
        //上课的班级
        public Guid InClassNo{get;set;}
        //下课时间
        public DateTime? StopTime{get;set;}
        // 排课表id
        public Guid? CourseArrangId { get; set; }
        // 一个记录对应一个状态
        public Guid? StatusId { get; set; }
        
        // 导航属性设置
        // 一节课只有一个班级上
        public ClassA? ClassA { get; set; }
        // 一个上课记录对应一个排课信息
        public CourseArrang? coursearrang { get; set; }
        // 一个记录对应一个状态
        public ClassStatus? classStatus { get; set; }
    }
}