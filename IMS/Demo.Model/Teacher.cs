using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.Model
{
    //教师表
    public class Teacher:BaseID
    {
      //ID所有用户外键
      public Guid UseID {get;set;}  

      //学历
      public string TcEducation{get;set;}
      
      //毕业学校
      public string TcSchool{get;set;}

      // 大学所学专业
      public string? Major {get;set;}

      //身份证
      public string TcCard{get;set;}

      //教师等级
      public Guid? LevelID{get;set;}
      
      //是否在职
      public Boolean? IsWorking{get;set;}
      // 入职时间
      public DateTime? EntryTime {get;set;}

      // 软删除
      public Boolean IsDeleted {get;set;}


      //导航属性对应用户表
      public User? TcD_user{get;set;}
      //导航属性一个老师对应多个班级
      public List<ClassA>? TcD_class{get;set;}
      // 导航属性 一个老师对一个等级
      public Level? TcD_tclevel {get;set;}

      //一个老师对应对课时
      public List<CourseArrang>? TcD_coursearrang{get;set;}

    }
}
