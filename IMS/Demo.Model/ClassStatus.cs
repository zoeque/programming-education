using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.Model
{
    //上课状态表
    public class ClassStatus : BaseID
    {
        //外键连接排课信息表
        // public Guid? StatusID{get;set;}
        //状态
        public string? StatusName {get;set;}

        //一个状态对多记录
        public List<ClassRecord>? CsD_ClassRecord{get;set;}
    }
}