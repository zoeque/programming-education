using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Demo.Model
{
    //课程资料表
    public class CourseFile :BaseID
    {
        //课程类型名称
        public string CourseTypeName {get;set;}
        //等级
        public Guid? LevelID {get;set;}
        //课时
        public string? CourseHour {get;set;}
        // 软删除
        public Boolean IsDeleted {get; set; }
        
        //导航属性 一个课程对多个知识点
        public List<Knowledge>? D_knowledge{get;set;}
        //多个课程对应一个等级
        public Level? D_level{get;set;}
        // 一个课程有多个排课信息
        public List<CourseArrang>? courseArrang {get;set;}
    }
}