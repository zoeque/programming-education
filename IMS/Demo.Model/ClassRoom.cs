using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.Model
{
    //教室表
    public class ClassRoom :BaseID
    {
        //教室编号
        public int ClassRoomNo{get;set;}
        //归属校区
        public Guid? CampusID{get;set;}
        //教室地址
        public string? ClassRoomSite{get;set;}
        //是否占用
        public Boolean? IsOccupy{get;set;}  // 默认为没被占用
        // 软删除
        public Boolean IsDeleted { get; set; }
        
                    // 导航属性
        //导航属性 一个教室对一个校区
        public Campus? D_Campus{get;set;}
        //一间教室对多课时
        public List<CourseArrang>? ClD_coursearrange{get;set;}
        
    }
}