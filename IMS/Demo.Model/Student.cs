namespace Demo.Model
{
    //学生表
    public class Student : BaseID
    {
        //学生在用户表的id
        public Guid UseId{get;set;}
        //所属班级
        public Guid? ClassNo{get;set;}
        // 入学时间
        public DateTime? EnrollTime {get;set;}
        // 软删除
        public Boolean IsDeleted { get; set; }
        //导航属性一个学生对应一个班级
        public ClassA? StD_Class{get;set;}
        //一个学生对应一个用户
        public User? StD_user {get;set;}
    }
}