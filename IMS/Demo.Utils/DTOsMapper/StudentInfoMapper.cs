﻿using AutoMapper;
using Demo.Model;
using Demo.Model.DTOs;

namespace Demo.Utils.DTOsMapper;
// StudentInfoMapper这个类 就是用来实现把Model映射到ModelDTO
public class StudentInfoMapper : Profile
{
    //在构造函数中配置
    public StudentInfoMapper() //此时的src相当于Student表
    {
        //base.CreateMap<>()  --> 创建一个映射
        //base.CreateMap<源头(谁要映射),映射给谁>()
        //ForMember() 用于为目标对象的特定属性设置自定义映射规则 或者说为类中哪个属性进行映射
        base.CreateMap<Student, StudentInfoDTOs>().ForMember(u => u.ID, opt =>
        {
            opt.MapFrom(src=>src.ID); // 
        }).ForMember(u => u.UseId, opt =>
        {
            opt.MapFrom(src => src.UseId); // 对学生的id进行映射
        }).ForMember(u => u.Sex, opt =>
        {
            opt.MapFrom(src => src.StD_user.Sex); // 对学生的名字进行映射
        }).ForMember(u => u.StuName, opt =>
        {
            opt.MapFrom(src => src.StD_user.Name); // 对学生的性别进行映射
        }).ForMember(u => u.ClassName, opt =>
        {
            opt.MapFrom(src => src.StD_Class.CalssNo); // 对学生的班级进行映射
        }).ForMember(u => u.Email, opt =>
        {
            opt.MapFrom(src => src.StD_user.Email); // 对学生的邮箱进行映射
        }).ForMember(u => u.Icon, opt =>
        {
            opt.MapFrom(src => src.StD_user.Icon); // 对学生的头像进行映射
        }).ForMember(u => u.Intro, opt =>
        {
            opt.MapFrom(src => src.StD_user.Intro); // 对学生的简介进行映射
        }).ForMember(u => u.EnrollTime, opt =>
        {
            opt.MapFrom(src => src.EnrollTime); // 对学生的入学时间进行映射
        });
    }
}