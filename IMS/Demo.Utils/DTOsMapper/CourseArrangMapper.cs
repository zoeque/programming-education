﻿using AutoMapper;
using Demo.Model;
using Demo.Model.DTOs;

namespace Demo.Utils.DTOsMapper;

public class CourseArrangMapper : Profile
{
    public CourseArrangMapper()
    {
        base.CreateMap<CourseArrang, CourseArrangDTOs>().ForMember(u => u.ID, opt =>
        {
            opt.MapFrom(src=>src.ID); 
        }).ForMember(c=>c.InCourseTime, opt =>
        {
            opt.MapFrom(src=>src.InCourseTime);
        }).ForMember(c=>c.CourseTypeName, opt =>
        {
            opt.MapFrom(src=>src.CourseFile.CourseTypeName);
        }).ForMember(c=>c.TeacherName, opt =>
        {
            opt.MapFrom(src=>src.CaD_teacher.TcD_user.UserName);
        }).ForMember(c=>c.ClassRoomNo, opt =>
        {
            opt.MapFrom(src=>src.CaD_classroom.ClassRoomNo);
        }).ForMember(c=>c.InClassNo, opt =>
        {
            opt.MapFrom(src=>src.InClassNo);
        });
    }
}