﻿using AutoMapper;
using Demo.Model;
using Demo.Model.DTOs;

namespace Demo.Utils.DTOsMapper;

public class CourseFileMapper : Profile
{
    public CourseFileMapper()
    {
        base.CreateMap<CourseFile, CourseFileDTOs>().ForMember(u => u.ID, opt =>
        {
            opt.MapFrom(src=>src.ID); 
        }).ForMember(c=>c.CourseTypeName, opt =>
        {
            opt.MapFrom(src=>src.CourseTypeName);
        }).ForMember(c=>c.LevelName, opt =>
        {
            opt.MapFrom(src=>src.D_level.LevelName);
        }).ForMember(c=>c.CourseHour, opt =>
        {
            opt.MapFrom(src=>src.CourseHour);
        }).ForMember(c=>c.KLName, opt =>
        {
            opt.MapFrom(src=>src.D_knowledge.Select(k=>k.KLName));
        });
    }
}