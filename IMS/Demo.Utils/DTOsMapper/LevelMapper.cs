﻿using AutoMapper;
using Demo.Model;
using Demo.Model.DTOs;

namespace Demo.Utils.DTOsMapper;

public class LevelMapper : Profile
{
    public LevelMapper()
    {
        base.CreateMap<Level, LevelDTOs>().ForMember(u => u.ID, opt =>
        {
            opt.MapFrom(src=>src.ID); 
        }).ForMember(l=>l.LevelName, opt =>
        {
            opt.MapFrom(src=>src.LevelName);
        }).ForMember(l=>l.CourseTypeName, opt =>
        {
            opt.MapFrom(src=>src.D_coursefile.Select(c=>c.CourseTypeName));
        });
    }
}