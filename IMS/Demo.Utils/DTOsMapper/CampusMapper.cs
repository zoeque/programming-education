﻿using AutoMapper;
using Demo.Model;
using Demo.Model.DTOs;

namespace Demo.Utils.DTOsMapper;

public class CampusMapper : Profile
{
    public CampusMapper()
    {
        base.CreateMap<Campus, CampusDTOs>().ForMember(u => u.ID, opt =>
        {
            opt.MapFrom(src=>src.ID); 
        }).ForMember(c => c.CmName, opt =>
        {
            opt.MapFrom(src=>src.CmName);
        }).ForMember(c => c.CmSite, opt =>
        {
            opt.MapFrom(src=>src.CmSite);
        }).ForMember(c => c.CmText, opt =>
        {
            opt.MapFrom(src=>src.CmText);
        }).ForMember(c => c.Classroom, opt =>
        {
            opt.MapFrom(src=>src.D_classroom!.Select(c=>c.ClassRoomNo));
        });
    }
}