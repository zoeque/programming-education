﻿using AutoMapper;
using Demo.Model;
using Demo.Model.DTOs;

namespace Demo.Utils.DTOsMapper;

public class ClassAMapper : Profile
{
    public ClassAMapper()
    {
        base.CreateMap<ClassA, ClassADTOs>().ForMember(c=>c.ID, opt =>
        {
            opt.MapFrom(src=>src.ID);
        }).ForMember(c=>c.CalssNo, opt =>
        {
            opt.MapFrom(src=>src.CalssNo);
        }).ForMember(c=>c.CampusNameID, opt =>
        {
            opt.MapFrom(src=>src.CampusNameID);
        }).ForMember(c=>c.ClassType, opt =>
        {
            opt.MapFrom(src=>src.ClassType);
        }).ForMember(c=>c.CourseOK, opt =>
        {
            opt.MapFrom(src=>src.CourseOK);
        }).ForMember(c=>c.IsGraduate, opt =>
        {
            opt.MapFrom(src=>src.IsGraduate);
        });
    }
}