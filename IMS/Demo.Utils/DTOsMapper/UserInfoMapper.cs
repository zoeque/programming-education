﻿using AutoMapper;
using Demo.Model;
using Demo.Model.DTOs;
using Microsoft.AspNetCore.Http;

namespace Demo.Utils.DTOsMapper;
// UserInfoMapper这个类 就是用来实现把Model映射到ModelDTO
public class UserInfoMapper : Profile
{   //在构造函数中配置
    public UserInfoMapper()//此时的src相当于User表
    {
        //base.CreateMap<>()  --> 创建一个映射
        //base.CreateMap<源头(谁要映射),映射给谁>()
        //ForMember() 用于为目标对象的特定属性设置自定义映射规则 或者说为类中哪个属性进行映射
        // base.CreateMap<string, IFormFile>().ConvertUsing<IconConverter>();
        base.CreateMap<User, UserInfoDTOs>().ForMember(u => u.ID, opt =>
        {
            opt.MapFrom(src=>src.Id); // 对用户的名字进行映射
        }).ForMember(u => u.UserName, opt =>
        {
            opt.MapFrom(src=>src.UserName); // 对用户英文名字进行映射
        }).ForMember(u => u.Name, opt =>
        {
            opt.MapFrom(src=>src.Name); // 对用户中文名字进行映射
        }).ForMember(u => u.Sex, opt =>
        {
            opt.MapFrom(src=>src.Sex);// 对用户的性别进行映射
        }).ForMember(u => u.Email, opt =>
        {
            opt.MapFrom(src=>src.Email);// 对用户的邮箱进行映射
        }).ForMember(u => u.Icon, opt =>
        {
            opt.MapFrom(src=>src.Icon);// 对用户的头像进行映射
        }).ForMember(u => u.Intro, opt =>
        {
            opt.MapFrom(src=>src.Intro);// 对用户的简介进行映射
        }).ForMember(u => u.IsDeleted, opt =>
        {
            opt.MapFrom(src=>src.IsDeleted);// 软删除
        });
            /*.ForMember(u => u.RoleName, opt =>
        {
            opt.MapFrom(src=>src.D_role.Name);// 对用户的角色进行映射
        })*/
    }
}