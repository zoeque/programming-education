﻿using Demo.Utils.ApiReturn;

namespace Demo.Utils.ApiReturn;

public class ApiReturnHelper
{
    //两个静态方法：一个时成功时的返回数据格式，一个是失败
    public static ApiReturns Success(dynamic data)
    {
        return new ApiReturns()
        {
            Code = 200,
            Msg = "OK",
            Data = data
        };
    }
    public static ApiReturns Fail(string msg)
    {
        return new ApiReturns()
        {
            Code = 500,
            Msg = msg,
            Data = null
        };
    }
}