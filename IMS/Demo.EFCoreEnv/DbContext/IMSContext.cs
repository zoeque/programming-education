using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Demo.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
namespace Demo.EFCoreEnv.DbContext
{
public class IMSContext : IdentityDbContext<User, Role, Guid>
    {
        public IMSContext(DbContextOptions<IMSContext>options) : base(options) { }
        //用户表
        public DbSet<User> users {get;set;}
        //角色表
        public DbSet<Role> roles {get;set;}
        
        public DbSet<IdentityUserRole<Guid>> UserRoles { get; set; }
        
        //校区表
        public DbSet<Campus> campuses {get;set;}
        //教室表
        public DbSet<ClassRoom> classrooms {get;set;}
        //知识点表
        public DbSet<Knowledge> knowledges {get;set;}
        //教室等级表
        public DbSet<Level> Levels {get;set;}
        //课程资料表
        public DbSet<CourseFile> coursefiles {get;set;}
        //班级表
        public DbSet<ClassA> classeas {get;set;}
        //学生表
        public DbSet<Student> students {get;set;}
        //教师表
        public DbSet<Teacher> teacheres {get;set;}
        //排课信息表
        public DbSet<CourseArrang> courseArrangs {get;set;}
        //上课状态表
        public DbSet<ClassStatus> classstatuses {get;set;}
        //上课记录表
        public DbSet<ClassRecord> classrecords {get;set;}

        // 结课表
        public DbSet<EndClass> endClasses { get; set; }
        protected override void OnModelCreating(ModelBuilder model){

                model.Entity<Campus>().Property(opt => opt.CmText).HasColumnType("nvarchar(1000)");
                model.Entity<Campus>().Property(opt => opt.CmName).HasColumnType("nvarchar(200)");
                model.Entity<Campus>().Property(opt => opt.CmSite).HasColumnType("nvarchar(200)");

                //修改校区简介为text
                //model.Entity<Campus>().Property(opt => opt.CmText).HasColumnType("text");
                //教室表的过滤器
                model.Entity<ClassRoom>(opt => opt.HasQueryFilter(c => !c.IsDeleted));
                model.Entity<ClassRoom>().Property(u => u.IsDeleted).HasDefaultValue(false);
                //班级表
                model.Entity<ClassA>(opt => opt.HasQueryFilter(c => !c.IsGraduate));
                model.Entity<ClassA>().Property(u => u.IsGraduate).HasDefaultValue(false);
                //教师表
                model.Entity<Teacher>(opt => opt.HasQueryFilter(c => !c.IsDeleted));
                model.Entity<Teacher>().Property(u => u.IsDeleted).HasDefaultValue(false);
                //学生表
                model.Entity<Student>(opt => opt.HasQueryFilter(c => !c.IsDeleted));
                model.Entity<Student>().Property(u => u.IsDeleted).HasDefaultValue(false);
                //用户表
                model.Entity<User>(opt => opt.HasQueryFilter(c => !c.IsDeleted));
                model.Entity<User>().Property(u => u.IsDeleted).HasDefaultValue(false);
                // 课程信息
                model.Entity<CourseFile>(opt => opt.HasQueryFilter(c => !c.IsDeleted));
                model.Entity<CourseFile>().Property(u => u.IsDeleted).HasDefaultValue(false);
                // 排课信息表
                model.Entity<CourseArrang>(opt => opt.HasQueryFilter(c => !c.IsDeleted));
                model.Entity<CourseArrang>().Property(u => u.IsDeleted).HasDefaultValue(false);

                //外键 用户对角色
                model.Entity<User>()
                .HasOne(art=>art.D_role)
                .WithMany(art=>art.RD_user)
                .HasForeignKey(art=>art.RoleID);
                //教室对校区 外键
                model.Entity<ClassRoom>()
                .HasOne(art=>art.D_Campus)
                .WithMany(art=>art.D_classroom)
                .HasForeignKey(art=>art.CampusID);
                //知识点对课程资料 外键
                model.Entity<Knowledge>()
                .HasOne(art=>art.D_coursefile)
                .WithMany(art=>art.D_knowledge)
                .HasForeignKey(art=>art.CourseFileId);
                //教师对教师等级 外键
                model.Entity<Teacher>()
                .HasOne(art=>art.TcD_tclevel)
                .WithMany(art=>art.D_teacher)
                .HasForeignKey(art=>art.LevelID);
                //教师等级对课程资料 
                // model.Entity<TcLevel>()
                // .HasOne(art=>art.D_coursefile)
                // .WithMany()
                // .HasForeignKey(art=>art.ID);
                        // 班级表外键
                //班级对学生
                model.Entity<Student>()
                .HasOne(art=>art.StD_Class)
                .WithMany(art=>art.D_student)
                .HasForeignKey(art=>art.ClassNo);
                // 班级对校区
                model.Entity<ClassA>()
                .HasOne(art=>art.campus)
                .WithMany()
                .HasForeignKey(art=>art.CampusNameID);
                // 班级对老师  
                model.Entity<ClassA>()
                .HasOne(art=>art.teacher)
                .WithMany(c=>c.TcD_class)
                .HasForeignKey(art=>art.TeacherId);
                
                
                //学生对用户
                model.Entity<Student>()
                .HasOne(art=>art.StD_user)
                .WithMany()
                .HasForeignKey(art=>art.UseId);
               //教师对用户
               model.Entity<Teacher>()
               .HasOne(art=>art.TcD_user)
               .WithMany()
               .HasForeignKey(art=>art.UseID);
               //课时对老师
                model.Entity<CourseArrang>()
                .HasOne(art=>art.CaD_teacher)
                .WithMany(art=>art.TcD_coursearrang)
                .HasForeignKey(art=>art.InTcnameID);
               //课时对教室
               model.Entity<CourseArrang>()
               .HasOne(art=>art.CaD_classroom)
               .WithMany(art=>art.ClD_coursearrange)
               .HasForeignKey(art=>art.InClassRoomNo); 
               // 上课的类型
               model.Entity<CourseArrang>()
                   .HasOne(art=>art.CourseFile)
                   .WithMany(art=>art.courseArrang)
                   .HasForeignKey(art=>art.CourseType); 
               // 上课的班级
               model.Entity<CourseArrang>()
                   .HasOne(art=>art.ClassA)
                   .WithMany(art=>art.courseArrangs)
                   .HasForeignKey(art=>art.InClassNo); 

                                //上课记录表外键设置
                model.Entity<CourseArrang>()
                .HasOne(art=>art.classRecord)
                .WithOne(art=>art.coursearrang)
                .HasForeignKey<ClassRecord>(art=>art.CourseArrangId);

                model.Entity<ClassRecord>()
               .HasOne(art=>art.classStatus)
               .WithMany(art=>art.CsD_ClassRecord)
               .HasForeignKey(art=>art.StatusId);
                
                model.Entity<ClassRecord>()
                    .HasOne(art=>art.ClassA)
                    .WithMany(art=>art.ClassRecord)
                    .HasForeignKey(art=>art.InClassNo);
                
                    // 结课表外键
                model.Entity<ClassA>()
                    .HasOne(c => c.endclass)
                    .WithOne(e=>e.End_Class)
                    .HasForeignKey<EndClass>(e=>e.ClassId);
                // 用户和角色关联表
                model.Entity<IdentityUserRole<Guid>>(entity =>
                {
                    entity.ToTable("aspnetuserroles");
                    entity.HasKey(ur => new { ur.UserId, ur.RoleId });
                
                    entity.HasOne<User>()
                        .WithMany()
                        .HasForeignKey(ur => ur.UserId)
                        .IsRequired();
                
                    entity.HasOne<Role>()
                        .WithMany()
                        .HasForeignKey(ur => ur.RoleId)
                        .IsRequired();
                });
                model.Entity<User>().ToTable("user");
                model.Entity<Role>().ToTable("role");
                model.Entity<Level>().ToTable("levels");
		model.Entity<CourseArrang>().ToTable("coursearrangs");
		model.Entity<EndClass>().ToTable("endclasses");
                // 删除 IdentityUserLogin<Guid> 的配置
                 
                 model.Ignore<IdentityUserLogin<Guid>>();
                model.Ignore<IdentityUserLogin<string>>();
                model.Ignore<IdentityUserClaim<Guid>>();
                model.Ignore<IdentityUserClaim<string>>();
                model.Ignore<IdentityUserToken<Guid>>();
                model.Ignore<IdentityUserToken<string>>();
                model.Ignore<IdentityRoleClaim<Guid>>();
                model.Ignore<IdentityRoleClaim<string>>();

                // model.Entity<IdentityUserRole<Guid>>().HasKey();
                // model.Entity<IdentityUserRole<Guid>>().ToTable("AspNetUserRoles");
                // 数据约束
                // 设置登录失败的次数默认值为0
                model.Entity<User>().Property(u=>u.AccessFailedCount).HasDefaultValue(5);

                // 对用户表的手机号设置   唯一约束 
                model.Entity<User>().HasIndex(u=>u.PhoneNumber).IsUnique();

                // 对用户表的邮箱设置   唯一约束 
                model.Entity<User>().HasIndex(u=>u.Email).IsUnique();

                // 对老师表的身份证号设置唯一约束
                model.Entity<Teacher>().HasIndex(u=>u.TcCard).IsUnique();
                
                                // 默认值 
                model.Entity<User>().Property(u => u.EmailConfirmed).HasDefaultValue(false);
                model.Entity<User>().Property(u => u.PhoneNumberConfirmed).HasDefaultValue(false);
                model.Entity<User>().Property(u => u.TwoFactorEnabled).HasDefaultValue(false);
                model.Entity<User>().Property(u => u.LockoutEnabled).HasDefaultValue(false);
                // 教师是否在职 设置默认为true  默认在职
                model.Entity<Teacher>().Property(u => u.IsWorking).HasDefaultValue(true);
                // 班级表是否毕业  默认为false  未毕业
                model.Entity<ClassA>().Property(u => u.IsGraduate).HasDefaultValue(false);
                // 教室是否被占用默认为 false 未被占用
                model.Entity<ClassRoom>().Property(u => u.IsOccupy).HasDefaultValue(false);


        }
    }}