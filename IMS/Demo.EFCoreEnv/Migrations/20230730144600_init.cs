﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Demo.EFCoreEnv.Migrations
{
    /// <inheritdoc />
    public partial class init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("MySQL:Charset", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "campuses",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "char(36)", nullable: false),
                    CmName = table.Column<string>(type: "nvarchar(200)", nullable: false),
                    CmSite = table.Column<string>(type: "nvarchar(200)", nullable: true),
                    CmText = table.Column<string>(type: "nvarchar(1000)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_campuses", x => x.ID);
                })
                .Annotation("MySQL:Charset", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "classstatuses",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "char(36)", nullable: false),
                    StatusName = table.Column<string>(type: "longtext", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_classstatuses", x => x.ID);
                })
                .Annotation("MySQL:Charset", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "levels",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "char(36)", nullable: false),
                    LevelName = table.Column<string>(type: "longtext", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_levels", x => x.ID);
                })
                .Annotation("MySQL:Charset", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "role",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false),
                    Name = table.Column<string>(type: "longtext", nullable: true),
                    NormalizedName = table.Column<string>(type: "longtext", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "longtext", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_role", x => x.Id);
                })
                .Annotation("MySQL:Charset", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "classrooms",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "char(36)", nullable: false),
                    ClassRoomNo = table.Column<int>(type: "int", nullable: false),
                    CampusID = table.Column<Guid>(type: "char(36)", nullable: true),
                    ClassRoomSite = table.Column<string>(type: "longtext", nullable: true),
                    IsOccupy = table.Column<bool>(type: "tinyint(1)", nullable: true, defaultValue: false),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_classrooms", x => x.ID);
                    table.ForeignKey(
                        name: "FK_classrooms_campuses_CampusID",
                        column: x => x.CampusID,
                        principalTable: "campuses",
                        principalColumn: "ID");
                })
                .Annotation("MySQL:Charset", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "coursefiles",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "char(36)", nullable: false),
                    CourseTypeName = table.Column<string>(type: "longtext", nullable: false),
                    LevelID = table.Column<Guid>(type: "char(36)", nullable: true),
                    CourseHour = table.Column<string>(type: "longtext", nullable: true),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_coursefiles", x => x.ID);
                    table.ForeignKey(
                        name: "FK_coursefiles_levels_LevelID",
                        column: x => x.LevelID,
                        principalTable: "levels",
                        principalColumn: "ID");
                })
                .Annotation("MySQL:Charset", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "user",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false),
                    Account = table.Column<string>(type: "longtext", nullable: false),
                    Password = table.Column<string>(type: "longtext", nullable: false),
                    Name = table.Column<string>(type: "longtext", nullable: true),
                    Sex = table.Column<string>(type: "longtext", nullable: false),
                    Email = table.Column<string>(type: "varchar(255)", nullable: false),
                    Icon = table.Column<string>(type: "longtext", nullable: true),
                    Intro = table.Column<string>(type: "longtext", nullable: true),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false),
                    RoleID = table.Column<Guid>(type: "char(36)", nullable: false),
                    UserName = table.Column<string>(type: "longtext", nullable: true),
                    NormalizedUserName = table.Column<string>(type: "longtext", nullable: true),
                    NormalizedEmail = table.Column<string>(type: "longtext", nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false),
                    PasswordHash = table.Column<string>(type: "longtext", nullable: true),
                    SecurityStamp = table.Column<string>(type: "longtext", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "longtext", nullable: true),
                    PhoneNumber = table.Column<string>(type: "varchar(255)", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false),
                    TwoFactorEnabled = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false),
                    AccessFailedCount = table.Column<int>(type: "int", nullable: false, defaultValue: 5)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user", x => x.Id);
                    table.ForeignKey(
                        name: "FK_user_role_RoleID",
                        column: x => x.RoleID,
                        principalTable: "role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySQL:Charset", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "knowledges",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "char(36)", nullable: false),
                    kid = table.Column<string>(type: "longtext", nullable: true),
                    KLName = table.Column<string>(type: "longtext", nullable: false),
                    KLDescribe = table.Column<string>(type: "longtext", nullable: true),
                    CourseFileId = table.Column<Guid>(type: "char(36)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_knowledges", x => x.ID);
                    table.ForeignKey(
                        name: "FK_knowledges_coursefiles_CourseFileId",
                        column: x => x.CourseFileId,
                        principalTable: "coursefiles",
                        principalColumn: "ID");
                })
                .Annotation("MySQL:Charset", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "aspnetuserroles",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "char(36)", nullable: false),
                    RoleId = table.Column<Guid>(type: "char(36)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_aspnetuserroles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_aspnetuserroles_role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_aspnetuserroles_user_UserId",
                        column: x => x.UserId,
                        principalTable: "user",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySQL:Charset", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "teacheres",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "char(36)", nullable: false),
                    UseID = table.Column<Guid>(type: "char(36)", nullable: false),
                    TcEducation = table.Column<string>(type: "longtext", nullable: false),
                    TcSchool = table.Column<string>(type: "longtext", nullable: false),
                    Major = table.Column<string>(type: "longtext", nullable: true),
                    TcCard = table.Column<string>(type: "varchar(255)", nullable: false),
                    LevelID = table.Column<Guid>(type: "char(36)", nullable: true),
                    IsWorking = table.Column<bool>(type: "tinyint(1)", nullable: true, defaultValue: true),
                    EntryTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_teacheres", x => x.ID);
                    table.ForeignKey(
                        name: "FK_teacheres_levels_LevelID",
                        column: x => x.LevelID,
                        principalTable: "levels",
                        principalColumn: "ID");
                    table.ForeignKey(
                        name: "FK_teacheres_user_UseID",
                        column: x => x.UseID,
                        principalTable: "user",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySQL:Charset", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "classeas",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "char(36)", nullable: false),
                    CalssNo = table.Column<string>(type: "longtext", nullable: false),
                    CampusNameID = table.Column<Guid>(type: "char(36)", nullable: true),
                    TeacherId = table.Column<Guid>(type: "char(36)", nullable: true),
                    ClassType = table.Column<string>(type: "longtext", nullable: true),
                    CourseOK = table.Column<int>(type: "int", nullable: true),
                    IsGraduate = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_classeas", x => x.ID);
                    table.ForeignKey(
                        name: "FK_classeas_campuses_CampusNameID",
                        column: x => x.CampusNameID,
                        principalTable: "campuses",
                        principalColumn: "ID");
                    table.ForeignKey(
                        name: "FK_classeas_teacheres_TeacherId",
                        column: x => x.TeacherId,
                        principalTable: "teacheres",
                        principalColumn: "ID");
                })
                .Annotation("MySQL:Charset", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "coursearrangs",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "char(36)", nullable: false),
                    CourseType = table.Column<Guid>(type: "char(36)", nullable: false),
                    InClassNo = table.Column<Guid>(type: "char(36)", nullable: false),
                    InTcnameID = table.Column<Guid>(type: "char(36)", nullable: true),
                    InCourseTime = table.Column<string>(type: "longtext", nullable: true),
                    InClassRoomNo = table.Column<Guid>(type: "char(36)", nullable: false),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_coursearrangs", x => x.ID);
                    table.ForeignKey(
                        name: "FK_coursearrangs_classeas_InClassNo",
                        column: x => x.InClassNo,
                        principalTable: "classeas",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_coursearrangs_classrooms_InClassRoomNo",
                        column: x => x.InClassRoomNo,
                        principalTable: "classrooms",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_coursearrangs_coursefiles_CourseType",
                        column: x => x.CourseType,
                        principalTable: "coursefiles",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_coursearrangs_teacheres_InTcnameID",
                        column: x => x.InTcnameID,
                        principalTable: "teacheres",
                        principalColumn: "ID");
                })
                .Annotation("MySQL:Charset", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "endclasses",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "char(36)", nullable: false),
                    IsEndClass = table.Column<string>(type: "longtext", nullable: false),
                    ClassId = table.Column<Guid>(type: "char(36)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_endclasses", x => x.ID);
                    table.ForeignKey(
                        name: "FK_endclasses_classeas_ClassId",
                        column: x => x.ClassId,
                        principalTable: "classeas",
                        principalColumn: "ID");
                })
                .Annotation("MySQL:Charset", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "students",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "char(36)", nullable: false),
                    UseId = table.Column<Guid>(type: "char(36)", nullable: false),
                    ClassNo = table.Column<Guid>(type: "char(36)", nullable: true),
                    EnrollTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_students", x => x.ID);
                    table.ForeignKey(
                        name: "FK_students_classeas_ClassNo",
                        column: x => x.ClassNo,
                        principalTable: "classeas",
                        principalColumn: "ID");
                    table.ForeignKey(
                        name: "FK_students_user_UseId",
                        column: x => x.UseId,
                        principalTable: "user",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySQL:Charset", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "classrecords",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "char(36)", nullable: false),
                    InClassNo = table.Column<Guid>(type: "char(36)", nullable: false),
                    StopTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    CourseArrangId = table.Column<Guid>(type: "char(36)", nullable: true),
                    StatusId = table.Column<Guid>(type: "char(36)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_classrecords", x => x.ID);
                    table.ForeignKey(
                        name: "FK_classrecords_classeas_InClassNo",
                        column: x => x.InClassNo,
                        principalTable: "classeas",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_classrecords_classstatuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "classstatuses",
                        principalColumn: "ID");
                    table.ForeignKey(
                        name: "FK_classrecords_coursearrangs_CourseArrangId",
                        column: x => x.CourseArrangId,
                        principalTable: "coursearrangs",
                        principalColumn: "ID");
                })
                .Annotation("MySQL:Charset", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_aspnetuserroles_RoleId",
                table: "aspnetuserroles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_classeas_CampusNameID",
                table: "classeas",
                column: "CampusNameID");

            migrationBuilder.CreateIndex(
                name: "IX_classeas_TeacherId",
                table: "classeas",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_classrecords_CourseArrangId",
                table: "classrecords",
                column: "CourseArrangId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_classrecords_InClassNo",
                table: "classrecords",
                column: "InClassNo");

            migrationBuilder.CreateIndex(
                name: "IX_classrecords_StatusId",
                table: "classrecords",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_classrooms_CampusID",
                table: "classrooms",
                column: "CampusID");

            migrationBuilder.CreateIndex(
                name: "IX_coursearrangs_CourseType",
                table: "coursearrangs",
                column: "CourseType");

            migrationBuilder.CreateIndex(
                name: "IX_coursearrangs_InClassNo",
                table: "coursearrangs",
                column: "InClassNo");

            migrationBuilder.CreateIndex(
                name: "IX_coursearrangs_InClassRoomNo",
                table: "coursearrangs",
                column: "InClassRoomNo");

            migrationBuilder.CreateIndex(
                name: "IX_coursearrangs_InTcnameID",
                table: "coursearrangs",
                column: "InTcnameID");

            migrationBuilder.CreateIndex(
                name: "IX_coursefiles_LevelID",
                table: "coursefiles",
                column: "LevelID");

            migrationBuilder.CreateIndex(
                name: "IX_endclasses_ClassId",
                table: "endclasses",
                column: "ClassId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_knowledges_CourseFileId",
                table: "knowledges",
                column: "CourseFileId");

            migrationBuilder.CreateIndex(
                name: "IX_students_ClassNo",
                table: "students",
                column: "ClassNo");

            migrationBuilder.CreateIndex(
                name: "IX_students_UseId",
                table: "students",
                column: "UseId");

            migrationBuilder.CreateIndex(
                name: "IX_teacheres_LevelID",
                table: "teacheres",
                column: "LevelID");

            migrationBuilder.CreateIndex(
                name: "IX_teacheres_TcCard",
                table: "teacheres",
                column: "TcCard",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_teacheres_UseID",
                table: "teacheres",
                column: "UseID");

            migrationBuilder.CreateIndex(
                name: "IX_user_Email",
                table: "user",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_user_PhoneNumber",
                table: "user",
                column: "PhoneNumber",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_user_RoleID",
                table: "user",
                column: "RoleID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "aspnetuserroles");

            migrationBuilder.DropTable(
                name: "classrecords");

            migrationBuilder.DropTable(
                name: "endclasses");

            migrationBuilder.DropTable(
                name: "knowledges");

            migrationBuilder.DropTable(
                name: "students");

            migrationBuilder.DropTable(
                name: "classstatuses");

            migrationBuilder.DropTable(
                name: "coursearrangs");

            migrationBuilder.DropTable(
                name: "classeas");

            migrationBuilder.DropTable(
                name: "classrooms");

            migrationBuilder.DropTable(
                name: "coursefiles");

            migrationBuilder.DropTable(
                name: "teacheres");

            migrationBuilder.DropTable(
                name: "campuses");

            migrationBuilder.DropTable(
                name: "levels");

            migrationBuilder.DropTable(
                name: "user");

            migrationBuilder.DropTable(
                name: "role");
        }
    }
}
