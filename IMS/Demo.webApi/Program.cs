using System.Reflection;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Demo.EFCoreEnv.DbContext;
using Demo.Model;
using Demo.IRepository;
using Demo.Repository;
using Demo.IService;
using Demo.Service;
using Demo.Utils.DTOsMapper;
using AutoMapper;
using Demo.Utils.JWT;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);


//配置接口注释

builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "编程教育后端系统", Version = "v1" });
    // 设置 XML 文件的路径

    // 为 Swagger JSON and UI设置xml文档注释路径

    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    c.IncludeXmlComments(xmlPath);
});



// Add services to the container.
// 跨域问题
// builder.Services.AddCors(c=>c.AddPolicy("any",
//     p=>p.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin()
// ));

builder.Services.AddCors(c=>c.AddPolicy("cor",
    p=>p.AllowAnyHeader().AllowAnyMethod().WithOrigins("http://ims.yujinxing.com").AllowCredentials()
));


builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    var schemeName = "Bearer";
    options.AddSecurityDefinition(schemeName, new OpenApiSecurityScheme
    {
        Description = "格式 Bearer [token]",
        Name = "Authorization",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.ApiKey
    });

    options.AddSecurityRequirement(new OpenApiSecurityRequirement {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = schemeName
                }
            },
            new string[0]
        }
    });
});






builder.Services.AddDbContext<IMSContext>(options =>
{
    // options.UseSqlServer(builder.Configuration.GetConnectionString("Default"));
    options.UseMySQL(builder.Configuration.GetConnectionString("MySqlConnection"));
    options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
});
//注册仓储层,服务层
builder.Services.AddCustomService();
//注册

// AutoMapper注入
builder.Services.AddAutoMapper(typeof(RoleMapper));
builder.Services.AddAutoMapper(typeof(LoginMapper));
builder.Services.AddAutoMapper(typeof(UserInfoMapper));
builder.Services.AddAutoMapper(typeof(StudentInfoMapper));
builder.Services.AddAutoMapper(typeof(TeacherMapper));
builder.Services.AddAutoMapper(typeof(CampusMapper));
builder.Services.AddAutoMapper(typeof(ClassRoomMapper));
builder.Services.AddAutoMapper(typeof(LevelMapper));
builder.Services.AddAutoMapper(typeof(ClassAMapper));
builder.Services.AddAutoMapper(typeof(KnowledgeMapper));
builder.Services.AddAutoMapper(typeof(CourseFileMapper));
builder.Services.AddAutoMapper(typeof(CourseArrangMapper));
builder.Services.AddAutoMapper(typeof(ClassRecordMapper));
builder.Services.AddAutoMapper(typeof(ClassStatusMapper));
builder.Services.AddAutoMapper(typeof(EndClassMapper));
//identity注入
builder.Services.AddUserServices();
//JWT
builder.Services.AddJwtService(builder);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
var client = new HttpClient();
client.MaxResponseContentBufferSize = 1048576000; 
//使用注释
app.UseSwagger();
app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
    c.RoutePrefix = string.Empty;
});
// 跨域
app.UseCors("cor");

//授权中间件
app.UseAuthentication();
//鉴权中间件，   先授权，再鉴权
app.UseAuthorization();

app.MapControllers();

app.Run();



//一些服务的注册
public static class ExtendService
{
    public static IServiceCollection AddCustomService(this IServiceCollection services)
    {
        try
        {
            //仓储层注册
            services.AddScoped<ICampusRepository, CampusRepository>();
            services.AddScoped<ITeacharRepository, TeacherRepository>();
            services.AddScoped<IClassARepository, ClassARepository>();
            services.AddScoped<IClassRecordRepository, ClassRecordRepository>();
            services.AddScoped<IClassStatusRepository, ClassStatusRepository>();
            services.AddScoped<ICourseFileRepository, CourseFileRepository>();
            services.AddScoped<ICourseArrangRepository, CourseArrangRepository>();
            services.AddScoped<IKnowledgeRepository, KnowledgeRepository>();
            services.AddScoped<ILevelRepository, LevelRepository>();
            services.AddScoped<IStudentRepository, StudentRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IClassRoomRepository, ClassRoomRepository>();
            services.AddScoped<IRoleRepository,RoleRepository>();
            services.AddScoped<IEndClassRepository, EndClassRepository>();
            //服务层注册
            services.AddScoped<ICampusService, CampusService>();
            services.AddScoped<ITeacherService, TeacherService>();
            services.AddScoped<IClassAService, ClassAService>();
            services.AddScoped<IClassRecordService, ClassRecordService>();
            services.AddScoped<IClassStatusService, ClassStatusService>();
            services.AddScoped<ICourseFileService, CourseFileService>();
            services.AddScoped<ICourseArrangService, CourseArrangService>();
            services.AddScoped<IKnowkedgeService, KnowledgeService>();
            services.AddScoped<ILevelService, LevelService>();
            services.AddScoped<IStudentService, StudentService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IClassRoomService,ClassRoomSerive>();
            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<IEndClassService, EndClassService>();
            services.AddScoped<UserManager<User>, UserManager<User>>();
            services.AddScoped<RoleManager<Role>, RoleManager<Role>>();
        }
        catch (System.Exception ex)
        {
            System.Console.WriteLine(ex);
        }
        return services;
    }
    
    public static IServiceCollection AddUserServices(this IServiceCollection services)
    {
        try
        {
            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<IMSContext>() //identity使用的数据库上下文
                .AddDefaultTokenProviders() //默认token
                //可以直接操作User表，内部有许多方法进行增删改查
                //注册一个自定义的UserManager<User>作为用户管理器，用于操作User表进行增删改查等操作。
                .AddUserManager<UserManager<User>>() 
                //.AddScoped<UserManager<User>>();
                .AddRoleManager<RoleManager<Role>>();
            services.AddDataProtection();//数据保护
            // 密码配置
            services.Configure<IdentityOptions>(options =>
            {
                // 密码长度限制
                //options.Password.equiredLength = 8;
                options.Password.RequireNonAlphanumeric = false; // 禁止非字母数字字符
                options.Password.RequireDigit = true; // 要求数字
                options.Password.RequireLowercase = true; // 要求小写字母
                options.Password.RequireUppercase = false; // 要求大写字母
            });
            
        }
        catch (Exception)
        {

            throw;
        }
        return services;
    }
    //注册&&配置 JWT
    public static IServiceCollection AddJwtService(this IServiceCollection services, WebApplicationBuilder builder)
    {
        try
        {
            //授权方式一
            // 通过 configuring 获取了appsettings.json 文件中应用程序的配置信息，将其注册
            /* 通过 builder.Services 获取 IServiceProvider 实例，
                然后使用 GetRequiredService 方法获取 IConfiguration 实例。
                IConfiguration 用于读取 appsettings.json 文件中的配置信息。
            */
            
            // var configuration = builder.Services.BuildServiceProvider().GetRequiredService<IConfiguration>();
            // 获取名为 JWTOption 的配置节，并将其映射到 JWTOption 类型的实例中。
            // var jwtSetting = configuration.GetSection(nameof(JwtSettings));

            //授权方式二
            var jwtSetting = builder.Configuration.GetSection("JwtSettings");
            services.Configure<JwtSettings>(jwtSetting);
            //获取appsetting中jwt的实例
            
            /* 
                通过 toKenSection.Get 泛型方法获取 JWTOption 类型的实例，
                其作用是将 JWTOption 配置节中的所有配置项绑定到 JWTOption 对象的属性上。
            */
            var jwtConfig = jwtSetting.Get<JwtSettings>();
            //配置鉴权服务器
            // services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            // .AddJwtBearer(opt =>
            // {
            //     opt.TokenValidationParameters = new TokenValidationParameters
            //     {
            //         ValidateIssuer = true, //是否在令牌期间验证签发者
            //         ValidateAudience = true, //是否验证接收者
            //         ValidateLifetime = true, //是否验证失效时间
            //         ValidateIssuerSigningKey = true, // 是否验证签名
            //         ValidAudience = jwtConfig.Audience, //接收者
            //         ValidIssuer = jwtConfig.Issuer, //签发者
            //         IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtConfig.SecKey!)) // 秘钥
            //     };
            // });
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(opt =>
                {
                    opt.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true, //是否在令牌期间验证签发者
                        ValidateAudience = true, //是否验证接收者
                        ValidateLifetime = true, //是否验证失效时间
                        ValidateIssuerSigningKey = true, // 是否验证签名
                        ValidAudience = jwtConfig!.Audience, //接收者
                        ValidIssuer = jwtConfig.Issuer, //签发者
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtConfig.SecKey!)) // 秘钥
                    };
                });
        }
        catch (System.Exception)
        {

            throw;
        }
        return services;
    }
}