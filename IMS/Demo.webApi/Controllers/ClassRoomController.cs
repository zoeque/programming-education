﻿using System.Text.RegularExpressions;
using AutoMapper;
using Demo.IService;
using Demo.Model;
using Demo.Model.DTOs;
using Demo.Utils.ApiReturn;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Demo_webApi.Controllers;
[ApiController]
[Route("api/[controller]/[action]")]
public class ClassRoomController : Controller
{
    private readonly IClassRoomService _classRoomService;
    private readonly ICampusService _campusService;
    private readonly IMapper _mapper;

    public ClassRoomController(IClassRoomService classRoomService,ICampusService campusService,IMapper mapper)
    {
        _classRoomService = classRoomService ;
        _campusService = campusService ;
        _mapper = mapper ;
    }

    [HttpGet]
    // 教室信息  除了普通用户都可以查看
    [Authorize(Roles = "manager,teacher,student",AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> FindAll(int page,int size)
    {
        var total = await _classRoomService.FindAllAsync();
        var res = await _classRoomService.PagingAsync(page,size);
        if (res.Count > 0)
        {
            var dto = _mapper.Map<List<ClassRoomDTOs>>(res);
            return ApiReturnHelper.Success(new{data=dto,total = total.Count});
        }
        else
        {
            return ApiReturnHelper.Fail("没有数据");
        }
    }

    [HttpGet]
    // 教室信息  除了普通用户都可以查看
    [Authorize(Roles = "manager,teacher,student",AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> FindMany(string text)
    {
        var res = await _classRoomService.FindManyAsync(
        c=>c.ClassRoomSite.Contains(text) || c.D_Campus.CmName.Contains(text)
        );
        if (res.Count > 0)
        {
            var dto = _mapper.Map<List<ClassRoomDTOs>>(res);
            return ApiReturnHelper.Success(dto);
        }
        else
        {
            return ApiReturnHelper.Fail("没有数据");
        }
    }

    [HttpGet]
    // 教室信息  除了普通用户都可以查看
    [Authorize(Roles = "manager,teacher,student",AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> FindId(Guid id)
    {
        var res = await _classRoomService.FindIdAsync(id);
        if (res!=null)
        {
            var dto = _mapper.Map<ClassRoomDTOs>(res);
            return ApiReturnHelper.Success(dto);
        }
        else 
        {
            return ApiReturnHelper.Fail("该id不存在");
        }
    }
    [HttpPost]
    // 添加教室  只有管理员可以
    [Authorize(Roles = "manager",AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> Create(ClassRoomDTOs classRoom)
    {
        var addclassroom = new ClassRoom()
        {
            ClassRoomNo = classRoom.ClassRoomNo,
            CampusID = classRoom.CampusID,
            ClassRoomSite = classRoom.ClassRoomSite,
            IsOccupy = false,
            IsDeleted = false
        };
        var res = await _classRoomService.CreateAsync(addclassroom);
        if (res)
        {
            return ApiReturnHelper.Success(res);
        }
        else 
        {
            return ApiReturnHelper.Fail("添加失败");
        }
    }

    [HttpPut]
    // 只有管理员可以修改教室基础信息   但是当老师上课的时候也可以调用此接口 修改教室的状态 是否被占用
    public async Task<ApiReturns> Update(ClassRoomDTOs classRoom)
    {
        var updateclassroom = await _classRoomService.FindIdAsync(classRoom.ID);
        updateclassroom.ClassRoomNo = classRoom.ClassRoomNo;
        updateclassroom.CampusID = classRoom.CampusID;
        updateclassroom.IsOccupy = classRoom.IsOccupy;
        var res = await _classRoomService.UpdateAsync(updateclassroom);
        if (res)
        {
            return ApiReturnHelper.Success(res);
        }
        else
        {
            return ApiReturnHelper.Fail("修改失败");
        }
    }
    [HttpDelete]
    // 删除教室  只有管理员可以
    [Authorize(Roles = "manager",AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> Delete(Guid id)
    {
        var delclssroom = await _classRoomService.FindIdAsync(id);
        delclssroom.IsDeleted = true;
        var res = await _classRoomService.DeleteAsync(delclssroom);
        if (res)
        {
            return ApiReturnHelper.Success(res);
        }
        else
        {
            return ApiReturnHelper.Fail("该id不存在或已删除");
        }
    }
}