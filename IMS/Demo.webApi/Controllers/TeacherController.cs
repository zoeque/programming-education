﻿using System.Text.RegularExpressions;
using Azure.Core;
using Microsoft.AspNetCore.Mvc;
using Demo.Model;
using Demo.IService;
using Demo.Model.DTOs;
using Demo.Utils;
using Demo.Utils.ApiReturn;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Demo_webApi.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class TeacherController : ControllerBase
    {
        private readonly ITeacherService _teacherService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly IStudentService _studentService;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly IRoleService _roleService;
        private readonly IClassAService _classAService;
        public TeacherController
        (
            ITeacherService teacherService,IUserService userService,IRoleService roleService,
            IMapper mapper,IStudentService studentService,
            UserManager<User> userManager,
            RoleManager<Role> roleManager,
            IClassAService classAService
        )
        {
            _teacherService = teacherService;
            _userService = userService;
            _mapper = mapper;
            _studentService = studentService;
            _userManager = userManager;
            _roleManager = roleManager;
            _roleService = roleService;
            _classAService = classAService;
        }

        [HttpGet]
        // 老师的基础信息  只有管理员可以查看
        [Authorize(Roles = "manager",AuthenticationSchemes = "Bearer")]
        public async Task<ApiReturns> FindAll(int page,int size)
        {
            //获取多少条数据
            var total = await _teacherService.FindAllAsync();
            var res = await _teacherService.PagingAsync(page,size);
            if (res.Count>0)
            {
                var dto = _mapper.Map<List<TeacherDTOs>>(res);
                return ApiReturnHelper.Success(new{ data=dto,total = total.Count});
            }
            else
            {
                return ApiReturnHelper.Fail("没有数据");
            }
        }
        [HttpGet]
        // id查询老师的基础信息  管理员可以查看 ， 教师也可以看自己的信息
        [Authorize(Roles = "manager,teacher",AuthenticationSchemes = "Bearer")]
        public async Task<ApiReturns> FindId(Guid id)
        {
            var useres = await _userService.FindIdAsync(id);
            var res = await _teacherService.FindOneAsync(t=>t.UseID==useres.Id);
            if (res!=null)
            {
                var dto = _mapper.Map<TeacherDTOs>(res);
                return ApiReturnHelper.Success(dto);
            }
            else
            {
                return ApiReturnHelper.Fail("该id不存在");
            }
        }
        [HttpGet]
        // 查询符合条件的老师的基础信息  只有管理员可以查看
        [Authorize(Roles = "manager",AuthenticationSchemes = "Bearer")]
        public async Task<ApiReturns> FindMany(Guid id)
        {
            var classas = await _classAService.FindOneAsync(c => c.TeacherId == id);
            var res = await _studentService.FindManyAsync(s => s.ClassNo == classas.ID);
            /*var res = await _teacherService.FindManyAsync
            (
            t=>
                t.TcCard.Contains(text) || t.TcSchool.Contains(text) ||
                t.Major.Contains(text) || t.TcEducation == text
            );*/
            if (res.Count > 0)
            {
                var dto = _mapper.Map<List<StudentInfoDTOs>>(res);
                return ApiReturnHelper.Success(dto);
            }
            else
            {
                return ApiReturnHelper.Fail("没有数据");
            }
        }
        // 添加老师
        [HttpPost]
        //  用户先向管理员申请当老师  此时老师表中有这名用户的信息  但是为不在职状态  在管理员通过审核之后 管理员设置为在职老师
        [Authorize(Roles = "manager,normal",AuthenticationSchemes = "Bearer")]
        public async Task<ApiReturns> Create(TeacherDTOs teacher)
        {
            //获取该用户是否存在
            var confirteacher = await _userService.FindIdAsync(teacher.UseID);
            if (confirteacher == null) return ApiReturnHelper.Fail("没有这个用户");
            
            var addteacher = new Teacher()
            {
                UseID = teacher.UseID,             // 在用户表的Id
                TcEducation = teacher.TcEducation, // 老师学历
                TcSchool = teacher.TcSchool,  // 老师的毕业学校
                Major = teacher.Major,      // 老师的专业
                TcCard = teacher.TcCard,    // 老师的身份证
                LevelID = teacher.LevelID, // 老师的等级
                EntryTime = DateTime.Now, // 入职时间已经设置为当前时间
                IsWorking = false, // false 为不在职
                IsDeleted = false  
            };
            // 获取到原用户的角色
            var role = await _roleManager.FindByIdAsync(confirteacher.D_role.Id.ToString());
            // 将他的角色外键改为教师
            // 获取到角色表中教师角色的Id
            var tearole = await _roleManager.FindByNameAsync("teacher");
            if (await _roleManager.RoleExistsAsync(tearole.Name))
                {
                
                    await _userManager.IsInRoleAsync(confirteacher, tearole.Name!);
                    await _userManager.SetUserNameAsync(confirteacher, confirteacher.UserName);
                    // 给用户添加新的角色
                    var addRoleResult   = await _userManager.AddToRoleAsync(confirteacher, tearole.Name);
                    // 修改新的角色用户删除之前的角色
                    await _userManager.RemoveFromRoleAsync(confirteacher, role.Name);
                    confirteacher.RoleID = tearole.Id;
                    await _userService.UpdateAsync(confirteacher);
                
                    if (addRoleResult .Succeeded)
                    {
                    
                        var res = await _teacherService.CreateAsync(addteacher);
                    
                        if (res)
                        {
                            return ApiReturnHelper.Success(res);
                        }

                    }
                }
                return ApiReturnHelper.Fail("添加教师失败");
        }
        [HttpPut]
        // 修改老师的基础信息  管理员可以修改  老师也可以修改自己的基本信息
        [Authorize(Roles = "manager,teacher",AuthenticationSchemes = "Bearer")]
        public async Task<ApiReturns> Update(TeacherDTOs teacher)
        {
            //获取教师是否存在
            var confirteacher = await _userService.FindIdAsync(teacher.UseID);
            if (confirteacher.D_role.Name != "teacher") return ApiReturnHelper.Fail("该用户不是教师");
            var updateteacher = await _teacherService.FindIdAsync(teacher.ID);
            // 修改老师的基本信息
            updateteacher.TcEducation = teacher.TcEducation; // 学历
            updateteacher.TcSchool = teacher.TcSchool; // 毕业学校
            updateteacher.Major = teacher.Major; // 所学专业
            updateteacher.IsDeleted = false;
            
            
            //根据身份证查找是否存在
            var card = await _teacherService.FindOneAsync(t => t.TcCard == teacher.TcCard);
            if (card != null) return ApiReturnHelper.Fail("该身份证已经存在");
            updateteacher.TcCard = teacher.TcCard;
            // 判断是否修改了老师的等级  如果不为空说明管理员修改了教师的等级
            // 如果为空说明是教师修改自己的信息 但是教师不能改变自己的等级  所以继续使用原来的等级
            if (teacher.LevelID!=null)
            {
                updateteacher.LevelID = teacher.LevelID;
            }
            // 是否在职
            // 如果是管理员和老师自己修改的基本信息 那么就不用填写IsWorking的数据 已设置为继续在职
            if (teacher.IsWorking!=null) 
            {
                /*
                   求职者求职的时候 教师表中会有记录 但是IsWorking为 false 意为不在职状态
                   如果管理员同意这名用户成为教师那么IsWorking改为true  
                   如果管理员不同意该用户成为教师那么 把 IsWorking 改为false 并且调用删除接口把该名用户在教师记录表中的记录删除掉
                */
                updateteacher.IsWorking = teacher.IsWorking;// 如果提升用户的权限为老师  则改为在职
            }
            
            var res = await _teacherService.UpdateAsync(updateteacher);
            if (res)
            {
                return ApiReturnHelper.Success(res);
            }
            else
            {
                return ApiReturnHelper.Fail("修改失败");
            }
        }
        
        [HttpGet]
        // 删除老师之前把当前这个老师先修改为普通角色
        [Authorize(Roles = "manager",AuthenticationSchemes = "Bearer")]
        public async Task<ApiReturns> DeleteTeaRole(Guid id) // 传入当前老师id
        {
            // 当前老师
            var delteacher = await _teacherService.FindIdAsync(id);
            // 要删除老师先要把老师的角色从teacher改为normal

            // 1. 获取到教师角色
            var role = await _roleManager.FindByNameAsync("teacher");
            // 2. 获取到这名教师在用户表的信息

            var userteacher = await _userManager.FindByIdAsync(delteacher.UseID.ToString());

            // 3. 获取到普通用户的角色
            var rolenormal = await _roleManager.FindByNameAsync("normal");
            // 如果 res 为 false 那么没删除成功 返回 id 不存在  删除成功则继续删除教师的角色
            
            // 修改用户表的外键为普通用户
            userteacher.RoleID = rolenormal.Id;

            // await _userManager.UpdateAsync(userteacher);
            if (await _roleManager.RoleExistsAsync(rolenormal.Name!))
            {
                
                await _userManager.IsInRoleAsync(userteacher, rolenormal.Name!);
                await _userManager.SetUserNameAsync(userteacher, userteacher.UserName);
                // 给用户添加新的角色
                var addRoleResult   = await _userManager.AddToRoleAsync(userteacher, rolenormal.Name);
                // 修改新的角色用户删除之前的角色
                await _userManager.RemoveFromRoleAsync(userteacher, role.Name);
                var a =  await _userManager.UpdateAsync(userteacher);

                if (addRoleResult .Succeeded)
                {
                    return ApiReturnHelper.Success(addRoleResult .Succeeded);
                }
            }
            return ApiReturnHelper.Fail("修改老师的角色失败");

        }

        [HttpDelete]
        // 删除老师  只有管理员可以删除
        [Authorize(Roles = "manager",AuthenticationSchemes = "Bearer")]
        public async Task<ApiReturns> Delete(Guid id)
        {
            var delteacher = await _teacherService.FindIdAsync(id);
            delteacher.IsDeleted = true;
            var res = await _teacherService.DeleteAsync(delteacher);
            if (res)
            {
                return ApiReturnHelper.Success(res);
            }
            return ApiReturnHelper.Success("没有这个老师");
            
        }
        
        
        
    }
}
