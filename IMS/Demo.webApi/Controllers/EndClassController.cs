﻿using AutoMapper;
using Demo.IService;
using Demo.Model;
using Demo.Model.DTOs;
using Demo.Utils.ApiReturn;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Demo_webApi.Controllers;
[ApiController]
[Route("api/[controller]/[action]")]
public class EndClassController : ControllerBase
{
    private readonly IEndClassService _endClassService;
    private readonly IMapper _mapper;

    public EndClassController(IMapper mapper, IEndClassService endClassService)
    {
        _endClassService = endClassService;
        _mapper = mapper;
    }

    [HttpGet]
    // 结课信息 管理员和教师都可看
    [Authorize(Roles = "manager,teacher",AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> FindAll()
    {
        var res = await _endClassService.FindAllAsync();
        if (res.Count > 0)
        {
            var dto = _mapper.Map<List<EndClassDTO>>(res);
            return ApiReturnHelper.Success(dto);
        }
        else
        {
            return ApiReturnHelper.Fail("没有数据");
        }
    }
    [HttpPost]
    // 添加结课信息  当班级毕业以后添加此班级到结课信息表中
    [Authorize(Roles = "manager,teacher",AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> Create(EndClassDTO endClass)
    {
        var addendclass = new EndClass()
        {
            IsEndClass = endClass.IsEndClass,
            ClassId = endClass.ClassId
        };
        var res = await _endClassService.CreateAsync(addendclass);
        if (res)
        {
            return ApiReturnHelper.Success(res);
        }
        else
        {
            return ApiReturnHelper.Fail("添加失败");
        }
    }
    [HttpPut]
    // 修改结课信息
    [Authorize(Roles = "manager,teacher",AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> Update(EndClassDTO endClass)
    {
        var updateendclass = await _endClassService.FindIdAsync(endClass.Id);
        updateendclass.IsEndClass = endClass.IsEndClass;
        updateendclass.ClassId = endClass.ClassId;
        var res = await _endClassService.UpdateAsync(updateendclass);
        if (res)
        {
            return ApiReturnHelper.Success(res);
        }
        else
        {
            return ApiReturnHelper.Fail("添加失败");
        }
    }
}