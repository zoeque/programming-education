using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Demo.IService;
using Demo.Model;
using Demo.Model.DTOs;
using Demo.Utils.ApiReturn;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace Demo_EFCoreEnv.DbContext
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class LevelController : ControllerBase
    {
        private readonly ILevelService _level;
        private readonly IMapper _mapper;

        public LevelController(ILevelService level,IMapper mapper)
        {
            _level = level;
            _mapper = mapper;
        }

        [HttpGet]
        // 管理员可查看所有等级
        [Authorize(Roles = "manager",AuthenticationSchemes = "Bearer")]
        public async Task<ApiReturns> FindAll()
        {
            var res = await _level.FindAllAsync();
            if (res.Count > 0)
            {
                var dto = _mapper.Map<List<LevelDTOs>>(res);
                return ApiReturnHelper.Success(dto);
            }
            else
            {
                return ApiReturnHelper.Fail("没有数据");
            }
        }

        [HttpGet]
        // 教师和管理员 都可以查看  教师根据等级外键
        [Authorize(Roles = "manager,teacher",AuthenticationSchemes = "Bearer")]
        public async Task<ApiReturns> FindId(Guid id)
        {
            var res = await _level.FindIdAsync(id);
            if (res != null)
            {
                var dto = _mapper.Map<LevelDTOs>(res);
                return ApiReturnHelper.Success(dto);
            }
            else
            {
                return ApiReturnHelper.Fail("该id不存在");
            }
        }
        
        /*[HttpPut]
        // 管理员可以修改等级信息
        [Authorize(Roles = "manager",AuthenticationSchemes = "Bearer")]
        public async Task<ApiReturns> Update(LevelDTOs level)
        {
            //根据id获取等级信息
            var updatelevel = await _level.FindIdAsync(level.ID);
            //根据等级名称进行查找是否有该等级
            var confirlevel = await _level.FindOneAsync(l => l.LevelName == level.LevelName);
            if (confirlevel != null) return ApiReturnHelper.Fail("该等级名称已经存在");
            if (updatelevel == null) return ApiReturnHelper.Fail("该id不存在");
            updatelevel.LevelName = level.LevelName;
            var res = await _level.UpdateAsync(updatelevel);
            if (res)
            {
                return ApiReturnHelper.Success(res);
            }
            else
            {
                return ApiReturnHelper.Fail("修改失败");
            }
        }*/
    }
}