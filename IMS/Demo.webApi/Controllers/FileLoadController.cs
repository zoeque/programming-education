﻿using System.Drawing;
using Demo.IService;
using Microsoft.AspNetCore.Mvc;

namespace Demo.webApi.Controllers;
[ApiController]
[Route("api/[controller]/[action]")]
public class FileLoadController : ControllerBase
{
    private readonly IUserService _userService;
    public FileLoadController(IUserService userService)
    {
        _userService = userService;
    }
    //用户修改头像
    [HttpPost]
    public async Task<IActionResult> UserLoad(IFormFile file,Guid id)
    {
        var users = await _userService.FindIdAsync(id);
        if (file.Length > 0)
        {
            //获取当前目录
            var CurrentDir = Directory.GetCurrentDirectory();
            //文件格式
            var ImgForMat = new String[] { "image/png", "image/jpg", "image/gif", "image/jpeg" };
            //判断格式是否正确
            if (!ImgForMat.Contains(file.ContentType))
            {
                return BadRequest("格式错误");
            }
            //获取当前日期和时间
            var now = DateTime.Now;
            //保存位置
            var SaveDir = Path.Combine("Image", $"{now.Year.ToString()}-{now.Month.ToString()}-{now.Day.ToString()}");
            //获取完整保存路径
            var ConfirmDir = Path.Combine(CurrentDir, SaveDir);
            //判断路径是否存在
            if (!Directory.Exists(ConfirmDir))
            {
                //创建
                Directory.CreateDirectory(ConfirmDir);
            }
            //创建文件流
            using (var FileStream = new FileStream(Path.Combine(ConfirmDir, file.FileName), FileMode.Create))
            {
                //复制
               await file.CopyToAsync(FileStream);
            }
            users.Icon = file.FileName;
            var res =  await _userService.UpdateAsync(users);
            if (res)
            {
                return Ok("修改成功");
            }
        }
        return NotFound("文件上传失败");
    }
}