﻿using AutoMapper;
using Demo.IService;
using Demo.Model;
using Demo.Model.DTOs;
using Demo.Utils.ApiReturn;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Demo_webApi.Controllers;
[ApiController]
[Route("api/[controller]/[action]")]
public class ClassAController : ControllerBase
{
    private readonly IMapper _mapper;
    private readonly IClassAService _classAService;
    private readonly IUserService _userService;
    private readonly ITeacherService _teacherService;

    public ClassAController(IClassAService classAService, IMapper mapper,IUserService userService,ITeacherService teacherService)
    {
        _classAService = classAService;
        _mapper = mapper;
        _userService = userService;
        _teacherService = teacherService;
    }

    [HttpGet]
    //  班级信息 教师 管理员 学生也可看
    [Authorize(Roles = "manager,teacher,student",AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> FindAll(int page,int size)
    {
        var total = await _classAService.FindAllAsync();
        var res = await _classAService.PagingAsync(page, size);
        if (res.Count>0)
        {
            var dto = _mapper.Map<List<ClassADTOs>>(res);
            return ApiReturnHelper.Success(new{data=dto,total = total.Count});
        }
        else
        {
            return ApiReturnHelper.Fail("没有数据");
        }
    }

    [HttpGet]
    // 单个班级信息  管理员，教师,学生也可看
    [Authorize(Roles = "manager,teacher,student",AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> FindOne(string text)
    {
        var res = await _classAService.FindOneAsync
            (
                c => 
                    c.CalssNo == text || 
                    c.ID.ToString() == text
            );
        if (res!=null)
        {
            var dto = _mapper.Map<ClassADTOs>(res);
            return ApiReturnHelper.Success(dto);
        }
        else
        {
            return ApiReturnHelper.Fail("没有数据");
        }
    }
    
    [HttpPost]
    // 创建班级  管理员和教师都可以
    [Authorize(Roles = "manager,teacher",AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> Create(ClassADTOs classA)
    {
        var confirclassa = await _classAService.FindOneAsync(c=>c.CalssNo== classA.CalssNo);
        if (confirclassa != null) return ApiReturnHelper.Fail("该班级已经存在");
        var addclassa = new ClassA()
        {
            CalssNo = classA.CalssNo,
            CampusNameID = classA.CampusNameID,
            TeacherId = classA.TeacherId,
            ClassType = classA.ClassType,
            CourseOK = classA.CourseOK,
            IsGraduate = false
        };
        var res = await _classAService.CreateAsync(addclassa);
        if (res)
        {
            return ApiReturnHelper.Success(res);
        }
        else
        {
            return ApiReturnHelper.Fail("添加失败");
        }
    }
    [HttpPut]
    // 修改班级信息  管理员和教师都可以
    [Authorize(Roles = "manager,teacher",AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> Update(ClassADTOs classA)
    {
        var updateclassa = await _classAService.FindIdAsync(classA.ID);
        updateclassa.CalssNo = classA.CalssNo;
        updateclassa.CampusNameID = classA.CampusNameID;
        updateclassa.TeacherId = classA.TeacherId;
        updateclassa.ClassType = classA.ClassType;
        updateclassa.CourseOK = classA.CourseOK;
        var res = await _classAService.UpdateAsync(updateclassa);
        if (res)
        {
            return ApiReturnHelper.Success(res);
        }
        else
        {
            return ApiReturnHelper.Fail("修改失败");
        }
    }
    [HttpDelete]
    // 删除班级(毕业)  管理员和教师都可以
    [Authorize(Roles = "manager,teacher",AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> Delete(Guid id)
    {
        var delclassa = await _classAService.FindIdAsync(id);
        delclassa.IsGraduate = true;
        var res = await _classAService.DeleteAsync(delclassa);
        if (res)
        {
            return ApiReturnHelper.Success(res);
        }
        else
        {
            return ApiReturnHelper.Fail("改id不存在或已删除");
        }
    }
    /// <summary>
    /// 用于找到当前教师所带的班级
    /// </summary>
    /// <param name="id">用户ID</param>
    /// <returns>API返回结果</returns>
    [HttpGet]
    public async Task<ApiReturns> TeacherClass(Guid id)
    {
        var teacherid = await _teacherService.FindOneAsync(t => t.UseID == id);
        var res = await _classAService.FindManyAsync
        (
            c => 
                c.TeacherId == teacherid.ID
        );
        if (res!=null&& res.Count>0)
        {
            var dto = _mapper.Map<List<ClassADTOs>>(res);
            return ApiReturnHelper.Success(dto);
        }
        else
        {
            return ApiReturnHelper.Fail("没有数据");
        }
    }
}