﻿using AutoMapper;
using Demo.IService;
using Demo.Model;
using Demo.Model.DTOs;
using Demo.Utils.ApiReturn;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Demo_webApi.Controllers;
[ApiController]
[Route("api/[controller]/[action]")]
public class RoleController : ControllerBase
{
    private readonly IRoleService _roleService;
    private readonly IMapper _mapper;

    public RoleController(IRoleService roleService,IMapper mapper)
    {
        _roleService = roleService;
        _mapper = mapper;
    }

    [HttpGet]
    // 所有角色  只有管理员可以查看
    [Authorize(Roles = "manager",AuthenticationSchemes = "Bearer")]
    public async Task<ApiReturns> FindAll()
    {
        var res =await _roleService.FindAllAsync();
        if (res.Count>0)
        {
            var dto = _mapper.Map<List<RoleDTOs>>(res);
            return ApiReturnHelper.Success(dto);
        }
        else
        {
            return ApiReturnHelper.Fail("没有数据");
        }
    }
}