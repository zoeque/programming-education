﻿using Demo.EFCoreEnv.DbContext;
using Demo.IRepository;
using Demo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Repository
{
    public class ClassStatusRepository:BaseRepository<ClassStatus>,IClassStatusRepository
    {
        private readonly IMSContext _IMSContext;
        public ClassStatusRepository(IMSContext context)
        {
            base.IMSContext = context;
            _IMSContext = context;
        }
    }
}
