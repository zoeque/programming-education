﻿using Demo.EFCoreEnv.DbContext;
using Demo.IRepository;
using Demo.Model;

namespace Demo.Repository;

public class RoleRepository : BaseRepository<Role> ,IRoleRepository
{
    private readonly IMSContext _IMSContext;

    public RoleRepository(IMSContext context)
    {
        base.IMSContext = context;
        _IMSContext = context;
    }
    
}