﻿using Demo.EFCoreEnv.DbContext;
using Demo.IRepository;
using Demo.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Repository
{
    public class KnowledgeRepository:BaseRepository<Knowledge>,IKnowledgeRepository
    {
        private readonly IMSContext _IMSContext;
        public KnowledgeRepository(IMSContext context)
        {
            base.IMSContext = context;
            _IMSContext = context;
        }
        public override async Task<Knowledge> FindIdAsync(Guid id)
        {
            return await _IMSContext.knowledges.Include(k => k.D_coursefile).OrderBy(k=>k.kid).FirstOrDefaultAsync(k => k.ID == id);
        }
        public override async Task<Knowledge> FindOneAsync(Expression<Func<Knowledge, bool>> exp)
        {
            return await _IMSContext.knowledges.Include(k => k.D_coursefile).OrderBy(k=>k.kid).FirstOrDefaultAsync(exp);
        }
        public override async Task<List<Knowledge>> FindAllAsync()
        {
            return await _IMSContext.knowledges.Include(k => k.D_coursefile).OrderBy(k=>k.kid).ToListAsync();
        }
        public override async Task<List<Knowledge>> FindManyAsync(Expression<Func<Knowledge, bool>> exp)
        {
            return await _IMSContext.knowledges.Include(k => k.D_coursefile).Where(exp).OrderBy(k=>k.kid).ToListAsync();
        }

        public override async Task<List<Knowledge>> PagingAsync(int page, int size)
        {
            return await _IMSContext.knowledges.Include(k => k.D_coursefile).OrderBy(k=>k.kid).Skip((page - 1) * size).Take(size)
                .ToListAsync();
        }
    }
}
