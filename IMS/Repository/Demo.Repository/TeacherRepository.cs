﻿using Demo.EFCoreEnv.DbContext;
using Demo.IRepository;
using Demo.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Repository
{
     public  class TeacherRepository:BaseRepository<Teacher>,ITeacharRepository
    {
        private readonly IMSContext _IMSContext;
        public TeacherRepository(IMSContext context)
        {
            base.IMSContext = context; 
            _IMSContext = context;
        }
        public async override Task<Teacher> FindIdAsync(Guid id)
        {
            return await _IMSContext.teacheres.Include(t=>t.TcD_user).Include(t=>t.TcD_tclevel).Include(t=>t.TcD_class).FirstOrDefaultAsync(t=>t.ID==id);
        }
        public async override Task<Teacher> FindOneAsync(Expression<Func<Teacher, bool>> exp)
        {
            return await _IMSContext.teacheres.Include(t=>t.TcD_user).Include(t=>t.TcD_tclevel).Include(t=>t.TcD_class).FirstOrDefaultAsync(exp);
        }
        public async override Task<List<Teacher>> FindAllAsync()
        {
            return await _IMSContext.teacheres.Include(t=>t.TcD_user).Include(t=>t.TcD_tclevel).Include(t=>t.TcD_class).ToListAsync();
        }
        public async override Task<List<Teacher>> FindManyAsync(Expression<Func<Teacher, bool>> exp)
        {
            return await _IMSContext.teacheres.Include(t=>t.TcD_user).Include(t=>t.TcD_tclevel)
                .Include(t=>t.TcD_class).Where(exp).ToListAsync();
        }

        public async override Task<List<Teacher>> PagingAsync(int page, int size)
        {
            return await _IMSContext.teacheres.Include(t => t.TcD_user).Include(t => t.TcD_tclevel)
                .Include(t => t.TcD_class).OrderBy(t=>t.LevelID).Skip((page - 1) * size).Take(size).ToListAsync();
        }
    }
}
