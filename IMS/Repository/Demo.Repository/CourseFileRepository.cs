﻿using Demo.EFCoreEnv.DbContext;
using Demo.IRepository;
using Demo.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Repository
{
    public class CourseFileRepository:BaseRepository<CourseFile>,ICourseFileRepository
    {
        private readonly IMSContext _IMSContext;
        public CourseFileRepository(IMSContext context)
        {
            base.IMSContext = context;
            _IMSContext = context;
        }
        public override async Task<CourseFile> FindIdAsync(Guid id)
        {
            return await _IMSContext.coursefiles.Include(c => c.D_level).Include(c => c.D_knowledge).FirstOrDefaultAsync(c => c.ID == id);
        }
        public override async Task<CourseFile> FindOneAsync(Expression<Func<CourseFile, bool>> exp)
        {
            return await _IMSContext.coursefiles.Include(c => c.D_level).Include(c => c.D_knowledge).FirstOrDefaultAsync(exp);
        }
        public override async Task<List<CourseFile>> FindAllAsync()
        {
            return await _IMSContext.coursefiles.Include(c => c.D_level).Include(c => c.D_knowledge).ToListAsync();
        }
        public override async Task<List<CourseFile>> FindManyAsync(Expression<Func<CourseFile, bool>> exp)
        {
            return await _IMSContext.coursefiles.Include(c => c.D_level).Include(c => c.D_knowledge).Where(exp).ToListAsync();
        }
        public override async Task<List<CourseFile>> PagingAsync(int page, int size)
        {
            return await _IMSContext.coursefiles.Include(c => c.D_level).Include(c => c.D_knowledge) 
                .Skip((page - 1) * size).Take(size).ToListAsync();
        }
    }
}
