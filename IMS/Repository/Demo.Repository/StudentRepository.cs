﻿using Demo.EFCoreEnv.DbContext;
using Demo.IRepository;
using Demo.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Repository
{
    public class StudentRepository:BaseRepository<Student>,IStudentRepository
    {
        private readonly IMSContext _IMSContext;
        public StudentRepository(IMSContext context)
        {
            base.IMSContext = context;
            _IMSContext = context;
        }
        public async override Task<Student> FindIdAsync(Guid id)
        {
            return await _IMSContext.students.Include(s=>s.StD_user).Include(s=>s.StD_Class).FirstOrDefaultAsync(s=>s.ID==id);
        }
        public async override Task<Student> FindOneAsync(Expression<Func<Student, bool>> exp)
        {
            return await _IMSContext.students.Include(s=>s.StD_user).Include(s=>s.StD_Class).FirstOrDefaultAsync(exp);
        }
        public async override Task<List<Student>> FindAllAsync()
        {
            return await _IMSContext.students.Include(s=>s.StD_user).Include(s=>s.StD_Class).ToListAsync();
        }
        public async override Task<List<Student>> PagingAsync(int page,int size)
        {
            return await _IMSContext.students.Include(s=>s.StD_user).Include(s=>s.StD_Class).OrderBy(s=>s.ClassNo).Skip((page-1)*size).Take(size).ToListAsync();
        }
        public async override Task<List<Student>> FindManyAsync(Expression<Func<Student, bool>> exp)
        {
            return await _IMSContext.students.Include(s=>s.StD_user).Include(s=>s.StD_Class).Where(exp).ToListAsync();
        }
    }
}
