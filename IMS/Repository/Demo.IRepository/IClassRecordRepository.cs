﻿using Demo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.IRepository
{
    public interface IClassRecordRepository:IBaseRepository<ClassRecord>
    {

    }
}
