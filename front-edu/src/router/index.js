import { createRouter, createWebHistory } from 'vue-router'
import Login from '../views/Login.vue'
import sreach from '@/views/General/Sreach.vue'
import Pagin from '@/views/General/Pagin.vue'
import WSchedule from '@/views/Teacher/TeacherMain/ClassCenter/wschedule.vue';
const lx = {
  template: '<div class="nojwt"><button>报班</button><button>求职</button></div > '
}
const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/main',
      name: 'main',
      component: () => import('../views/Main.vue'),
      children: [
        {
          path: 'admin',
          component: () => import('../views/Admin/AdminAside.vue'),
          children: [{
            path: '',
            name: 'admin',
            component: () => import('../views/Admin/AdminMain.vue'),
            children: [
              {
                path: 'Auser',
                name: 'Auser',
                component: () => import('../views/Admin/AdminMain/AdminUserPerson/AdminUserPage.vue'),
                children: [
                  {
                    path: 'Astudent',
                    name: 'Astudent',
                    component: () => import('../views/Admin/AdminMain/AdminUserPerson/AdminStudentPage.vue')
                  },

                  {
                    path: 'Anormal',
                    name: 'Anormal',
                    component: () => import('../views/Admin/AdminMain/AdminUserPerson/AdminNormalPage.vue')
                  }
                ]
              },
              {
                path: 'Ateacher',
                name: 'Ateacher',
                component: () => import('../views/Admin/AdminMain/AdminUserPerson/AdminTeacherPage.vue')
              },
              {
                path: 'Acourse',
                name: 'Acourse',
                component: () => import('../views/Admin/AdminMain/AdminCourseArrang.vue')
              },
              {
                path: 'Acampus',
                name: 'Acampus',
                component: () => import('../views/Admin/AdminMain/AdminCampus.vue')
              },
              {
                path: 'ACoursefile',
                name: 'ACoursefile',
                component: () => import('../views/Admin/AdminMain/AdminCoursefile.vue')    // 8.1新增加的路由
              },
              {
                path: 'Aknowledge',
                name: 'Aknowledge',
                component: () => import('../views/Admin/AdminMain/AdminKnowLedge.vue')
              },
              {
                path: 'Acoursetype',
                name: 'Acoursetype',
                component: () => import('../views/Admin/AdminMain/AdminCourseType.vue')
              },
              {
                path: 'Aclass',
                name: 'Aclass',
                component: () => import('../views/Admin/AdminMain/AdminClassManage.vue')
              },
              {
                path: 'Aclassroom',
                name: 'Aclassroom',
                component: () => import('../views/Admin/AdminMain/AdminClassRoom.vue')
              },
              {
                path: 'Aperson',
                name: 'Aperson',
                component: () => import('../views/General/PersonalPage.vue'),
                children: [{
                  path: 'Abasicinfo',
                  name: 'Abasicinfo',
                  component: () => import('../views/General/BasicInfo.vue')
                },
                {
                  path: 'Aportrait',
                  name: 'Aportrait',
                  component: () => import('../views/General/Portrait.vue')
                },
                {
                  path: 'Apwdmana',
                  name: 'Apwdmana',
                  component: () => import('../views/General/PwdManagement.vue')
                }
                ]
              },
            ]
          }]
        },
        {
          path: 'teacher',
          component: () => import('../views/Teacher/TeacherAside.vue'),
          children: [{
            path: '',
            name: 'teacher',
            component: () => import('../views/Teacher/TeacherMain.vue'),
            children: [
              {
                path: 'Tstudent',
                name: 'Tstudent',
                component: () => import('../views/Teacher/TeacherMain/TeacherStudentPage.vue')
              }, {
                path: 'Tclass',
                name: 'Tclass',
                component: () => import('../views/Teacher/TeacherMain/TeacherClassPage.vue'),
                children: [
                  {
                    path: 'TclassInfo',
                    name: 'TclassInfo',
                    component: () => import('../views/Teacher/TeacherMain/TeacherClass/TeacherClassInfoPage.vue')
                  }, {
                    path: 'TcreateClass',
                    name: 'TcreateClass',
                    component: () => import('../views/Teacher/TeacherMain/TeacherClass/TeacherCreateClass.vue')
                  }, {
                    path: 'TcreateMember',
                    name: 'TcreateMember',
                    component: () => import('../views/Teacher/TeacherMain/TeacherClass/TeacherCreateMember.vue')    //  8.1新增加的路由
                  }
                ]
              },
              {
                path: 'EndClassinfo',
                name: 'EndClassinfo',
                component: () => import('../views/Teacher/TeacherMain/ClassCenter/EndClassinfo.vue'),  // 8.2 新增
              },
              {
                path: 'GiveClass',
                name: 'GIveClass',
                component: () => import('../views/Teacher/TeacherMain/ClassCenter/GiveClass.vue'),
              }, {
                path: 'PrepareLessons',
                name: 'PrepareLessons',
                component: () => import('../views/Teacher/TeacherMain/ClassCenter/PrepareLessons.vue')
              },
              {
                path: 'Tperson',
                name: 'Tperson',
                component: () => import('../views/General/PersonalPage.vue'),
                children: [{
                  path: 'Tbasicinfo',
                  name: 'Tbasicinfo',
                  component: () => import('../views/General/BasicInfo.vue')
                },
                {
                  path: 'Tportrait',
                  name: 'Tportrait',
                  component: () => import('../views/General/Portrait.vue')
                },
                {
                  path: 'Tpwdmana',
                  name: 'Tpwdmana',
                  component: () => import('../views/General/PwdManagement.vue')
                }
                ]
              },
            ]
          }]
        },
        {
          path: '/',
          component: lx
        },


      ]
    },
    {
      path: "/student",
      component: () => import('../views/Student/StudentAside.vue'),
    },
    {
      path: '/students',
      name: 'students',
      component: () => import('../views/Student/StudentMain.vue'),
      children: [
        {
          path: 'Sperson',
          name: 'Sperson',
          component: () => import('../views/General/PersonalPage.vue'),
          children: [{
            path: 'Sbasicinfo',
            name: 'Sbasicinfo',
            component: () => import('../views/General/BasicInfo.vue')
          },
          {
            path: 'Sportrait',
            name: 'Sportrait',
            component: () => import('../views/General/Portrait.vue')
          },
          {
            path: 'Spwdmana',
            name: 'Spwdmana',
            component: () => import('../views/General/PwdManagement.vue')
          }
          ]
        },
        {
          path: 'studyinfo',
          name: 'studyinfo',
          component: () => import('../views/Student/studyInfo.vue')
        }, {
          path: 'classInfo',
          name: 'classInfo',
          component: () => import('../views/Student/classInfo.vue')
        }, {
          path: 'tostudy',
          name: 'tostudy',
          component: () => import('../views/Student/tostudy.vue'),
          children: [
            {
              path: 'htmlcss',
              name: 'htmlcss',
              component: () => import('../views/Student/tostudy/htmlcss.vue'),
            }, {
              path: 'mvc',
              name: 'mvc',
              component: () => import('../views/Student/tostudy/mvc.vue'),
            }, {
              path: 'vue',
              name: 'vue',
              component: () => import('../views/Student/tostudy/vue.vue'),
            }, {
              path: 'webapi',
              name: 'webapi',
              component: () => import('../views/Student/tostudy/webapi.vue'),
            },
          ]
        }
      ]
    },
    {
      path:"/test",
      component:()=>import("../views/test.vue")
    }

  ],

}

)

router.beforeEach((to, from, next) => {
  const tokenstr = window.sessionStorage.getItem('token')
  if (to.name !== "login" && !tokenstr) {
    next({ name: 'login' })
  } else {
    next()
  }
}
)




export default router
