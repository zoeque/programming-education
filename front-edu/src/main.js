import cors from 'cors'

// import Jwt from 'jsonwebtoken';
import { createApp } from 'vue'
import { createPinia } from 'pinia'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from './App.vue'
import router from './router'
import * as ElementPlusIconsVue from '@element-plus/icons-vue';
import axios from 'axios';



// import videoPlay from 'vue-video-play' // 引入组件 
// import 'vue-video-play/dist/style.css' // 引入css 
import vue3videoPlay from 'vue3-video-play'
import 'vue3-video-play/dist/style.css'
const app = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}

axios.interceptors.request.use(
    (config) => {
        // config 是 axios 配置对象
        // 获取token
        let token = window.sessionStorage.getItem("token");
        // console.log(token);
        // 添加token
        //Bearer为token类型，根据自己的类型更改
        if (token) {
            config.headers.Authorization = `Bearer ${token}`;
            // console.log(token);
        }
        return config;
    },
    (error) => {
        // 请求出错
        return Promise.reject(error);
    }
);
app.use(vue3videoPlay)
app.use(cors)
app.use(createPinia())
app.use(router)
app.use(ElementPlus)


app.mount('#app')



